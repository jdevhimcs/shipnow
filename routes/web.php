<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('home'));
});

Auth::routes(['verify' => true]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

/*
*	Admin Route
*/
Route::prefix('admin')
	->name('admin.')
	->middleware(['role:admin'])
	->group(function() {
		Route::get('/', function(){
			return view('auth.login');
		})
		->name('login')
		->middleware('guest');

		Route::get('/dashboard',[App\Http\Controllers\Admin\DashboardController::class,'index'])->name('dashboard.index');
		Route::resource('users', App\Http\Controllers\Admin\UserController::class)->only([
			'index', 'show','destroy'
		]);
		Route::resource('profile', App\Http\Controllers\Admin\ProfileController::class)->only([
			'create','store'
		]);
		Route::resource('change-password',App\Http\Controllers\Admin\ChangePasswordController::class)->only([
			'index', 'store'
		]);
		Route::resource('agents', App\Http\Controllers\Admin\AgentController::class);

		// Assign user to agent
		Route::get('agents/{user}/assign-user', [App\Http\Controllers\Admin\AssignUserController::class, 'create'])->name('agent-assign-user');
		Route::post('agents/{user}/assign-user/store', [App\Http\Controllers\Admin\AssignUserController::class, 'store'])->name('store-agent-assign-user');
	});

Route::prefix('user')
	->name('user.')
	->middleware(['auth', 'role:user'])
	->group(function() {
		Route::resource('change-password',App\Http\Controllers\User\ChangePasswordController::class)->only([
			'index', 'store'
		]);
		Route::resource('profile', App\Http\Controllers\User\ProfileController::class)->only([
			'create','store'
		]);
		Route::resource('dashboard', App\Http\Controllers\User\DashboardController::class)->only([
			'index'
		]);
		Route::resource('account-settings', App\Http\Controllers\User\AccountSettingController::class)->only([
			'create', 'store'
		]);
		Route::resource('order-preferences', App\Http\Controllers\User\OrderPreferenceController::class)->only([
			'index', 'create' ,'store', 'update', 'edit', 'destroy'
		]);

		Route::resource('address-book-list', App\Http\Controllers\User\AddressBookController::class)->only([
			'index', 'store', 'update', 'edit', 'destroy'
		]);
		
		Route::get('get-states-list', [App\Http\Controllers\User\CountryController::class, 'getStates'])->name('states.list');
		
		Route::get('get-cities-list', [App\Http\Controllers\User\CountryController::class, 'getCities'])->name('cities.list');

		Route::resource('products', App\Http\Controllers\User\ProductController::class)->only([
			'index','store','edit','update'
		]);

		Route::resource('manage-cards', App\Http\Controllers\User\ManageCardController::class)->only([
			'index','store','edit','update'
		]);
		
		Route::delete('order-preference-ref/{id}', [App\Http\Controllers\User\OrderReferenceController::class, 'destroy']);

		Route::delete('order-preference-broker/{id}', [App\Http\Controllers\User\OrderBrokerController::class, 'destroy']);
		
		//Letter
		Route::get('letter-quote/create', [App\Http\Controllers\User\LetterQuoteController::class, 'create'])->name('letter-quote.create');
		Route::post('letter-quote', [App\Http\Controllers\User\LetterQuoteController::class, 'store'])->name('letter-quote.store');

		//PAK
		Route::get('pak-quote/create', [App\Http\Controllers\User\PakQuoteController::class, 'create'])->name('pak-quote.create');
		Route::post('pak-quote', [App\Http\Controllers\User\PakQuoteController::class, 'store'])->name('pak-quote.store');
		
		//PACKAGE
		Route::get('package-quote/create', [App\Http\Controllers\User\PackageController::class, 'create'])->name('package-quote.create');
		Route::post('package-quote', [App\Http\Controllers\User\PackageController::class, 'store'])->name('package-quote.store');

		//Get shipment rates
		Route::post('get-shipment-rates', [App\Http\Controllers\User\ShipmentRateController::class, 'getRates'])->name('shipment-rates');		
		Route::post('set-shipment-rate', [App\Http\Controllers\User\ShipmentRateController::class, 'setShipmentRate'])->name('shipment.set.rate');

		//Checkout
		Route::get('shipment-checkout', [App\Http\Controllers\User\ShipmentRateController::class, 'checkout'])->name('shipment.checkout');
		Route::post('shipment-checkout', [App\Http\Controllers\User\CheckoutController::class, 'store'])->name('checkout');

		//Order Summary page
		Route::get('order-summary/{id}', [App\Http\Controllers\User\OrderController::class, 'show'])->name('order.summary');

		//My Shipemensts List
		Route::get('my-shipments', [App\Http\Controllers\User\MyShipmentController::class, 'index'])->name('my.shipments');
	});

/**
 * Agent routes 
 */
Route::prefix('agent')->name('agent.')->group(function() {
	Route::get('/', function() {
		return view('auth.login');
	})
	->name('login')
	->middleware('guest');

	Route::get('/dashboard',[App\Http\Controllers\Agent\DashboardController::class,'index'])
	->name('dashboard.index')
	->middleware(['auth', 'role:agent']);

	Route::get('/assigned-user',[App\Http\Controllers\Agent\AssignedUserController::class,'index'])
	->name('assigned-user.index')
	->middleware(['auth', 'role:agent']);

	Route::get('/assigned-user/create',[App\Http\Controllers\Agent\AssignedUserController::class,'create'])
	->name('assigned-user.create')
	->middleware(['auth', 'role:agent']);

	Route::post('/assigned-user/store',[App\Http\Controllers\Agent\AssignedUserController::class,'store'])
	->name('store-agent-assign-user')
	->middleware(['auth', 'role:agent']);
});

Route::prefix('agent')->name('agent.')->middleware(['auth', 'role:agent,user'])->group(function() {
	Route::post('/login-as-customer', [App\Http\Controllers\Agent\AssignedUserController::class,'loginAsCustomer'])->name('login-as-customer')->middleware(['auth']);
});

Route::get('/signup', function () {
	return view('pages.signup');
});

Route::get('/forget-password', function () {
	return view('pages.forget-password');
});

Route::get('/address-book', function () {
	return view('pages.address-book');
});

Route::get('/distributions', function () {
	return view('pages.distributions');
});

Route::get('/add-product', function () {
	return view('pages.add-product');
});

Route::get('/account-settings-page', function () {
	return view('pages.account-settings');
});

Route::get('/home-page', function () {
	return view('pages.home');
});


Route::get('/my-shipment-all', function () {
	return view('pages.my-shipment-all');
});

Route::get('/active-shipment', function () {
	return view('pages.active-shipment');
});

Route::get('/delivered-shipment', function () {
	return view('pages.delivered-shipment');
});

Route::get('/my-quote', function () {
	return view('pages.my-quote');
});

Route::get('/my-quote-list', function () {
	return view('pages.my-quote-list');
});

Route::get('/choose-packaging', function () {
	return view('pages.choose-packaging');
});

Route::get('/letter', function () {
	return view('pages.letter');
});

Route::get('/pak', function () {
	return view('pages.pak');
});

Route::get('/package', function () {
	return view('pages.package');
});

Route::get('/ltl', function () {
	return view('pages.ltl');
});

// Fedex
Route::get('sample-fedex', [App\Http\Controllers\FedExController::class, 'store']);
Route::get('sample-ltl-fedex', [App\Http\Controllers\FedExController::class, 'ltl']);

// Create pending FedEx shipment
Route::get('create-fedex-open-shipment', [App\Http\Controllers\FedExController::class, 'createOpenShipment']);

// UPS get single rate
Route::get('get-ups-rate', [App\Http\Controllers\UpsController::class, 'store']);

// UPS get service rates 
Route::get('get-ups-transit-rate', [App\Http\Controllers\UpsController::class, 'rateTimeInTransit']);

Route::get('create-ups-shipment', [App\Http\Controllers\UpsController::class, 'createShipment']);

// Canada Post rating
Route::get('get-canada-post-rate', [App\Http\Controllers\CanadaPostController::class, 'getRates']);

//Create Quote home page

Route::post('request-quote/create', [App\Http\Controllers\RequestQuoteController::class, 'store'])->name('request.quote.create');

/*
//Courier canpar API demo
// Route::get('courier/canpar-api-tracking', [App\Http\Controllers\CourierApiController::class, 'canparApiCourierTracking']);

//Courier ArcBest API demo
// Route::get('courier/arc-best-api-get-rates', [App\Http\Controllers\CourierApiController::class, 'arcbestapiGetRates']);

//Canada Post API sandbox rates between 2 points
// Route::get('courier/canada-post-api-get-rates-two-points', [App\Http\Controllers\CourierApiController::class, 'canadaPostApiGetRates']);
*/

//AutoComplete city
Route::get('search-cities-suggestions', [App\Http\Controllers\CitySearchController::class, 'searchCity'])->name('autocomplete.city');

//Import Excel
Route::get('import-excel', [App\Http\Controllers\ImportExcelController::class, 'index'])->name('import.list');
Route::post('import-excel', [App\Http\Controllers\ImportExcelController::class, 'store'])->name('import.excel.store');

//Track UPS Shipment
Route::get('track-ups-shipment/{trackId}', [App\Http\Controllers\UpsController::class, 'trackShipment']);
Route::get('cancel-ups-shipment/{shipmentid}', [App\Http\Controllers\UpsController::class, 'cancelShipment']);