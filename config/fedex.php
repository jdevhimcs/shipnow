<?php
return [
	'FEDEX_ACCOUNT_NUMBER' => env('FEDEX_ACCOUNT_NUMBER', null),
	'FEDEX_METER_NUMBER' => env('FEDEX_METER_NUMBER', null),
	'FEDEX_API_KEY' => env('FEDEX_API_KEY', null),
	'FEDEX_PASSWORD' => env('FEDEX_PASSWORD', null),
	'FEDEX_SANDBOX' => env('FEDEX_SANDBOX', true)
];