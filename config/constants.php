<?php

return [
	'UNIT_TYPE' => [
		0 => 'Weight unit',
		1 => 'Dimension unit'
	],
	'SHIPMENT_STATUS' => [
		1 => 'Pending',
		2 => 'Completed',
		3 => 'Cancelled and payment failed',
		4 => 'Cancelled by customer'
	]
];