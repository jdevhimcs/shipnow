( function(){
    $.validator.addMethod("pwcheckallowedchars", function (value) {
        return /^(?=\D*\d)(?=[^a-z]*[a-z])[0-9a-z]+$/i.test(value)
    }, "The password must contain at least one character");

    jQuery.validator.addMethod("pwcheckallowedchars",
    function(value, element, param) {
        if (this.optional(element)) {
            return true;
        }else if (!/[a-zA-Z]/.test(value)) {
            return false;
        } else if (!/[0-9]/.test(value)) {
            return false;
        }

        return true;
    },
    "The password must contain at least one character and digit");

    jQuery.validator.addMethod("intlTelNumber", function (value, element) {
        if (element.value.trim()) {
            console.log(iti, iti.isValidNumber());
            if (iti.isValidNumber()) {            
                return true;
            } else {				
                return false;
            }
        }
    }, 'Please enter a valid International Phone Number');

    $('#form-register-user').validate({
        rules: {
            'password':{
                required:true,
                minlength:8,
                pwcheckallowedchars: true
            },
            password_confirmation: {
                equalTo: "#password"
            },
            'email': {
                required:true,
                email: true
            },
            'phone_number': {
                required: true,
                intlTelNumber: true
            },
        }
    });
    
})();