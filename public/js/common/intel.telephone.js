var iti;

var input = document.querySelector("#phone");

var errorMap = [
    "Invalid number",
    "Invalid country code",
    "Too short",
    "Too long",
    "Invalid number"
];

var reset = function() {
    inputPhoneNo.classList.remove("error");
    errorMsg.innerHTML = "";
    errorMsg.classList.add("hide");
    validMsg.classList.add("hide");
};

iti = window.intlTelInput(input, {
    preferredCountries: [ "ca", "us" ],
    separateDialCode:true,
    hiddenInput: "full_phone",
    initialCountry: "auto",
  	geoIpLookup: function(callback) {
		$.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
		var countryCode = (resp && resp.country) ? resp.country : "us";
		callback(countryCode);
		});
  	},
    utilsScript: "/intl-tel-input-master/build/js/utils.js"
    // any initialisation options go here
});