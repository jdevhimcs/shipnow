( function(){
    $('#product_add').validate({
        rules: {
            'name':{
                required:true,
            }, 
            'description':{
                required:true,
            },
            'length':{
                required:true,
            }, 
            'width':{
                required:true,
            },
            'height':{
                required:true,
            }, 
            'weight':{
                required:true,
            },
            'class':{
                required:true,
            }, 
        }
    });
    
})();