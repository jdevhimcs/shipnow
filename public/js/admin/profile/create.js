(function(){
	$('#profile').on('click', function(){
		$('#file-input').trigger('click');
	});
	$('#file-input').on('change', function(){
		updateImageDisplay(this,'profile-image')
	});


	$('#editProfile').validate({
		rules: {
			'user_profile[first_name]':{
				required:true,
				minlength:3,
			},
			'user_profile[last_name]':{
				required:true,
				minlength:3
			},
			
			'email':{
				required:true,
				email:true
			},
	    }
    });

	function updateImageDisplay(input, imagepreview) {
	    var curFiles = input.files;
	    if (curFiles.length === 0) {
	        console.log('No files currently selected for upload')
	    } else {
	    	var image = document.querySelector(`img[data-id="${imagepreview}"]`);
	        for (var i = 0; i < curFiles.length; i++) {
	            image.src = window.URL.createObjectURL(curFiles[i]);
	        }
	    }
	}

})();