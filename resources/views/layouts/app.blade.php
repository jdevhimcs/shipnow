<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <script src="https://js.stripe.com/v3/"></script> -->

    <title>{{ config('app.name', 'Ship Now').': ' }}@yield('title', 'Home')</title>

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/font.css') }}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('intl-tel-input-master/build/css/intlTelInput.min.css') }}" rel="stylesheet">

    @stack('css')
</head>

<body class="bg-white">
    <div id="app">        
        <main>
            @include('elements.header')

            @yield('content')

            @include('elements.footer')
        </main>
    </div>
    <script src="{{ asset('frontend/js/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('frontend/js/popper.min.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/common/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('intl-tel-input-master/build/js/intlTelInput.min.js') }}"></script>
    <script src="{{ asset('js/common/register.js') }}"></script>
    @stack('scripts')
</body>

</html>