<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Ship Now').': ' }}@yield('title', 'Dashboard')</title>

    <!-- <script src="{{ asset('js/tablesort.min.js') }}"></script> -->

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}"></script> -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/paper-dashboard.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('intl-tel-input-master/build/css/intlTelInput.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-ui.css') }}" rel="stylesheet">

    @stack('css')
</head>

<body>
    <div id="app">
        <main>
            <div class="wrapper ">
                @include('elements.auth.sidebar')
                <div class="main-panel">
                    <!-- Navbar -->
                    @include('elements.auth.nav')
                    <!-- End Navbar -->

                    @yield('content')
                    @include('elements.auth.footer')
                </div>
            </div>
        </main>
    </div>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/common/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('js/chartjs.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-notify.js') }}"></script>
    <script src="{{ asset('js/paper-dashboard.min.js') }}"></script>
    <script src="{{ asset('intl-tel-input-master/build/js/intlTelInput.min.js') }}"></script>
    <script src="{{ asset('js/jquery-ui.js') }}"></script>


    <!-- <script>
        $('.nav li').on('click', function() {
            $('.nav li.active').removeClass('active');
            $(this).addClass('active');
        });
    </script> -->


    @stack('scripts')
</body>

</html>