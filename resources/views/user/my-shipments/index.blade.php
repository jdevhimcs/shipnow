@extends('layouts.dashboard')
@section('title', 'My Shipments')
@section('content')

<div class="center-navbar">
    <ul>
        <li class="active"><a href="{{ route('user.my.shipments') }}">All</a></li>
        <li><a href="/active-shipment">Active Shipment</a></li>
        <li><a href="/delivered-shipment">Delivered</a></li>
    </ul>
</div>
<div class="content">
    <div class="search-container bg-white shadow-sm p-4 mb-5">
        <form action="#">
            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        <div class="search-outer search-location">                            
                            <input type="text" name="city_from" placeholder="City From" class="form-control light-placeholder custom-text-input" value="{{ $request->query('city_from') }}">
                        </div>
                    </div>
                </div>
                 <div class="col-3">
                    <div class="form-group">
                        <div class="search-outer search-location">                            
                            <input type="text" name="city_to" placeholder="City To" class="form-control light-placeholder custom-text-input" value="{{ $request->query('city_to') }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        <label>Date From</label>
                        <div class="calender-outer">
                            <img class="user-img" src="{{ asset('img/calender.png') }}">
                            <input type="text" name="ship_from_date" id="form" placeholder="Form" class="form-control light-placeholder custom-text-input" value="{{ $request->query('ship_from_date') }}">
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label>Date To</label>
                        <div class="calender-outer">
                            <img class="user-img" src="{{ asset('img/calender.png') }}">
                            <input type="text" name="ship_to_date" id="to" placeholder="To" class="form-control light-placeholder custom-text-input" value="{{ $request->query('ship_to_date') }}">
                        </div>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label>Tracking Id</label>
                        <input type="text" name="tracking_number" placeholder="#Tracking Id" class="form-control light-placeholder custom-text-input" value="{{ $request->query('tracking_number') }}">
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label>Carrier</label>
                        <select name="carrier" class="form-control light-placeholder custom-text-input">
                            <option value="">Select Carrier</option>
                            <option value="ups" {{ $request->query('carrier') == 'ups' ? 'selected' : ''}}>UPS</option>
                            <option value="fedex" {{ $request->query('carrier') == 'fedex' ? 'selected' : ''}}>FEDEX</option>
                        </select>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" class="form-control light-placeholder custom-text-input">
                            <option value="">Select Status</option>
                            @foreach(config('constants.SHIPMENT_STATUS') as $key => $value)
                                <option value="{{ $key }}" {{ $request->query('status') == $key ? 'selected' : ''}}>{{ $value }}</option>
                            @endforeach
                            {{-- <option value="Intransit" {{ $request->query('status') == 'Intransit' ? 'selected' : ''}}>In Transit</option>
                            <option value="Delivered" {{ $request->query('status') == 'Delivered' ? 'selected' : ''}}>Delivered</option> --}}
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        <label>Address From</label>
                        <input type="text" name="address_from" placeholder="Enter Location" class="form-control light-placeholder custom-text-input" value="{{ $request->query('address_from') }}">
                    </div>
                </div>                
                <div class="col-3">
                    <div class="form-group">
                        <label>Address To</label>
                        <input type="text" name="address_to" placeholder="Enter Location" class="form-control light-placeholder custom-text-input" value="{{ $request->query('address_to') }}">
                    </div>
                </div>                
            </div>
            <div class="row align-items-center">                
                <div class="col-4 ml-auto text-right">
                    <button class="btn btn-search" type="submit">Search Now</button>
                    <button type="reset" class="btn-reset">Clear all</button>
                </div>
            </div>
        </form>
    </div>
    <div class="db-custom-table">
        <table class="table">
            <thead>
                <tr>
                    <th>Origin destination</th>
					<th>Tracking Number</th>
                    <th>Ship Date</th>
                    <th>Carrier</th>
                    <th>Order Id</th>
                    <!-- <th>Tracking Id</th> -->
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
				@if (count($shipments))
					@foreach ($shipments as $shipment)
						<tr>
							<td>
								<ul class="location-list">
									<li>From: {{ $shipment->postal_from }}, {{ $shipment->city_from }}, CA</li>
									<li>{{ $shipment->postal_to }}, {{ $shipment->city_to }} CA </li>
								</ul>
							</td>
							<td>{{ $shipment->identification_number }}</td>
							<td>{{ $shipment->formatted_ship_date }}</td>
							<td>{{ strtoupper($shipment->carrier) }}</td>
							<td>
                                <a href="{{ route('user.order.summary', base64_encode($shipment->id)) }}">SDV-{{ $shipment->ref_code }}</a>
                            </td>
							<!-- <td>								
                                @foreach ($shipment->shipmentPackageResults as $key => $package)
                                <a href="#" class="link-text-color">
                                    <small>Package #{{ $key + 1}} {{ $package->tracking_number }}</small><br>
                                </a>
                                @endforeach
							</td> -->
							<td>
								{{ config('constants.SHIPMENT_STATUS')[$shipment->status] ?? ''}}
							</td>
						</tr>
					@endforeach
				@else
					<tr>
						<td colspan="6">{{__('No Record Found')}}</td></tr>
					</tr>
				@endif
            </tbody>
        </table>       
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(function() {
            $("#form").datepicker();
        });
        $(function() {
            $("#to").datepicker();
        });
    </script>
@endpush