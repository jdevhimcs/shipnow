@extends('layouts.dashboard')
@section('title', 'Order Preference')
@section('content')
<div class="content">
	@include('elements.auth.my_account_nav')
	<div class="content-container">
		<div class="row mb-4">
			<div class="col-12">
                <x-alert/>
				<div class="bg-white p-4 shadow-sm mb-4">
					<div class="row">
						<div class="col-7">
							<div class="seetting-heading">
								<h2>
									Order Preferences
								</h2>
							</div>
						</div>
                         <div class="col-5">
                            <div class="add-btn text-right">
                                <a href="{{ route('user.order-preferences.create') }}" class="add-btn bg-dark-blue text-white border-0 py-2 px-4 font-weight-bold rounded">Add More</a>
                            </div>
                        </div>
						<div class="col-12">
							@foreach($orders as $key => $order)
								<div class="border-gray mb-4">
									<div class="d-flex order-name mb-4">
										<div class="reference col-2">
											<span class="d-block">Reference</span>
											@foreach($order->orderReferences as $key => $orderReference)
												<strong>{{ ucfirst($orderReference->reference_title) }}</strong>
											@endforeach
										</div>
										<div class="broker-name col-3">
											<span class="d-block">Broker Name</span>
											@foreach($order->orderBrokers as $key => $orderBroker)
												<strong>{{ ucfirst($orderBroker->broker_title) }}</strong>
											@endforeach
										</div>
										<div class="email-id col-2">
											<span class="d-block">Email Id</span>
											<strong>{{ $order->email }}</strong>
										</div>
										<div class="lang col-2">
											<span class="d-block">Language</span>
											@foreach ($order->languages as $language)
												<strong>{{ $language->title }}</strong>
											@endforeach
										</div>
										<div class="mob col-2">
											<span class="d-block">Phone number</span>
											<stron >{{ $order->phone }}</strong>
										</div>
										<!-- <div class="carrier-name col-2">
											<span class="d-block">Carrier Name</span>
											<strong class="d-block">{{ ucfirst($order->carrier_name) }}</strong>
										</div> -->
										<div class="action col-1">
											<a href="{{ route('user.order-preferences.edit', $order->id) }}" class="btn-edit">
												<img src="{{ asset('img/edit.png') }}">
											</a>
											<a href="{{ route('user.order-preferences.destroy', encrypt($order->id))}}" class="btn-delete" onclick="event.preventDefault(); document.getElementById('order-del-form').submit();">
												<img src="{{ asset('img/delete.png') }}">
											</a>
											<form id="order-del-form" action="{{ route('user.order-preferences.destroy', encrypt($order->id))}}" method="POST" style="display: none;">
												@csrf
												@method('DELETE')
											</form>
										</div>
									</div>
									<div class="d-flex order-name">
										<div class="unit col-2">
											<span class="d-block">Unit of Measurment</span>
											@foreach ($order->units as $unit)
												<strong>{{ $unit->title }}</strong>
											@endforeach
										</div>
										<div class="size col-3">
											<span class="d-block">Label size</span>
											<strong>{{ $order->label_size }}</strong>
										</div>
										<div class="size col-2">
											<span class="d-block">Default Packaging</span>
											<strong>
												{{ !empty($order->orderPreferencePackage->package) ? ucwords($order->orderPreferencePackage->package->title): '' }}
											</strong>
										</div>
										<!-- <div class="discount col-3">
											<strong class="d-block">Discount Applied!</strong>
										</div> -->
									</div>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection