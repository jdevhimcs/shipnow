@extends('layouts.dashboard')
@section('title', 'Order Preference')
@section('content')
<div class="content">
	@include('elements.auth.my_account_nav')
	<div class="content-container">
		<div class="row mb-4">
			<div class="col-12">
				<div class="bg-white p-4 shadow-sm mb-4">
					<div class="row">
						<div class="col-12">
							<div class="seetting-heading">
								<h2>
									Add New Order Preference
								</h2>
							</div>
						</div>
					</div>
					<form method="POST" action="{{ route('user.order-preferences.store') }}" id="order-setting">
						@csrf
						<div class="row">
							<div class="col-12 mb-4">
								<div class="refer shadow-sm py-3">
									<div class="row">
										<div class="col-8">
											<!-- <div class="form-group col-xl-4 fieldGroup">
												<label class="custom-label-text">Reference</label>
												<input type="text" name="references[]" placeholder="Enter Reference" class="form-control light-placeholder @error('reference') is-invalid @enderror">
												<a href="javascript:void(0)" class="btn btn-sm btn-success addMore">
													Add More
												</a>
											</div> -->
											<div id="reference-div">
											@if(old('references')!="")
													<div id="countVar" data-count = "{{ count(old('references')) }}"></div>
												@foreach(old('references') as $key => $value)
													<div class="form-group col-xl-4 field-reference-group-js" data-id={{ ($key == 0)?$key+1:$key }}>
														<label class="custom-label-text">Reference</label>
														<input type="text" class="form-control light-placeholder @error('references.'.$key.'.ref_title') is-invalid @enderror" name="references[{{$key}}][ref_title]" value="{{ old('references.'.$key.'.ref_title') }}" placeholder="Enter Reference" autofocus>
														@error('references.'.$key.'.ref_title')
															<span class="invalid-feedback" role="alert">
																<strong>{{ $message }}</strong>
															</span>
														@enderror
														@if($key == 1)
															<button type="button" class="btn btn-success plus-ref add_btn"> Add </button>
														@else
															<button type="button" class="btn btn-success plus-ref add_btn"> Add </button>
															<button type="button" class="btn btn-danger minus-ref remove-btn"> Remove </button>
														@endif
													</div>                               
												@endforeach
											@else
												<div id="countVar" data-count = "0"></div>
												<div class="form-group col-xl-4 field-reference-group-js" data-id="1">
													<label class="custom-label-text">Reference</label>
													<input type="text" class="form-control light-placeholder" name="references[1][ref_title]" placeholder="Enter Reference" autofocus>
													@error('references.1.ref_title')
														<span class="invalid-feedback" role="alert">
															<strong>{{ $message }}</strong>
														</span>
													@enderror
													<button type="button" class="btn btn-success plus-ref add_btn"> ADD </button>
												</div>												                                												
											@endif
											</div>
																						
											<div id="broker-div">
											@if(old('brokers')!="")
													<div id="countVar" data-count = "{{ count(old('brokers')) }}"></div>
												@foreach(old('brokers') as $key => $value)
													<div class="form-group col-xl-4 field-broker-group-js" data-id={{ ($key == 0)?$key+1:$key }}>
														<label class="custom-label-text">Custom Broker</label>
														<input type="text" class="form-control light-placeholder @error('brokers.'.$key.'.broker_title') is-invalid @enderror" name="brokers[{{$key}}][broker_title]" value="{{ old('brokers.'.$key.'.broker_title') }}" placeholder="Name" autofocus>
														@error('brokers.'.$key.'.broker_title')
															<span class="invalid-feedback" role="alert">
																<strong>{{ $message }}</strong>
															</span>
														@enderror
														@if($key == 1)
															<button type="button" class="btn btn-success plus-broker add_btn"> Add </button>
														@else
															<button type="button" class="btn btn-success plus-broker add_btn"> Add </button>
															<button type="button" class="btn btn-danger minus-broker remove-btn"> Remove </button>
														@endif
													</div>                               
												@endforeach
											@else
												<div id="countVar" data-count = "0"></div>
												<div class="form-group col-xl-4 field-broker-group-js" data-id="1">
													<label class="custom-label-text">Custom Broker</label>
													<input type="text" class="form-control light-placeholder" name="brokers[1][broker_title]" placeholder="Name" autofocus>
													@error('brokers.1.broker_title')
														<span class="invalid-feedback" role="alert">
															<strong>{{ $message }}</strong>
														</span>
													@enderror
													<button type="button" class="btn btn-success add_btn plus-broker"> ADD </button>
												</div>												                                												
											@endif
											</div>

											<div class="d-flex flex-wrap">
												<div class="form-group col-xl-4">
													<label class="custom-label-text">Phone Number</label>
													<input type="text" name="order_phone" placeholder="Enter Phone number" class="form-control light-placeholder @error('order_phone') is-invalid @enderror" id="order-phone">
													@error('order_phone')
														<span class="invalid-feedback" role="alert">
															<strong>{{$message}}</strong>
														</span>
													@enderror
												</div>
												<div class="form-group col-xl-4">
													<label class="custom-label-text">Email Address</label>
													<input type="text" name="order_email" placeholder="Enter Email Address" class="form-control light-placeholder @error('order_email') is-invalid @enderror" id="order-email">
													@error('order_email')
														<span class="invalid-feedback" role="alert">
															<strong>{{$message}}</strong>
														</span>
													@enderror
												</div>
											</div>
											<div class="form-group col-12">
												<label class="custom-label-text">Language</label>
												<div class="d-flex flex-wrap">
													@foreach($languages as $key => $language)
													<div class="custom-control custom-checkbox custom-check pr-4 mb-3">
														<input type="checkbox" class="custom-control-input lng @error('languages') is-invalid @enderror" id="inlineCheckboxLng{{$key}}" name="languages[]" value="{{$key}}">
														<label class="custom-control-label font-weight-bold pl-4 pt-1" for="inlineCheckboxLng{{$key}}">{{ $language }}</label>
													</div>
													@endforeach
													@error('languages')
														<span class="invalid-feedback" role="alert">
															<strong>{{$message}}</strong>
														</span>
													@enderror
												</div>
											</div>
											<div class="form-group col-12">
												<label class="custom-label-text">Unit of measurement</label>
												<div class="d-flex flex-wrap">
													@foreach($units as $key => $unit)
													<div class="custom-control custom-checkbox custom-check pr-4 mb-3">
														<input type="checkbox" class="unit custom-control-input @error('units') is-invalid @enderror" id="inlineCheckboxinch{{$key}}" name="units[]" value="{{$key}}">
														<label class="custom-control-label font-weight-bold pl-4 pt-1" for="inlineCheckboxinch{{$key}}">{{ $unit }}</label>
													</div>
													@endforeach
													@error('units')
														<span class="invalid-feedback" role="alert">
															<strong>{{$message}}</strong>
														</span>
													@enderror
												</div>
											</div>
											<div class="form-group col-12">
												<label class="custom-label-text">Default Packaging</label>
												@foreach($packages as $key => $package)
													<div class="custom-control custom-radio custom-radio-button">
														<input type="radio" class="custom-control-input @error('package') is-invalid @enderror" id="inlineRadioPackage{{$key}}" name="package" value="{{$key}}">
														<label class="custom-control-label pl-3 text-dark pt-1" for="inlineRadioPackage{{$key}}">{{ $package }}</label>
													</div>
												@endforeach
												@error('package')
													<span class="invalid-feedback" role="alert">
														<strong>{{$message}}</strong>
													</span>
												@enderror
											</div>
											<div class="form-group col-4">
												<button class="add-discount-rate my-4" type="button">Add My Contract Discount Rate</button>
											</div>
											<!-- <div class="form-group col-xl-4">
												<label class="custom-label-text">Carrier Name</label>
												<input type="text" name="carrier_name" placeholder="Carrier" class="form-control light-placeholder @error('carrier_name') is-invalid @enderror" id="carrier-name">
												@error('carrier_name')
												<span class="invalid-feedback" role="alert">
													<strong>{{$message}}</strong>
												</span>
												@enderror
											</div> -->
										</div>
										<div class="col-4">
											<div class="form-group col-xl-6">
												<label class="custom-label-text">Label Size</label>
												<select name="label_size" class="form-control @error('label_size') is-invalid @enderror" id="label-size-dropdown">
													<option value="">Please Select</option>
													<option value="8×12" selected>8×12</option>
												</select>
												@error('label_size')
													<span class="invalid-feedback" role="alert">
														<strong>{{$message}}</strong>
													</span>
												@enderror
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-12 text-right">
								<button class="add-save" type="submit">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('scripts')
<script>
	//Manage Reference Field
	$('body').on('click', '.plus-ref', function() { 
        // i = $('#tab_logic tr').length; 
        var i =  $('#reference-div').find('.field-reference-group-js:last').data('id');
        i = i+1;
        $('#reference-div').append(`<div class="form-group col-xl-4 field-reference-group-js" data-id="${i}">
			<label class="custom-label-text">Reference</label>
			<input type="text" class="form-control light-placeholder" name="references[${i}][ref_title]" placeholder="Enter Reference" autofocus>	
			<button type="button" class="btn btn-success plus-ref add_btn"> Add </button>
			<button type="button" class="btn btn-danger minus-ref remove-btn"> Remove </button>
		</div>`);
        // i++;
    });

    $('body').on('click', '.minus-ref', function() {
        $(this).closest('div').remove();
        // i--;
    });

	//Manage Broker Field
	$('body').on('click', '.plus-broker', function() { 
        // i = $('#tab_logic tr').length; 
        var i =  $('#broker-div').find('.field-broker-group-js:last').data('id');
        i = i+1;
        $('#broker-div').append(`<div class="form-group col-xl-4 field-broker-group-js" data-id="${i}">
			<label class="custom-label-text">Custom Broker</label>
			<input type="text" class="form-control light-placeholder" name="brokers[${i}][broker_title]" placeholder="Name" autofocus>	
			<button type="button" class="btn btn-success plus-broker add_btn"> Add </button>
			<button type="button" class="btn btn-danger minus-broker remove-btn"> Remove </button>
		</div>`);
        // i++;
    });

    $('body').on('click', '.minus-broker', function() {
        $(this).closest('div').remove();
        // i--;
    });
</script>
@endpush