@extends('layouts.dashboard')
@section('title', 'Account Settings')
@section('content')

<div class="content no-sub-nav">
    <div class="row mb-4">
        <div class="col-7">
            <div class="select-product">
                <form>
                    <div class="form-inline">
                        <div class="form-group">
                            <input type="text" name="filter" class="form-control" placeholder="Name">
                        </div>
                        <div class="form-group ml-4">
                            <button type="submit" class="bg-dark-blue text-white border-0 py-2 px-5 font-weight-bold rounded">Search</button>
                        </div>
                    </div>
                </form>
                {{-- <select name="select-product" class="form-control">
                    <option value="">Select Product</option>
                </select> --}}
            </div>
        </div>
        <div class="col-5">
            <div class="add-btn text-right">
                <button class="add-btn bg-dark-blue text-white border-0 py-2 px-4 font-weight-bold rounded" type="button" data-toggle="modal" data-target="#staticBackdrop">Add Product</button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <x-alert/>	
            <div class="table-responsive-sm product-table">
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th>Name </th>
                            <th>Description</th>
                            <th>Dimensions</th>
                            <th>Weight</th>
                            <th>Class</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($products))
                            @foreach ($products as $product)
                                
                            
                                <tr>
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->description}}</td>
                                    <td>
                                        {!!$product->dimension!!}
                                    </td>
                                    <td>
                                        <strong>{{$product->weight_with_unit}}</strong>
                                    </td>
                                    <td>
                                        <strong>{{$product->class}}</strong>
                                    </td>
                                    <td>
                                        <a href="javascript:void(0)" class="edit-data" data-url="{{route('user.products.edit',$product->id)}}">Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan=4>{{__('No Record Found')}}</td></tr>
                            </tr>
                        @endif
                    </tbody>
                </table>
                <div class="text-right">
                    {!! $products->appends(\Request::except('page'))->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- add product Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable  modal-lg">
        <div class="modal-content">
            <div class="modal-header header-custom py-4 px-5">
                <h5 class="modal-title" id="staticBackdropLabel">Add Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pb-4">
                <div class="form-add px-4">
                    <form action="{{route('user.products.store')}}" method="POST" id="product_add">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="company name" value="{{ old('name') }}">
                                    @error('name')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea name="description" class="form-control @error('description') is-invalid @enderror"></textarea>
                                    @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Length</label>
                                    <input type="text" name="length" class="form-control @error('length') is-invalid @enderror" placeholder="Lenght" value="{{ old('length') }}">
                                    @error('length')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Width</label>
                                    <input type="text" name="width" class="form-control @error('width') is-invalid @enderror" placeholder="Width" value="{{ old('width') }}">
                                    @error('width')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Height</label>
                                    <input type="text" name="height" class="form-control @error('height') is-invalid @enderror" placeholder="height" value="{{ old('height') }}">
                                    @error('height')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Weight</label>
                                    <input type="text" name="weight" class="form-control @error('weight') is-invalid @enderror" placeholder="Weight" value="{{ old('weight') }}">
                                    @error('weight')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Class</label>
                                    <input type="text" name="class" class="form-control  @error('class') is-invalid @enderror" placeholder="Class" value="{{ old('class') }}">
                                    @error('class')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 mt-4">
                                <div class="form-group">
                                    <button type="submit"  class="bg-dark-blue text-white border-0 py-2 px-5 font-weight-bold rounded">Add Product</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable  modal-lg">
        <div class="modal-content">
            <div class="modal-header header-custom py-4 px-5">
                <h5 class="modal-title" id="staticBackdropLabel">Edit Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pb-4 propertyModelBody">
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script src="{{asset('js/user/product_validation.js')}}"></script>
    <script>
        $('.edit-data').on('click', function(){
            url = $(this).attr('data-url');
            $.ajax({
                url: url,
                success: function(resp) {
                    $('#editModel').find('.propertyModelBody').html(resp);
                    $('#editModel').modal('show');

                    $('#editModel').on('shown.bs.modal');
                },
                error: function(xhr) {
                }
            });
        });  
    
    </script>
@endpush
@push('css')
    <style>
        label.error {
            color: #f10000;
        }
    </style>
@endpush