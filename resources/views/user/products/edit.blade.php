<div class="form-add px-4">
    <form action="{{route('user.products.update', $product->id)}}" method="POST" id="product_add">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="company name" value="{{ $product->name }}" required>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label>Description</label>
                    <textarea name="description" class="form-control @error('description') is-invalid @enderror" required>{{$product->description}}</textarea>
                    @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-3">
                <div class="form-group">
                    <label>Length</label>
                    <input type="text" name="length" class="form-control @error('length') is-invalid @enderror" placeholder="Lenght" value="{{ $product->length }}" required>
                    @error('length')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <label>Width</label>
                    <input type="text" name="width" class="form-control @error('width') is-invalid @enderror" placeholder="Width" value="{{$product->width }}" required>
                    @error('width')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <label>Height</label>
                    <input type="text" name="height" class="form-control @error('height') is-invalid @enderror" placeholder="height" value="{{$product->height }}" required>
                    @error('height')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label>Weight</label>
                    <input type="text" name="weight" class="form-control @error('weight') is-invalid @enderror" placeholder="Weight" value="{{$product->weight }}" required>
                    @error('weight')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Class</label>
                    <input type="text" name="class" class="form-control  @error('class') is-invalid @enderror" placeholder="Class" value="{{$product->class }}" required>
                    @error('class')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 mt-4">
                <div class="form-group">
                    <button type="submit"  class="bg-dark-blue text-white border-0 py-2 px-5 font-weight-bold rounded">Edit Product</button>
                </div>
            </div>
        </div>
    </form>
</div>