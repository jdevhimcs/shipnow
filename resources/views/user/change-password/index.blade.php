@extends('layouts.dashboard')
@section('title', 'Change Password')
@section('content')
<div class="content">
	@include('elements.auth.my_account_nav')
	<div class="content-container">		
		<div class="row">
			<div class="col-12">
				<form method="POST" action="{{ route('user.change-password.store') }}" id="ChangePasswordForm" >
				@csrf
					<div class="bg-white p-4 shadow-sm">
						<div class="row">
							<div class="col-12">
								@if(session()->has('message'))
									<div class="alert alert-success alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>
											<strong>{{ session()->get('message') }}</strong>
									</div>
								@endif
								@if(session()->has('error'))
									<div class="alert alert-danger alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>
											<strong>{{ session()->get('error') }}</strong>
									</div>
								@endif
								<div class="seetting-heading">
									<h2>
										Change Password
									</h2>
								</div>
							</div>
							<div class="col-6">
								<div class="form-group">
									<label for="Password">{{ __('Old Password') }}</label>
									<input type="password" name="current_password" class="form-control @error('current_password') is-invalid @enderror" id="Password">
									@error('current_password')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
								</div>
								<div class="form-group">
									<label for="exampleInputPassword1">{{ __('Create New Password') }}</label>
									<input type="password" name="new_password" class="form-control @error('new_password') is-invalid @enderror" id="exampleInputPassword1">
									@error('new_password')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
								</div>
								<div class="form-group">
									<label for="ConfirmPassword">{{ __('Confirm Password') }}</label>
									<input type="password" name="new_confirm_password" class="form-control @error('new_confirm_password') is-invalid @enderror" id="ConfirmPassword">
									@error('new_confirm_password')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
								</div>
								<div class="form-group text-right">
									<button type="submit" class="btn-change-password mt-">{{ __('Change Password') }}</button>
								</div>
							</div>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection