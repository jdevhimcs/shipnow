@extends('layouts.dashboard')
@section('title', 'Account Settings')
@section('content')
<div class="content">
    <div class="row mb-4">
        <div class="col-7">
            <div class="quote-number">
                <h2>{{ strtoupper($order->carrier) }}</h2>
            </div>
        </div>
        <div class="col-5">
            <div class="add-btn text-right">
                <button class="add-btn bg-dark-blue text-white border-0 py-2 px-4 font-weight-bold rounded mr-3">Print Receipt</button>
                <button class="add-btn bg-dark-blue text-white border-0 py-2 px-4 font-weight-bold rounded">Email Receipt</button>
            </div>
        </div>
    </div>
    <div class="quote-form">
        <div class="row">
            <div class="col-9">
                <div class="bg-white p-4 summary-order">
                    <table class="table table-dimensions mb-5">
                        <tbody>
                            <tr>
                                <td>
                                    Origin:
                                    <strong class="d-block">
                                        {!! $order->origin !!}
                                    </strong>
                                </td>
                                <td>
                                    Destination:
                                    <strong class="d-block">
                                        {!! $order->destination !!}
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Attn:
                                    <strong class="d-block">SENDING</strong>
                                </td>
                                <td>
                                    Attn:
                                    <strong class="d-block">RECEIVING</strong>
                                </td>
                                <td>
                                </td>
                            </tr>
							<tr>
                                <td>
                                   	Phone:
                                    <strong class="d-block">{{ $order->origin_phone }}</strong>
                                </td>
                                <td>
                                    Phone:
                                    <strong class="d-block">{{ $order->destination_phone }}</strong>
                                </td>
                                <td>
                                </td>
                            </tr>
							<tr>
                                <td>
                                   	Email:
                                    <strong class="d-block">{{ $order->origin_email }}</strong>
                                </td>
                                <td>
                                    Email:
                                    <strong class="d-block">{{ $order->destination_email }}</strong>
                                </td>
                                <td>
                                </td>
                            </tr>
							<tr>
                                <td>
                                   	Company:
                                    <strong class="d-block">{{ $order->origin_company }}</strong>
                                </td>
                                <td>
                                    Company:
                                    <strong class="d-block">{{ $order->destination_company }}</strong>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   	Attention Name:
                                    <strong class="d-block">{{ $order->origin_attn_name }}</strong>
                                </td>
                                <td>
                                    Attention Name:
                                    <strong class="d-block">{{ $order->destination_attn_name }}</strong>
                                </td>
                                <td>
                                </td>
                            </tr>
							<tr>
                                <td>
                                   	Instruction:
                                    <strong class="d-block">PO# {{ $order->order_ref_id }}</strong>
                                </td>
                                <td>
                                    Instruction:
                                    <strong class="d-block">N/A</strong>
                                </td>
                                <td>
                                </td>
                            </tr>
							<tr>
                                <td>
                                    Payment Terms:
                                    <strong class="d-block">Prepaid</strong>
                                </td>
                                <td>

                                    <strong class="d-block"></strong>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </tbody>
                    </table>
					<h6 class="mt-4">Credit Card Charges:</h6>
					<hr>
					<table class="table table-bordered">
						<thead>
							<tr>
							<th scope="col">#</th>
							<th scope="col">Transaction #</th>
							<th scope="col">C Card #</th>
							<th scope="col">Amount</th>
							<th scope="col">Status</th>
							</tr>
						</thead>
						<tbody>
							@foreach($order->transactions as $transaction)
							<tr>
								<th scope="row">1</th>
								<td>{{ $transaction->trx_id }}</td>
								<td>xxxx-xxxx-xxxx-{{ $transaction->userCard->last_digits }}</td>
								<td>${{ $transaction->trx_amount }}</td>
								<td>{{ $transaction->status ? "CHARGED" : 'FAILED' }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					<h6 class="mt-4">Package Details:</h6>
					<hr>
					<div class="row">
						<div class="col-md-4">
							<dl class="row">
								<dt class="col-sm-4">Quantity :</dt>
								<dd class="col-sm-4">
									{{ count($order->shipmentItems) }}
								</dd>
                            </dl>
						</div>
						<div class="col-sm-12">
							<table class="table table-bordered">
								<thead>
									<tr>
									<th scope="col">#</th>
									<th scope="col">Length</th>
									<th scope="col">Width</th>
									<th scope="col">Height</th>
                                    <th scope="col">Weight</th>
									<th scope="col">Unit</th>
									</tr>
								</thead>
								<tbody>
									@foreach($order->shipmentItems as $key => $item)
									<tr>
										<th scope="row">{{ $key +1 }}</th>
										<td>{{ $item->length ?? 'N/A' }}</td>
										<td>{{ $item->width ?? 'N/A' }}</td>
										<td>{{ $item->height ?? 'N/A' }}</td>
                                        <td>{{ $item->weight ?? 'N/A' }}</td>
										<td>{{ ucwords($item->item_dimension) ?? 'N/A' }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
                </div>
                <div class="order-btn mt-4">
                    <button class="add-btn bg-dark-blue text-white border-0 py-2 px-4 font-weight-bold rounded mr-3">View Print</button>
                    <button class="add-btn bg-dark-blue text-white border-0 py-2 px-4 font-weight-bold rounded mr-3">View Print BOL</button>
                    <button class="add-btn bg-dark-blue text-white border-0 py-2 px-4 font-weight-bold rounded mr-3">Send BOL</button>
                </div>
            </div>
            <div class="col-3">
                <div class="cost bg-white shadow-sm p-4">
                    <div class="total-cost border-bottom mb-3 pb-3">
                        <span>Total Cost:</span>
                        <h2>${{ $order->total_charges }}</h2>
                    </div>
                    <div class="project-delivery-date border-bottom mb-3 pb-3">
                        <span>Shipment Date:</span>
                        <h2>{{ $order->formatted_ship_date }}</h2>
                    </div>
					<div class="total-cost border-bottom mb-3 pb-3">
                        <span>Carrier:</span>
                        <h2>{{ strtoupper($order->carrier) }}</h2>
                    </div>
					<div class="total-cost border-bottom mb-3 pb-3">
                        <span>Service:</span>
                        <h2>{{ strtoupper($order->service_name) }}</h2>
                    </div>
					<div class="total-cost border-bottom mb-3 pb-3">
                        <span>Carrier Tracking # :</span>
                        <h2>{{ strtoupper($order->identification_number) }}</h2>
                    </div>
					<div class="total-cost border-bottom mb-3 pb-3">
                        <span>Transport Charges:</span>
                        <h2>${{ $order->total_transportation_charges }}</h2>
                    </div>
					<div class="total-cost border-bottom mb-3 pb-3">
                        <span>Service Charges:</span>
                        <h2>${{ $order->total_service_options_charges }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection