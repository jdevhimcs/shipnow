@extends('layouts.dashboard')
@section('title', 'Account Settings')
@section('content')
<div class="content">
	@include('elements.auth.my_account_nav')
	<div class="content-container">
		<div class="row mb-4">
			<div class="col-12">
				<form method="POST" action="{{ route('user.account-settings.store') }}" id="account-setting" enctype="multipart/form-data">
					@csrf
					
					<div class="bg-white p-4 shadow-sm">
						<div class="row">
							<div class="col-12">
								@if(session()->has('message'))
								<div class="alert alert-success alert-block">
									<button type="button" class="close" data-dismiss="alert">×</button>
									<strong>{{ session()->get('message') }}</strong>
								</div>
								@endif
								@if(session()->has('error'))
								<div class="alert alert-danger alert-block">
									<button type="button" class="close" data-dismiss="alert">×</button>
									<strong>{{ session()->get('error') }}</strong>
								</div>
								@endif
								<div class="seetting-heading">
									<h2>
										Company Information
									</h2>
								</div>
							</div>
							<div class="col-xl-4">
								<div class="form-group">
									<label>Company Name</label>
									<input type="text" name="company_name" placeholder="What your company full name?" class="form-control light-placeholder @error('company_name') is-invalid @enderror" id="company-name" value="{!! old('company_name', optional($user->companyDetail)->company_name) !!}">
									@error('company_name')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-4">
								<div class="form-group">
									<label>Address</label>
									<input type="text" name="address" placeholder="Company address here" class="form-control light-placeholder @error('address') is-invalid @enderror" id="company-address" value="{!! old('address', optional($user->companyDetail)->address) !!}">
									@error('address')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-4 col-md-6">
								<div class="form-group">
									<label>Country</label>
									<select name="company_country" class="form-control @error('company_country') is-invalid @enderror" id="country-dropdown">
										<option value="">Select Country</option>
										@foreach ($countries as $country)
										<option value="{{$country->id}}">{{$country->name}}</option>
										@endforeach
									</select>
									@error('company_country')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
							<div class="col-xl-4 col-md-6">
								<div class="form-group">
									<label>State</label>
									<select name="company_state" class="state form-control @error('company_state') is-invalid @enderror" id="state-dropdown"></select>
									@error('company_state')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-4 col-md-6">
								<div class="form-group">
									<label>City</label>
									<input type="text" name="company_city" placeholder="City" class="form-control light-placeholder @error('company_city') is-invalid @enderror" id="postal-code" value="{!! old('company_city', optional($user->companyDetail)->city) !!}">
									@error('company_city')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
							<div class="col-xl-4 col-md-6">
								<div class="form-group">
									<label>Postal</label>
									<input type="text" name="postal_code" placeholder="Postal Code" class="form-control light-placeholder @error('postal_code') is-invalid @enderror" id="postal-code" value="{!! old('postal_code', optional($user->companyDetail)->postal_code) !!}">
									@error('postal_code')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-4 col-md-6">
								<div class="form-group">
									<label>Email Address</label>
									<input type="text" name="company_email" placeholder="example@shipnow.com" class="form-control light-placeholder @error('company_email') is-invalid @enderror" id="company-email" value="{!! old('company_email', optional($user->companyDetail)->email) !!}">
									@error('company_email')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
							<div class="col-xl-4 col-md-6">
								<div class="form-group">
									<label>Phone Number</label>
									<input type="text" name="company_phone" placeholder="123 456 7890" class="form-control light-placeholder @error('company_phone') is-invalid @enderror" id="company-phone" value="{!! old('company_phone', optional($user->companyDetail)->phone_no) !!}">
									@error('company_phone')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<div class="form-group">
									<div class="custom-control custom-checkbox custom-check">
										<input type="checkbox" class="custom-control-input" id="bill" name="is_bill_same" value="1" {{ !empty($user->companyDetail->is_bill_same) ? 'checked' : ''}}>
										<label class="custom-control-label font-weight-bold pl-4 pt-1" for="bill">Bill same as above.</label>
									</div>
								</div>
							</div>
						</div>
					</div>					
					<!-- BILLING -->
					<div class="bg-white p-4 shadow-sm">
						<div class="row">
							<div class="col-12">
								<div class="seetting-heading">
									<h2>
										Billing Information
									</h2>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-4">
								<div class="form-group">
									<label>Address</label>
									<input type="text" name="billing_address" placeholder="Billing address here" class="form-control light-placeholder @error('billing_address') is-invalid @enderror" id="billing-address" value="{!! old('billing_address', optional($user->billingAddress)->address) !!}">
									@error('billing_address')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-4 col-md-6">
								<div class="form-group">
									<label>Country</label>
									<input type="text" name="billing_country" placeholder="Country" class="form-control light-placeholder @error('billing_country') is-invalid @enderror" id="billing-country" value="{!! old('billing_country', optional($user->billingAddress)->country) !!}">
									@error('billing_country')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
							<div class="col-xl-4 col-md-6">
								<div class="form-group">
									<label>State</label>
									<input type="text" name="billing_state" placeholder="State" class="form-control light-placeholder @error('billing_state') is-invalid @enderror" id="billing-state" value="{!! old('billing_state', optional($user->billingAddress)->state) !!}">
									@error('billing_state')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-4 col-md-6">
								<div class="form-group">
									<label>City</label>
									<input type="text" name="billing_city" placeholder="City" class="form-control light-placeholder @error('billing_city') is-invalid @enderror" id="billing-city" value="{!! old('billing_city', optional($user->billingAddress)->city) !!}">
									</select>
									@error('billing_city')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
							<div class="col-xl-4 col-md-6">
								<div class="form-group">
									<label>Postal</label>
									<input type="text" name="billing_postal_code" placeholder="Type Postal Code" class="form-control light-placeholder @error('billing_postal_code') is-invalid @enderror" id="billing-postal-code" value="{!! old('billing_postal_code', optional($user->billingAddress)->postal_code) !!}">
									@error('billing_postal_code')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-4 col-md-6">
								<div class="form-group">
									<label>Email Address</label>
									<input type="text" name="billing_email" placeholder="example@shipnow.com" class="form-control light-placeholder @error('billing_email') is-invalid @enderror" id="billing-email" value="{!! old('billing_email', optional($user->billingAddress)->email) !!}">
									@error('billing_email')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
							<div class="col-xl-4 col-md-6">
								<div class="form-group">
									<label>Phone Number</label>
									<input type="text" name="billing_phone" placeholder="Type Phone Number" class="form-control light-placeholder @error('billing_phone') is-invalid @enderror" id="billing-phone" value="{!! old('billing_phone', optional($user->billingAddress)->phone_no) !!}">
									@error('billing_phone')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
						</div>
					</div>
					<div class="bg-white p-4 shadow-sm ">
						<div class="row">
							<div class="col-12 mb-2">
								<div class="seetting-heading">
									<h2>
										Notification Settings
										<span>(Only for LTL)</span>
									</h2>
								</div>
							</div>
							@foreach($notificationSettings as $key => $notificationSetting)
							<div class="col-xl-2 col-md-4">
								<div class="form-group">
									<div class="custom-control custom-checkbox custom-check">
										<input type="checkbox" class="custom-control-input" id="inlineCheckbox{{$key}}" name="notificationSettings[]" value="{{$key}}" {{ $user->notificationSettings->contains($key) ? 'checked' : '' }}>
										<label class="custom-control-label pl-4 pt-1" for="inlineCheckbox{{$key}}">{{ $notificationSetting }}</label>
									</div>
								</div>
							</div>
							@endforeach							
						</div>
					</div>
					<div class="bg-white p-4 shadow-sm">
						<div class="row">
							<div class="col-12">								
								<div class="seetting-heading">
									<h2>
										My Account Settings
									</h2>
								</div>
							</div>
							<div class="col-xl-4 col-md-6">
								<div class="form-group">
									<label class="custom-label-text">First Name</label>
									<input type="text" name="first_name" placeholder="Type First Name" class="form-control light-placeholder @error('first_name') is-invalid @enderror" id="first-name" value="{{ old('first_name', $user->profile->first_name) }}">
									@error('first_name')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
							<div class="col-xl-4 col-md-6">
								<div class="form-group">
									<label class="custom-label-text">Last Name</label>
									<input type="text" name="last_name" placeholder="Type Last Name" class="form-control light-placeholder @error('last_name') is-invalid @enderror" id="last-name" value="{{ old('last_name', $user->profile->last_name) }}">
									@error('last_name')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-4 col-md-6">
								<div class="form-group">
									<label class="custom-label-text">Email Address</label>
									<input type="email" name="email" placeholder="example@shipnow.com" class="form-control light-placeholder @error('email') is-invalid @enderror" id="company-email" value="{{ old('email', $user->email) }}">
									@error('email')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
							<div class="col-xl-4 col-md-6">
								<div class="form-group">
									<label class="custom-label-text">Phone Number</label>
									<input type="tel" name="phone_number" class="form-control light-placeholder @error('phone_no') is-invalid @enderror" id="phone" value="{{ old('phone_no',$user->profile->phone_no) }}">
									@error('phone_no]')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
						</div>						
					</div>
					<div class="bg-white p-4 shadow-sm mb-4">
						<div class="row">
							<div class="col-12">								
								<div class="seetting-heading">
									<h2>
										Accounts Payable Contact
									</h2>
								</div>
							</div>
							<div class="col-xl-4 col-md-6">
								<div class="form-group">
									<label class="custom-label-text">Contact</label>
									<input type="text" name="contact_name" placeholder="Type Contact Name" class="form-control light-placeholder @error('contact_name') is-invalid @enderror" id="contact-payable-name" value="{!! old('contact_name', optional($user->accountPayableDetail)->contact_name) !!}">
									@error('contact_name')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
							<div class="col-xl-4 col-md-6">
								<div class="form-group">
									<label class="custom-label-text">Email</label>
									<input type="email" name="account_payable_email" placeholder="example@shipnow.com" class="form-control light-placeholder @error('account_payable_email') is-invalid @enderror" id="account-pay-email" value="{!! old('account_payable_email', optional($user->accountPayableDetail)->email) !!}">
									@error('account_payable_email')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>							
						</div>
						<div class="row">							
							<div class="col-xl-4 col-md-6">
								<div class="form-group">
									<label class="custom-label-text">Phone</label>
									<input type="text" name="account_payable_phone" class="form-control light-placeholder @error('account_payable_phone') is-invalid @enderror" id="phone_account_payable" value="{!! old('account_payable_phone', optional($user->accountPayableDetail)->phone) !!}">
									@error('account_payable_phone')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<div class="form-group">
									<div class="custom-control custom-checkbox custom-check">
										<input type="checkbox" class="custom-control-input mr-5" id="is_same_main_contact" name="is_same_main_contact" value="1" {{ !empty($user->accountPayableDetail->is_same_main_contact) ? 'checked' : ''}}>
										<label class="custom-control-label font-weight-bold pl-4 pt-1" for="is_same_main_contact">Same as main contact.</label>
									</div>
								</div>
							</div>
							<div class="col-12">
								<div class="form-group">
									<div class="custom-control custom-checkbox custom-check">
										<input type="checkbox" class="custom-control-input mr-5" id="is_prepaid" name="is_prepaid" value="1" {{ !empty($user->accountPayableDetail->is_prepaid) ? 'checked' : 'checked'}}>
										<label class="custom-control-label font-weight-bold pl-4 pt-1" for="is_prepaid">Prepaid</label>
									</div>
								</div>
							</div>
							<div class="form-group col-12">
								<label class="custom-label-text">Choose Your Payment Terms</label>
								<div class="d-flex flex-wrap">
									@php
										$paymentTerms = [
											'0-35' 	=> '0-35',
											'36-65' => '36-65',
											'66-90' => '66-90'
										];
									@endphp
									@foreach($paymentTerms as $key => $term)
									<div class="custom-control custom-checkbox custom-check pr-4 mb-3">
										<input type="checkbox" class="payment-term-input-js custom-control-input @error('payment_terms') is-invalid @enderror" id="inlineCheckboxtermspay{{$key}}" name="payment_terms" value="{{$key}}" 
										{{ !empty($user->accountPayableDetail->payment_terms) && $user->accountPayableDetail->payment_terms == $term  ? 'checked' : ''}}>
										<label class="custom-control-label font-weight-bold pl-4 pt-1" for="inlineCheckboxtermspay{{$key}}">{{ $term }}</label>
									</div>
									@endforeach
									@error('payment_terms')
									<span class="invalid-feedback" role="alert">
										<strong>{{$message}}</strong>
									</span>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">							
							<div class="col-12 text-right">
								<button class="add-save" type="submit">Save</button>
							</div>
						</div>
					</div>										
				</form>

				<div class="bg-white p-4 shadow-sm mb-4">
					<div class="row">
						<div class="col-12">
							<div class="seetting-heading">
								<h2>
									Manage Credit Cards 
								</h2>
							</div>
						</div>
						<div class="col-12">
							<form action="{{ route('user.manage-cards.store') }}" method="POST" id="payment-form">					
								@csrf

								<div class="row mb-4">
									<div class="col-8">
										<label for="card-element">
										Credit Card Info
										</label>
										<div id="card-element">
										<!-- A Stripe Element will be inserted here. -->
										</div>
					
										<!-- Used to display form errors. -->
										<div id="card-errors" class="error" role="alert"></div>
									</div>
									<div class="col-4">
										<button class="mt-4 add-save">Save</button>
									</div>
								</div>
							</form>
						</div>
						<div class="col-12">
							@foreach($myCards as $key => $card)
							<div class="border-gray mb-4">
								<div class="d-flex order-name mb-4 align-items-center">
									<div class="reference col-2">
										<span class="d-block">Card Type</span>
										<strong class="d-block">{{ $card->type }}</strong>
									</div>
									<div class="reference col-2">
										<span class="d-block">Card Number</span>
										<strong class="d-block">XXXX-XXXX-XXXX-{{ $card->last_digits }}</strong>
									</div>
									<div class="reference col-2">
										<span class="d-block">Exp Year</span>
										<strong class="d-block">{{ $card->expire_year }}</strong>
									</div>
									<div class="reference col-2">
										<span class="d-block">Stripe Source Id</span>
										<strong class="d-block">{{ $card->source_id }}</strong>
									</div>									
								</div>
							</div>
							@endforeach
						</div>						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('css/stripe/stripe.css') }}"></link>
@endpush
@push('scripts')
<script src="https://js.stripe.com/v3/"></script>
<script src="{{ asset('js/common/intel.telephone.js') }}"></script>
<script>
	var country_identifier = {{!empty($user-> companyDetail->country_id) ? $user->companyDetail->country_id : (int) false }};
	var state_identifier = {{!empty($user-> companyDetail->state_id) ? $user->companyDetail->state_id : (int) false }};
	// var city_identifier = {{ !empty($user->companyDetail->city_id) ? $user->companyDetail->city_id: (int) false }};

	$(function() {		
		//Country Identifier
		if (country_identifier) {
			setTimeout(function() {
				$('[id="country-dropdown"]')
					.val(country_identifier)
					.trigger('change');
			}, 100);

		}

		//Load States
		$('[id="country-dropdown"]').change(function() {
			// console.log(state_identifier);
			var __country_id = $(this).val();
			if (__country_id) {
				$.ajax({
					type: "get",
					url: "{{ route('user.states.list') }}",
					data: {
						country_id: __country_id,
						//_token: '{{csrf_token()}}'
					},
					success: function(res) {
						if (res) {
							$('[id="state-dropdown"]').empty();
							$('[id="city-dropdown"]').empty();
							$('[id="state-dropdown"]').append('<option>Select State</option>');
							$.each(res.states, function(key, state) {
								$('[id="state-dropdown"]').append('<option value="' + state.id + '">' + state.name + '</option>');
							});
							if (state_identifier) {
								$('[id="state-dropdown"]')
									.val(state_identifier)
									.trigger('change');
							}
						}
					}
				});
			}
		});
		//Billing
		$('[name="is_bill_same"]').on("change", function(){
			if (this.checked) {
				$("[name='billing_address']").val($("[name='address']").val());
				$("[name='billing_country']").val($("[name='company_country']").find('option:selected').text());
				$("[name='billing_state']").val($("[name='company_state']").find('option:selected').text());
				$("[name='billing_city']").val($("[name='company_city']").val());
				$("[name='billing_postal_code']").val($("[name='postal_code']").val());
				$("[name='billing_email']").val($("[name='company_email']").val());
				$("[name='billing_phone']").val($("[name='company_phone']").val());
			} else {
				$("[name='billing_address']").val('');
				$("[name='billing_country']").val('');
				$("[name='billing_state']").val('');
				$("[name='billing_city']").val('');
				$("[name='billing_postal_code']").val('');
				$("[name='billing_email']").val('');
				$("[name='billing_phone']").val('');
			}
		});

		$('.payment-term-input-js').click(function() {
			$('.payment-term-input-js').not(this).prop('checked', false);
		});
	});

	//Checkbox Lng and units
	/*
	$('.lng').click(function() {
		$('.lng').not(this).prop('checked', false);
	});

	$('.unit').click(function() {
		$('.unit').not(this).prop('checked', false);
	});
	*/	
</script>

<script>
	var publishable_key = "{{ $publishkey }}";
	// Create a Stripe client.
	var stripe = Stripe(publishable_key);
	
	// Create an instance of Elements.
	var elements = stripe.elements();
	
	// Custom styling can be passed to options when creating an Element.
	// (Note that this demo uses a wider set of styles than the guide below.)
	var style = {
		base: {
			color: '#66615b',
			fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
			fontSmoothing: 'antialiased',
			fontSize: '14px',
			'::placeholder': {
				color: '#abacad'
			}
		},
		invalid: {
			color: '#dc3545',
			iconColor: '#dc3545'
		}
	};
	
	// Create an instance of the card Element.
	var card = elements.create('card', {style: style});
	
	// Add an instance of the card Element into the `card-element` <div>.
	card.mount('#card-element');
	
	// Handle real-time validation errors from the card Element.
	card.addEventListener('change', function(event) {
		var displayError = document.getElementById('card-errors');
		if (event.error) {
			displayError.textContent = event.error.message;
		} else {
			displayError.textContent = '';
		}
	});
	
	// Handle form submission.
	var form = document.getElementById('payment-form');
	form.addEventListener('submit', function(event) {
		event.preventDefault();
	
		stripe.createToken(card).then(function(result) {
			if (result.error) {
				// Inform the user if there was an error.
				var errorElement = document.getElementById('card-errors');
				errorElement.textContent = result.error.message;
			} else {
				// Send the token to your server.
				stripeTokenHandler(result.token);
			}
		});
	});
	
	// Submit the form with the token ID.
	function stripeTokenHandler(token) {
		// Insert the token ID into the form so it gets submitted to the server
		var form = document.getElementById('payment-form');
		var hiddenInput = document.createElement('input');
		hiddenInput.setAttribute('type', 'hidden');
		hiddenInput.setAttribute('name', 'stripeToken');
		hiddenInput.setAttribute('value', token.id);
		form.appendChild(hiddenInput);
	
		// Submit the form
		form.submit();
	}
</script>
@endpush