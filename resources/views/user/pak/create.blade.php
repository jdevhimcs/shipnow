@extends('layouts.dashboard')
@section('title', 'PAK')
@section('content')

<div id="overlay">
  <div class="cv-spinner">
	<span class="spinner"></span>
  </div>
</div>
<div class="content">
	<div class="row mb-4">
		<div class="col-12">
			<div class="db-sub-heading">
				<h2>{{ __('Choose Your Packaging') }}</h2>
			</div>
		</div>
		<div class="col-12">
			@include('elements.auth.package_quote_nav')
		</div>
	</div>
	<div class="quote-form">
		<div class="row">

			<div class="col-12">
				<div class="alert alert-danger alert-dismissible fade show d-none" role="alert">
					<span data-id="alert-error-block"></span>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="alert alert-success alert-dismissible fade show d-none" role="alert">
					<span data-id="alert-success-block"></span>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form method="POST" action="{{ route('user.shipment-rates') }}" id="pak-form">
				@csrf

				<input type="hidden" name="type" value="PAK">
				<div class="bg-white shadow-sm ">
					<div class="d-flex align-items-center p-4 ship-details">
						<div class="ship-form col-5 pl-0">
							<div class="border border-1 px-3 pb-3 rounded">
								<div class="row">
									<div class="col-12 px-3 py-3 mb-3 card-header-quotes">
										<h2>Ship From :</h2>
									</div>
								</div>
								<div class="row">
									<div class="col-12">
										<div class="form-group">
											Company Name <strong class="text-danger">*</strong>
											<input type="text" name="ship_from_business" class="form-control" placeholder="Company Name">
										</div>
									</div>
									<div class="col-12">
										<div class="form-group">
											Street Address <strong class="text-danger">*</strong>
											<input type="text" name="street_from_address" class="form-control" placeholder="Address">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											Country <strong class="text-danger">*</strong>
											<select class="form-control" name="ship_from_country"  data-ship="from">
												<option value="">Please Select</option>
												<option value="ca">Canada</option>
												<option value="us">US</option>
											</select>
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											ZIP / Postal <strong class="text-danger">*</strong>
											<input type="text" name="ship_from_postal" class="form-control" placeholder="Enter Postal Code"  data-ship="from">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											City <strong class="text-danger">*</strong>
											<input type="text" name="ship_from_city" class="form-control" placeholder="Enter City" data-ship="from">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group" data-id="province-from-div-ca">
											State <strong class="text-danger">*</strong>
											<select name="ship_from_state" class="form-control" data-select="CA-from-province"  data-ship="from">
												<option value="">Please Select</option>
												<option value="AB">Alberta</option>
												<option value="BC">British Columbia</option>
												<option value="MB">Manitoba</option>
												<option value="NB">New Brunswick</option>
												<option value="NL">Newfoundland and Labrador</option>
												<option value="NS">Nova Scotia</option>
												<option value="NT">Northwest Territories</option>
												<option value="NU">Nunavut</option>
												<option value="ON">Ontario</option>
												<option value="PE">Prince Edward Island</option>
												<option value="QC">Quebec</option>
												<option value="SK">Saskatchewan</option>
												<option value="YT">Yukon</option>
											</select>
										</div>
										<div class="form-group d-none" data-id="province-from-div-us">
			                               	Province<select name="ship_from_state" class="form-control" data-select="US-from-province" data-ship="from">
			                                   <option value="">Please Select</option>
			                                   <option value="AL">Alabama</option>
			                                   <option value="AK">Alaska</option>
			                                   <option value="AZ">Arizona</option>
			                                   <option value="AR">Arkansas</option>
			                                   <option value="CA">California</option>
			                                   <option value="CO">Colorado</option>
			                                   <option value="CT">Connecticut</option>
			                                   <option value="DE">Delaware</option>
			                                   <option value="DC">District Of Columbia</option>
			                                   <option value="FL">Florida</option>
			                                   <option value="GA">Georgia</option>
			                                   <option value="HI">Hawaii</option>
			                                   <option value="ID">Idaho</option>
			                                   <option value="IL">Illinois</option>
			                                   <option value="IN">Indiana</option>
			                                   <option value="IA">Iowa</option>
			                                   <option value="KS">Kansas</option>
			                                   <option value="KY">Kentucky</option>
			                                   <option value="LA">Louisiana</option>
			                                   <option value="ME">Maine</option>
			                                   <option value="MD">Maryland</option>
			                                   <option value="MA">Massachusetts</option>
			                                   <option value="MI">Michigan</option>
			                                   <option value="MN">Minnesota</option>
			                                   <option value="MS">Mississippi</option>
			                                   <option value="MO">Missouri</option>
			                                   <option value="MT">Montana</option>
			                                   <option value="NE">Nebraska</option>
			                                   <option value="NV">Nevada</option>
			                                   <option value="NH">New Hampshire</option>
			                                   <option value="NJ">New Jersey</option>
			                                   <option value="NM">New Mexico</option>
			                                   <option value="NY">New York</option>
			                                   <option value="NC">North Carolina</option>
			                                   <option value="ND">North Dakota</option>
			                                   <option value="OH">Ohio</option>
			                                   <option value="OK">Oklahoma</option>
			                                   <option value="OR">Oregon</option>
			                                   <option value="PA">Pennsylvania</option>
			                                   <option value="RI">Rhode Island</option>
			                                   <option value="SC">South Carolina</option>
			                                   <option value="SD">South Dakota</option>
			                                   <option value="TN">Tennessee</option>
			                                   <option value="TX">Texas</option>
			                                   <option value="UT">Utah</option>
			                                   <option value="VT">Vermont</option>
			                                   <option value="VA">Virginia</option>
			                                   <option value="WA">Washington</option>
			                                   <option value="WV">West Virginia</option>
			                                   <option value="WI">Wisconsin</option>
			                                   <option value="WY">Wyoming</option>
			                               </select>
			                           </div>
									</div>
									<div class="col-6">
										<div class="form-group">
											Phone <strong class="text-danger">*</strong>
											<input type="text" name="origin_phone" class="form-control" placeholder="Phone">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											Attention <strong class="text-danger">*</strong>
											<input type="text" name="origin_attn_name" class="form-control" placeholder="Attention">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											Email
											<input type="email" name="origin_email" class="form-control" placeholder="Email">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="switch text-center px-4">
							<img class="d-block" src="{{ asset('img/reload.png') }}">
							<span class="d-block ext-dark-blue mt-1">Switch</span>
						</div>
						<div class="ship-to col-5">
							<div class="border border-1 px-3 pb-3 rounded">
								<div class="row">
									<div class="col-12 px-3 py-3 mb-3 card-header-quotes">
										<h2>Ship To :</h2>
									</div>
								</div>
								<div class="row">
									<div class="col-12">
										<div class="form-group">
											Company Name <strong class="text-danger">*</strong>
											<input type="text" name="ship_to_business" class="form-control" placeholder="Company Name">
										</div>

									</div>
									<div class="col-12">
										<div class="form-group">
											Street Address <strong class="text-danger">*</strong>
											<input type="text" name="street_to_address" class="form-control" placeholder="Address">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											Country <strong class="text-danger">*</strong>
											<select class="form-control" name="ship_to_country" data-ship="to">
												<option value="">Please Select</option>
												<option value="ca">Canada</option>
												<option value="us">US</option>
											</select>
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											ZIP / Postal <strong class="text-danger">*</strong>
											<input type="text" name="ship_to_postal" class="form-control" placeholder="Enter Postal Code" data-ship="to">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											City <strong class="text-danger">*</strong>
											<input type="text" name="ship_to_city" class="form-control" placeholder="Enter City" data-ship="to">
										</div>
									</div>

									<div class="col-6">
										<div class="form-group" data-id="province-to-div-ca">
											State <strong class="text-danger">*</strong>
											<select name="ship_to_state" class="form-control" data-select="CA-to-province" data-ship="to">
												<option value="">Please Select</option>
												<option value="AB">Alberta</option>
												<option value="BC">British Columbia</option>
												<option value="MB">Manitoba</option>
												<option value="NB">New Brunswick</option>
												<option value="NL">Newfoundland and Labrador</option>
												<option value="NS">Nova Scotia</option>
												<option value="NT">Northwest Territories</option>
												<option value="NU">Nunavut</option>
												<option value="ON">Ontario</option>
												<option value="PE">Prince Edward Island</option>
												<option value="QC">Quebec</option>
												<option value="SK">Saskatchewan</option>
												<option value="YT">Yukon</option>
											</select>
										</div>
										<div class="form-group d-none" data-id="province-to-div-us">
										   	Province
										   	<select name="ship_to_state" class="form-control" data-select="US-to-province" data-ship="to">
												<option value="">Please Select</option>
												<option value="AL">Alabama</option>
												<option value="AK">Alaska</option>
												<option value="AZ">Arizona</option>
												<option value="AR">Arkansas</option>
												<option value="CA">California</option>
												<option value="CO">Colorado</option>
												<option value="CT">Connecticut</option>
												<option value="DE">Delaware</option>
												<option value="DC">District Of Columbia</option>
												<option value="FL">Florida</option>
												<option value="GA">Georgia</option>
												<option value="HI">Hawaii</option>
												<option value="ID">Idaho</option>
												<option value="IL">Illinois</option>
												<option value="IN">Indiana</option>
												<option value="IA">Iowa</option>
												<option value="KS">Kansas</option>
												<option value="KY">Kentucky</option>
												<option value="LA">Louisiana</option>
												<option value="ME">Maine</option>
												<option value="MD">Maryland</option>
												<option value="MA">Massachusetts</option>
												<option value="MI">Michigan</option>
												<option value="MN">Minnesota</option>
												<option value="MS">Mississippi</option>
												<option value="MO">Missouri</option>
												<option value="MT">Montana</option>
												<option value="NE">Nebraska</option>
												<option value="NV">Nevada</option>
												<option value="NH">New Hampshire</option>
												<option value="NJ">New Jersey</option>
												<option value="NM">New Mexico</option>
												<option value="NY">New York</option>
												<option value="NC">North Carolina</option>
												<option value="ND">North Dakota</option>
												<option value="OH">Ohio</option>
												<option value="OK">Oklahoma</option>
												<option value="OR">Oregon</option>
												<option value="PA">Pennsylvania</option>
												<option value="RI">Rhode Island</option>
												<option value="SC">South Carolina</option>
												<option value="SD">South Dakota</option>
												<option value="TN">Tennessee</option>
												<option value="TX">Texas</option>
												<option value="UT">Utah</option>
												<option value="VT">Vermont</option>
												<option value="VA">Virginia</option>
												<option value="WA">Washington</option>
												<option value="WV">West Virginia</option>
												<option value="WI">Wisconsin</option>
												<option value="WY">Wyoming</option>
										   	</select>
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											Phone <strong class="text-danger">*</strong>
											<input type="text" name="destination_phone" class="form-control" placeholder="Phone">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											Attention <strong class="text-danger">*</strong>
											<input type="text" name="destination_attn_name" class="form-control" placeholder="Attention">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											Email
											<input type="email" name="destination_email" class="form-control" placeholder="Email">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="p-4 ship-details">
						<div class="row">
							<div class="col-5">
								<div class="border border-1 px-3 pb-3 rounded">
									<div class="row">
										<div class="col-12 px-3 py-3 mb-3 card-header-quotes">
											<h2>DIMENSIONS & WEIGHT :</h2>
										</div>
									</div>
									<div class="row">
										<div class="col-12">
											<div class="form-group">
												<label>Quantity</label>
												<select class="form-control" id="qty-select-js">
													@for ($i = 1; $i <= 30; $i++)
														<option value="{{ $i }}" @if($i == 1) selected @endif>{{ $i }}</option>
													@endfor
												</select>
											</div>
										</div>
									</div>
									<hr>
									<div class="row" id="pak-weight-div-js">																														
										<div class="col-4">									
											<div class="form-group">
												<label>PAK #</label>
												<strong>ITEM 1</strong>
											</div>
										</div>
										<div class="col-4">
											<div class="form-group">
												<label>Weight</label>
												<select name="item[0][weight]" class="form-control">
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
												</select>
											</div>
										</div>
										<div class="col-4">
											<div class="form-group">
												<label>Unit</label>
												<select name="item[0][weight_unit_id]" class="form-control">
													@foreach($weightUnits as $key => $value)
														<option value="{{ $key }}">{{ $value }}</option>
													@endforeach
												</select>
											</div>
										</div>										
									</div>									
								</div>								
							</div>
							<div class="switch text-center px-4" style="width:96px;"></div>
							<div class="col-5">
								<div class="border border-1 px-3 pb-3 rounded">
									<div class="row">
										<div class="col-12 px-3 py-3 mb-3 card-header-quotes">
											<h2>INSURANCE :</h2>
										</div>
									</div>
									<div class="row">
										<div class="col-6">
											<div class="form-group">
												<label>Insurance</label>											
												<input type="text" placeholder="Enter Amount" class="form-control" name="insurance_amount">
											</div>										
										</div>
										<div class="col-6">
											<div class="form-group">
												<label>Currency</label>
												<div class="form-group">
													<select class="form-control" name="currency">
														<option value="CAD">CAD</option>													
													</select>												
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>					
				</div>
				<div class="bottom-submit text-right mt-3">
					<a href="{{ route('user.pak-quote.create') }}" class="btn-quote-reset mr-3">Clear</a>
					<button type="submit" class="btn-quote" id="get-a-quote-btn"> Get a Quote</button>
				</div>
				</form>
				<div class="col-sm-12 bg-white mt-4">
					<form action="{{ route('user.shipment.set.rate') }}" id="final-rates-form">
					<table class="table d-none">
						<thead>
							<tr>
							<th scope="col">#</th>
							<th scope="col">Carrier</th>
							<th scope="col">Shipment Service</th>
							<th scope="col">Rate</th>
							<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody id="table-response-js">
						</tbody>
					</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@push('css')
	<style>
		#overlay{
		position: fixed;
		top: 0;
		z-index: 100;
		width: 100%;
		height:100%;
		display: none;
		background: rgba(0,0,0,0.6);
		}
		.cv-spinner {
		height: 100%;
		display: flex;
		justify-content: center;
		align-items: center;
		}
		.spinner {
		width: 40px;
		height: 40px;
		border: 4px #ddd solid;
		border-top: 4px #2e93e6 solid;
		border-radius: 50%;
		animation: sp-anime 0.8s infinite linear;
		}
		@keyframes sp-anime {
		100% {
			transform: rotate(360deg);
		}
		}
		.is-hide{
		display:none;
		}
	</style>
@endpush
@push('scripts')
<script>
	$(document).ready(function() {
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		//Autocomplete
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

		$('[name="ship_from_city"], [name="ship_from_postal"], [name="ship_to_city"], [name="ship_to_postal"]').autocomplete({
			source: function( request, response ) {
			// Fetch data
			$.ajax({
				url:"{{ route('autocomplete.city') }}",
				type: 'get',
				dataType: "json",
				data: {
					_token: CSRF_TOKEN,
					search: request.term
				},
				success: function( data ) {
					response(data);
				}
			});
			},
			select: function (event, ui) {
				console.log($(this).data('ship'), ui.item.label, ui.item.value, ui.item.state, ui.item.country, ui.item.city);

				var city = ui.item.city;

				var state = ui.item.state;

				var country = ui.item.country;

				var postal = ui.item.zipcode;

				$(`[name="ship_${$(this).data('ship')}_city"]`).val(city);
				$(`[name="ship_${$(this).data('ship')}_state"]`).val(state);
				$(`[name="ship_${$(this).data('ship')}_country"]`).val(country);
				$(`[name="ship_${$(this).data('ship')}_postal"]`).val(postal);

				hideShowShipStates(country, $(this).data('ship'));

				return false;
			},
			minLength:2
		});

		function hideShowShipStates(country, shipattr) {
			console.log(country);
			$(`[data-id="province-${shipattr}-div-${country}"]`).removeClass('d-none');
			$(`[data-id="province-${shipattr}-div-${country}"]`).find('select').removeAttr('disabled');

			if(country == 'ca') {
				$(`[data-id="province-${shipattr}-div-us"]`).addClass('d-none');
				$(`[data-id="province-${shipattr}-div-us"]`).find('select').attr('disabled', true);
			}

			if(country == 'us') {
				$(`[data-id="province-${shipattr}-div-ca"]`).addClass('d-none');
				$(`[data-id="province-${shipattr}-div-ca"]`).find('select').attr('disabled', true);
			}
		}

		$('[id="pak-form"]').on('submit', function(e) {

			if (!$(this).valid()) {
				return false;
			}

			e.preventDefault();

			var _token = $("input[name='_token']").val();

			$.ajax({
				url: $('[id="pak-form"]').attr('action'),
				type:'POST',
				data: $('[id="pak-form"]').serialize(),
				beforeSend:function() {
					$("#overlay").fadeIn(300);
					$("[id='table-response-js']").html('');
					$('[data-id="alert-error-block"]').parent().addClass('d-none');
					$('[data-id="alert-error-block"]').html('');
				},
				success: function(data) {
					setTimeout(function() {
						$("#overlay").fadeOut(300);
					},500);

					if(!$.isEmptyObject(data.errors)) {
						var errorsData = data.errors;
						var htmlText = errorsData.map(function(o) {
							return `<p>${o}</p>`;
						}).join('');

						$('[data-id="alert-error-block"]').parent().removeClass('d-none');
						$('[data-id="alert-error-block"]').append(`<strong>${htmlText}</strong>`);
					}

					if(data.response) {
						result = data.response;
						console.log(data.response);
						var tableData = '';
						var i = 0;
						$.each(result,function(index, row) {
							i = i+1;
							tableData += `
								<tr>
									<td>${i}</td>
									<td><img src="${row.logo}" height="40"></td>
									<td>${index}</td>
									<td>${row.amount}</td>
									<td>
										<form action="{{ route('user.shipment.set.rate') }}" id="final-rates-form">
										<input type="hidden" name="final_service_code"   id="final_service_code_${i}" value="${row.service_code}">
										<input type="hidden" name="final_service_type"   id="final_service_type_${i}" value="${index}">
										<input type="hidden" name="carrier"             id="carrier_${i}" value="${row.type}">
										<input type="hidden" name="total_base_charge"   id="total_base_charge_${i}" value="${row.total_base_charge}">
										<input type="hidden" name="transport_charges"   id="transport_charges_${i}" value="${row.transport_charges}">
										<input type="hidden" name="service_charges"     id="service_charges_${i}" value="${row.service_charges}">
										<input type="hidden" name="sur_charges"         id="total_sur_charges_${i}" value="${row.sur_charges}">
										<input type="radio" name="final_shipment_rate"  id="final_shipment_rate_${i}" value="${row.amount}">
										<input type="hidden" name="delivery_timestamp"  id="delivery_timestamp_${i}" value="${row.delivery_timestamp}">
										</form>
									</td>
								</tr>
							`;
						});

						$("[id='table-response-js']").append(tableData);
						$("[id='table-response-js']").parent().removeClass('d-none');
					}
				},
				error: function (jqXHR, textStatus, error) {
                    console.log(jqXHR.responseJSON);
					setTimeout(function() {
						$("#overlay").fadeOut(300);
					},500);
                    if (jqXHR.responseJSON) {
                        alert(jqXHR.responseJSON.message);
                        return false;
                    }
                }
			});
		});

		$(document).on('click', '[name="final_shipment_rate"]', function() {
			$.ajax({
				url: $(this).closest("form").attr('action'),
				type:'POST',
				data: $(this).closest("form").serialize(),
				beforeSend:function() {
					$("#overlay").fadeIn(300);
				},
				success: function(data) {
					setTimeout(function() {
						$("#overlay").fadeOut(300);
					},500);

					$(location).attr('href', '/user/shipment-checkout');
				}
			});
		});

		$(document).on('change', '[id="qty-select-js"]', function() {
			var qty = $(this).val();

			var $weightHtml  = '';

			for ( var i = 0, l = qty; i < l; i++ ) {
				$weightHtml += `<div class="col-4">
					<div class="form-group">
						<label>PAK #</label>
						<strong>ITEM ${i+1}</strong>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label>Weight</label>
						<select name="item[${i}][weight]" class="form-control">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
						</select>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label>Unit</label>
						<select name="item[${i}][weight_unit_id]" class="form-control">
							@foreach($weightUnits as $key => $value)
								<option value="{{ $key }}">{{ $value }}</option>
							@endforeach
						</select>
					</div>
				</div>`;
			}

			$('[id="pak-weight-div-js"]').html($weightHtml);

		});

		 $(`[name="ship_from_country"]`).on('change', function() {
			var selected_val = $(this).val();

			$(`[data-id="province-from-div-${selected_val}"]`).removeClass('d-none');
			$(`[data-id="province-from-div-${selected_val}"]`).find('select').removeAttr('disabled');

			if(selected_val == 'ca') {
				$(`[data-id="province-from-div-us"]`).addClass('d-none');
				$(`[data-id="province-from-div-us"]`).find('select').attr('disabled', true);
			}

			if(selected_val == 'us') {
				$(`[data-id="province-from-div-ca"]`).addClass('d-none');
				$(`[data-id="province-from-div-ca"]`).find('select').attr('disabled', true);
			}
		});

		$(`[name="ship_to_country"]`).on('change', function() {
			var selected_val = $(this).val();

			$(`[data-id="province-to-div-${selected_val}"]`).removeClass('d-none');
			$(`[data-id="province-to-div-${selected_val}"]`).find('select').removeAttr('disabled');

			if(selected_val == 'ca') {
				$(`[data-id="province-to-div-us"]`).addClass('d-none');
				$(`[data-id="province-to-div-us"]`).find('select').attr('disabled', true);
			}

			if(selected_val == 'us') {
				$(`[data-id="province-to-div-ca"]`).addClass('d-none');
				$(`[data-id="province-to-div-ca"]`).find('select').attr('disabled', true);
			}
		});

		//Validate Quotes Fields
		$(document).on('keydown', '#email', function(e) {
			if (e.keyCode == 32) return false;
		});

		/**
		 * Validate Form
		 */
		$('[id="pak-form"]').validate({
			rules: {
				'origin_email': {
					email: true
				},
				'destination_email': {
					email: true
				},
				'ship_from_city': {
					required: true,
				},
				'ship_to_city': {
					required: true,
				},
				'ship_from_postal': {
					required: true,
				},
				'ship_to_postal': {
					required: true,
				},
				'ship_from_country': {
					required: true,
				},
				'ship_to_country': {
					required: true,
				},
				'ship_from_state': {
					required: true,
				},
				'ship_to_state': {
					required: true,
				},
				'street_from_address': {
					required: true,
				},
				'street_to_address': {
					required: true,
				},
				'ship_from_business': {
					required: true,
				},
				'ship_to_business': {
					required: true,
				},
				'origin_attn_name': {
					required: true,
				},
				'destination_attn_name': {
					required: true,
				},
				'origin_phone': {
					required: true,
					digits:true
				},
				'destination_phone': {
					required: true,
					digits:true
				}
			},
			messages: {
				'origin_email': {
					required: 'Please enter an email.',
				}
			},
			errorElement:'span',
			errorClass:'help-inline',
			errorPlacement: function(error, element) {
				let errorDiv = $('<div></div>', {
					"class": "invalid-feedback"
				});

				let isErrorElementExist = element.parent().find('.invalid-feedback').length;

				if (!isErrorElementExist) {
					element.parent().append(errorDiv);
				}

				errorDiv.html(error);

				if (!element.hasClass('is-invalid')) {
					element.addClass('is-invalid');
				}
			},
			highlight: function (element, errorClass) {
				$(element).addClass('is-invalid');
				$(element).addClass('error');
			},
			unhighlight: function (element, errorClass) {
				$(element).removeClass('is-invalid');
				$(element).removeClass('error');
				$(element).addClass('is-valid');
			}
		});
	});
</script>
@endpush