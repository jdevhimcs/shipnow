@extends('layouts.dashboard')
@section('title', 'Account Settings')
@section('content')
<div class="content">
	<form method="POST" action="{{ route('user.checkout') }}" id="payment-form">
	@csrf
    <div class="row mb-4">
        <div class="col-4">
            <div class="quote-number">
                <span></span>
                <h2>{{ strtoupper($quotes['carrier']) }} Shipment</h2>
            </div>
        </div>
        <div class="col-6">
            <div class="add-btn text-right">
                <a type="button" class="add-btn bg-dark-blue text-white border-0 py-2 px-4 font-weight-bold rounded mr-3" href="{{ route('user.letter-quote.create') }}">BACK</a>
                <button type="submit" class="add-btn bg-dark-blue text-white border-0 py-2 px-4 font-weight-bold rounded" data-btn="shipnow">SHIP NOW</button>
            </div>
        </div>
    </div>	
	<div class="quote-form">
		<div class="row">
			<div class="col-8">
				@if(session()->has('message'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<strong>{{ session()->get('message') }}</strong>
				</div>
				@endif
				@if(session()->has('error'))
				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<strong>{{ session()->get('error') }}</strong>
				</div>
				@endif				
				<div class="bg-white p-4">
					<h5> Shipping Details </h5><hr>
					<table class="table table-dimensions mb-5">
						<tbody>
							<tr>
								<td>
									Ship From:
									<strong class="d-block">
										{{ strtoupper($quotes['ship_from_country']) }}, 
										{{ strtoupper($quotes['ship_from_state']) }}, <br>
										{{ strtoupper($quotes['ship_from_city']) }}, <br>{{ strtoupper($quotes['ship_from_postal']) }} 
									</strong>
								</td>
								<td>
									Ship To:
									<strong class="d-block">
									{{ strtoupper($quotes['ship_to_country']) }}, 
									{{ strtoupper($quotes['ship_to_state']) }}, <br>
									{{ strtoupper($quotes['ship_to_city']) }}, <br>{{ strtoupper($quotes['ship_to_postal']) }}
									</strong>
								</td>                                
							</tr>
							<tr>
								<td>
									Carrier
									<strong class="d-block">
									{{ strtoupper($quotes['carrier']) }}
									</strong>
								</td>
								<td>
								Service
									<strong class="d-block">
										{{ strtoupper($quotes['shipment_final_service']) }}
									</strong>
								</td>                                
							</tr>
							<tr>
								<td>
									REF #
									<strong class="d-block">
									{{ strtoupper($quotes['ref_num']) }}
									</strong>
								</td>
								<td>
								Broker
									<strong class="d-block">
										N/A
									</strong>
								</td>                                
							</tr>							                 
						</tbody>
					</table>
					
					<h5> Payment Details </h5><hr>
					<div class="row mb-4">
						<div class="col-8">
							<label for="card-element">
							Credit Card Info
							</label>
							<div id="card-element">
							<!-- A Stripe Element will be inserted here. -->
							</div>
		
							<!-- Used to display form errors. -->
							<div id="card-errors" class="error" role="alert"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-2">
				<div class="cost bg-white shadow-sm p-4">					                   
					<div class="total-cost border-bottom mb-3 pb-3">
						<span>Base Rate:</span>
						<h2>${{ $quotes['total_base_charge'] }}</h2>
					</div>
					<div class="total-cost border-bottom mb-3 pb-3">
						<span>Transport Charges:</span>
						<h2>${{ $quotes['transport_charges'] }}</h2>
					</div>
					<div class="total-cost border-bottom mb-3 pb-3">
						<span>Service Charges:</span>
						<h2>${{ $quotes['service_charges'] }}</h2>
					</div>
					<div class="total-cost border-bottom mb-3 pb-3">
						<span>Total:</span>
						<h2>${{ $quotes['shipment_final_rate'] }}</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	</form>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('css/stripe/stripe.css') }}"></link>
@endpush
@push('scripts')
<script src="https://js.stripe.com/v3/"></script>
<script>
	var publishable_key = "{{ $publishkey }}";
	// Create a Stripe client.
	var stripe = Stripe(publishable_key);
	
	// Create an instance of Elements.
	var elements = stripe.elements();
	
	// Custom styling can be passed to options when creating an Element.
	// (Note that this demo uses a wider set of styles than the guide below.)
	var style = {
		base: {
			color: '#66615b',
			fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
			fontSmoothing: 'antialiased',
			fontSize: '14px',
			'::placeholder': {
				color: '#abacad'
			}
		},
		invalid: {
			color: '#dc3545',
			iconColor: '#dc3545'
		}
	};
	
	// Create an instance of the card Element.
	var card = elements.create('card', {style: style});
	
	// Add an instance of the card Element into the `card-element` <div>.
	card.mount('#card-element');
	
	// Handle real-time validation errors from the card Element.
	card.addEventListener('change', function(event) {
		var displayError = document.getElementById('card-errors');
		if (event.error) {
			displayError.textContent = event.error.message;
		} else {
			displayError.textContent = '';
		}
	});
	
	// Handle form submission.
	var form = document.getElementById('payment-form');
	form.addEventListener('submit', function(event) {
		event.preventDefault();
	
		stripe.createToken(card).then(function(result) {
			if (result.error) {
				// Inform the user if there was an error.
				var errorElement = document.getElementById('card-errors');
				errorElement.textContent = result.error.message;
			} else {
				// Send the token to your server.
				stripeTokenHandler(result.token);
			}
		});
	});
	
	// Submit the form with the token ID.
	function stripeTokenHandler(token) {
		// Insert the token ID into the form so it gets submitted to the server
		var form = document.getElementById('payment-form');
		var hiddenInput = document.createElement('input');
		hiddenInput.setAttribute('type', 'hidden');
		hiddenInput.setAttribute('name', 'stripeToken');
		hiddenInput.setAttribute('value', token.id);
		form.appendChild(hiddenInput);
	
		// Submit the form
		form.submit();
	}
</script>
@endpush