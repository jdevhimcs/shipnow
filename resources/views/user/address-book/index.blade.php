@extends('layouts.dashboard')
@section('title', 'Account Settings')
@section('content')
<div class="content">
	@include('elements.auth.my_account_nav')
	<div class="content-container">
		<div class="row mb-4">
			<div class="col-7">
				<div class="search-form">
					<form action="{{ route('user.address-book-list.index') }}">
						<div class="search-outer">
							<img class="user-img" src="{{ asset('img/search-icon.png') }}">
							<input name="filter" type="text" class="form-control" placeholder="Search" value="{{ $request->filter }}">
						</div>
					</form>
				</div>
			</div>
			<div class="col-5">
				<div class="add-btn text-right">
					<button class="add-btn bg-dark-blue text-white border-0 py-2 px-4 font-weight-bold rounded" type="button" data-toggle="modal" data-target="#address-book-modal">Add New</button>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				@foreach($addressBooks as $addressBook)
				<div class="d-flex flex-wrap bg-white align-items-center justify-content-between rounded shadow-sm mb-3">
					<div class="product-item-name col-md-4 col-sm-6 p-3">
						<h3>{{ ucwords($addressBook->company_name) }}</h3>
						<p>
							<img src="{{ asset('img/location.png') }}">
							{{ $addressBook->address }}
						</p>
					</div>
					<div class="phone-no col-md-3 col-sm-6 p-3">
						<img src="{{ asset('img/phone.png') }}">
						<span>{{ $addressBook->phone_no }}</span>
					</div>
					<div class="email-id col-md-3 col-sm-6 p-3">
						<p>
							<img src="{{ asset('img/message.png') }}">
							{{ $addressBook->email }}
						</p>
					</div>
					<div class="action col-md-2 col-sm-6 text-left text-md-right p-3">
						<a href="{{ route('user.address-book-list.edit', $addressBook->id) }}" class="btn-edit">
							<img src="{{ asset('img/edit.png') }}" class="mr-3">
						</a>
						<a href="{{ route('user.address-book-list.destroy', encrypt($addressBook->id)) }}" class="btn-delete" onclick="event.preventDefault(); document.getElementById('address-book-del-form').submit();">
							<img src="{{ asset('img/delete.png') }}">
						</a>
						<form id="address-book-del-form" action="{{ route('user.address-book-list.destroy', encrypt($addressBook->id)) }}" method="POST" style="display: none;">
							@csrf
							@method('DELETE')
						</form>
					</div>
				</div>
				@endforeach
				<!-- row 2 -->
				<!-- pagination -->
				<nav aria-label="Page navigation " class="custom-pagination">
					<ul class="pagination justify-content-end">
						<li class="page-item">
							<a class="page-link" href="#" aria-label="Previous">
								<span aria-hidden="true">
									<img src="{{ asset('img/arrow-prev.png') }}">
								</span>
							</a>
						</li>
						<li class="page-item active"><a class="page-link" href="#">1</a></li>
						<li class="page-item"><a class="page-link" href="#">2</a></li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item">
							<a class="page-link" href="#" aria-label="Next">
								<span aria-hidden="true">
									<img src="{{ asset('img/next.png') }}">
								</span>
							</a>
						</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="address-book-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="address-book-modal-label" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable  modal-lg">
		<div class="modal-content">
			<div class="modal-header header-custom py-4 px-5">
				<h5 class="modal-title" id="address-book-modal-label">Add New Address Book</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body pb-4">
				<div class="form-add px-4">
					<form action="#" id="book-add-form" data-action="{{ route('user.address-book-list.store') }}">
						<div class="row">
							<div class="col-6">
								<div class="form-group">
									<label>Company Name</label>
									<input type="text" name="company_name" class="form-control" placeholder="company name">
									<span class="error">
										<strong id="company-name-error"></strong>
									</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<div class="form-group">
									<label>Address</label>
									<input type="text" name="address" class="form-control" placeholder="Address">
									<span class="error">
										<strong id="address-error"></strong>
									</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-4">
								<div class="form-group">
									<label>Country</label>
									<select name="company_country" class="form-control custom-text-input" id="country-dropdown">
										<option value="">Select Country</option>
										@foreach ($countries as $country)
										<option value="{{$country->id}}">{{$country->name}}</option>
										@endforeach
									</select>
									<span class="error">
										<strong id="company-country-error"></strong>
									</span>
								</div>
							</div>
							<div class="col-4">
								<div class="form-group">
									<label>State</label>
									<select name="company_state" class="state form-control custom-text-input" id="state-dropdown"></select>
									<span class="error">
										<strong id="company-state-error"></strong>
									</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-4">
								<div class="form-group">
									<label>City</label>
									<input type="text" name="company_city" placeholder="City" class="form-control custom-text-input" id="company-city">
									<span class="error">
										<strong id="company-city-error"></strong>
									</span>
								</div>
							</div>
							<div class="col-4">
								<div class="form-group">
									<label>Postal</label>
									<input type="text" name="postal_code" placeholder="Postal Code" class="form-control custom-text-input" id="postal-code">
									<span class="error">
										<strong id="postal-code-error"></strong>
									</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<div class="form-group">
									<label>Email Address</label>
									<input type="text" name="email" class="form-control" placeholder="Email Address">
									<span class="error">
										<strong id="email-error"></strong>
									</span>
								</div>
							</div>
							<div class="col-6">
								<div class="form-group">
									<label>Phone Number</label>
									<input type="text" name="phone_no" class="form-control" placeholder="Phone Number">
									<span class="error">
										<strong id="phone-no-error"></strong>
									</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-auto">
								<div class="form-group">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input check-box-input-js-picked" id="picked" name="is_picked" value="1">
										<label class="custom-control-label" for="picked">De Picked up</label>

									</div>
								</div>
							</div>
							<div class="col-auto">
								<div class="form-group">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input check-box-input-js-picked" id="default" name="is_picked" value="2">
										<label class="custom-control-label" for="default">Default Drop off</label>
									</div>
								</div>
							</div>
							<div class="col-12">
								<span class="error">
									<strong id="is-picked-error"></strong>
								</span>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<div class="form-group">
									<label>Shipping hours</label>
									<div class="d-flex align-items-center">
										<div class="input-group">
											<input name="ship_hours_from" type="time" class="form-control date-input" aria-label="Text input with segmented dropdown button">
											<span class="error">
												<strong id="ship-hours-from-error"></strong>
											</span>
										</div>
										<div class="to px-3">TO</div>
										<div class="input-group">
											<input name="ship_hours_to" type="time" class="form-control date-input" aria-label="Text input with segmented dropdown button">
											<span class="error">
												<strong id="ship-hours-to-error"></strong>
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-6">
								<div class="form-group">
									<label>Default Note</label>
									<input type="text" name="note" class="form-control" placeholder="Type Notes..">
									<span class="error">
										<strong id="note-error"></strong>
									</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<div class="form-group">
									<label>Business with dock</label>
									<div class="d-flex">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input check-box-input-js-bussiness" id="yes1" name="is_business_dock" value="1">
											<label class="custom-control-label" for="yes1">Yes</label>
										</div>
										<div class="custom-control custom-checkbox ml-3">
											<input type="checkbox" class="custom-control-input check-box-input-js-bussiness" id="no1" name="is_business_dock" value="0">
											<label class="custom-control-label" for="no1">No</label>
										</div>
									</div>
									<span class="error">
										<strong id="is-business-dock-error"></strong>
									</span>
								</div>
							</div>
							<div class="col-6">
								<label>Residence</label>
								<div class="d-flex">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input check-box-input-js-residence" id="yes2" name="is_residence" value="1">
										<label class="custom-control-label" for="yes2">Yes</label>
										<span class="error">
											<strong id="is-residence-error"></strong>
										</span>
									</div>
									<div class="custom-control custom-checkbox ml-3">
										<input type="checkbox" class="custom-control-input check-box-input-js-residence" id="no2" name="is_residence" value="0">
										<label class="custom-control-label" for="no2">No</label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<div class="form-group">
									<label>Appointment delivery</label>
									<div class="d-flex">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input check-box-input-js-delivery" id="yes3" name="appointment_delivery" value="1">
											<label class="custom-control-label" for="yes3">Yes</label>

										</div>
										<div class="custom-control custom-checkbox ml-3">
											<input type="checkbox" class="custom-control-input check-box-input-js-delivery" id="no3" name="appointment_delivery" value="0">
											<label class="custom-control-label" for="no3">No</label>
										</div>
									</div>
									<span class="error">
										<strong id="appointment-delivery-error"></strong>
									</span>
								</div>
							</div>
							<div class="col-6">
								<div class="form-group">
									<label>Straight Truck</label>
									<div class="d-flex">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input check-box-input-js-truck" id="yes4" name="straight_truck" value="1">
											<label class="custom-control-label" for="yes4">Yes</label>
										</div>
										<div class="custom-control custom-checkbox ml-3">
											<input type="checkbox" class="custom-control-input check-box-input-js-truck" id="no4" name="straight_truck" value="0">
											<label class="custom-control-label" for="no4">No</label>
										</div>
									</div>
									<span class="error">
										<strong id="straight-truck-error"></strong>
									</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 mt-4">
								<div class="form-group">
									<button type="submit" name="Save" class="bg-dark-blue text-white border-0 py-2 px-5 font-weight-bold rounded" id="add-book-btn">Save</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('css')
<style>
	span.error {
		position: absolute;
		font-weight: 400 !important;
		font-size: 80%;
		color: #dc3545;
	}
</style>
@endpush
@push('scripts')
<script>
	$(function() {
		//Add Form
		var add_book_form = $('[id="book-add-form"]');

		//Error Fields
		var company_name_error = $('[id="company-name-error"]');
		var address_error = $('[id="address-error"]');
		var company_country_error = $('[id="company-country-error"]');
		var company_state_error = $('[id="company-state-error"]');
		var company_city_error = $('[id="company-city-error"]');
		var postal_code_error = $('[id="postal-code-error"]');
		var email_error = $('[id="email-error"]');
		var phone_no_error = $('[id="phone-no-error"]');
		var is_picked_error = $('[id="is-picked-error"]');
		var ship_hours_from_error = $('[id="ship-hours-from-error"]');
		var ship_hours_to_error = $('[id="ship-hours-to-error"]');
		var note_error = $('[id="note-error"]');
		var is_business_dock_error = $('[id="is-business-dock-error"]');
		var is_residence_error = $('[id="is-residence-error"]');
		var appointment_delivery_error = $('[id="appointment-delivery-error"]');
		var straight_truck_error = $('[id="straight-truck-error"]');

		//Action Btns
		var add_book_btn = '[data-id="add-book-btn"]';

		//Ajax token
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		//Add New Book with errors
		add_book_form.on("submit", function(event) {
			event.preventDefault();
			var form_data = $(this).serialize();
			$.post({
				type: 'POST',
				url: $(this).attr('action'),
				data: form_data,
				beforeSend: function() {
					company_name_error.html("");
					address_error.html("");
					company_country_error.html("");
					company_state_error.html("");
					company_city_error.html("");
					postal_code_error.html("");
					email_error.html("");
					phone_no_error.html("");
					is_picked_error.html("");
					ship_hours_from_error.html("");
					ship_hours_to_error.html("");
					note_error.html("");
					is_business_dock_error.html("");
					is_residence_error.html("");
					appointment_delivery_error.html("");
					straight_truck_error.html("");
				},
			}).done(function(data) {
				if (data.errors) {
					if (data.errors.company_name) {
						$.each(data.errors.company_name, function(key, value) {
							company_name_error.append(value);
						});
					}
					if (data.errors.address) {
						$.each(data.errors.address, function(key, value) {
							address_error.append(value);
						});
					}
					if (data.errors.company_country) {
						$.each(data.errors.company_country, function(key, value) {
							company_country_error.append(value);
						});
					}
					if (data.errors.company_state) {
						$.each(data.errors.company_state, function(key, value) {
							company_state_error.append(value);
						});
					}
					if (data.errors.company_city) {
						$.each(data.errors.company_city, function(key, value) {
							company_city_error.append(value);
						});
					}
					if (data.errors.postal_code) {
						$.each(data.errors.postal_code, function(key, value) {
							postal_code_error.append(value);
						});
					}
					if (data.errors.email) {
						$.each(data.errors.email, function(key, value) {
							email_error.append(value);
						});
					}
					if (data.errors.phone_no) {
						$.each(data.errors.phone_no, function(key, value) {
							phone_no_error.append(value);
						});
					}
					if (data.errors.is_picked) {
						$.each(data.errors.is_picked, function(key, value) {
							is_picked_error.append(value);
						});
					}

					if (data.errors.ship_hours_from) {
						$.each(data.errors.ship_hours_from, function(key, value) {
							ship_hours_from_error.append(value);
						});
					}
					if (data.errors.ship_hours_to) {
						$.each(data.errors.ship_hours_to, function(key, value) {
							ship_hours_to_error.append(value);
						});
					}
					if (data.errors.note) {
						$.each(data.errors.note, function(key, value) {
							note_error.append(value);
						});
					}
					if (data.errors.is_business_dock) {
						$.each(data.errors.is_business_dock, function(key, value) {
							is_business_dock_error.append(value);
						});
					}
					if (data.errors.is_residence) {
						$.each(data.errors.is_residence, function(key, value) {
							is_residence_error.append(value);
						});
					}

					if (data.errors.appointment_delivery) {
						$.each(data.errors.appointment_delivery, function(key, value) {
							appointment_delivery_error.append(value);
						});
					}
					if (data.errors.straight_truck) {
						$.each(data.errors.straight_truck, function(key, value) {
							straight_truck_error.append(value);
						});
					}
				}

				if (data.statusCode === 200) {
					location.reload();
				}
			});
		});
		//Load States
		$('[id="country-dropdown"]').change(function() {
			// console.log(state_identifier);
			var __country_id = $(this).val();
			if (__country_id) {
				$.ajax({
					type: "get",
					url: "{{ route('user.states.list') }}",
					data: {
						country_id: __country_id,
						//_token: '{{csrf_token()}}'
					},
					success: function(res) {
						if (res) {
							$('[id="state-dropdown"]').empty();
							$('[id="city-dropdown"]').empty();
							$('[id="state-dropdown"]').append('<option>Select State</option>');
							$.each(res.states, function(key, state) {
								$('[id="state-dropdown"]').append('<option value="' + state.id + '">' + state.name + '</option>');
							});
							if (state_identifier) {
								$('[id="state-dropdown"]')
									.val(state_identifier)
									.trigger('change');
							}
						}
					}
				});
			}
		});

		//Check Box selected once at a time
		$('.check-box-input-js-bussiness').click(function() {
			$('.check-box-input-js-bussiness').not(this).prop('checked', false);
		});

		$('.check-box-input-js-residence').click(function() {
			$('.check-box-input-js-residence').not(this).prop('checked', false);
		});

		$('.check-box-input-js-delivery').click(function() {
			$('.check-box-input-js-delivery').not(this).prop('checked', false);
		});

		$('.check-box-input-js-truck').click(function() {
			$('.check-box-input-js-truck').not(this).prop('checked', false);
		});

		$('.check-box-input-js-picked').click(function() {
			$('.check-box-input-js-picked').not(this).prop('checked', false);
		});
	});
</script>
@endpush