@extends('layouts.dashboard')
@section('title', 'Edit address book')
@section('content')
<div class="content">
	@include('elements.auth.my_account_nav')
	<div class="content-container">		
		<div class="row">
			<div class="col-12">
				<form method="POST" action="{{ route('user.address-book-list.update', $addressBook->id) }}" id="edit-book-form" >
				@csrf
				@method('PUT')
					<div class="bg-white p-4 shadow-sm">
						<div class="row">
							<div class="col-12">
								@if(session()->has('message'))
									<div class="alert alert-success alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>
											<strong>{{ session()->get('message') }}</strong>
									</div>
								@endif
								@if(session()->has('error'))
									<div class="alert alert-danger alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>
											<strong>{{ session()->get('error') }}</strong>
									</div>
								@endif
								<div class="seetting-heading">
									<h2>
										Edit Address Book
									</h2>
								</div>
							</div>
							<div class="col-10">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Company Name</label>
                                            <input type="text" name="company_name" class="form-control @error('company_name') is-invalid @enderror" value="{{ old('company_name', $addressBook->company_name) }}" placeholder="company name">
                                            @error('company_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{$message}}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Address</label>
                                            <input type="text" name="address" class="form-control @error('address') is-invalid @enderror" value="{{ old('address', $addressBook->address) }}" placeholder="Address">
                                            @error('address')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{$message}}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label>Country</label>
                                            <select name="company_country" class="form-control @error('company_country') is-invalid @enderror custom-text-input" value="{{ old('company_country', $addressBook->company_country) }}" id="country-dropdown">
                                                <option value="">Select Country</option>
                                                @foreach ($countries as $country)
                                                <option value="{{$country->id}}" @if($country->id === $addressBook->country_id) selected @endif>{{$country->name}}</option>
                                                @endforeach
                                            </select>									
                                            @error('company_country')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{$message}}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label>State</label>
                                            <select name="company_state" class="state form-control @error('company_state') is-invalid @enderror custom-text-input" id="state-dropdown"></select>
                                            @error('company_state')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{$message}}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label>City</label>
                                            <input type="text" name="company_city" placeholder="City" class="form-control @error('company_city') is-invalid @enderror custom-text-input" value="{{ old('company_city', $addressBook->city) }}" id="company-city">
                                            @error('company_city')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{$message}}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label>Postal</label>
                                            <input type="text" name="postal_code" placeholder="Postal Code" class="form-control @error('postal_code') is-invalid @enderror custom-text-input" value="{{ old('postal_code', $addressBook->postal_code) }}" id="postal-code">
                                            @error('postal_code')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{$message}}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label>Email Address</label>
                                            <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email Address" value="{{ old('email', $addressBook->email) }}">
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{$message}}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label>Phone Number</label>
                                            <input type="text" name="phone_no" class="form-control @error('phone_no') is-invalid @enderror" placeholder="Phone Number" value="{{ old('phone_no', $addressBook->phone_no) }}">
                                            @error('phone_no')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{$message}}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-auto">
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input check-box-input-js-picked" id="picked" name="is_picked" value="1" {{ old('is_picked', $addressBook->is_picked) == 1 ? 'checked' : '' }}>
                                                <label class="custom-control-label" for="picked">De Picked up</label>
                                                @error('is_picked')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{$message}}</strong>
                                                </span>
                                            @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input check-box-input-js-picked" id="default" name="is_picked" value="2" {{ old('is_picked', $addressBook->is_picked) == 2 ? 'checked' : '' }}>
                                                <label class="custom-control-label" for="default">Default Drop off</label>										
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5">
                                        <div class="form-group">
                                            <label>Shipping hours</label>
                                            <div class="d-flex align-items-center">
                                                <div class="input-group">
                                                    <input name="ship_hours_from" type="time" class="form-control @error('ship_hours_from') is-invalid @enderror date-input" value="{{ old('ship_hours_from', $addressBook->ship_hours_from) }}" aria-label="Text input with segmented dropdown button">
                                                    @error('ship_hours_from')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{$message}}</strong>
                                                </span>
                                            @enderror
                                                </div>
                                                <div class="to px-3">TO</div>
                                                <div class="input-group">
                                                    <input name="ship_hours_to" type="time" class="form-control @error('ship_hours_to') is-invalid @enderror date-input" value="{{ old('ship_hours_to', $addressBook->ship_hours_to) }}" aria-label="Text input with segmented dropdown button">
                                                    @error('ship_hours_to')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{$message}}</strong>
                                                </span>
                                            @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                  
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Default Note</label>
                                            <input type="text" name="note" class="form-control @error('note') is-invalid @enderror" value="{{ old('note', $addressBook->note) }}" placeholder="Type Notes..">
                                            @error('note')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{$message}}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Business with dock</label>
                                            <div class="d-flex">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input check-box-input-js-bussiness" id="yes1" name="is_business_dock" value="1" {{ old('is_business_dock', $addressBook->is_business_dock) == 1 ? 'checked' : '' }}>
                                                    <label class="custom-control-label" for="yes1">Yes</label>
                                                </div>
                                                @error('is_business_dock')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{$message}}</strong>
                                                    </span>
                                                @enderror
                                                <div class="custom-control custom-checkbox ml-3">
                                                    <input type="checkbox" class="custom-control-input check-box-input-js-bussiness" id="no1" name="is_business_dock" value="0" {{ old('is_business_dock', $addressBook->is_business_dock) == 0 ? 'checked' : '' }}>
                                                    <label class="custom-control-label" for="no1">No</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label>Residence</label>
                                        <div class="d-flex">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input check-box-input-js-residence" id="yes2" name="is_residence" value="1" {{ old('is_residence', $addressBook->is_residence) == 1 ? 'checked' : '' }}>
                                                <label class="custom-control-label" for="yes2">Yes</label>
                                                @error('is_residence')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{$message}}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="custom-control custom-checkbox ml-3">
                                                <input type="checkbox" class="custom-control-input check-box-input-js-residence" id="no2" name="is_residence" value="0" {{ old('is_residence', $addressBook->is_residence) == 0 ? 'checked' : '' }}>
                                                <label class="custom-control-label" for="no2">No</label>										
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Appointment delivery</label>
                                            <div class="d-flex">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input check-box-input-js-delivery" id="yes3" name="appointment_delivery" value="1" {{ old('appointment_delivery', $addressBook->appointment_delivery) == 1 ? 'checked' : '' }}>
                                                    <label class="custom-control-label" for="yes3">Yes</label>
                                                    @error('appointment_delivery')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{$message}}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="custom-control custom-checkbox ml-3">
                                                    <input type="checkbox" class="custom-control-input check-box-input-js-delivery" id="no3" name="appointment_delivery" value="0" {{ old('appointment_delivery', $addressBook->appointment_delivery) == 0 ? 'checked' : '' }}>
                                                    <label class="custom-control-label" for="no3">No</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label>Straight Truck</label>
                                        <div class="d-flex">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input check-box-input-js-truck" id="yes4" name="straight_truck" value="1" {{ old('straight_truck', $addressBook->straight_truck) == 1 ? 'checked' : '' }}>
                                                <label class="custom-control-label" for="yes4">Yes</label>
                                                @error('straight_truck')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{$message}}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="custom-control custom-checkbox ml-3">
                                                <input type="checkbox" class="custom-control-input check-box-input-js-truck" id="no4" name="straight_truck" value="0" {{ old('straight_truck', $addressBook->straight_truck) == 0 ? 'checked' : '' }}>
                                                <label class="custom-control-label" for="no4">No</label>										
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 mt-4">
                                        <div class="form-group">
                                            <button type="submit" name="Save" class="bg-dark-blue text-white border-0 py-2 px-5 font-weight-bold rounded" id="add-book-btn">Update</button>
                                        </div>
                                    </div>
                                </div>								
							</div>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@push('scripts')
<script>
	var country_identifier = {{ !empty($addressBook->country_id) ? $addressBook->country_id : (int) false }};
	var state_identifier = {{ !empty($addressBook->state_id) ? $addressBook->state_id : (int) false }};
	$(function () {
		//Country Identifier
		if(country_identifier) {
			setTimeout(function() { 
				$('[id="country-dropdown"]')
				.val(country_identifier)
				.trigger('change');			
			}, 100);

		}	
		//Load States
		$('[id="country-dropdown"]').change(function() {
			// console.log(state_identifier);
			var __country_id = $(this).val();
			if(__country_id){
				$.ajax({
					type:"get",
					url:"{{ route('user.states.list') }}",
					data: {
						country_id: __country_id,
						//_token: '{{csrf_token()}}'
					},
					success:function(res)
					{
						if(res)
						{
							$('[id="state-dropdown"]').empty();
							$('[id="city-dropdown"]').empty();
							$('[id="state-dropdown"]').append('<option>Select State</option>');
							$.each(res.states,function(key, state) {
								$('[id="state-dropdown"]').append('<option value="'+state.id+'">'+state.name+'</option>');
							});
							if(state_identifier) {
								$('[id="state-dropdown"]')
								.val(state_identifier)
								.trigger('change');
							}
						}
					}
				});
			}
		});		
		
		//Check Box selected once at a time
		$('.check-box-input-js-bussiness').click(function() {
			$('.check-box-input-js-bussiness').not(this).prop('checked', false);
		});
	
		$('.check-box-input-js-residence').click(function() {
			$('.check-box-input-js-residence').not(this).prop('checked', false);
		});
	
		$('.check-box-input-js-delivery').click(function() {
			$('.check-box-input-js-delivery').not(this).prop('checked', false);
		});
	
		$('.check-box-input-js-truck').click(function() {
			$('.check-box-input-js-truck').not(this).prop('checked', false);
		});

		$('.check-box-input-js-picked').click(function() {
			$('.check-box-input-js-picked').not(this).prop('checked', false);
		});		
	});	

</script>
@endpush