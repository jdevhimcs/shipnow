@extends('layouts.app')

@section('content')
                    
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="login-section bg-white">
                    <h2>{{ __('Reset Password') }}</h2>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="form-group">
                            <label>Email address</label>
                            <input type="email" name="email" class="custom-input form-control @error('email') is-invalid @enderror" placeholder="Type your email address here" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group text-right">
                            <span class="d-block color-light-gray">
                                Know your password? <a href="{{ route('login') }}" class="color-sky font-weight-bold">Sign In</a>
                            </span>
                        </div>
                        <div class="form-group text-center mt-5">
                            <button type="submit" class="btn-password-reset"> {{ __('Send Password Reset Link') }}</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('css')
<style>
	body.bg-white {
		background-color: #F9F9F9 !important;
	}
</style>
@endpush
