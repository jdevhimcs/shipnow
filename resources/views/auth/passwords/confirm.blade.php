@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row ">
        <div class="col-12">
            <div class="login-section bg-white">
                <h2>{{ __('Confirm Password') }}</h2>


                <form method="POST" action="{{ route('password.confirm') }}">
                    @csrf

                    <div class="form-group ">
                        <label for="password">{{ __('Password') }}</label>

                        <input id="password" type="password" class="custom-input form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group text-center mt-5">
                        <button type="submit" class="btn-password-reset">
                            {{ __('Confirm Password') }}
                        </button>

                        @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
