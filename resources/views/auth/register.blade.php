@extends('layouts.app')
@section('title', 'Signup')
@section('content')
<div class="container-home">
    <div class="row">
        <div class="col-12 px-0">
            <div class="bg-white">
				@if(session()->has('message'))
					<div class="alert alert-success alert-block">
						<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>{{ session()->get('message') }}</strong>
					</div>
				@endif
				@if(session()->has('error'))
					<div class="alert alert-danger alert-block">
						<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>{{ session()->get('error') }}</strong>
					</div>
				@endif
                <div class="d-flex flex-wrap sign-up-container py-3 pl-3">
                    <div class="signup col-lg-4 d-none d-lg-block" style="background-image: url('../img/signup.png');">
                        <h3>{{ __('Get Started Absolutely Free.') }}</h3>
                        <ul>
                            <li>{{ __('International shipping.') }}</li>
                            <li>{{ __('Ecommerce platform integration.') }}</li>
                            <li>{{ __('Branding packaging') }}</li>
                        </ul>
                    </div>
                    <div class="sign-up-form col-lg-8">
                        <h2>{{ __('Sign Up') }}</h2>
                        <form action="{{route('register')}}" method="POST" id="form-register-user">
                            @csrf

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group mb-4">
                                        <label>{{ __('First Name') }}</label>
                                        <input type="text" value="{{ old('first_name') }}" name="first_name" class="custom-input form-control @error('first_name') is-invalid @enderror" required>
                                        @error('first_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{$message}}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-4">
                                        <label>Email Address</label>
                                        <input type="email" value="{{ old('email') }}" name="email" class="custom-input form-control @error('email') is-invalid @enderror">
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{$message}}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-4">
                                        <label>Password</label>
                                        <input type="password" name="password" class="custom-input form-control @error('password') is-invalid @enderror" id="password">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{$message}}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group mb-4">
                                        <label>Last Name</label>
                                        <input type="text" value="{{ old('last_name') }}" name="last_name" class="custom-input form-control @error('last_name') is-invalid @enderror">
                                        @error('last__name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{$message}}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-4">
                                        <label>Phone Number</label>
                                        <input type="tel" value="{{ old('phone_number') }}" name="phone_number" class="custom-input form-control" id="phone">
                                        @error('phone_no')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{$message}}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-4">
                                        <label>Confirm password</label>
                                        <input type="password" name="password_confirmation" class="custom-input form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex flex-wrap align-items-center mt-3">
                                <button class="btn-sign-up" type="submit">Sign up now</button>
                                <p class="color-light-gray mb-0 text-center text-md-left already-account">
                                    Already have an account? <a href="{{route('login')}}" class="color-sky font-weight-bold">Log in</a>
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('js/common/intel.telephone.js') }}"></script>
@endpush


@push('css')
<style>
	body.bg-white {
		background-color: #F9F9F9 !important;
	}
</style>
@endpush
