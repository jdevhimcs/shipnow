@extends('layouts.app')
@section('title', 'Login')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-12 screen-h">
			@if(session()->has('confirmation'))
			<div class="alert alert-success alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>{{ session()->get('confirmation') }}</strong>
			</div>
			@endif
			@if(session()->has('message'))
			<div class="alert alert-success alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>{{ session()->get('message') }}</strong>
			</div>
			@endif
			@if(session()->has('error'))
			<div class="alert alert-danger alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>{{ session()->get('error') }}</strong>
			</div>
			@endif
			<div class="login-section bg-white">
				<div class="login-top text-right">
					<img src="{{ asset('img/icon-top.png') }}">
				</div>
				<h2>Login</h2>
				<form method="POST" action="{{ route('login') }}" id="form-login-user">
					@csrf
					<div class="form-group">
						<label>Email address</label>
						<input type="email" name="email" class="custom-input custom-input-home form-control @error('email') is-invalid @enderror" placeholder="Type your email address here" value="{{ old('email') }}" autocomplete="email" autofocus>
						@error('email')
						<span class="invalid-feedback" role="alert">
							<strong>{{$message}}</strong>
						</span>
						@enderror
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="password" name="password" class="form-control custom-input-home custom-input @error('password') is-invalid @enderror" placeholder="Type password here" autocomplete="current-password">
						@error('password')
						<span class="invalid-feedback" role="alert">
							<strong>{{$message}}</strong>
						</span>
						@enderror
					</div>

					<div class="form-group text-right">
						<span class="d-block color-light-gray forget-password">
							Forgot your password? <a href="{{ route('password.request') }}" class="color-sky">Remind</a>
						</span>
					</div>
					<div class="form-group text-center mt-4">
						<button type="submit" class="btn brn-login">Log In</button>
					</div>
					<p class="mt-5 text-center  color-light-gray">Don’t have an account? <a href="{{route('register')}}" class="color-sky font-weight-bold">Sign up</a></p>
				</form>

			</div>
		</div>
	</div>
</div>


@endsection

@push('css')
<style>
	body.bg-white {
		background-color: #F9F9F9 !important;
	}
</style>
@endpush