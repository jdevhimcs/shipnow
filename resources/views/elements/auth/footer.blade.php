<footer class="footer footer-black  footer-white ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="credits text-center">
                    <span class="copyright">
                        © 2020 Shipnow LTD. All rights reserved. Terms & Conditions | Privacy Policy
                    </span>
                </div>
            </div>
        </div>
    </div>
</footer>