<div class="sidebar" data-color="white" data-active-color="danger">
	<div class="logo-image-small">
		<a href="{{ route('home') }}"><img src="{{ asset('img/logo-db.png') }}"></a>
	</div>
	<div class="sidebar-wrapper">
		<ul class="nav">
			<li class="{{ route('user.dashboard.index') == url()->current() ? 'active': null }}">
				<a href="{{ route('user.dashboard.index') }}">
					<p>Dashboard</p>
				</a>
			</li>

			@if (!Session::has('hasClonedUser'))
				<li class="{{ route('user.change-password.index') == url()->current() ? 'active': null }} 
				{{ route('user.account-settings.create') == url()->current() ? 'active': null }}
	        	{{ route('user.order-preferences.create') == url()->current() ? 'active': null }}
				{{ route('user.order-preferences.index') == url()->current() ? 'active': null }}
	        	{{ request()->is('user/order-preferences/*') ? 'active' : '' }}">
					<a href="{{ route('user.change-password.index')}}">
						<p>My Account</p>
					</a>
				</li>
			@endif

			<li>
				<a href="{{ route('user.letter-quote.create') }}">
					<p>My Quotes</p>
				</a>
			</li>
			<li class="{{ route('user.my.shipments') == url()->current() ? 'active': null }}">
				<a href="{{ route('user.my.shipments') }}">
					<p>My Shipments</p>
				</a>
			</li>
			<li>
				<a href="#">
					<p>Invoices</p>
				</a>
			</li>
			<li>
				<a href="{{ route('user.products.index') }}">
					<p>My Products</p>
				</a>
			</li>
			<li>
				<a href="#">
					<p>Help</p>
				</a>
			</li>
		</ul>
		<div class="logout-footer">
			@if (Session::has('hasClonedUser'))
                <a href="{{ route('agent.login-as-customer') }}" onclick="event.preventDefault(); document.getElementById('logout-consultant-form').submit();">
                        <img src="{{ asset('img/logout.png') }}">
                        <p>{{ __('Return back to agent') }}</p>
                </a>
                <form id="logout-consultant-form" action="{{ route('agent.login-as-customer') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            @else
				<a href="{{ route('logout') }}" class="" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
					<img src="{{ asset('img/logout.png') }}">
					<p>{{ __('Logout') }}</p>
				</a>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					@csrf
				</form>
			@endif
		</div>
	</div>
</div>