<div class="bg-white shadow-sm">
    <ul class="package-nav">
        <li class="{{ route('user.letter-quote.create') == url()->current() ? 'active-nav': null }}"><a href="{{ route('user.letter-quote.create') }}">Letter</a></li>
        <li class="{{ route('user.pak-quote.create') == url()->current() ? 'active-nav': null }}"><a href="{{ route('user.pak-quote.create') }}">PAK</a></li>
        <li class="{{ route('user.package-quote.create') == url()->current() ? 'active-nav': null }}"><a href="{{ route('user.package-quote.create') }}">Package</a></li>
        <li><a href="#">LTL</a></li>
        <li><a href="#">FTL</a></li>
    </ul>
</div>