<div class="center-navbar">
    <ul>
        <li class="{{ route('user.change-password.index') == url()->current() ? 'account-active': null }}">
            <a href="{{ route('user.change-password.index')}}">Change Password</a></li>
        <li><a href="#">Statement</a></li>
        <li><a href="#">Distributions</a></li>
        <li class="{{ route('user.address-book-list.index') == url()->current() ? 'account-active': null }}">
            <a href="{{ route('user.address-book-list.index') }}">Address Book</a></li>
        <li class="{{ route('user.account-settings.create') == url()->current() ? 'account-active': null }}">
            <a href="{{ route('user.account-settings.create') }}">My Account</a>
        </li>
        <li class="{{ route('user.order-preferences.index') == url()->current() ? 'account-active': null }}
        {{ route('user.order-preferences.create') == url()->current() ? 'account-active': null }}
        {{ request()->is('user/order-preferences/*') ? 'account-active' : '' }}
        ">
            <a href="{{ route('user.order-preferences.index') }}">Order Prefrences</a>
        </li>
    </ul>
</div>