<header class="header">
    <div class="container-home ">
        <nav class="navbar navbar-expand-lg navbar-white bg-white custom-nav">
            <a class="navbar-brand" href="{{ route('home') }}">
                <img src="{{ asset('img/logo.png') }}">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon">
                    <svg version="1.1" fill="#fff" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 384 384" style="enable-background:new 0 0 384 384;" xml:space="preserve">
                        <g>
                            <rect x="0" y="277.333" width="384" height="42.667" />
                            <rect x="0" y="170.667" width="384" height="42.667" />
                            <rect x="0" y="64" width="384" height="42.667" />
                        </g>
                    </svg>
                </span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('home') }}">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">About us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Demo</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Contact Us</a>
                    </li>

                    @auth
                        @if (auth()->user()->role_id == 3)
        					<li class="nav-item">
        						<a class="nav-link" href="{{ route('agent.dashboard.index') }}">My Account</a>
        					</li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="/user/change-password">My Account</a>
                            </li>
                        @endif
					@endauth
                </ul>
                @auth
                    @if (Session::has('hasClonedUser'))
                        <a href="{{ route('agent.login-as-customer') }}" class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-consultant-form').submit();">
                                <i class="fa fa-sign-out"></i>
                                {{ __('Return back to agent') }}
                        </a>
                        <form id="logout-consultant-form" action="{{ route('agent.login-as-customer') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @else
    					<a href="{{ route('logout') }}" class="nav-link"
    						onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
    							<i class="fa fa-sign-out"></i>
    							{{ __('Logout') }}
    					</a>
    					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    						@csrf
    					</form>
                    @endif
				@else
				    <button class="btn ml-3 btn-sign-in"><a href="{{route('login')}}" class="text-white">Sign in</a></button>
				@endauth
            </div>
        </nav>
    </div>
</header>
