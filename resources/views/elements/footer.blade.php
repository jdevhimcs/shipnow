<!-- footer -->
<footer class="footer">
    <div class="footer-top">
        <div class="container-home ">
            <div class="row">
                <div class="col-md-6">
                    <div class="d-flex mb-3 justify-content-md-start justify-content-center">
                        <div class="img-icon">
                            <img src="{{ asset('img/email.png')}}">
                        </div>
                        <div class="icon-text text-white ml-3">
                            Info@shipnow.com
                        </div>
                    </div>
                    <div class="d-flex justify-content-md-start justify-content-center mb-4">
                        <div class="img-icon">
                            <img src="{{ asset('img/call.png') }}">
                        </div>
                        <div class="icon-text text-white ml-3">
                            +1 123 456 7890
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="footer-link text-md-left text-center mb-md-0 mb-4">
                        <ul class="m-0 p-0">
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Services</a></li>
                            <li><a href="#">Demo</a></li>
                            <li><a href="#">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="footer-link text-md-left text-center mb-md-0 mb-4">
                        <ul class="m-0 p-0">
                            <li><a href="#">Retail & consumer</a></li>
                            <li><a href="#">Amazone</a></li>
                            <li><a href="#">fedex </a></li>
                            <li><a href="#">Delievery</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="footer-link text-md-left text-center mb-md-0 mb-4">
                        <ul class="m-0 p-0">
                            <li><a href="#">Warehousing</a></li>
                            <li><a href="#"> Air Freinght</a></li>
                            <li><a href="#">Ocean </a></li>
                            <li><a href="#">freinght</a></li>
                            <li><a href="#">Road freight</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-8 text-md-left text-center">
                    <p>
                        © 2020 Shipnow LTD. All rights reserved. Terms & Conditions | Privacy Policy
                    </p>
                </div>
                <div class="col-md-4">
                    <div class="footer-media-link">
                        <ul class="m-0 p-0 text-md-right text-center">
                            <li>
                                <a href="#">
                                    <img src="{{ asset('img/facebook.png') }}">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="{{ asset('img/twitter.png') }}">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="{{ asset('img/linkedin.png') }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>