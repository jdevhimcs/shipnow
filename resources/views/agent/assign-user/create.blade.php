@extends('layouts.agent')
@section('title', 'Users')
@section('content')
    <section>
        <div class="container-fluid">
            <!-- Page Header-->
            <header> 
                <div class="row">
                    <div class="col-md-8">
                    </div>
                    <div class="col-md-4 text-right">
                        <a href="{{ route('agent.assigned-user.index') }}" class="btn text-primary">
                            {{ __('Back') }}
                        </a>
                    </div>
                </div>
                <!-- alert message component -->
                <x-alert/>  
            </header>

            <div class="row">
                <div class="col-md-4 mx-auto">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{ route('agent.store-agent-assign-user') }}" id="AssignUserForm">
                                @csrf

                                <div class="form-group">
                                    <select id="user-multiselect" multiple="multiple" class="form-control" name="users[]">
                                        @foreach($customers as $key => $customer)
                                            <option value="{{ $customer->id }}" {{ in_array($customer->id, $assignUsers) ? 'selected': '' }}>{{ $customer->profile->full_name ?? $customer->email }}</option>
                                        @endforeach
                                    </select>

                                    @error('users')
                                        <label class="">{{ $message }}</label>
                                    @enderror
                                </div>

                                <div class="form-group text-right">
                                    <a href="{{ route('agent.assigned-user.index') }}" class="btn btn-default">
                                        {{ __('Cancel') }} 
                                    </a>

                                    <button type="submit" class="btn btn-primary1">
                                        {{ __('Submit') }}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-multiselect.min.css') }}"></link>
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/admin/assign-user/main.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-multiselect.min.js') }}"></script>
@endpush