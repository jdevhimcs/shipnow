@extends('layouts.agent')
@section('title', 'Assigned Users')
@section('content')
  	<section>
        <div class="container-fluid">
          	<!-- Page Header-->
          	<header> 
          		<div class="row">
          			<div class="col-md-8">
            			<h2 class="h3 display">{{__(strtoupper('Users'))}}</h2>
          			</div>
          			<div class="col-md-4 text-right">
                        <a href="{{ route('agent.assigned-user.create') }}" class="btn text-primary">
                            {{ __('Assign New Customer') }}
                        </a>
                    </div>
          		</div>
          		<!-- alert message component -->
            	<x-alert/>	
          	</header>
			  <div class="row">
            	<div class="col">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone Number</th>
                                <th>Actions</th>
                            </tr>
                            @foreach ($customers as $customer)
                                <tr>
                                    <td>{{$loop->index+1}}</td>
                                    <td>{{$customer->customer->profile->full_name ?? ''}}</td> 
                                    <td>{{$customer->customer->email}}</td>
                                    <td>{{$customer->customer->profile->phone_no ?? ''}}</td>
                                    <td>
                                        <form action='{{ route('agent.login-as-customer') }}' method='post'>
                                            {{ csrf_field() }}
                                            <input type="hidden" name="user_id" value="{{ $customer->id }}">
                                            <button type="submit" class="btn-primary btn">
                                                {{ __('Login To Customer Account') }} 
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
          	</div>
          	
        </div>
  	</section>
@endsection