<nav class="side-navbar">
    <div class="side-navbar-wrapper">
        <div class="sidenav-header d-flex align-item-center justify-content-center">
            <div class="sidenav-header-inner text-center">
                <h2 class="h5">Welcome</h2><span>{{!empty(Auth::user()->profile->full_name) ? Auth::user()->profile->full_name : 'Agent'}}</span>
            </div>
            <div class="sidenav-header-logo">
                <a href="" class="brand-small text-center">
                   {{!empty(Auth::user()->profile->full_name) ? Auth::user()->profile->full_name : 'Agent'}}
                </a>
            </div>
        </div>

        <div class="main-menu">
            <ul id="side-main-menu" class="side-menu list-unstyled">
                <li class="{{route('agent.dashboard.index') == url()->current() ? 'active': null}}">
                    <a href="{{route('agent.dashboard.index')}}">
                        <i class="fa fa-dashboard"></i>{{__('Dashboard')}}
                    </a>
                </li>
                <li class="{{route('agent.assigned-user.index') == url()->current() ? 'active': null}}">
                    <a href="{{route('agent.assigned-user.index')}}">
                        <i class="fa fa-users"></i>{{__('Assigned Customer')}}
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>