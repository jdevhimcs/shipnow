@extends('layouts.dashboard')
@section('title', 'Change Password')
@section('content')
<div class="content">
	<div class="content-container">
		<div class="container">
			<h3 align="center">Import Excel File</h3>
			<br />
			@if(count($errors) > 0)
			<div class="alert alert-danger">
				Upload Validation Error<br><br>
				<ul>
					@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			@if($message = Session::get('success'))
			<div class="alert alert-success alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>{{ $message }}</strong>
			</div>
			@endif
			<form method="post" enctype="multipart/form-data" action="{{ route('import.excel.store') }}">
				{{ csrf_field() }}
				<div class="form-group">
					<table class="table">
						<tr>
							<td width="40%" align="right"><label>Select File for Upload</label></td>
							<td width="30">
								<input type="file" name="select_file" >
							</td>

						</tr>
						<tr>
							<td width="40%" align="right"></td>
							<td width="30"><span class="text-muted">.xls, .xlsx</span></td>
							<td width="30%" align="left"></td>
						</tr>
					</table>

				</div>
				<input type="submit" name="upload" class="btn btn-primary" value="Upload">
			</form>
			<br />
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-bordered table-striped">
							<tr>
								<th>ZipCode</th>
								<th>City</th>
							</tr>
							@foreach($data as $row)
							<tr>
								<td>{{ $row->zip }}</td>
								<td>{{ $row->city }}</td>
							</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection