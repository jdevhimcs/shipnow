@extends('layouts.admin')
@section('content')
<section>
	<div class="container-fluid">
		<!-- Page Header-->
		<header>
			<div class="row">
				<div class="col-6">

					<h2 class="h3 display">{{ __(strtoupper('Change Password')) }}</h2>
				</div>
			</div>
			<x-alert />
		</header>
		<div class="row">
			<div class="col-md-4 mx-auto">
				<div class="card">
					<div class="card-body">
						<form method="POST" action="{{ route('admin.change-password.store') }}" id="ChangePasswordForm">
							@csrf
							<div class="form-group">
								<label for="Password">{{ __('Current Password') }}</label>
								<input type="password" name="current_password" class="form-control" id="Password" required>
								@error('current_password')
								<label class="">{{ $message }}</label>
								@enderror
							</div>
							<div class="form-group">
								<label for="NewPassword">{{ __('New Password') }}</label>
								<input type="password" name="new_password" class="form-control" id="NewPassword" required>
							</div>
							<div class="form-group">
								<label for="ConfirmPassword">{{ __('Confirm New Password') }}</label>
								<input type="password" name="new_confirm_password" class="form-control" id="ConfirmPassword" required>
							</div>
							<div class="form-group text-right">
								<a href="{{route('admin.dashboard.index')}}" class="btn btn-default">Cancel</a>
								<button type="submit" class="btn btn-primary1">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('css/common/jquery-ui.css') }}">
</link>
@endpush
@push('scripts')
<script type="text/javascript" src="{{asset('js/common/jquery-ui.js')}}"></script>
<script src="{{ asset('js/common/jquery-validation/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/change-password.js') }}"></script>


@endpush