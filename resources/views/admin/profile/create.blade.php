@extends('layouts.admin')
@section('content')
    <section>
        <?php $profile = $user->profile; ?>
        <div class="container-fluid">
            <!--Page Header-->
            <header> 
                <div class="row">
                    <div class="col-6">
                        <h2>Profile Settings</h2>
                    </div>
                </div>  
                <x-alert/>	            
            </header>
            <div class="row">
                <div class="col-md-10 mx-auto">
                    <div class="card">
                        <div class="card-header">Account Information Details</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4 text-center">
                                        <img src="{{ ($profile)?$profile->thumb_image:asset('images/profile.png') }}" alt="user" height="150" width="150" class='rounded-circle' data-id='profile-image'>
                                    <div class="mt-3">
                                        <button class="btn btn-default" id="profile">
                                            Change Profile
                                        </button>
                                    </div>
                                    <div class="d-none">
                                        <label for="file-input" class="file-icon">
                                            <img src="{{asset('images/app/upload.png')}}" alt="upload" >
                                        </label>
                                        <input id="file-input" form="editProfile" type="file" name="photo" accept='image/*' />
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    
                            <form method="POST" action="{{ route('admin.profile.store') }}" id="editProfile" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="FirstName">{{ __('First Name') }}</label>
                                    <input name="user_profile[first_name]" class="form-control" id="FirstName" value="{{ old('first_name', $user->profile->first_name) }}" required> 
                                    @error('first_name')
                                        <label class="">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="LastName">{{ __('Last Name') }}</label>
                                    <input name="user_profile[last_name]" class="form-control" id="LastName" value="{{ old('last_name', $user->profile->last_name) }}" required> 
                                    @error('last_name')
                                        <label class="">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="Email">{{ __('Email') }}</label>
                                    <input name="email" class="form-control" id="Email" value="{{ old('email', $user->email) }}" required> 
                                    @error('email')
                                        <label class="">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group">
                                   
                                    <label for="Phone">{{ __('Phone') }}</label>
                                    <input name="user_profile[phone_no]" class="form-control" id="Phone" value="{{ old('phone_no',$user->profile->phone_no) }}" required> 
                                    @error('phone_no]')
                                        <label class="">{{ $message }}</label>
                                    @enderror
                                </div>

                                <div class="form-group text-right">
                                    <a href="{{route('admin.dashboard.index')}}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </form>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script src="{{ asset('js/common/jquery-validation/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/admin/profile/create.js') }}"></script>
@endpush