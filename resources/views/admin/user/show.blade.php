@extends('layouts.admin')

@section('content')
    <section>
        <div class="container-fluid">
            <!-- Page Header-->
            <header> 
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="">{{ __('View Details') }}</h1>
                    </div>
                    <div class="col-md-6 text-right">
                        <a href="{{route('admin.users.index')}}" class="btn btn-blue">
                            Back
                        </a>
                    </div>
                </div>
                @if (session('status'))
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <x-alert/>	
            </header>
            <div class="row">
			<!-- /.col-->
			<div class="col-lg-8">
				<div class="card">
					<div class="card-header">Account Information</div>
					<div class="card-body">						
						<div class="bd-example">
							<dl class="row">
								<dt class="col-sm-2">Name</dt>
								<dd class="col-sm-10">
									{{ 
                                        ($user->profile)
                                        ?
                                            $user->profile->full_name
                                        :"N/A"
                                    }}									
								</dd>															
                            </dl>
                            <dl class="row">
								<dt class="col-sm-2">Email</dt>
								<dd class="col-sm-10">
									{{  ($user->email) }}									
								</dd>															
                            </dl>
                            <dl class="row">
								<dt class="col-sm-2">Phone Number</dt>
								<dd class="col-sm-10">
									{{  ($user->profile->phone_no) }}									
								</dd>															
                            </dl>
						</div>												
                    </div>
                    <div class="card-header">Company Information</div>
					<div class="card-body">						
						<div class="bd-example">
							<dl class="row">
								<dt class="col-sm-2">Company Name</dt>
								<dd class="col-sm-10">
									{{ 
                                        ($user->companyDetail)
                                        ?
                                            $user->companyDetail->company_name
                                        :"N/A"
                                    }}									
								</dd>															
                            </dl>
                            <dl class="row">
								<dt class="col-sm-2">Address</dt>
								<dd class="col-sm-10">
									{{ 
                                        ($user->companyDetail)
                                        ?
                                            $user->companyDetail->address
                                        :"N/A"
                                    }}								
								</dd>															
                            </dl>
                            <dl class="row">
								<dt class="col-sm-2">Phone Number</dt>
								<dd class="col-sm-10">
									{{ 
                                        ($user->companyDetail)
                                        ?
                                            $user->companyDetail->phone_no
                                        :"N/A"
                                    }}										
								</dd>															
                            </dl>
                            <dl class="row">
								<dt class="col-sm-2">Email</dt>
								<dd class="col-sm-10">
									{{ 
                                        ($user->companyDetail)
                                        ?
                                            $user->companyDetail->email
                                        :"N/A"
                                    }}										
								</dd>															
                            </dl>
                            <dl class="row">
								<dt class="col-sm-2">Country</dt>
								<dd class="col-sm-10">
									{{ 
                                        ($user->companyDetail)
                                        ?
                                            $user->companyDetail->country->name
                                        :"N/A"
                                    }}										
								</dd>															
                            </dl>
                            <dl class="row">
								<dt class="col-sm-2">State</dt>
								<dd class="col-sm-10">
									{{ 
                                        ($user->companyDetail)
                                        ?
                                            $user->companyDetail->state->name
                                        :"N/A"
                                    }}										
								</dd>															
                            </dl>
                            <dl class="row">
								<dt class="col-sm-2">City</dt>
								<dd class="col-sm-10">
									{{ 
                                        ($user->companyDetail)
                                        ?
                                            $user->companyDetail->city
                                        :"N/A"
                                    }}										
								</dd>															
                            </dl>
                            <dl class="row">
								<dt class="col-sm-2">Postal Code</dt>
								<dd class="col-sm-10">
									{{ 
                                        ($user->companyDetail)
                                        ?
                                            $user->companyDetail->postal_code
                                        :"N/A"
                                    }}										
								</dd>															
                            </dl>
						</div>												
                    </div>
                    
                    <div class="card-header">Billing Information</div>
					<div class="card-body">						
						<div class="bd-example">							
                            <dl class="row">
								<dt class="col-sm-2">Address</dt>
								<dd class="col-sm-10">
									{{ 
                                        ($user->billingAddress)
                                        ?
                                            $user->billingAddress->address
                                        :"N/A"
                                    }}								
								</dd>															
                            </dl>
                            <dl class="row">
								<dt class="col-sm-2">Phone Number</dt>
								<dd class="col-sm-10">
									{{ 
                                        ($user->billingAddress)
                                        ?
                                            $user->billingAddress->phone_no
                                        :"N/A"
                                    }}										
								</dd>															
                            </dl>
                            <dl class="row">
								<dt class="col-sm-2">Email</dt>
								<dd class="col-sm-10">
									{{ 
                                        ($user->billingAddress)
                                        ?
                                            $user->billingAddress->email
                                        :"N/A"
                                    }}										
								</dd>															
                            </dl>
                            <dl class="row">
								<dt class="col-sm-2">Country</dt>
								<dd class="col-sm-10">
									{{ 
                                        ($user->billingAddress)
                                        ?
                                            $user->billingAddress->country
                                        :"N/A"
                                    }}										
								</dd>															
                            </dl>
                            <dl class="row">
								<dt class="col-sm-2">State</dt>
								<dd class="col-sm-10">
									{{ 
                                        ($user->billingAddress)
                                        ?
                                            $user->billingAddress->state
                                        :"N/A"
                                    }}										
								</dd>															
                            </dl>
                            <dl class="row">
								<dt class="col-sm-2">City</dt>
								<dd class="col-sm-10">
									{{ 
                                        ($user->billingAddress)
                                        ?
                                            $user->billingAddress->city
                                        :"N/A"
                                    }}										
								</dd>															
                            </dl>
                            <dl class="row">
								<dt class="col-sm-2">Postal Code</dt>
								<dd class="col-sm-10">
									{{ 
                                        ($user->billingAddress)
                                        ?
                                            $user->billingAddress->postal_code
                                        :"N/A"
                                    }}										
								</dd>															
                            </dl>
						</div>												
					</div>
				</div>
			</div>
			<!-- /.col-->
		</div>
        </div>
    </section>
@endsection




