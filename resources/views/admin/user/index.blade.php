@extends('layouts.admin')
@section('title', 'Users')
@section('content')
  	<section>
        <div class="container-fluid">
          	<!-- Page Header-->
          	<header> 
          		<div class="row">
          			<div class="col-md-8">
            			<h2 class="h3 display">{{__(strtoupper('Users'))}}</h2>
          			</div>
          			<div class="col-md-4 text-right">
          			</div>
          		</div>
          		<!-- alert message component -->
            	<x-alert/>	
          	</header>
			  <div class="row">
            	<div class="col">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone Number</th>
                                <th>Action</th>
                            </tr>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{$loop->index+1}}</td>
                                    <td>{{$user->profile->full_name ?? ''}}</td> 
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->profile->phone_no ?? ''}}</td>
                                    <td class="d-flex">
                                        @if(!$user->deleted_at)
                                            <form action="{{ route('admin.users.destroy',$user->id)}}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }} 
                                                <a href="#">
                                                    <i class="fa fa-trash" data-message="deactivate">
                                                        Deactivate
                                                    </i>
                                                </a>
                                            </form>
                                        @else
                                            <form action="{{ route('admin.users.destroy',$user->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }} 
                                                <a href="#">
                                                    <i class="fa fa-undo text-danger" data-message="restore">
                                                        Restore
                                                    </i>
                                                </a>
                                            </form>
                                        @endif
                                        <a class="ml-3" href="{{ route('admin.users.show', [ 'user'=> $user->id ]) }}" >
                                            <i class="fa fa-eye"></i> View
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
          	</div>
          	
        </div>
  	</section>
@endsection

@push('scripts')
	<script type="text/javascript">
		$('.fa-trash, .fa-undo').on('click', function(e){
			let msg = $(this).attr('data-message');
			let form = $(this).closest('form');
			if(confirm(`Are you sure want to ${msg} the user`)) {
				form.submit();
			}
		});
	</script>	
@endpush;