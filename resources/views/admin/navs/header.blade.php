<header class="header">
    <nav class="navbar">
        <div class="d-flex w-100 pl-2 pr-4">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
                <div class="navbar-header">
                    <a id="toggle-btn" href="#" class="menu-btn">
                        <i class="fa fa-bars"> </i>
                    </a>
                    <a href="{{route('admin.users.index')}}" class="navbar-brand">
                        <h3> {{ __('Ship Now Admin Panel') }}</h3>
                    </a>
                </div>
                <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                    <!-- Log out-->
                    <li class="nav-item">
                        
                    </li>
                </ul>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{!empty(Auth::user()->profile->full_name) ? Auth::user()->profile->full_name : 'Admin'}}
                    </button>

                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="{{route('admin.profile.create')}}">
                            <i class="fa fa-user"></i>
                            {{ __('My Profile') }}
                        </a>
                        <a href="{{ route('logout') }}" class="dropdown-item" 
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i>
                                {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</header>
