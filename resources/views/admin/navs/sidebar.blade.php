<nav class="side-navbar">
    <div class="side-navbar-wrapper">
        <div class="sidenav-header d-flex align-item-center justify-content-center">
            <div class="sidenav-header-inner text-center">
                {{-- @if(auth()->user() && auth()->user()->profile)
                    <img src="#" alt="profile_image" class="img-fluid rounded-circle">
                @endif --}}
                <h2 class="h5">Welcome</h2><span>{{!empty(Auth::user()->profile->full_name) ? Auth::user()->profile->full_name : 'Admin'}}</span>
            </div>
            <div class="sidenav-header-logo">
                <a href="" class="brand-small text-center">
                   {{!empty(Auth::user()->profile->full_name) ? Auth::user()->profile->full_name : 'Admin'}}
                </a>
            </div>
        </div>

        <div class="main-menu">
            <ul id="side-main-menu" class="side-menu list-unstyled">
                <li class="{{route('admin.dashboard.index') == url()->current() ? 'active': null}}">
                    <a href="{{route('admin.dashboard.index')}}">
                        <i class="fa fa-dashboard"></i>{{__('Dashboard')}}
                    </a>
                </li>
                <li class="{{route('admin.users.index') == url()->current() ? 'active': null}}">
                    <a href="{{route('admin.users.index')}}">
                        <i class="fa fa-users"></i>{{__('Users')}}
                    </a>
                </li>
                <li class="{{ route('admin.agents.index') == url()->current() ? 'active': null }}">
                    <a href="{{ route('admin.agents.index') }}">
                        <i class="fa fa-users"></i>{{__('Agents')}}
                    </a>
                </li>
                <li class="{{route('admin.change-password.index') == url()->current() ? 'active': null}}">
                    <a href="{{route('admin.change-password.index')}}">
                        <i class="fa fa-lock"></i>{{__('Change Password')}}
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>