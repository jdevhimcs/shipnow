@extends('layouts.admin')
@section('title', 'Dashboard')
@section('content')
  	<section>
        <div class="container-fluid">
          	<!-- Page Header-->
          	<header> 
          		<div class="row">
          			<div class="col-md-8">
            			<h2 class="h3 display">{{__(strtoupper('Dashboard'))}}</h2>
          			</div>
          			<div class="col-md-4 text-right">
          			</div>
          		</div>
          		<!-- alert message component -->
            	<x-alert/>	
          	</header>
			  <div class="row">
            	<div class="col">
              		<div class="card">
		                <div class="card-body">
                            <div class="card-body bg-primary text-white">
                                <h2>{{$userCount}}</h2>
                                <h4>Total Registered User</h4>
                            </div>
		                </div>
	              	</div>
                </div>
                <div class="col">
                        <!-- <div class="card">
                          <div class="card-body">
                              <div class="card-body bg-success text-white">
                                  <h2>{{$adminCount}}</h2>
                                  <h4>Total Registered Admin</h4>
                              </div>
                          </div>
                        </div> -->
                  </div>
          	</div>
          	
        </div>
  	</section>
@endsection
