@extends('layouts.admin')
@section('title', 'Users')
@section('content')
    <section>
        <div class="container-fluid">
            <!-- Page Header-->
            <header> 
                <div class="row">
                    <div class="col-md-8">
                        <h2 class="h3 display">{{ __('AGENTS') }}</h2>
                    </div>
                    <div class="col-md-4 text-right">
                        <a href="{{ route('admin.agents.index') }}" class="btn text-primary">
                            {{ __('Back') }}
                        </a>
                    </div>
                </div>
                <!-- alert message component -->
                <x-alert/>  
            </header>

            <div class="row">
                <div class="col-md-4 mx-auto">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{ route('admin.agents.store') }}" id="AgentForm">
                                @csrf

                                <div class="form-group">
                                    <label for="first_name">{{ __('First Name') }}</label>
                                    <input type="text" name="first_name" class="form-control" id="first_name" required value="{{ old('first_name') }}">

                                    @error('first_name')
                                        <label class="">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="last_name">{{ __('Last Name') }}</label>
                                    <input type="text" name="last_name" class="form-control" id="last_name" required value="{{ old('last_name') }}">

                                    @error('last_name')
                                        <label class="">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="email">{{ __('Email') }}</label>
                                    <input type="email" name="email" class="form-control" id="email" required value="{{ old('email') }}">

                                    @error('email')
                                        <label class="">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="password">{{ __('Password') }}</label>
                                    <input type="password" name="password" class="form-control" id="password" required>
                                </div>
                                <div class="form-group">
                                    <label for="ConfirmPassword">{{ __('Confirm Password') }}</label>
                                    <input type="password" name="confirm_password" class="form-control" id="ConfirmPassword" required>
                                </div>
                                <div class="form-group text-right">
                                    <a href="{{ route('admin.agents.index') }}" class="btn btn-default">
                                        {{ __('Cancel') }} 
                                    </a>

                                    <button type="submit" class="btn btn-primary1">
                                        {{ __('Submit') }}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/admin/agent/create.js') }}"></script>
@endpush