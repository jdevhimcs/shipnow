@component('mail::message')
# Request A Quote

A new quote requested by user. Below are the details:<br>
<p>Company Name: {{ $data['company_name'] }}</p>
<p>Name: {{ $data['name'] }}</p>
<p>Email: {{ $data['email'] }}</p>
<p>Phone Number: {{ $data['phone_number'] }}</p>
<p>Message: {!! nl2br(e($data['comment'])) !!}</p>
<p>What do you want to Ship?</p>
<p> @foreach($data['ship'] as $ship) {{ ucfirst($ship) }}<br> @endforeach</p>


Thanks,<br>
{{ config('app.name') }}
@endcomponent
