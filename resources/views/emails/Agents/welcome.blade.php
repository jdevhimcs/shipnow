@component('mail::message')
# Welcome to ShipNow

The admin has added you as an agent. You can logged in using below details:<br>
<p>Email: {{ $data['email'] }}</p>
<p>Password: {{ $data['password'] }}</p>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
