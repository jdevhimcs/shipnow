@extends('layouts.app')

@section('title', 'signup')

@section('content')

<!-- header -->
<header class="header">
    <div class="container">
        <nav class="navbar navbar-expand-md navbar-white bg-white custom-nav">
            <a class="navbar-brand" href="#">
                <img src="{{ asset('img/logo.png') }}">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">About us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Demo</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Contact Us</a>
                    </li>
                </ul>
                <button class="btn ml-3 btn-sign-in">Sign in</button>
            </div>
        </nav>
    </div>
</header>


<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="bg-white">
                <div class="d-flex sign-up-container py-3 pl-3">
                    <div class="signup col-4 position-relative" style="background-image: url('../img/signup.png');">
                        <h3>Get Started Absolutely Free.</h3>
                        <ul>
                            <li>International shipping.</li>
                            <li>Ecommerce platform integration.</li>
                            <li>Branding packaging</li>
                        </ul>
                        <div class="signup-footer">
                            <ul class="m-0 p-0"> 
                                <li>
                                    <a href="#">
                                        <img src="{{ asset('img/round-facebook.png')}}">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{ asset('img/round-twiter.png')}}">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{ asset('img/round-link.png')}}">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="sign-up-form col-8">
                        <h2>Sign Up</h2>
                        <form action="#">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="text" name="first-name" class="custom-input form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Email Address</label>
                                        <input type="text" name="email" class="custom-input form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="text" name="password" class="custom-input form-control">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" name="first-name" class="custom-input form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Phone Number</label>
                                        <input type="text" name="email" class="custom-input form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Confirm password</label>
                                        <input type="text" name="password" class="custom-input form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex align-items-center">
                                <button class="btn-sign-up rounded">Sign up now</button>
                                <p class="color-light-gray mb-0">
                                    Already have an account? <a href="#" class="color-sky font-weight-bold">Log in</a>
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<footer class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <div class="d-flex mb-3">
                        <div class="img-icon">
                            <img src="{{ asset('img/email.png')}}">
                        </div>
                        <div class="icon-text text-white ml-3">
                            Info@shipnow.com
                        </div>
                    </div>
                    <div class="d-flex ">
                        <div class="img-icon">
                            <img src="{{ asset('img/call.png') }}">
                        </div>
                        <div class="icon-text text-white ml-3">
                            +1 123 456 7890
                        </div>
                    </div>
                </div>
                <div class="col-2">
                    <div class="footer-link">
                        <ul class="m-0 p-0">
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Services</a></li>
                            <li><a href="#">Demo</a></li>
                            <li><a href="#">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-2">
                    <div class="footer-link">
                        <ul class="m-0 p-0">
                            <li><a href="#">Retail & consumer</a></li>
                            <li><a href="#">Amazone</a></li>
                            <li><a href="#">fedex </a></li>
                            <li><a href="#">Delievery</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-2">
                    <div class="footer-link">
                        <ul class="m-0 p-0">
                            <li><a href="#">Warehousing</a></li>
                            <li><a href="#"> Air Freinght</a></li>
                            <li><a href="#">Ocean </a></li>
                            <li><a href="#">freinght</a></li>
                            <li><a href="#">Road freight</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <p>
                        © 2020 Shipnow LTD. All rights reserved. Terms & Conditions | Privacy Policy
                    </p>
                </div>
                <div class="col-4">
                    <div class="footer-media-link">
                        <ul class="m-0 p-0 text-right">
                            <li>
                                <a href="#">
                                    <img src="{{ asset('img/facebook.png') }}">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="{{ asset('img/twitter.png') }}">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="{{ asset('img/linkedin.png') }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Understood</button>
      </div>
    </div>
  </div>
</div>  
@endsection