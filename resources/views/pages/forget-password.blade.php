@extends('layouts.dashboard')
@section('content')

<div class="wrapper ">
    <div class="sidebar" data-color="white" data-active-color="danger">
        <div class="logo-image-small">
            <img src="{{ asset('img/logo-db.png') }}">
        </div>
        <div class="sidebar-wrapper">
            <ul class="nav">
                <li class="active ">
                    <a href="./dashboard.html">
                        <img src="{{ asset('img/dashboard.png') }}">
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="./icons.html">
                        <img src="{{ asset('img/my-account.png') }}">
                        <p>My Account</p>
                    </a>
                </li>
                <li>
                    <a href="./map.html">
                        <img src="{{ asset('img/qoute.png') }}">
                        <p>My Quotes</p>
                    </a>
                </li>
                <li>
                    <a href="./notifications.html">
                        <img src="{{ asset('img/invoice.png') }}">
                        <p>Invoices</p>
                    </a>
                </li>
                <li>
                    <a href="./user.html">
                        <img src="{{ asset('img/my-product.png') }}">
                        <p>My Products</p>
                    </a>
                </li>
                <li>
                    <a href="./tables.html">
                        <img src="{{ asset('img/help.png') }}">
                        <p>Help</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-absolute nav-top fixed-top navbar-transparent">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <div class="navbar-toggle">
                        <button type="button" class="navbar-toggler">
                            <span class="navbar-toggler-bar bar1"></span>
                            <span class="navbar-toggler-bar bar2"></span>
                            <span class="navbar-toggler-bar bar3"></span>
                        </button>
                    </div>
                    <a class="navbar-brand top-title" href="javascript:;">My Account</a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navigation">
                    <ul class="navbar-nav">
                        <li class="nav-item btn-rotate dropdown">
                            <img class="bell" src="{{ asset('img/bell.png') }}">
                        </li>
                        <li class="nav-item">
                            <img class="user-img" src="{{ asset('img/user-img.png') }}">
                            Adam smith
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->

        <div class="content">
            <div class="center-navbar">
                <ul>
                    <li><a href="#">Change Password</a></li>
                    <li><a href="#">Statement</a></li>
                    <li><a href="#">Distributions</a></li>
                    <li><a href="#">Address Book</a></li>
                    <li><a href="#">Account</a></li>
                    <li><a href="#">Account Settings</a></li>
                </ul>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="change-password">
                        <form action="#">
                            <div class="form-group">
                                <label>Old Password</label>
                                <input type="text" name="old-password" class="form-control custom-input">
                            </div>
                            <div class="form-group">
                                <label>Create New Password</label>
                                <input type="text" name="new-password" class="form-control custom-input">
                            </div>
                            <div class="form-group">
                                <label>Confirm Password</label>
                                <input type="text" name="confirm-password" class="form-control custom-input">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn-change-password mt-4">Change Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer footer-black  footer-white ">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="credits text-center">
                            <span class="copyright">
                                © 2020 Shipnow LTD. All rights reserved. Terms & Conditions | Privacy Policy
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
@endsection