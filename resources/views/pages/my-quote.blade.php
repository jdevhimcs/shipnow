@extends('layouts.dashboard')
@section('title', 'Account Settings')
@section('content')
<div class="content">
    <div class="row mb-4">
        <div class="col-7">
            <div class="quote-number">
                <span>Dynamic LTL Quote</span>
                <h2>LMPV803310</h2>
            </div>
        </div>
        <div class="col-5">
            <div class="add-btn text-right">
                <button class="add-btn bg-dark-blue text-white border-0 py-2 px-4 font-weight-bold rounded mr-3">Request Pickup</button>
                <button class="add-btn bg-dark-blue text-white border-0 py-2 px-4 font-weight-bold rounded">Edit Quote</button>
            </div>
        </div>
    </div>
    <div class="quote-form">
        <div class="row">
            <div class="col-10">
                <div class="bg-white p-4">
                    <table class="table table-dimensions mb-5">
                        <tbody>
                            <tr>
                                <td>
                                    Origin:
                                    <strong class="d-block">
                                        LA JUNTA, CO 81050 Terminal: 110 <br />
                                        Direct
                                    </strong>
                                </td>
                                <td>
                                    Origin:
                                    <strong class="d-block">
                                        EDMONTON, AB T5A0A1<br />
                                        Terminal: 265 Direct
                                    </strong>
                                </td>
                                <td>
                                    Dimensions:
                                    <strong class="d-block">1 x Pallet - 83 x 13 x 48 Inches</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Payment Terms:
                                    <strong class="d-block">Prepaid</strong>
                                </td>
                                <td>
                                    PHHG Miles:
                                    <strong class="d-block">1441</strong>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-dimensions-data">
                        <thead>
                            <tr>
                                <th>Descriptions</th>
                                <th>Code / Reason</th>
                                <th>Rate</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>/ CROSS-BORDER ADMINISTRATIVE FEE </td>
                                <td>CUAF</td>
                                <td>-</td>
                                <td>Included</td>
                            </tr>
                            <tr>
                                <td>/ CONSIGNEE UNLOAD </td>
                                <td>CUAF</td>
                                <td>-</td>
                                <td>Included</td>
                            </tr>
                            <tr>
                                <td>/ SINGLE SHIPMENT </td>
                                <td>CUAF</td>
                                <td>-</td>
                                <td>Included</td>
                            </tr>
                            <tr>
                                <td>/ FUEL SURCHARGE </td>
                                <td>CUAF</td>
                                <td>-</td>
                                <td>Included</td>
                            </tr>
                            <tr>
                                <td>// GOODS & SERVICES TAX </td>
                                <td>CUAF</td>
                                <td>-</td>
                                <td>Included</td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Shipment Totals </strong>
                                </td>
                                <td>
                                    <strong>Weight: 200 lbs. </strong>
                                </td>
                                <td>
                                    <strong>Cube: 29.97 Cubic Feet </strong>
                                </td>
                                <td>
                                    <strong>$260.93</strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-2">
                <div class="cost bg-white shadow-sm p-4">
                    <div class="total-cost border-bottom mb-3 pb-3">
                        <span>Total Cost:</span>
                        <h2>$260.93</h2>
                        <a href="#">View Quote Details</a>
                    </div>
                    <div class="pikup-date border-bottom mb-3 pb-3">
                        <span>Pick Up Date:</span>
                        <h2>Nov 30, 2020</h2>
                    </div>
                    <div class="project-delivery-date ">
                        <span>Project Delivery Date:</span>
                        <h2>Dec 04, 2020</h2>
                        <span class="text-color-red mt-2">Transit Time 4 Days</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection