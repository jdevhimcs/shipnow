@extends('layouts.dashboard')
@section('title', 'Account Settings')
@section('content')
<div class="content">
    <div class="row mb-4">
        <div class="col-7">
            <div class="search-form ">
                <form action="#" class="d-flex">
                    <div class="search-outer">
                        <img class="user-img" src="{{ asset('img/search-icon.png') }}">
                        <input type="text" class="form-control" placeholder="Search by City">
                    </div>
                    <div class="select-product ml-3 qoute-list">
                        <select name="select-product" class="form-control">
                            <option value="">Standard LTL Quote</option>
                        </select>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-5">
            <div class="add-btn text-right">
                <button class="add-btn bg-dark-blue text-white border-0 py-2 px-4 font-weight-bold rounded mr-3">Get a Quote</button>
            </div>
        </div>
    </div>
    <div class="quote-form">
        <div class="row">
            <div class="col-12">
                <div class="bg-white p-4">
                    <table class="table table-qoute-list">
                        <thead>
                            <tr>
                                <th>Origin & Destination </th>
                                <th>Quote ID</th>
                                <th>Created Date</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <ul class="location-list">
                                        <li>From: Location, City, Country</li>
                                        <li>LA JUNTA, CO 81050 </li>
                                    </ul>
                                </td>
                                <td>LMFQWZ3300</td>
                                <td>July 12, 2020</td>
                                <td class="text-right">
                                    <button class="btn-heart mr-3">
                                        <img src="{{ asset('img/heart.png') }}">
                                    </button>
                                    <button class="btn-quote-edit mr-3">Edit</button>
                                    <button class="btn-view-quote ">View Quote</button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="location-list">
                                        <li>From: Location, City, Country</li>
                                        <li>LA JUNTA, CO 81050 </li>
                                    </ul>
                                </td>
                                <td>LMFQWZ3300</td>
                                <td>July 12, 2020</td>
                                <td class="text-right">
                                    <button class="btn-heart mr-3">
                                        <img src="{{ asset('img/heart.png') }}">
                                    </button>
                                    <button class="btn-quote-edit mr-3">Edit</button>
                                    <button class="btn-view-quote ">View Quote</button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="location-list">
                                        <li>From: Location, City, Country</li>
                                        <li>LA JUNTA, CO 81050 </li>
                                    </ul>
                                </td>
                                <td>LMFQWZ3300</td>
                                <td>July 12, 2020</td>
                                <td class="text-right">
                                    <button class="btn-heart mr-3">
                                        <img src="{{ asset('img/heart.png') }}">
                                    </button>
                                    <button class="btn-quote-edit mr-3">Edit</button>
                                    <button class="btn-view-quote ">View Quote</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection