@extends('layouts.dashboard')
@section('title', 'Account Settings')
@section('content')

<div class="center-navbar">
    <ul>
        <li><a href="/my-shipment-all">All</a></li>
        <li><a href="/active-shipment">Active Shipment</a></li>
        <li class="active"><a href="/delivered-shipment">Delivered</a></li>
    </ul>
</div>

<div class="content">
    <div class="content-container">
        <div class="row mb-4">
            <div class="col-7">
                <div class="search-form">
                    <form action="#" _lpchecked="1">
                        <div class="search-outer">
                            <img class="user-img" src="http://shipnow.test/img/search-icon.png">
                            <input type="text" class="form-control" placeholder="Search by City">
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-5">
                <div class="add-btn text-right">
                    <button class="add-btn bg-dark-blue text-white border-0 py-2 px-4 font-weight-bold rounded">Filter</button>
                </div>
            </div>
        </div>

        <div class="db-custom-table">
            <table class="table">
                <thead>
                    <tr>
                        <th>Origin destination</th>
                        <th>Ship Date</th>
                        <th>Carrier</th>
                        <th>Order Id</th>
                        <th>Tracking Id</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <ul class="location-list">
                                <li>From: Location, City, Country</li>
                                <li>LA JUNTA, CO 81050 </li>
                            </ul>
                        </td>
                        <td>July 12, 2020</td>
                        <td>Fedex</td>
                        <td>SDV15464</td>
                        <td>
                            <a href="#" class="link-text-color">#54416354161</a>
                        </td>
                        <td class="text-green"> 
                            Delivered
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul class="location-list">
                                <li>From: Location, City, Country</li>
                                <li>LA JUNTA, CO 81050 </li>
                            </ul>
                        </td>
                        <td>July 12, 2020</td>
                        <td>Fedex</td>
                        <td>SDV15464</td>
                        <td>
                            <a href="#" class="link-text-color">#54416354161</a>
                        </td>
                        <td>
                            Invoiced
                        </td>
                    </tr>
                </tbody>
            </table>
            <nav aria-label="Page navigation " class="custom-pagination">
                <ul class="pagination justify-content-end">
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">
                                <img src="http://shipnow.test/img/arrow-prev.png">
                            </span>
                        </a>
                    </li>
                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">
                                <img src="http://shipnow.test/img/next.png">
                            </span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(function() {
        $("#form").datepicker();
    });
    $(function() {
        $("#to").datepicker();
    });
</script>

@endpush