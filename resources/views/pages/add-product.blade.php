@extends('layouts.dashboard')
@section('title', 'Account Settings')
@section('content')

<div class="content no-sub-nav">
    <div class="row mb-4">
        <div class="col-7">
            <div class="select-product">
                <select name="select-product" class="form-control">
                    <option value="">Select Product</option>
                </select>
            </div>
        </div>
        <div class="col-5">
            <div class="add-btn text-right">
                <button class="add-btn bg-dark-blue text-white border-0 py-2 px-4 font-weight-bold rounded" type="button" data-toggle="modal" data-target="#staticBackdrop">Add Product</button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="table-responsive-sm product-table">
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th>Name </th>
                            <th>Description</th>
                            <th>Dimensions</th>
                            <th>Weight</th>
                            <th>Class</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>STEEL STRUCTURES ITEM 178690-08</td>
                            <td>stee structures item 178690-08</td>
                            <td>
                                <span>
                                    Length
                                    <strong>3cm</strong>
                                </span>
                                <span>
                                    Width
                                    <strong>3cm</strong>
                                </span>
                                <span>
                                    Height
                                    <strong>3cm</strong>
                                </span>
                            </td>
                            <td>
                                <strong>20kg</strong>
                            </td>
                            <td>
                                <strong>LTL</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>STEEL STRUCTURES ITEM 178690-08</td>
                            <td>stee structures item 178690-08</td>
                            <td>
                                <span>
                                    Length
                                    <strong>3cm</strong>
                                </span>
                                <span>
                                    Width
                                    <strong>3cm</strong>
                                </span>
                                <span>
                                    Height
                                    <strong>3cm</strong>
                                </span>
                            </td>
                            <td>
                                <strong>20kg</strong>
                            </td>
                            <td>
                                <strong>LTL</strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<!-- add product Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable  modal-lg">
        <div class="modal-content">
            <div class="modal-header header-custom py-4 px-5">
                <h5 class="modal-title" id="staticBackdropLabel">Add Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pb-4">
                <div class="form-add px-4">
                    <form action="#">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" name="name" class="form-control" placeholder="company name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea name="description" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Length</label>
                                    <input type="text" name="lenght" class="form-control" placeholder="Lenght">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Width</label>
                                    <input type="text" name="Width" class="form-control" placeholder="Width">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Height</label>
                                    <input type="text" name="height" class="form-control" placeholder="height">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Weight</label>
                                    <input type="text" name="weight" class="form-control" placeholder="Weight">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Class</label>
                                    <input type="text" name="class" class="form-control" placeholder="Class">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 mt-4">
                                <div class="form-group">
                                    <button type="submit" name="Save" class="bg-dark-blue text-white border-0 py-2 px-5 font-weight-bold rounded">Add Product</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection