@extends('layouts.dashboard')
@section('title', 'Letter')
@section('content')
<div class="content">
    <div class="row mb-4">
        <div class="col-12">
            <div class="db-sub-heading">
                <h2>Choose Your Packaging</h2>
            </div>
        </div>
        <div class="col-12">
            <div class="bg-white shadow-sm">
                <ul class="package-nav">
                    <li class="active-nav"><a href="#">Letter</a></li>
                    <li><a href="#">Pak</a></li>
                    <li><a href="#">Package</a></li>
                    <li><a href="#">LTL</a></li>
                    <li><a href="#">FTL</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="quote-form">
        <div class="row">
            <div class="col-12">
                <div class="bg-white shadow-sm ">
                    <div class="d-flex align-items-center p-4 ship-details">
                        <div class="ship-form col-3 pl-0">
                            <div class="form-group">
                                <label>Ship From :</label>
                                <input type="text" name="ship-form" class="form-control" placeholder="City, Postal, Zip">
                            </div>
                        </div>
                        <div class="switch text-center px-4">
                            <img class="d-block" src="{{ asset('img/reload.png') }}">
                            <span class="d-block ext-dark-blue mt-1">Switch</span>
                        </div>
                        <div class="ship-to col-3">
                            <div class="form-group">
                                <label>Ship To :</label>
                                <input type="text" name="ship-to" class="form-control" placeholder="Enter full address here">
                            </div>
                        </div>
                    </div>
                    <div class="bottom-package-form p-4 letter-height">
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex flex-wrap">
                                    <div class="form-group">
                                        <label>Insurance</label>
                                        <div class="input-group custom-input-group large-input-group">
                                            <input type="text" placeholder="Value">
                                            <select>
                                                <option value="$">$</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group ml-4">
                                        <label>Currency</label>
                                        <div class="input-group custom-input-group currency-input-group">
                                            <input type="text" placeholder="Value">
                                            <select>
                                                <option value="$">CDN</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="bottom-submit text-right mt-3">
                    <button type="reset" class="btn-quote-reset mr-3">Clear</button>
                    <button type="reset" class="btn-quote"> Get a Quote</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection