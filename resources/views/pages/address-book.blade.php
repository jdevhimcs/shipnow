@extends('layouts.dashboard')
@section('content')

<div class="wrapper ">
    <div class="sidebar" data-color="white" data-active-color="danger">
        <div class="logo-image-small">
            <img src="{{ asset('img/logo-db.png') }}">
        </div>
        <div class="sidebar-wrapper">
            <ul class="nav">
                <li>
                    <a href="./dashboard.html">
                        <img src="{{ asset('img/dashboard.png') }}">
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="active">
                    <a href="./icons.html">
                        <img src="{{ asset('img/my-account.png') }}">
                        <p>My Account</p>
                    </a>
                </li>
                <li>
                    <a href="./map.html">
                        <img src="{{ asset('img/qoute.png') }}">
                        <p>My Quotes</p>
                    </a>
                </li>
                <li>
                    <a href="./notifications.html">
                        <img src="{{ asset('img/invoice.png') }}">
                        <p>Invoices</p>
                    </a>
                </li>
                <li>
                    <a href="./user.html">
                        <img src="{{ asset('img/my-product.png') }}">
                        <p>My Products</p>
                    </a>
                </li>
                <li>
                    <a href="./tables.html">
                        <img src="{{ asset('img/help.png') }}">
                        <p>Help</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-absolute nav-top fixed-top navbar-transparent">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <div class="navbar-toggle">
                        <button type="button" class="navbar-toggler">
                            <span class="navbar-toggler-bar bar1"></span>
                            <span class="navbar-toggler-bar bar2"></span>
                            <span class="navbar-toggler-bar bar3"></span>
                        </button>
                    </div>
                    <a class="navbar-brand top-title" href="javascript:;">My Account</a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navigation">
                    <ul class="navbar-nav">
                        <li class="nav-item btn-rotate dropdown">
                            <img class="bell" src="{{ asset('img/bell.png') }}">
                        </li>
                        <li class="nav-item">
                            <img class="user-img" src="{{ asset('img/user-img.png') }}">
                            Adam smith
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->

        <div class="content">
            <div class="center-navbar">
                <ul>
                    <li><a href="#">Change Password</a></li>
                    <li><a href="#">Statement</a></li>
                    <li><a href="#">Distributions</a></li>
                    <li><a href="#">Address Book</a></li>
                    <li><a href="#">Account</a></li>
                    <li><a href="#">Account Settings</a></li>
                </ul>
            </div>
            <div class="content-container">
                <div class="row mb-4">
                    <div class="col-7">
                        <div class="search-form">
                            <form action="#">
                                <div class="search-outer">
                                    <img class="user-img" src="{{ asset('img/search-icon.png') }}">
                                    <input type="text" class="form-control" placeholder="Search by City">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="add-btn text-right">
                            <button class="add-btn bg-dark-blue text-white border-0 py-2 px-4 font-weight-bold rounded" type="button" data-toggle="modal" data-target="#staticBackdrop">Add More</button>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="d-flex bg-white align-items-center justify-content-between rounded shadow-sm
mb-3">
                            <div class="product-item-name p-3">
                                <h3>Furniture Home PVT LTD</h3>
                                <p>
                                    <img src="{{ asset('img/location.png') }}">
                                    1732 Rose Avenue, Metairie , LA Louisiana, 70001
                                </p>
                            </div>
                            <div class="phone-no p-3">
                                <img src="{{ asset('img/phone.png') }}">
                                <span>504289-5951,</span>
                                <span> 337-212-4402</span>
                            </div>
                            <div class="email-id p-3">
                                <p>
                                    <img src="{{ asset('img/message.png') }}">
                                    gqapsbdihn7@temporary-mail.net
                                </p>
                            </div>
                            <div class="action p-3">
                                <button class="btn-edit">
                                    <img src="{{ asset('img/edit.png') }}">
                                </button>
                                <button class="btn-delete">
                                    <img src="{{ asset('img/delete.png') }}">
                                </button>
                            </div>
                        </div>
                        <!-- row 2 -->
                        <div class="d-flex bg-white align-items-center justify-content-between rounded shadow-sm
mb-3">
                            <div class="product-item-name p-3">
                                <h3>Furniture Home PVT LTD</h3>
                                <p>
                                    <img src="{{ asset('img/location.png') }}">
                                    1732 Rose Avenue, Metairie , LA Louisiana, 70001
                                </p>
                            </div>
                            <div class="phone-no p-3">
                                <img src="{{ asset('img/phone.png') }}">
                                <span>504289-5951,</span>
                                <span> 337-212-4402</span>
                            </div>
                            <div class="email-id p-3">
                                <p>
                                    <img src="{{ asset('img/message.png') }}">
                                    gqapsbdihn7@temporary-mail.net
                                </p>
                            </div>
                            <div class="action p-3">
                                <button class="btn-edit">
                                    <img src="{{ asset('img/edit.png') }}">
                                </button>
                                <button class="btn-delete">
                                    <img src="{{ asset('img/delete.png') }}">
                                </button>
                            </div>
                        </div>
                        <!-- pagination -->
                        <nav aria-label="Page navigation " class="custom-pagination">
                            <ul class="pagination justify-content-end">
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">
                                            <img src="{{ asset('img/arrow-prev.png') }}">
                                        </span>
                                    </a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">
                                            <img src="{{ asset('img/next.png') }}">
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </nav>

                    </div>
                </div>
            </div>
        </div>
        <footer class="footer footer-black  footer-white ">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="credits text-center">
                            <span class="copyright">
                                © 2020 Shipnow LTD. All rights reserved. Terms & Conditions | Privacy Policy
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>




<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable  modal-lg">
        <div class="modal-content">
            <div class="modal-header header-custom py-4 px-5">
                <h5 class="modal-title" id="staticBackdropLabel">Edit company information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pb-4">
                <div class="form-add px-4">
                    <form action="#">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input type="text" name="company-name" class="form-control" placeholder="company name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Address</label>
                                    <input type="text" name="address" class="form-control" placeholder="Address">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>State</label>
                                    <select name="state" class="form-control">
                                        <option value="Select State">Select State</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Postal</label>
                                    <input type="text" name="postal" class="form-control" placeholder="Postal">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input type="text" name="email" class="form-control" placeholder="Email Address">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input type="text" name="phone-no" class="form-control" placeholder="Phone Number">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-auto">
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="Picked" name="example1">
                                        <label class="custom-control-label" for="Picked">De Picked up</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-auto">
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="Default" name="example2">
                                        <label class="custom-control-label" for="Default">Default Drop off</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label>Shipping hours</label>
                                    <div class="d-flex align-items-center">
                                        <div class="input-group">
                                            <input type="text" class="form-control date-input" aria-label="Text input with segmented dropdown button">
                                            <div class="input-group-append">
                                                <select name="time" class="time">
                                                    <option value="am">am</option>
                                                    <option value="am">am</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="to px-3">TO</div>
                                        <div class="input-group">
                                            <input type="text" class="form-control date-input" aria-label="Text input with segmented dropdown button">
                                            <div class="input-group-append">
                                                <select name="time" class="time">
                                                    <option value="am">am</option>
                                                    <option value="am">am</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="form-group">
                                    <label>Default Note</label>
                                    <input type="text" name="note" class="form-control" placeholder="AM">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Business with dock</label>
                                    <div class="d-flex">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="yes1" name="example1">
                                            <label class="custom-control-label" for="yes1">Yes</label>
                                        </div>
                                        <div class="custom-control custom-checkbox ml-3">
                                            <input type="checkbox" class="custom-control-input" id="no1" name="example2">
                                            <l0abel class="custom-control-label" for="no1">No</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <label>Residence</label>
                                <div class="d-flex">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="yes2" name="example1">
                                        <label class="custom-control-label" for="yes2">Yes</label>
                                    </div>
                                    <div class="custom-control custom-checkbox ml-3">
                                        <input type="checkbox" class="custom-control-input" id="no2" name="example2">
                                        <label class="custom-control-label" for="no2">No</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Appointment delivery</label>
                                    <div class="d-flex">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="yes3" name="example1">
                                            <label class="custom-control-label" for="yes3">Yes</label>
                                        </div>
                                        <div class="custom-control custom-checkbox ml-3">
                                            <input type="checkbox" class="custom-control-input" id="no3" name="example2">
                                            <label class="custom-control-label" for="no3">No</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <label>Straight Truck</label>
                                <div class="d-flex">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="yes4" name="example1">
                                        <label class="custom-control-label" for="yes4">Yes</label>
                                    </div>
                                    <div class="custom-control custom-checkbox ml-3">
                                        <input type="checkbox" class="custom-control-input" id="no4" name="example2">
                                        <label class="custom-control-label" for="no4">No</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 mt-4">
                                <div class="form-group">
                                    <button type="submit" name="Save" class="bg-dark-blue text-white border-0 py-2 px-5 font-weight-bold rounded">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection