@extends('layouts.dashboard')
@section('content')

<div class="wrapper ">
    <div class="sidebar" data-color="white" data-active-color="danger">
        <div class="logo-image-small">
            <img src="{{ asset('img/logo-db.png') }}">
        </div>
        <div class="sidebar-wrapper">
            <ul class="nav">
                <li>
                    <a href="./dashboard.html">
                        <img src="{{ asset('img/dashboard.png') }}">
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="active">
                    <a href="./icons.html">
                        <img src="{{ asset('img/my-account.png') }}">
                        <p>My Account</p>
                    </a>
                </li>
                <li>
                    <a href="./map.html">
                        <img src="{{ asset('img/qoute.png') }}">
                        <p>My Quotes</p>
                    </a>
                </li>
                <li>
                    <a href="./notifications.html">
                        <img src="{{ asset('img/invoice.png') }}">
                        <p>Invoices</p>
                    </a>
                </li>
                <li>
                    <a href="./user.html">
                        <img src="{{ asset('img/my-product.png') }}">
                        <p>My Products</p>
                    </a>
                </li>
                <li>
                    <a href="./tables.html">
                        <img src="{{ asset('img/help.png') }}">
                        <p>Help</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-absolute nav-top fixed-top navbar-transparent">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <div class="navbar-toggle">
                        <button type="button" class="navbar-toggler">
                            <span class="navbar-toggler-bar bar1"></span>
                            <span class="navbar-toggler-bar bar2"></span>
                            <span class="navbar-toggler-bar bar3"></span>
                        </button>
                    </div>
                    <a class="navbar-brand top-title" href="javascript:;">My Account</a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navigation">
                    <ul class="navbar-nav">
                        <li class="nav-item btn-rotate dropdown">
                            <img class="bell" src="{{ asset('img/bell.png') }}">
                        </li>
                        <li class="nav-item">
                            <img class="user-img" src="{{ asset('img/user-img.png') }}">
                            Adam smith
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->

        <div class="content">
            <div class="center-navbar">
                <ul>
                    <li><a href="#">Change Password</a></li>
                    <li><a href="#">Statement</a></li>
                    <li><a href="#">Distributions</a></li>
                    <li><a href="#">Address Book</a></li>
                    <li><a href="#">Account</a></li>
                    <li><a href="#">Account Settings</a></li>
                </ul>
            </div>
            <div class="content-container">
                <div class="row mb-4">
                    <div class="col-7">
                        <div class="search-form">
                            <form action="#">
                                <div class="search-outer">
                                    <img class="user-img" src="{{ asset('img/search-icon.png') }}">
                                    <input type="text" class="form-control" placeholder="Search by City">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="add-btn text-right">
                            <div class="upload-btn-wrapper">
                                <button class="btn-up">Upload </button>
                                <input type="file" name="myfile" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 mb-4 all-checkbox-select">
                        <div class="custom-control custom-checkbox custom-check">
                            <input type="checkbox" class="custom-control-input" id="select-all" name="example2">
                            <label class="custom-control-label pl-3 pt-2 text-dark-blue" for="select-all">Select all</label>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="d-flex bg-white align-items-center justify-content-between rounded shadow-sm
mb-3">
                            <div class="checkbox-select p-3 col-1">
                                <div class="custom-control custom-checkbox custom-check">
                                    <input type="checkbox" class="custom-control-input" id="select-1" name="example2">
                                    <label class="custom-control-label" for="select-1"></label>
                                </div>
                            </div>
                            <div class="product-item-name p-3 col-5">
                                <h3>Furniture Home PVT LTD</h3>
                                <p>
                                    <img src="{{ asset('img/location.png') }}">
                                    1732 Rose Avenue, Metairie , LA Louisiana, 70001
                                </p>
                            </div>
                            <div class="phone-no p-3 col-3">
                                <img src="{{ asset('img/phone.png') }}">
                                <span>504289-5951,</span>
                                <span> 337-212-4402</span>
                            </div>
                            <div class="email-id p-3 col-3">
                                <p>
                                    <img src="{{ asset('img/message.png') }}">
                                    gqapsbdihn7@temporary-mail.net
                                </p>
                            </div>
                        </div>
                        <!-- row 2 -->
                        <div class="d-flex bg-white align-items-center justify-content-between rounded shadow-sm
mb-3">
                            <div class="checkbox-select p-3 col-1">
                                <div class="custom-control custom-checkbox custom-check">
                                    <input type="checkbox" class="custom-control-input" id="select-2" name="example2">
                                    <label class="custom-control-label" for="select-2"></label>
                                </div>
                            </div>
                            <div class="product-item-name p-3 col-5">
                                <h3>Furniture Home PVT LTD</h3>
                                <p>
                                    <img src="{{ asset('img/location.png') }}">
                                    1732 Rose Avenue, Metairie , LA Louisiana, 70001
                                </p>
                            </div>
                            <div class="phone-no p-3 col-3">
                                <img src="{{ asset('img/phone.png') }}">
                                <span>504289-5951,</span>
                                <span> 337-212-4402</span>
                            </div>
                            <div class="email-id p-3 col-3">
                                <p>
                                    <img src="{{ asset('img/message.png') }}">
                                    gqapsbdihn7@temporary-mail.net
                                </p>
                            </div>

                        </div>
                        <!-- pagination -->
                        <nav aria-label="Page navigation " class="custom-pagination">
                            <ul class="pagination justify-content-end">
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">
                                            <img src="{{ asset('img/arrow-prev.png') }}">
                                        </span>
                                    </a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">
                                            <img src="{{ asset('img/next.png') }}">
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </nav>

                    </div>
                </div>
            </div>
        </div>
        <footer class="footer footer-black  footer-white ">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="credits text-center">
                            <span class="copyright">
                                © 2020 Shipnow LTD. All rights reserved. Terms & Conditions | Privacy Policy
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>

@endsection