@extends('layouts.dashboard')
@section('title', 'Account Settings')
@section('content')
<div class="content">
    <div class="row mb-4">
        <div class="col-12">
            <div class="db-sub-heading">
                <h2>Choose Your Packaging</h2>
            </div>
        </div>
        <div class="col-12">
            <div class="bg-white shadow-sm">
                <ul class="package-nav">
                    <li><a href="#">Letter</a></li>
                    <li><a href="#">Pak</a></li>
                    <li><a href="#">Package</a></li>
                    <li><a href="#">LTL</a></li>
                    <li class="active-nav"><a href="#">FTL</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="quote-form">
        <div class="row">
            <div class="col-12">
                <div class="bg-white shadow-sm ">
                    <div class="d-flex align-items-center p-4 ship-details">
                        <div class="ship-form col-3 pl-0">
                            <div class="form-group">
                                <label>Ship From :</label>
                                <input type="text" name="ship-form" class="form-control" placeholder="City, Postal, Zip">
                            </div>
                        </div>
                        <div class="switch text-center px-4">
                            <img class="d-block" src="{{ asset('img/reload.png') }}">
                            <span class="d-block ext-dark-blue mt-1">Switch</span>
                        </div>
                        <div class="ship-to col-3">
                            <div class="form-group">
                                <label>Ship To :</label>
                                <input type="text" name="ship-to" class="form-control" placeholder="Enter full address here">
                            </div>
                        </div>
                    </div>
                    <div class="bottom-package-form p-4">
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex justify-content-between mb-4">
                                    <div class="form-group">
                                        <label>Default</label>
                                        <div class="d-flex">
                                            <div class="custom-control custom-radio custom-radio-button">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label pl-3 text-dark pt-1" for="customRadio1">Trailer Space</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-radio-button pl-5">
                                                <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label pl-3 pt-1 text-dark" for="customRadio2">Pallets</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="like-quote">
                                        <img class="d-block" src="{{ asset('img/heart.png') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-auto mb-4">
                                <div class="form-group">
                                    <label class="custom-label-text">Trailer Space Size</label>
                                    <div class="input-group custom-input-group ">
                                        <input type="text" name="light" placeholder="L">
                                        <input type="text" name="weight" placeholder="W">
                                        <input type="text" name="height" placeholder="H">
                                        <select>
                                            <option>cm</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-auto mb-4">
                                <div class="form-group">
                                    <label class="custom-label-text">Weight</label>
                                    <div class="input-group custom-input-group weight">
                                        <input type="text" name="height" placeholder="H">
                                        <select>
                                            <option>lbs</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label class="custom-label-text">Discription </label>
                                    <textarea style="min-height: 60px;" name="discription" class="form-control custom-text-input light-placeholder" placeholder="Type here" id=""></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label class="custom-label-text">Special Instructions </label>
                                    <textarea style="min-height: 60px;" name="special-instructions" style="min-height: 60px;" class="form-control custom-text-input light-placeholder" placeholder="Type here"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Default</label>
                                    <div class="d-flex">
                                        <div class="custom-control custom-radio custom-radio-button">
                                            <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
                                            <label class="custom-control-label pl-3 text-dark pt-1" for="customRadio3">Quote</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-radio-button pl-5">
                                            <input type="radio" id="customRadio4" name="customRadio" class="custom-control-input">
                                            <label class="custom-control-label pl-3 pt-1 text-dark" for="customRadio4">Live Load</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-12">
                                <button type="button" name="send-request" class="btn-quote">Send Request</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottom-submit text-right mt-3">
                    <button type="reset" class="btn-quote-reset mr-3">Clear</button>
                    <button type="reset" class="btn-quote"> Get a Quote</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection