@extends('layouts.dashboard')
@section('title', 'Account Settings')
@section('content')

<div class="center-navbar">
    <ul>
        <li class="active"><a href="/my-shipment-all">All</a></li>
        <li><a href="/active-shipment">Active Shipment</a></li>
        <li><a href="/delivered-shipment">Delivered</a></li>
    </ul>
</div>
<div class="content">
    <div class="search-container bg-white shadow-sm p-4 mb-5">
        <form action="#">
            <div class="row">
                <div class="col-5">
                    <div class="form-group">
                        <div class="search-outer search-location">
                            <img class="user-img" src="{{ asset('img/search-icon.png') }}">
                            <input type="text" name="search" placeholder="Search by City " class="form-control light-placeholder custom-text-input">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        <label>Form</label>
                        <div class="calender-outer">
                            <img class="user-img" src="{{ asset('img/calender.png') }}">
                            <input type="text" id="form" placeholder="Form" class="form-control light-placeholder custom-text-input">
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label>To</label>
                        <div class="calender-outer">
                            <img class="user-img" src="{{ asset('img/calender.png') }}">
                            <input type="text" id="to" placeholder="To" class="form-control light-placeholder custom-text-input">
                        </div>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label>Tracking Id</label>
                        <input type="text" name="tracking" placeholder="#Tracking Id" class="form-control light-placeholder custom-text-input">
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label>Carrier</label>
                        <select name="carrier" class="form-control light-placeholder custom-text-input">
                            <option value="#">Select Carrier</option>
                        </select>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" class="form-control light-placeholder custom-text-input">
                            <option value="#">Select Status</option>
                            <option value="#">Delivered</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        <label>Form</label>
                        <input type="text" name="location" placeholder="Enter Location" class="form-control light-placeholder custom-text-input">
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <select name="status" class="form-control light-placeholder custom-text-input">
                            <option value="#">Select City</option>
                            <option value="#">Delivered</option>
                        </select>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label>To</label>
                        <input type="text" name="location" placeholder="Enter Location" class="form-control light-placeholder custom-text-input">
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <select name="status" class="form-control light-placeholder custom-text-input">
                            <option value="#">Select City</option>
                            <option value="#">Delivered</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row align-items-center">
                <div class="col-2">
                    <div class="form-group">
                        <label>User</label>
                        <select name="user" class="form-control light-placeholder custom-text-input">
                            <option value="#">Select User</option>
                        </select>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label>#Bol</label>
                        <input type="text" name="bol" placeholder="Enter Bol Number" class="form-control light-placeholder custom-text-input">
                    </div>
                </div>
                <div class="col-4 ml-auto text-right">
                    <button class="btn btn-search">Search Now</button>
                    <button type="reset" class="btn-reset">Clear all</button>
                </div>
            </div>
        </form>

    </div>

    <div class="db-custom-table">
        <table class="table">
            <thead>
                <tr>
                    <th>Origin destination</th>
                    <th>Ship Date</th>
                    <th>Carrier</th>
                    <th>Order Id</th>
                    <th>Tracking Id</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <ul class="location-list">
                            <li>From: Location, City, Country</li>
                            <li>LA JUNTA, CO 81050 </li>
                        </ul>
                    </td>
                    <td>July 12, 2020</td>
                    <td>Fedex</td>
                    <td>SDV15464</td>
                    <td>
                        <a href="#" class="link-text-color">#54416354161</a>
                    </td>
                    <td>
                        Dispatched
                    </td>
                </tr>
                <tr>
                    <td>
                        <ul class="location-list">
                            <li>From: Location, City, Country</li>
                            <li>LA JUNTA, CO 81050 </li>
                        </ul>
                    </td>
                    <td>July 12, 2020</td>
                    <td>Fedex</td>
                    <td>SDV15464</td>
                    <td>
                        <a href="#" class="link-text-color">#54416354161</a>
                    </td>
                    <td>
                        Dispatched
                    </td>
                </tr>
            </tbody>
        </table>
        <nav aria-label="Page navigation " class="custom-pagination">
            <ul class="pagination justify-content-end">
                <li class="page-item">
                    <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">
                            <img src="http://shipnow.test/img/arrow-prev.png">
                        </span>
                    </a>
                </li>
                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">
                            <img src="http://shipnow.test/img/next.png">
                        </span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(function() {
        $("#form").datepicker();
    });
    $(function() {
        $("#to").datepicker();
    });
</script>

@endpush