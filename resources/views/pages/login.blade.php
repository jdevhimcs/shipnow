@extends('layouts.app')
@section('content')

{{-- <!-- header -->
<header class="header">
    <div class="container">
        <nav class="navbar navbar-expand-md navbar-white bg-white custom-nav">
            <a class="navbar-brand" href="#">
                <img src="{{ asset('img/logo.png') }}">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">About us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Demo</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Contact Us</a>
                    </li>
                </ul>
                <button class="btn ml-3 btn-sign-in">Sign in</button>
            </div>
        </nav>
    </div>
</header> --}}


<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="login-section bg-white">
                <h2>Login</h2>
                <form action="#">
                    <div class="form-group">
                        <label>Email address</label>
                        <input type="text" name="email" class="form-control custom-input">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control custom-input">
                    </div>
                    <div class="form-group text-right">
                        <span class="d-block color-light-gray">
                            Forgot your password? <a href="#" class="color-sky font-weight-bold">Remind</a>
                        </span>
                    </div>
                    <div class="form-group text-center mt-5">
                        <button class="btn brn-login">Log In</button>
                    </div>
                    <p class="mt-5 text-center color-light-gray">Don’t have an account? <a href="{{route('signup')}}" class="color-sky font-weight-bold">Sign up</a></p>
                </form>
            </div>
        </div>
    </div>
</div>


{{-- <footer class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <div class="d-flex mb-3">
                        <div class="img-icon">
                            <img src="{{ asset('img/email.png')}}">
                        </div>
                        <div class="icon-text text-white ml-3">
                            Info@shipnow.com
                        </div>
                    </div>
                    <div class="d-flex ">
                        <div class="img-icon">
                            <img src="{{ asset('img/call.png') }}">
                        </div>
                        <div class="icon-text text-white ml-3">
                            +1 123 456 7890
                        </div>
                    </div>
                </div>
                <div class="col-2">
                    <div class="footer-link">
                        <ul class="m-0 p-0">
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Services</a></li>
                            <li><a href="#">Demo</a></li>
                            <li><a href="#">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-2">
                    <div class="footer-link">
                        <ul class="m-0 p-0">
                            <li><a href="#">Retail & consumer</a></li>
                            <li><a href="#">Amazone</a></li>
                            <li><a href="#">fedex </a></li>
                            <li><a href="#">Delievery</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-2">
                    <div class="footer-link">
                        <ul class="m-0 p-0">
                            <li><a href="#">Warehousing</a></li>
                            <li><a href="#"> Air Freinght</a></li>
                            <li><a href="#">Ocean </a></li>
                            <li><a href="#">freinght</a></li>
                            <li><a href="#">Road freight</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <p>
                        © 2020 Shipnow LTD. All rights reserved. Terms & Conditions | Privacy Policy
                    </p>
                </div>
                <div class="col-4">
                    <div class="footer-media-link">
                        <ul class="m-0 p-0 text-right">
                            <li>
                                <a href="#">
                                    <img src="{{ asset('img/facebook.png') }}">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="{{ asset('img/twitter.png') }}">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="{{ asset('img/linkedin.png') }}">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer> --}}
@endsection