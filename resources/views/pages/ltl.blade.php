@extends('layouts.dashboard')
@section('title', 'LTL')
@section('content')
<div class="content">
    <div class="row mb-4">
        <div class="col-12">
            <div class="db-sub-heading">
                <h2>Choose Your Packaging</h2>
            </div>
        </div>
        <div class="col-12">
            <div class="bg-white shadow-sm">
                <ul class="package-nav">
                    <li><a href="#">Letter</a></li>
                    <li><a href="#">Pak</a></li>
                    <li><a href="#">Package</a></li>
                    <li class="active-nav"><a href="#">LTL</a></li>
                    <li><a href="#">FTL</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="quote-form">
        <div class="row">
            <div class="col-12">
                <div class="bg-white shadow-sm ">
                    <div class="d-flex align-items-center p-4 ship-details">
                        <div class="ship-form col-3 pl-0">
                            <div class="form-group">
                                <label>Ship From :</label>
                                <input type="text" name="ship-form" class="form-control" placeholder="City, Postal, Zip">
                            </div>
                        </div>
                        <div class="switch text-center px-4 col-1">
                            <img class="d-block mx-auto" src="{{ asset('img/reload.png') }}">
                            <span class="d-block ext-dark-blue mt-1">Switch</span>
                        </div>
                        <div class="ship-to col-3">
                            <div class="form-group">
                                <label>Ship To :</label>
                                <input type="text" name="ship-to" class="form-control" placeholder="Enter full address here">
                            </div>
                        </div>
                    </div>
                    <div class="d-flex align-items-center p-4 ship-details">
                        <div class="ship-form col-3 pl-0">
                            <div class="form-group">
                                <label>Location :</label>
                                <input type="text" name="ship-form" class="form-control" placeholder="Business with dock">
                            </div>
                        </div>
                        <div class="switch text-center col-1 px-4">
                            
                        </div>
                        <div class="ship-to col-3">
                            <div class="form-group">
                                <label>Location To :</label>
                                <input type="text" name="ship-to" class="form-control" placeholder="Business with dock">
                            </div>
                        </div>
                    </div>
                    <div class="bottom-package-form p-4">
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex flex-wrap">
                                    <div class="form-group col-auto">
                                        <label>Quantity </label>
                                        <select class="input-group custom-input-group quantity">
                                            <option value="#">3</option>
                                            <option value="#">2</option>
                                            <option value="#">3</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-auto">
                                        <label class="custom-label-text">Trailer Space Size</label>
                                        <div class="input-group custom-input-group ">
                                            <input type="text" name="light" placeholder="L">
                                            <input type="text" name="weight" placeholder="W">
                                            <input type="text" name="height" placeholder="H">
                                            <select>
                                                <option>cm</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-auto">
                                        <label>Weight</label>
                                        <div class="input-group custom-input-group currency-input-group">
                                            <input type="text" placeholder="Value">
                                            <select>
                                                <option value="$">lbs</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label class="custom-label-text">Discription </label>
                                    <textarea style="min-height: 60px;" name="discription" class="form-control custom-text-input light-placeholder" placeholder="Type here" id=""></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="bottom-submit mt-2">
                                    <button type="reset" class="btn-quote">Add Now</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="package-table px-4">
                        <div class="row">
                            <div class="col-12">
                                <table class="table">
                                    <thead>
                                        <th>Package</th>
                                        <th>Dimensions</th>
                                        <th>Weight</th>
                                        <th>Actions</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>#1</td>
                                            <td>
                                                <span>Length</span>
                                                <strong>3cm,</strong>
                                                <span>Width</span>
                                                <strong>3cm,</strong>
                                                <span>Height</span>
                                                <strong>3cm,</strong>
                                            </td>
                                            <td>
                                                20kg
                                            </td>
                                            <td>
                                                <button class="btn-duplicate">Duplicate</button>
                                                <button class="btn-heart-like"><img src="{{ asset('img/heart.png') }}"></button>
                                                <button class="btn-delete"><img src="{{ asset('img/delete.png') }}"></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>#2</td>
                                            <td>
                                                <span>Length</span>
                                                <strong>3cm,</strong>
                                                <span>Width</span>
                                                <strong>3cm,</strong>
                                                <span>Height</span>
                                                <strong>3cm,</strong>
                                            </td>
                                            <td>
                                                20kg
                                            </td>
                                            <td>
                                                <button class="btn-duplicate">Duplicate</button>
                                                <button class="btn-heart-like"><img src="{{ asset('img/heart.png') }}"></button>
                                                <button class="btn-delete"><img src="{{ asset('img/delete.png') }}"></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="px-4">
                        <div class="row">
                            <div class="col-12">
                                <div class="heading-class">
                                    <h2>Fright Class Calculator</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-2 mb-3 px-0">
                                <div class="custom-control custom-radio custom-radio-button pl-5">
                                    <input type="radio" id="customRadio5" name="leftgate" class="custom-control-input">
                                    <label class="custom-control-label pl-3 pt-1 text-dark" for="customRadio5">Lift Gate</label>
                                </div>
                            </div>
                            <div class="col-2 mb-3 px-0">
                                <div class="custom-control custom-radio custom-radio-button pl-5">
                                    <input type="radio" id="customRadio6" name="inside" class="custom-control-input">
                                    <label class="custom-control-label pl-3 pt-1 text-dark" for="customRadio6">Inside Pickup</label>
                                </div>
                            </div>
                            <div class="col-2 mb-3 px-0">
                                <div class="custom-control custom-radio custom-radio-button pl-5">
                                    <input type="radio" id="airport-pickup" name="airport" class="custom-control-input">
                                    <label class="custom-control-label pl-3 pt-1 text-dark" for="airport-pickup">Airport Pickup</label>
                                </div>
                            </div>
                            <div class="col-3 mb-3 px-0">
                                <div class="custom-control custom-radio custom-radio-button pl-5">
                                    <input type="radio" id="site-pickup" name="site" class="custom-control-input">
                                    <label class="custom-control-label pl-3 pt-1 text-dark" for="site-pickup">Construction Site Pickup</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-2 mb-3 px-0">
                                <div class="custom-control custom-radio custom-radio-button pl-5">
                                    <input type="radio" id="customRadio5" name="leftgate" class="custom-control-input">
                                    <label class="custom-control-label pl-3 pt-1 text-dark" for="customRadio5">Pallet Jack</label>
                                </div>
                            </div>
                            <div class="col-2 mb-3 px-0">
                                <div class="custom-control custom-radio custom-radio-button pl-5">
                                    <input type="radio" id="customRadio6" name="inside" class="custom-control-input">
                                    <label class="custom-control-label pl-3 pt-1 text-dark" for="customRadio6">Inside Delivery</label>
                                </div>
                            </div>
                            <div class="col-2 mb-3 px-0">
                                <div class="custom-control custom-radio custom-radio-button pl-5">
                                    <input type="radio" id="airport-pickup" name="airport" class="custom-control-input">
                                    <label class="custom-control-label pl-3 pt-1 text-dark" for="airport-pickup">Airport Delivery</label>
                                </div>
                            </div>
                            <div class="col-2 mb-3 px-0">
                                <div class="custom-control custom-radio custom-radio-button pl-5">
                                    <input type="radio" id="site-pickup" name="site" class="custom-control-input">
                                    <label class="custom-control-label pl-3 pt-1 text-dark" for="site-pickup">Construction Site Drop</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-2 px-0">
                                <div class="custom-control custom-radio custom-radio-button pl-5">
                                    <input type="radio" id="customRadio5" name="leftgate" class="custom-control-input">
                                    <label class="custom-control-label pl-3 pt-1 text-dark" for="customRadio5">Straight Truck</label>
                                </div>
                            </div>
                            <div class="col-2 px-0">
                                <div class="custom-control custom-radio custom-radio-button pl-5">
                                    <input type="radio" id="customRadio6" name="inside" class="custom-control-input">
                                    <label class="custom-control-label pl-3 pt-1 text-dark" for="customRadio6">Appointment Delivery</label>
                                </div>
                            </div>
                            <div class="col-2 px-0">
                                <div class="custom-control custom-radio custom-radio-button pl-5">
                                    <input type="radio" id="airport-pickup" name="airport" class="custom-control-input">
                                    <label class="custom-control-label pl-3 pt-1 text-dark" for="airport-pickup">Hazmat</label>
                                </div>
                            </div>
                            <div class="col-2 px-0">
                                <div class="custom-control custom-radio custom-radio-button pl-5">
                                    <input type="radio" id="site-pickup" name="site" class="custom-control-input">
                                    <label class="custom-control-label pl-3 pt-1 text-dark" for="site-pickup">Limited Access</label>
                                </div>
                            </div>
                        </div>
                        <div class="row pt-4 pb-5">
                            <div class="col-auto">
                                <div class="form-group">
                                    <label>Quantity </label>
                                    <select class="input-group custom-input-group quantity">
                                        <option value="#">No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-auto">
                                <div class="form-group">
                                    <label>Additional </label>
                                    <select class="input-group custom-input-group quantity">
                                        <option value="#">No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-auto">
                                <div class="form-group">
                                    <label>Insurance</label>
                                    <div class="input-group custom-input-group large-input-group">
                                        <input type="text" placeholder="Value">
                                        <select>
                                            <option value="$">$</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-auto">
                                <div class="form-group ml-4">
                                    <label>Currency</label>
                                    <div class="input-group custom-input-group currency-input-group">
                                        <input type="text" placeholder="Value">
                                        <select>
                                            <option value="$">CDN</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottom-submit text-right mt-3">
                    <button type="reset" class="btn-quote-reset mr-3">Clear</button>
                    <button type="reset" class="btn-quote"> Get a Quote</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection