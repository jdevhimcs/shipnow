@extends('layouts.dashboard')
@section('content')

<div class="wrapper ">
    <div class="sidebar" data-color="white" data-active-color="danger">
        <div class="logo-image-small">
            <img src="{{ asset('img/logo-db.png') }}">
        </div>
        <div class="sidebar-wrapper">
            <ul class="nav">
                <li>
                    <a href="./dashboard.html">
                        <img src="{{ asset('img/dashboard.png') }}">
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="active">
                    <a href="./icons.html">
                        <img src="{{ asset('img/my-account.png') }}">
                        <p>My Account</p>
                    </a>
                </li>
                <li>
                    <a href="./map.html">
                        <img src="{{ asset('img/qoute.png') }}">
                        <p>My Quotes</p>
                    </a>
                </li>
                <li>
                    <a href="./notifications.html">
                        <img src="{{ asset('img/invoice.png') }}">
                        <p>Invoices</p>
                    </a>
                </li>
                <li>
                    <a href="./user.html">
                        <img src="{{ asset('img/my-product.png') }}">
                        <p>My Products</p>
                    </a>
                </li>
                <li>
                    <a href="./tables.html">
                        <img src="{{ asset('img/help.png') }}">
                        <p>Help</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-absolute nav-top fixed-top navbar-transparent">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <div class="navbar-toggle">
                        <button type="button" class="navbar-toggler">
                            <span class="navbar-toggler-bar bar1"></span>
                            <span class="navbar-toggler-bar bar2"></span>
                            <span class="navbar-toggler-bar bar3"></span>
                        </button>
                    </div>
                    <a class="navbar-brand top-title" href="javascript:;">My Account</a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navigation">
                    <ul class="navbar-nav">
                        <li class="nav-item btn-rotate dropdown">
                            <img class="bell" src="{{ asset('img/bell.png') }}">
                        </li>
                        <li class="nav-item">
                            <img class="user-img" src="{{ asset('img/user-img.png') }}">
                            Adam smith
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->

        <div class="content">
            <div class="center-navbar">
                <ul>
                    <li><a href="#">Change Password</a></li>
                    <li><a href="#">Statement</a></li>
                    <li><a href="#">Distributions</a></li>
                    <li><a href="#">Address Book</a></li>
                    <li><a href="#">Account</a></li>
                    <li><a href="#">Account Settings</a></li>
                </ul>
            </div>
            <div class="content-container">
                <div class="row mb-4">
                    <div class="col-12">
                        <div class="bg-white p-4 shadow-sm mb-4">
                            <div class="row">
                                <div class="col-12">
                                    <div class="seetting-heading">
                                        <h2>
                                            My Account Information
                                        </h2>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="custom-label-text">First Name</label>
                                        <input type="text" name="first-name" placeholder="Adam" class="form-control custom-text-input">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" name="last-name" placeholder="Type Last Name" class="form-control custom-text-input">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Email Address</label>
                                        <input type="text" name="email-address" placeholder="example@shipnow.com" class="form-control custom-text-input">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Phone Number</label>
                                        <input type="text" name="phone-no" placeholder="123 456 7890" class="form-control custom-text-input">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox custom-check">
                                            <input type="checkbox" class="custom-control-input mr-5" id="contact" name="example1">
                                            <label class="custom-control-label font-weight-bold pl-4 pt-1" for="contact">Same as main contact.</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bg-white p-4 shadow-sm mb-4">
                            <div class="row">
                                <div class="col-12">
                                    <div class="seetting-heading">
                                        <h2>
                                            Company Information
                                        </h2>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Company Name</label>
                                        <input type="text" name="compamy-name" placeholder="What your company full name?" class="form-control light-placeholder custom-text-input">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Addess</label>
                                        <input type="text" name="company-address" placeholder="Company address here" class="form-control light-placeholder custom-text-input">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>State</label>
                                        <select name="state" class="state form-control">
                                            <option value="Select state">Select State</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Country</label>
                                        <input type="text" name="country" placeholder="Country" class="form-control light-placeholder custom-text-input">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>City</label>
                                        <input type="text" name="city" placeholder="Enter City Name" class="form-control light-placeholder custom-text-input">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Postal</label>
                                        <input type="text" name="postal-code" placeholder="Postal Code" class="form-control light-placeholder custom-text-input">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Email Address</label>
                                        <input type="text" name="email" placeholder="example@shipnow.com" class="form-control light-placeholder custom-text-input">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Phone Number</label>
                                        <input type="text" name="phone-no" placeholder="123 456 7890" class="form-control light-placeholder custom-text-input">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox custom-check">
                                            <input type="checkbox" class="custom-control-input" id="bill" name="example1">
                                            <label class="custom-control-label font-weight-bold pl-4 pt-1" for="bill">Bill same as above.</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="bg-white p-4 shadow-sm mb-4">
                            <div class="row">
                                <div class="col-12">
                                    <div class="seetting-heading">
                                        <h2>
                                            Notification Settings
                                            <span>(Only for LTL)</span>
                                        </h2>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox custom-check">
                                            <input type="checkbox" class="custom-control-input" id="pick" name="example1">
                                            <label class="custom-control-label pl-4 pt-1" for="pick">Picked up</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox custom-check">
                                            <input type="checkbox" class="custom-control-input" id="delivered" name="example1">
                                            <label class="custom-control-label pl-4 pt-1" for="delivered">Delivered</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bg-white p-4 shadow-sm mb-4">
                            <div class="row">
                                <div class="col-12">
                                    <div class="seetting-heading">
                                        <h2>
                                            Order Preferences
                                        </h2>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="border-gray mb-4">
                                        <div class="d-flex order-name mb-4 align-items-center">
                                            <div class="reference col-2">
                                                <span class="d-block">Reference</span>
                                                <strong class="d-block">Frank15414741</strong>
                                            </div>
                                            <div class="broker-name col-2">
                                                <span class="d-block">Broker Name</span>
                                                <strong class="d-block">Frank Stevens</strong>
                                            </div>
                                            <div class="email-id col-2">
                                                <span class="d-block">Email Id</span>
                                                <strong class="d-block">franks@shpnow.com</strong>
                                            </div>
                                            <div class="mob col-2">
                                                <span class="d-block">Phone number</span>
                                                <strong class="d-block">(822) 931-4473</strong>
                                            </div>
                                            <div class="carrier-name col-2">
                                                <span class="d-block">Carrier Name</span>
                                                <strong class="d-block">Carrier123</strong>
                                            </div>
                                            <div class="action col-2">
                                                <button class="btn-edit">
                                                    <img src="{{ asset('img/edit.png') }}">
                                                </button>
                                                <button class="btn-delete">
                                                    <img src="{{ asset('img/delete.png') }}">
                                                </button>
                                            </div>
                                        </div>
                                        <div class="d-flex order-name align-items-center">
                                            <div class="lang col-2">
                                                <span class="d-block">Language</span>
                                                <strong class="d-block">Engish</strong>
                                            </div>
                                            <div class="unit col-2">
                                                <span class="d-block">Unit of Measurment</span>
                                                <strong class="d-block">Inch</strong>
                                            </div>
                                            <div class="size col-2 col-2">
                                                <span class="d-block">Label size</span>
                                                <strong class="d-block">8in x 12in</strong>
                                            </div>
                                            <div class="discount col-3">
                                                <strong class="d-block">Discount Applied!</strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="refer shadow-sm py-3">
                                        <div class="row">
                                            <div class="col-8">
                                                <div class="form-group col-4">
                                                    <label class="custom-label-text">Reference</label>
                                                    <input type="text" name="reference" class="form-control light-placeholder custom-text-input" placeholder="Enter Reference">
                                                </div>
                                                <div class="form-group col-4">
                                                    <label class="custom-label-text">Custom Broker</label>
                                                    <input type="text" name="broker" class="form-control light-placeholder custom-text-input" placeholder="Name">
                                                </div>
                                                <div class="d-flex">
                                                    <div class="form-group col-4">
                                                        <label class="custom-label-text">Phone Number</label>
                                                        <input type="text" name="reference" class="form-control light-placeholder custom-text-input" placeholder="Enter Phone number">
                                                    </div>
                                                    <div class="form-group col-4">
                                                        <label class="custom-label-text">Email Address</label>
                                                        <input type="text" name="reference" class="form-control light-placeholder custom-text-input" placeholder="Enter Email Address">
                                                    </div>
                                                </div>
                                                <div class="form-group col-12">
                                                    <label class="custom-label-text">Language</label>
                                                    <div class="d-flex">
                                                        <div class="custom-control custom-checkbox custom-check pr-4">
                                                            <input type="checkbox" class="custom-control-input" id="english" name="example1">
                                                            <label class="custom-control-label font-weight-bold pl-4 pt-1" for="english">English</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox custom-check">
                                                            <input type="checkbox" class="custom-control-input" id="francais" name="example1">
                                                            <label class="custom-control-label font-weight-bold pl-4 pt-1" for="francais">Francais</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-12">
                                                    <label class="custom-label-text">Unit of measurment</label>
                                                    <div class="d-flex">
                                                        <div class="custom-control custom-checkbox custom-check pr-4">
                                                            <input type="checkbox" class="custom-control-input" id="inch" name="example1">
                                                            <label class="custom-control-label font-weight-bold pl-4 pt-1" for="inch">Inch</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox custom-check">
                                                            <input type="checkbox" class="custom-control-input" id="centimeter" name="example1">
                                                            <label class="custom-control-label font-weight-bold pl-4 pt-1" for="centimeter">Centimeter</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-4">
                                                    <button class="add-discount-rate my-4">Add My Contract Discount Rate</button>
                                                </div>
                                                <div class="form-group col-4">
                                                    <label class="custom-label-text">Carrier Name</label>
                                                    <input type="text" name="Carrier" class="form-control light-placeholder custom-text-input" placeholder="Carrier">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group col-6">
                                                    <label class="custom-label-text">Label Size</label>
                                                    <select name="label-size" class="form-control">
                                                        <option value="">Label Size</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bg-white p-4 shadow-sm mb-4">
                            <div class="row">
                                <div class="col-12">
                                    <div class="seetting-heading">
                                        <h2>
                                            Add New User
                                        </h2>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="border-gray mb-5">
                                        <div class="d-flex order-name mb-4 align-items-center">
                                            <div class="reference col-2">
                                                <span class="d-block">Reference</span>
                                                <strong class="d-block">Frank15414741</strong>
                                            </div>
                                            <div class="broker-name col-2">
                                                <span class="d-block">Broker Name</span>
                                                <strong class="d-block">Frank Stevens</strong>
                                            </div>
                                            <div class="email-id col-2">
                                                <span class="d-block">Email Id</span>
                                                <strong class="d-block">franks@shpnow.com</strong>
                                            </div>
                                            <div class="mob col-2">
                                                <span class="d-block">Phone number</span>
                                                <strong class="d-block">(822) 931-4473</strong>
                                            </div>
                                            <div class="carrier-name col-2">
                                                <span class="d-block">Carrier Name</span>
                                                <strong class="d-block">Carrier123</strong>
                                            </div>
                                            <div class="action col-2">
                                                <button class="btn-edit">
                                                    <img src="{{ asset('img/edit.png') }}">
                                                </button>
                                                <button class="btn-delete">
                                                    <img src="{{ asset('img/delete.png') }}">
                                                </button>
                                            </div>
                                        </div>
                                        <div class="d-flex order-name align-items-center">
                                            <div class="lang col-2">
                                                <span class="d-block">Language</span>
                                                <strong class="d-block">Engish</strong>
                                            </div>
                                            <div class="unit col-2">
                                                <span class="d-block">Unit of Measurment</span>
                                                <strong class="d-block">Inch</strong>
                                            </div>
                                            <div class="size col-2 col-2">
                                                <span class="d-block">Label size</span>
                                                <strong class="d-block">8in x 12in</strong>
                                            </div>
                                            <div class="discount col-3">
                                                <strong class="d-block">Discount Applied!</strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="refer shadow-sm p-4">
                                        <h3 class="mb-1">Add New User</h3>
                                        <div class="row">
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label>Name</label>
                                                    <input type="text" name="name" class="form-control light-placeholder custom-text-input" placeholder="Enter Name">
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label>Phone Number</label>
                                                    <input type="text" name="Phone-number" class="form-control light-placeholder custom-text-input" placeholder="Enter Phone Number">
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label>Email Address</label>
                                                    <input type="text" name="name" class="form-control light-placeholder custom-text-input" placeholder="Enter Email Address">
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input type="text" name="name" class="form-control light-placeholder custom-text-input" placeholder="Enter Password">
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label>Right</label>
                                                    <select class="form-control">
                                                        <option value="">Admin</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <button class="add-user " type="button">Add User</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <button class="add-save" type="button">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer footer-black  footer-white ">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="credits text-center">
                            <span class="copyright">
                                © 2020 Shipnow LTD. All rights reserved. Terms & Conditions | Privacy Policy
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>




<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable  modal-lg">
        <div class="modal-content">
            <div class="modal-header header-custom py-4 px-5">
                <h5 class="modal-title" id="staticBackdropLabel">Edit company information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pb-4">
                <div class="form-add px-4">
                    <form action="#">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input type="text" name="company-name" class="form-control" placeholder="company name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Address</label>
                                    <input type="text" name="address" class="form-control" placeholder="Address">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>State</label>
                                    <select name="state" class="form-control">
                                        <option value="Select State">Select State</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Postal</label>
                                    <input type="text" name="postal" class="form-control" placeholder="Postal">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input type="text" name="email" class="form-control" placeholder="Email Address">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input type="text" name="phone-no" class="form-control" placeholder="Phone Number">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-auto">
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="Picked" name="example1">
                                        <label class="custom-control-label" for="Picked">De Picked up</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-auto">
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="Default" name="example2">
                                        <label class="custom-control-label" for="Default">Default Drop off</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label>Shipping hours</label>
                                    <div class="d-flex align-items-center">
                                        <div class="input-group">
                                            <input type="text" class="form-control date-input" aria-label="Text input with segmented dropdown button">
                                            <div class="input-group-append">
                                                <select name="time" class="time">
                                                    <option value="am">am</option>
                                                    <option value="am">am</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="to px-3">TO</div>
                                        <div class="input-group">
                                            <input type="text" class="form-control date-input" aria-label="Text input with segmented dropdown button">
                                            <div class="input-group-append">
                                                <select name="time" class="time">
                                                    <option value="am">am</option>
                                                    <option value="am">am</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="form-group">
                                    <label>Default Note</label>
                                    <input type="text" name="note" class="form-control" placeholder="AM">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Business with dock</label>
                                    <div class="d-flex">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="yes1" name="example1">
                                            <label class="custom-control-label" for="yes1">Yes</label>
                                        </div>
                                        <div class="custom-control custom-checkbox ml-3">
                                            <input type="checkbox" class="custom-control-input" id="no1" name="example2">
                                            <label class="custom-control-label" for="no1">No</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <label>Residence</label>
                                <div class="d-flex">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="yes2" name="example1">
                                        <label class="custom-control-label" for="yes2">Yes</label>
                                    </div>
                                    <div class="custom-control custom-checkbox ml-3">
                                        <input type="checkbox" class="custom-control-input" id="no2" name="example2">
                                        <label class="custom-control-label" for="no2">No</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Appointment delivery</label>
                                    <div class="d-flex">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="yes3" name="example1">
                                            <label class="custom-control-label" for="yes3">Yes</label>
                                        </div>
                                        <div class="custom-control custom-checkbox ml-3">
                                            <input type="checkbox" class="custom-control-input" id="no3" name="example2">
                                            <label class="custom-control-label" for="no3">No</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <label>Straight Truck</label>
                                <div class="d-flex">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="yes4" name="example1">
                                        <label class="custom-control-label" for="yes4">Yes</label>
                                    </div>
                                    <div class="custom-control custom-checkbox ml-3">
                                        <input type="checkbox" class="custom-control-input" id="no4" name="example2">
                                        <label class="custom-control-label" for="no4">No</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 mt-4">
                                <div class="form-group">
                                    <button type="submit" name="Save" class="bg-dark-blue text-white border-0 py-2 px-5 font-weight-bold rounded">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection