@extends('layouts.app')
@section('content')

<!-- hero img -->
<div class="hero-img" style="background-image: url('../img/hero.png');">
    <div class="container-home">
        <div class="row">
            <div class="col-12">
                <div class="hero-heading">
                    <h2>
                        We are committed<br />
                        to give best shipping<br />
                        Services
                    </h2>
                    <p>
                        Competitive advantage to some of the largest <br /> companies allover the world.
                    </p>
                    <button class="btn-demo mb-4">Book a Demo</button>
                    <button class="btn-play d-block">
                        <img class="img-fluid mr-2" src="{{ asset('img/play.png') }}">
                        Watch our video
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="gap-bottom book-demo-form">
    <div class="container-home">
        <div class="row">
            <div class="col-12">
                <ul class="nav nav-tabs custom-tab" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Request a Quote</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Track and Trace</a>
                    </li>
                </ul>
                <div class="tab-content shadow-tab p-sm-4" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row">
                            <div class="col-xl-9">
                                <div class="d-flex flex-wrap">
                                    <div class="col-md-3 col-sm-6 px-2">
                                        <div class="form-group">
                                            <label>Company Name</label>
                                            <input type="text" name="Company-name" class="form-control light-placeholder custom-text-input" placeholder="Company Name">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 px-2">
                                        <div class="form-group">
                                            <label>Your Name</label>
                                            <input type="text" name="your-name" class="form-control light-placeholder custom-text-input" placeholder="Your Name">
                                            <span class="req">
                                                <img src="{{ asset('img/info.png') }}">
                                                This is required field
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 px-2">
                                        <div class="form-group">
                                            <label>Email Address</label>
                                            <input type="text" name="Email" class="form-control light-placeholder custom-text-input" placeholder="Email Address">
                                            <span class="req">
                                                <img src="{{ asset('img/info.png') }}">
                                                This is required field
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 px-2">
                                        <div class="form-group">
                                            <label>Phone Number</label>
                                            <input type="text" name="Email" class="form-control light-placeholder custom-text-input" placeholder="Phone Number">
                                            <span class="req">
                                                <img src="{{ asset('img/info.png') }}">
                                                This is required field
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-12 px-2">
                                        <div class="form-group">
                                            <label>Comment</label>
                                            <textarea name="comment" class="form-control light-placeholder custom-text-input" cols="20" placeholder="Enter your Comments"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-12 py-4">
                                        <h2 class="slip-heading">What do you want to ship?</h2>
                                        <div class="row">
                                            <div class="col-md-3 col-sm-6">
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox custom-check">
                                                        <input type="checkbox" class="custom-control-input" id="letter" name="example2">
                                                        <label class="custom-control-label pl-4 pt-1" for="letter">Letter</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6">
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox custom-check">
                                                        <input type="checkbox" class="custom-control-input" id="pah" name="example2">
                                                        <label class="custom-control-label pl-4 pt-1" for="pah">PAH</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6">
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox custom-check">
                                                        <input type="checkbox" class="custom-control-input" id="package" name="example2">
                                                        <label class="custom-control-label pl-4 pt-1" for="package">Package</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6">
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox custom-check">
                                                        <input type="checkbox" class="custom-control-input" id="LTL" name="example2">
                                                        <label class="custom-control-label pl-4 pt-1" for="LTL">LTL</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button class="btn-submit">Submit</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 d-none d-xl-block">
                                <div class="signup tab-img">
                                    <ul>
                                        <li>International shipping.</li>
                                        <li>Ecommerce platform integration.</li>
                                        <li>Branding packaging</li>
                                    </ul>
                                    <div class="tab-img-bottom">
                                        <h2>
                                            For other quotes please contact
                                        </h2>
                                        <p>
                                            Customer Service at <strong>12345-67890</strong>
                                        </p>
                                        <p>
                                            Email <strong>customerservice@shipnow.com</strong>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- transportation  -->
<div class="transportation gap-bottom ">
    <div class="container-home">
        <div class="row">
            <div class="col-lg-7 mb-4 mb-lg-0">
                <div class="home-heading">
                    <p>Providing full Range Of Transportation allover the world.</p>
                    <h2>
                        Reliable Transportation & Logistic <br />
                        Solutions
                    </h2>
                </div>
                <div class="d-flex flex-wrap">
                    <div class="transportation-left col-lg-7 pl-lg-0">
                        <p>
                            Quite a number of devices and sources on the market today will work with the use of HDMI. The Blu-Ray disc player, a relatively new innovation, was created with the use of HDMI specifically in mind.
                        </p>
                        <p>
                            Some of the people that are playing, and that have few rounds to play in the tournament don’t count the drinks they have during their games and at the very end of the day they don’t have power to continue till the last game, which is a pity to loose such a big opportunity, so, don’t drink a lot and be prepare for the last round.
                        </p>
                    </div>
                    <div class="transportation-right col-lg-5 ">
                        <div>
                            <h2>
                                <span class="dot-icon"></span>
                                Manage couriers in one place
                            </h2>
                            <p class="ml-4">
                                Quite a number of devices and sources on the market today will work with the use of <HDMI class="P"></HDMI>
                            </p>
                        </div>
                        <div>
                            <h2>
                                <span class="dot-icon"></span>
                                Reliablility
                            </h2>
                            <p class="ml-4">
                                We Provide safety throughout al the stages of our delivery process.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="transportation-img">
                    <img class="img-fluid" src="{{ asset('img/transportation.png') }}">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- logo -->
<div class="company-logo gap-bottom">
    <div class="container-home">
        <div class="row">
            <div class="col-12">
                <ul>
                    <li>
                        <img class="img-fluid" src="{{ asset('img/logo2.png') }}">
                    </li>
                    <li>
                        <img class="img-fluid" src="{{ asset('img/logo-1.png') }}">
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- testimonials -->
<div class="testimonials gap-bottom">
    <div class="container-home">
        <div class="row">
            <div class="col-12 mb-5 pb-4">
                <div class="home-heading text-center text-center-heading">
                    <h2 class="text-white">People love Shipnow.</h2>
                    <p class="text-white">See some of the stories.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="carouselExampleIndicators" class="carousel slide home-page-slider" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="row">
                                <div class="col-lg-7">
                                    <div class="qoute-text">
                                        “Life before <i>Shipnow</i> was very
                                        easy Recently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system.”
                                    </div>
                                    <div class="customer-name mt-5">
                                        <h2>Frank Hardy</h2>
                                        <span>Customer</span>
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="user-img">
                                        <img class="img-fluid" src="{{ asset('img/img-client.png') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item ">
                            <div class="row">
                                <div class="col-lg-7">
                                    <div class="qoute-text">
                                        “Life before <i>Shipnow</i> was very
                                        easy Recently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system.”
                                    </div>
                                    <div class="customer-name mt-5">
                                        <h2>Frank Hardy</h2>
                                        <span>Customer</span>
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="user-img">
                                        <img class="img-fluid" src="{{ asset('img/img-client.png') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item ">
                            <div class="row">
                                <div class="col-lg-7">
                                    <div class="qoute-text">
                                        “Life before <i>Shipnow</i> was very
                                        easy Recently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system.”
                                    </div>
                                    <div class="customer-name mt-5">
                                        <h2>Frank Hardy</h2>
                                        <span>Customer</span>
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="user-img">
                                        <img class="img-fluid" src="{{ asset('img/img-client.png') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- discount -->
<div class="discount gap-bottom ">
    <div class="container-home ">
        <div class="row">
            <div class="col-lg-7 order-2 order-lg-1">
                <div class="home-heading">
                    <h2 class="mb-4">Discount for Startup and <br /> Small Business.</h2>
                </div>
                <p class="home-text-color ">
                    Silent sir say desire fat him letter. Whatever settling goodness too and honoured she building answered her. Strongly thoughts remember mr to do consider debating. Spirits musical behaved on we he farther letters.
                </p>
                <div class="d-flex mt-5">
                    <button class="btn btn-book-demo mr-3">Book a Demo</button>
                    <button class="btn btn-create-account">Create Account</button>
                </div>
            </div>
            <div class="col-lg-5 order-1 order-lg-2 mb-4 mb-lg-0">
                <div class="discount-video">
                    <img class="img-fluid" src="{{ asset('img/video.png') }}">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- blog -->
<div class="blog gap-bottom ">
    <div class="container-home ">
        <div class="row">
            <div class="col-12">
                <div class="home-heading">
                    <h2 class="mb-4">
                        <span>News/blog</span>
                        From Our Blog
                    </h2>
                </div>
            </div>
            <div class="col-12">
                <div class="d-flex flex-wrap">
                    <div class="col-lg-3 px-3 col-sm-6 blog-box mb-4">
                        <div class="blog-img position-relative">
                            <img class="img-fluid" src="{{ asset('img/blog-1.png') }}">
                            <div class="blog-date">06 Jan 2020</div>
                        </div>
                        <h3 class="blog-heading">Courier Services</h3>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy.
                        </p>
                        <a href="#">
                            read more
                            <img class="ml-2" src="{{ asset('img/arrow-small.png') }}">
                        </a>
                    </div>
                    <div class="col-lg-3 px-3 col-sm-6 blog-box mb-4">
                        <div class="blog-img position-relative">
                            <img class="img-fluid" src="{{ asset('img/blog-2.png') }}">
                            <div class="blog-date">06 Jan 2020</div>
                        </div>
                        <h3 class="blog-heading">Courier Services</h3>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy.
                        </p>
                        <a href="#">
                            read more
                            <img class="ml-2" src="{{ asset('img/arrow-small.png') }}">
                        </a>
                    </div>
                    <div class="col-lg-3 px-3 col-sm-6 blog-box mb-4">
                        <div class="blog-img position-relative">
                            <img class="img-fluid" src="{{ asset('img/blog-3.png') }}">
                            <div class="blog-date">06 Jan 2020</div>
                        </div>
                        <h3 class="blog-heading">Courier Services</h3>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy.
                        </p>
                        <a href="#">
                            read more
                            <img class="ml-2" src="{{ asset('img/arrow-small.png') }}">
                        </a>
                    </div>
                    <div class="col-lg-3 px-3 col-sm-6 blog-box mb-4">
                        <div class="blog-img position-relative">
                            <img class="img-fluid" src="{{ asset('img/blog-4.png') }}">
                            <div class="blog-date">06 Jan 2020</div>
                        </div>
                        <h3 class="blog-heading">Courier Services</h3>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy.
                        </p>
                        <a href="#">
                            read more
                            <img class="ml-2" src="{{ asset('img/arrow-small.png') }}">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection