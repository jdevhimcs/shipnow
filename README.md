## Pre-requisite installations

Composer
PHP Version -: 7.3
Mysql : 5.7

## About Project
    For installation Please proceed further steps:
    1) Git Clone: git clone git@bitbucket.org:zealsoft/shipnow.git
    2) Create .env file
        cp .env.example .env
    3) Generate an application key
        php artisan key:generate
    4) Setup database connection
        You need to create a database in your environment
        DB_CONNECTION=mysql
        DB_HOST=127.0.0.1
        DB_PORT=3306
        DB_DATABASE=db_name
        DB_USERNAME=db_user
        DB_PASSWORD=db_pass
    
    5) Install dependencies via Composer
        composer install
    6) Run migrate
        php artisan migrate:fresh --seed
