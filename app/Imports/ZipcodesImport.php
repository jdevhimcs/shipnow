<?php

namespace App\Imports;

use App\Models\Zipcode;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;


class ZipcodesImport implements ToModel, WithBatchInserts, WithChunkReading
{
	/**
	* @param array $row
	*
	* @return \Illuminate\Database\Eloquent\Model|null
	*/
	public function model(array $row)
	{
		return new Zipcode([
            'zip'    => $row[0],
            'city' => $row[1],
            'state'    => $row[2],
            'latitude' => $row[3],
			'longitude' => $row[4],
			'country' => strtolower($row[5])
		]);
	}

	public function batchSize(): int
    {
        return 1000;
    }
    
    public function chunkSize(): int
    {
        return 1000;
    }
}
