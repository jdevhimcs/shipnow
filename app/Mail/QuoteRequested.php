<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;

class QuoteRequested extends Mailable
{
	use Queueable, SerializesModels;

	public $data;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build(Request $request)
	{
		return $this->from([
				'address' => $request->email,
				'name' => $request->name
			])
			->to( env('ADMIN_EMAIL') )
			->markdown('emails.Quotes.new_quote_request')
			->subject('Request A Quote')
			->with('data', $this->data);
	}
}
