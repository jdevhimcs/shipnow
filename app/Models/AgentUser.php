<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AgentUser extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'agent_id',
        'user_id'
    ];

    /**
     * Agent user belongs to Agent
     *
     * @param 
     * @return
     */
    public function agent()
    {
        return $this->belongsTo('App\Models\User', 'agent_id');
    }

    /**
     * Agent user belongs to customer
     *
     * @param 
     * @return
     */
    public function customer()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
