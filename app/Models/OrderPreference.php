<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderPreference extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'reference',
        'broker_name',
        'email',
        'phone',
        'carrier_name',
        'label_size'
    ];

    /**
     * Order belongs to User
     *
     * @param 
     * @return
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Order can have Many Languages
     *
     * @param 
     * @return
     */
    public function languages()
    {
        return $this->belongsToMany('App\Models\Language')->withTimestamps();
    }

    /**
     * Order can have Many Unit
     *
     * @param 
     * @return
     */
    public function units()
    {
        return $this->belongsToMany('App\Models\Unit')->withTimestamps();
    }

    /**
	 * User has many references
	 *
	 * @param
	 * @return
	 */
	public function orderReferences()
	{
		return $this->hasMany('App\Models\OrderReference');
    }
    
    /**
	 * User has many references
	 *
	 * @param
	 * @return
	 */
	public function orderBrokers()
	{
		return $this->hasMany('App\Models\OrderBroker');
    }
    
    /**
	 * User has one default package
	 *
	 * @param
	 * @return
	 */
	public function orderPreferencePackage()
	{
		return $this->hasOne('App\Models\OrderPreferencePackage');
	}
}
