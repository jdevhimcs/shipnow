<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
    ];

    /**
	 * Scope dimension units
	 *
	 * @param  \Illuminate\Database\Eloquent\Builder  $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeDimensionUnits($query)
	{
		return $query->where('type', array_search('Dimension unit', Config('constants.UNIT_TYPE')));
	}

	/**
	 * Scope weight units
	 *
	 * @param  \Illuminate\Database\Eloquent\Builder  $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeWeightUnits($query)
	{
		return $query->where('type', array_search('Weight unit', Config('constants.UNIT_TYPE')));
	}

	/**
	 * Scope unit title
	 *
	 * @param  \Illuminate\Database\Eloquent\Builder  $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeTitle($query, $id)
	{
		return $query->where('id', $id)->firstOrFail();
	}

	/**
	 * Scope weight units
	 *
	 * @param  \Illuminate\Database\Eloquent\Builder  $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeDimensionWeightUnit($query, $dimensionId)
	{
		$result = $query->where('id', $dimensionId)->firstOrFail();

		// Check if dimension unit is imperial, then weight unit should be set as LB
		if ($result->title == 'IMPERIAL (In)') {
			$weight = Self::where('title', 'LBS')->firstOrFail();
			return $weight->id;
		}

		// Check if dimension unit is metric, then weight unit should be set as KG
		$weight = Self::where('title', 'KG')->firstOrFail();
		return $weight->id;
	}
}