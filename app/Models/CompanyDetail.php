<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyDetail extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'country_id',
        'state_id',
        'city',
        'company_name',
        'address',
        'postal_code',
        'email',
        'phone_no',
        'is_bill_same'
    ];

	/**
     * Company Belongs To User
     *
     * @param 
     * @return
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

	/**
     * Company Belongs To Country
     *
     * @param 
     * @return
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

	/**
     * Company Belongs To State
     *
     * @param 
     * @return
     */
    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }	
}
