<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Shipment extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'carrier',
        'identification_number',
        'tracking_number',
        'order_ref_id',
        'total_transportation_charges',
        'total_service_options_charges',
        'total_charges',
        'currency',
        'billing_unit',
        'billing_weight',
        'address_from',
        'city_from',
        'postal_from',
        'address_to',
        'city_to',
        'postal_to',
        'ship_date',
        'is_residential',
        'is_paid',
        'status',
        'file',
        'origin_company',
        'destination_company',
        'origin_attn_name',
        'destination_attn_name',
        'origin_phone',
        'destination_phone',
        'origin_email',
        'destination_email',
        'service_name',
        'package_type',
        'ref_code'
    ];

    protected $append = [
        'origin', 
        'destination',
        'formatted_ship_date'
    ];

    /**
     * Get the user
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the items for the shipment.
     */
    public function shipmentItems()
    {
        return $this->hasMany(ShipmentItem::class);
    }

	/**
     * Get the package results for the shipment.
     */
    public function shipmentPackageResults()
    {
        return $this->hasMany(ShipmentPackageResult::class);
    }

	/**
     * Get the transactions for the shipment.
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    /**
     * Scope CityOrigin
     *
	 * @param $query | string $cityOrigin
     * @return $query
     */
	public function scopeWhereCityOrigin($query, $cityOrigin)
	{
		return $query->where('city_from', 'LIKE', "%$cityOrigin%");
	}

    /**
     * Scope CityDestination
     *
	 * @param $query | string $cityDestination
     * @return $query
     */
	public function scopeWhereCityDestination($query, $cityDestination)
	{
		return $query->where('city_to', 'LIKE', "%$cityDestination%");
	}

	/**
     * Scope AddressOrigin
     *
	 * @param $query | string $addressOrigin
     * @return $query
     */
	public function scopeWhereAddressOrigin($query, $addressOrigin)
	{
		return $query->where('address_from', 'LIKE', "%$addressOrigin%");
	}

    /**
     * Scope AddressDestination
     *
	 * @param $query | string $addressDestination
     * @return $query
     */
	public function scopeWhereAddressDestination($query, $addressDestination)
	{
		return $query->where('address_to', 'LIKE', "%$addressDestination%");
	}

    /**
     * Scope Reference Number
     *
	 * @param $query | int $refNumber
     * @return $query
     */
	public function scopeWhereRefNumber($query, $refNumber)
	{
		return $query->where('order_ref_id', '=', $refNumber);
	}

    /**
     * Scope Carrier
     *
	 * @param $query | String $carrier
     * @return $query
     */
	public function scopeWhereCarrier($query, $carrier)
	{
		return $query->where('carrier', '=', $carrier);
	}

    /**
     * Scope Status
     *
	 * @param $query | String $status
     * @return $query
     */
	public function scopeWhereStatus($query, $status)
	{
		return $query->where('status', '=', $status);
	}

    /**
     * Scope Ship Date From / To
     *
	 * @param $query | $from_date | $to_date
     * @return $query
     */
	public function scopeWhereShipDateBetween($query, $from_date, $to_date)
	{	
        $dateFormat = 'Y-m-d';

		if (!empty($from_date) && !empty($to_date)) {
			return $query->whereBetween(
				DB::raw('DATE(ship_date)'),
				[
                    Carbon::parse($from_date)->format($dateFormat), 
                    Carbon::parse($to_date)->format($dateFormat)
                ]
			);
		}		
		
		return $query;
	}

	/**
     * Scope Order
     *
	 * @param $query | $orderByField | $orderBy
     * @return $query
     */
	public function scopeWhereOrder($query, $orderByField, $orderBy)
	{
		return $query->orderBy($orderByField, $orderBy);
	}

    /**
     * scopre With and where relatinoship
     *
	 * @param $query | $relation | $constraint
     * @return $query
     */
	public function scopeWithAndWhereHas($query, $relation, $constraint){
		return $query->whereHas($relation, $constraint)
					->with([$relation => $constraint]);
	}

    /**
     * Scope Tracking Number
     *
	 * @param $query | int $trackNumber
     * @return $query
     */
	public function scopeWhereTrackNumber($query, $trackNumber)
	{
		return $query->where('identification_number', '=', $trackNumber);
	}

    /**
     * Get Origin address
     *
	 * @param NULL
     * @return $address
     */
    public function getOriginAttribute()
    {
        $originAddress = strtoupper($this->city_from) . '<br>' . strtoupper($this->address_from) . '<br>' . strtoupper($this->postal_from);  
        
        return $originAddress;
    }
    
    /**
     * Get Destination address
     *
	 * @param NULL
     * @return $address
     */
    public function getDestinationAttribute()
    {
        $destinationAddress = strtoupper($this->city_to) . '<br>' . strtoupper($this->address_to) . '<br>' . strtoupper($this->postal_to);  
        
        return $destinationAddress;
    }

    /**
	 * Get Formatted Ship Date
	 * 
	 * @param $date $value
	 * @return $date
	 */
	public function getFormattedShipDateAttribute($value)
	{
		$dateFormat = 'd F, Y';
		return Carbon::parse($this->ship_date)->format($dateFormat);
	}

}
