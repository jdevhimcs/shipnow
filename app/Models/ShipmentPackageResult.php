<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShipmentPackageResult extends Model
{
	use HasFactory;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'shipment_id',
		'tracking_number',
		'service_options_charges',
		'options_currency',
		'label_image'
	];

	/**
	 * Scope Tracking Number
	 *
	 * @param $query | String $trackingNumber
	 * @return $query
	 */
	public function scopeWhereTrackingNumber($query, $trackingNumber)
	{
		return $query->where('tracking_number', '=', $trackingNumber);
	}
}
