<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BillingAddress extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'user_id',
       'address',
       'country',
       'state',
       'city',
       'postal_code',
       'email',
       'phone_no'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
