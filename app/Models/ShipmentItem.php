<?php

namespace App\Models;

use App\Models\Unit;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShipmentItem extends Model
{
    use HasFactory;

    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'shipment_id',
		'length',
		'width',
		'height',
		'weight',
		'dimension_unit_id',
		'weight_unit_id'
	];

	/**
     * Get Dimension Unit
     *
     * @param 
     * @return
     */
    public function dimensionUnit()
    {
        return $this->belongsTo(Unit::class, 'dimension_unit_id');
    }

    /**
     * Get weight Unit
     *
     * @param 
     * @return
     */
    public function weightUnit()
    {
        return $this->belongsTo(Unit::class, 'weight_unit_id');
    }
}
