<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements MustVerifyEmail
{
	use HasFactory, Notifiable, SoftDeletes;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'role_id',
		'email',
		'password',
		'stripe_key',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
	];

	protected $appends = ['full_name'];

	public function getFullNameAttribute()
	{
		return $this->first_name. ' '. $this->last_name;
	}

	/**
	 * User belongs to role
	 *
	 * @param
	 * @return
	 */
	public function role()
	{
		return $this->belongsTo('App\Models\Role');
	}

	/**
	 * User has one profile
	 *
	 * @param
	 * @return
	 */
	public function profile()
	{
		return $this->hasone('App\Models\UserProfile');
	}

	/**
	 * User has many assign customers
	 *
	 * @param
	 * @return
	 */
	public function assignCustomers()
	{
		return $this->hasMany('App\Models\AgentUser', 'agent_id');
	}

	/**
	 * User has one company detail
	 *
	 * @param
	 * @return
	 */
	public function companyDetail()
	{
		return $this->hasone('App\Models\CompanyDetail');
	}

	/**
	 * User has many order preferences
	 *
	 * @param
	 * @return
	 */
	public function orderPreferences()
	{
		return $this->hasMany('App\Models\OrderPreference');
	}

	/**
	 * User has many Notification Settings
	 *
	 * @param
	 * @return
	 */
	public function notificationSettings()
	{
		 return $this->belongsToMany('App\Models\NotificationSetting')->withTimestamps();
	}

	/**
	 * User has one Billing Address
	 *
	 * @param
	 * @return
	 */
	public function billingAddress()
	{
		return $this->hasone('App\Models\BillingAddress');
	}
	
	public function products()
	{
		return $this->hasMany('App\Models\Products');
	}

	/**
     * Get the stripe customer account cards
     */
    public function cards()
    {
        return $this->hasOne(UserCard::class);
	}
	
	/**
	 * User has one payable Account
	 *
	 * @param
	 * @return
	 */
	public function accountPayableDetail()
	{
		return $this->hasone('App\Models\AccountPayableDetail');
	}

 	/**
     * Get the shipments for the user.
     */
    public function shipments()
    {
        return $this->hasMany(Shipment::class);
	}
	
	/**
     * Check role existence
     */
    public function hasRole($role)
    {
        if ($this->role()->where('name', $role)->first()) {
            return true;
        }

        return false;
    }
}
