<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'name',
       'countrty_id'
    ];

	/**
     * Company Belongs To Country
     *
     * @param 
     * @return
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }
}
