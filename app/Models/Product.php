<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','name','description','length','height','weight','width','class'];
    protected $append = ['weight_with_unit','dimension'];

    public function getWeightWithUnitAttribute()
    {
        return $this->weight.'Kg';
    }
    public function getDimensionAttribute()
    {
      return  ' <span>Length <strong>'.$this->length.'cm</strong> </span>
        <span>Width <strong>'.$this->width.'cm</strong></span>
        <span>Height <strong>'.$this->height.'cm</strong></span>';
        // return $dimension;
    }
    public function user()
    {
        return $this->belongsTo('App\Models\user');
    }
}
