<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Storage;

class UserProfile extends Model
{
    use HasFactory;


    protected $fillable = [
        'image',
        'user_id',
        'first_name',
        'last_name',
        'phone_no',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    protected $appends = ['thumb_image', 'full_name'];

    public function getThumbImageAttribute()
    {
        if($this->image)
        {
            $thumbnail = 'avatar/'.$this->user_id. '/thumb_'.$this->image;

            if(Storage::disk('public')->exists($thumbnail)){
                return Storage::url($thumbnail);
            }
        }
        return 'img/profile.png';
    }

    public function getFullNameAttribute()
    {
        return ucfirst($this->first_name). ' '. ucfirst($this->last_name);
    }

}
