<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddressBook extends Model
{
    use HasFactory;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'company_name',
        'address',
        'country_id',
        'state_id',
        'city',
        'postal_code',
        'email',
        'phone_no',
        'ship_hours_from',
        'ship_hours_to',
        'note',
        'is_picked',
        'is_business_dock',
        'is_residence',
        'appointment_delivery',
        'straight_truck',
    ];
}
