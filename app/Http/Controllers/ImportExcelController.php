<?php

namespace App\Http\Controllers;

use App\Imports\ZipcodesImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\Zipcode;

class ImportExcelController extends Controller
{
    function index()
    {
        $data = []; //Zipcode::where('country', '!=', 'ca')->orderBy('id', 'ASC')->get();
        return view('import.import_excel', compact('data'));
    }

    function store(Request $request)
    {
        // ini_set('max_execution_time', 0);
        // ini_set('memory_limit', '2024M');
        $this->validate($request, [
            'select_file'  => 'required|mimes:xls,xlsx'
        ]);

        $data  = Excel::import(new ZipcodesImport, $request->file('select_file')->store('temp'));

        return back()->with('success', 'Excel Data Imported successfully.');
    }
}
