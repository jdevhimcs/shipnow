<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DOMDocument;
use SimpleXMLElement;
use Illuminate\Support\Facades\Http;

class CanadaPostController extends Controller
{
	/**
     * Base URLs.
     */
    const BASE_URL_DEVELOPMENT = 'https://ct.soa-gw.canadapost.ca';
    const BASE_URL_PRODUCTION = 'https://soa-gw.canadapost.ca';

    /**
     * Get Rates from the canada post
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getRates(Request $request)
    {
    	try {

	    	$request->request->add([
	    		'originPostalCode' => 'H2B1A0',
	    		'destinationPostalCode' => 'K1K4T3',
	    		'weight' => 12,
	    		'length' => 12,
	    		'height' => 12,
	    		'width' => 12,
				'service_codes' => [
					'DOM.RP',
					'DOM.EP'
				],
				'option_codes' => [
					'SO'
				],
				'promo_code' => ''	//In case of promocode, add the code here
	    	]);

	    	// Get Configuration
	        $configuration = $this->setCredentials();

	        // Get base url
	        $baseUrl = $this->baseUrl();

	    	// Canada Post API needs all postal codes to be uppercase and no spaces.
	    	//source postal code
	        $originPostalCode = strtoupper(str_replace(' ', '', $request->get('originPostalCode')));

	        // Destination postal code
	        $DestinationPostalCode = strtoupper(str_replace(' ', '', $request->get('destinationPostalCode')));

	        // Data based on canada post
	        $content = [
	            'customer-number' => Config('canadapost.CANADA_POST_CUSTOMER_NUMBER'),
	            'contract-id' => $configuration['contract_id'],
	            'promo-code' => $request->get('promo_code'),
	            'quote-type' => $request->get('quote_type'),
	            'expected-mailing-date' => $request->get('expected_mailing_date'),
	            'parcel-characteristics' => [
	                'weight' => $request->get('weight'),
	            ],
	            'origin-postal-code' => $originPostalCode,
	            'destination' => [
	                'domestic' => [
	                    'postal-code' => $DestinationPostalCode,
	                ],
	            ],
	        ];

	       	// Validate service codes
	        if (!empty($request->get('service_codes'))) {
	            $content['services']['service-code'] = $this->parseServiceCodes($request->get('service_codes'));
	        }

	        // Validate option codes
	        if (!empty($request->get('option_codes'))) {
	            $content['options']['option'] = $this->parseOptionCodes($request->get('option_codes'));
	        }

	        // Add parcel characteristics of length
	        // Details of the parcel dimensions in centimeters.
	        if (!empty($request->get('length'))) {
	        	$content['parcel-characteristics']['dimensions']['length'] = $request->get('length');
	        }

	        // Add parcel characteristics of width
	        // Details of the parcel dimensions in centimeters.
	        if (!empty($request->get('width'))) {
	        	$content['parcel-characteristics']['dimensions']['width'] = $request->get('width');
	        }

	        // Add parcel characteristics of height
	        // Details of the parcel dimensions in centimeters.
	        if (!empty($request->get('height'))) {
	        	$content['parcel-characteristics']['dimensions']['height'] = $request->get('height');
	        }

	        // true | false
	        // defaults to "false"
	        // true indicates that the parcel will be unpackaged (e.g. tires)
	        if (!empty($request->get('unpackaged'))) {
	        	$content['unpackaged'] = $request->get('unpackaged');
	        }

	        // true | false
	        // defaults to "false"
			// true indicates that the object will be shipped in a mailing tube
	        if (!empty($request->get('mailing-tube'))) {
	        	$content['mailing-tube'] = $request->get('mailing-tube');
	        }

	        // true | false
	        // defaults to "false"
			// true indicates that the object has oversized dimensions
			// automatically set correctly if dimensions are provided
	        if (!empty($request->get('oversized'))) {
	        	$content['oversized'] = $request->get('oversized');
	        }

	        // Defines destination in the United States.
	        // Used when country code = US. Format is 5 digits or 5-4 (for extended Zip Codes)
	        // E.g. 12345 or 12345-6789
	        if (!empty($request->get('united-states')) && !empty($request->get('zip-code'))) {
	        	$content['destination']['united-states']['zip-code'] = strtoupper(str_replace(' ', '', $request->get('zip-code')));
	        }

	        // Defines an international destination (destination other than the United States or Canada). Only one of international, us-destination or domestic can be specified.
	        // The destination country code. Standard 2-character country code (valid country code other than CA or US)
	        if (!empty($request->get('international')) && !empty($request->get('country-code'))) {
	        	$content['destination']['international']['country-code'] = $request->get('country-code');
	        	$content['destination']['international']['postal-code'] = strtoupper(str_replace(' ', '', $request->get('postal-code')));
	        }

	        // Create XML
	        $document = $xml = new DOMDocument();
	        $xml->formatOutput = true;

	        $root = $xml->createElement('mailing-scenario');

	        // Add Attribute and value
	        $rootAttribute = $xml->createAttribute('xmlns');
	        $rootAttribute->value='http://www.canadapost.ca/ws/ship/rate-v4';

	        $root->appendChild($rootAttribute);

	        $root = $xml->appendChild($root);

	        // Customer Number
	        $customerNumber = $xml->createElement('customer-number', $content['customer-number']);

	        $root->appendChild($customerNumber);

	        // Contract id (https://www.canadapost.ca/cpo/mc/business/productsservices/developers/services/rating/getrates/default.jsf)
	        if (!empty($content['contract-id'])) {
	        	$contractId = $xml->createElement('contract-id', $content['contract-id']);
	        	$root->appendChild($contractId);
	        }

	        // In case of promo code
	        if (!empty($content['promo-code'])) {
	        	$promoCode = $xml->createElement('promo-code', $content['promo-code']);
	        	$root->appendChild($promoCode);
	        }

	        // In case of quote_type code
	        if (!empty($content['quote-type'])) {
	        	$quoteType = $xml->createElement('quote-type', $content['quote-type']);
	        	$root->appendChild($quoteType);
	        }

	        // In case of expecting mailing date (YYYY-MM-DD date format)
	        if (!empty($content['expected-mailing-date'])) {
	        	$expectedMailingDate = $xml->createElement('expected-mailing-date', $content['expected-mailing-date']);
	        	$root->appendChild($expectedMailingDate);
	        }

	        // In case of adding options
	        if (!empty($content['options'])) {
	        	$optionsRoot = $xml->createElement('options');
	        	$subOptionRoot = $xml->createElement('option');

	        	// Add all optioncode
	        	foreach ($content['options']['option'] as $key => $optionCode) {
	        		$optionCode = $xml->createElement('option-code', $optionCode['option-code']);
	        		$subOptionRoot->appendChild($optionCode);
	        	}

	        	$optionsRoot->appendChild($subOptionRoot);

	        	$root->appendChild($optionsRoot);
	        }

	        // In case of parcel characteristics
	    	$parcel = $xml->createElement('parcel-characteristics');
	        if (!empty($content['parcel-characteristics']['weight'])) {
	        	$weightNode = $xml->createElement('weight', $content['parcel-characteristics']['weight']);

	        	$parcel->appendChild($weightNode);
	        }

	        if (!empty($content['parcel-characteristics']['dimensions'])) {
	        	$dimensions = $xml->createElement('dimensions');
	        	$length = $xml->createElement('length', $content['parcel-characteristics']['dimensions']['length']);
	        	$width = $xml->createElement('width', $content['parcel-characteristics']['dimensions']['width']);
	        	$height = $xml->createElement('height', $content['parcel-characteristics']['dimensions']['height']);

	        	$dimensions->appendChild($length);
	        	$dimensions->appendChild($width);
	        	$dimensions->appendChild($height);

	        	$parcel->appendChild($dimensions);
	        }

	    	$root->appendChild($parcel);

	    	// In case of unpackaged
	    	// true | false
			// defaults to "false"
			// true indicates that the parcel will be unpackaged (e.g. tires)
	    	if (!empty($content['unpackaged'])) {
	        	$unpackaged = $xml->createElement('unpackaged', $content['unpackaged']);

	        	$root->appendChild($unpackaged);
	        }

			// true | false
			// defaults to "false"
			// true indicates that the object will be shipped in a mailing tube
			if (!empty($content['mailing-tube'])) {
	        	$mailingTube = $xml->createElement('mailing-tube', $content['mailing-tube']);

	        	$root->appendChild($mailingTube);
	        }

			// true | false
			// defaults to "false"
			// true indicates that the object has oversized dimensions
			// automatically set correctly if dimensions are provided
			if (!empty($content['oversized'])) {
	        	$oversized = $xml->createElement('oversized', $content['oversized']);

	        	$root->appendChild($oversized);
	        }

	        // Specifies the services to rate for this mailing scenario.
	        if (!empty($content['services'])) {
	        	$services = $xml->createElement('services');

	        	foreach ($content['services']['service-code'] as $key => $serviceCode) {
	        		$serviceCode = $xml->createElement('service-code', $serviceCode);
	        		$services->appendChild($serviceCode);
	        	}

	        	$root->appendChild($services);
	        }

	        // Postal Code from which the parcel will be sent.
			// Format ANANAN (only accepted with uppercase)
	        if (!empty($content['origin-postal-code'])) {
	        	$originPostalCode = $xml->createElement('origin-postal-code', $content['origin-postal-code']);

	        	$root->appendChild($originPostalCode);
	        }

	        // Defines the destination of the parcel.
	        // In case of canada
	    	$destination = $xml->createElement('destination');
	        if (!empty($content['destination']['domestic'])) {
	    		$domestic = $xml->createElement('domestic');
	    		$domesticPostalCode = $xml->createElement('postal-code', $content['destination']['domestic']['postal-code']);

	        	$domestic->appendChild($domesticPostalCode);
	        	$destination->appendChild($domestic);
	        }

	        // In case of USA
	        // Defines destination in the United States.
	        if (!empty($content['destination']['united-states'])) {
	    		$unitedStates = $xml->createElement('united-states');
	    		$unitedStatesPostalCode = $xml->createElement('zip-code', $content['destination']['united-states']['zip-code']);

	        	$unitedStates->appendChild($unitedStatesPostalCode);
	        	$destination->appendChild($unitedStates);
	        }

	        // Defines an international destination (destination other than the United States or Canada). Only one of international, us-destination or domestic can be specified.
	        if (!empty($content['destination']['international'])) {
	    		$international = $xml->createElement('international');
	    		$countryCode = $xml->createElement('country-code', $content['destination']['international']['country-code']);
	    		$postalCode = $xml->createElement('country-code', $content['destination']['international']['postal-code']);

	        	$international->appendChild($countryCode);
	        	$international->appendChild($postalCode);
	        	$destination->appendChild($international);
	        }

	        $root->appendChild($destination);
	        $xml = $xml->saveXML();

	        $response = Http::withBasicAuth($configuration['username'], $configuration['password'])
	        ->withHeaders([
				'Accept' => 'application/vnd.cpc.ship.rate-v4+xml',
				'Content-Type' => 'application/vnd.cpc.ship.rate-v4+xml'
			])->send('POST', $baseUrl.'/rs/ship/price', [
				'body' => $xml
			]);

			$responseBody = $response->body();
			$xml = simplexml_load_string($responseBody, "SimpleXMLElement", LIBXML_NOCDATA);
			$json = json_encode($xml);
			$output = json_decode($json, TRUE);

			dd($output);
    	} catch (\Exception $error) {
    		dd($error);
    	}
    }

    /**
     * Get the Canada Post specific service codes,
     *
     * @return array
     *   The array of service codes.
     *
     * @see https://www.canadapost.ca/cpo/mc/business/productsservices/developers/services/rating/getrates/default.jsf
     */
    public static function getServiceCodes()
    {
        return [
            'DOM.EP' => 'Expedited Parcel',
            'DOM.RP' => 'Regular Parcel',
            'DOM.PC' => 'Priority',
            'DOM.XP' => 'Xpresspost',
            'DOM.XP.CERT' => 'Xpresspost Certified',
            'DOM.LIB' => 'Library Materials',
            'USA.EP' => 'Expedited Parcel USA',
            'USA.PW.ENV' => 'Priority Worldwide Envelope USA',
            'USA.PW.PAK' => 'Priority Worldwide pak USA',
            'USA.PW.PARCEL' => 'Priority Worldwide Parcel USA',
            'USA.SP.AIR' => 'Small Packet USA Air',
            'USA.TP' => 'Tracked Packet – USA',
            'USA.TP.LVM' => 'Tracked Packet – USA (LVM) (large volume mailers)',
            'USA.XP' => 'Xpresspost USA',
            'INT.XP' => 'Xpresspost International',
            'INT.IP.AIR' => 'International Parcel Air',
            'INT.IP.SURF' => 'International Parcel Surface',
            'INT.PW.ENV' => 'Priority Worldwide Envelope Int’l',
            'INT.PW.PAK' => 'Priority Worldwide pak Int’l',
            'INT.PW.PARCEL' => 'Priority Worldwide parcel Int’l',
            'INT.SP.AIR' => 'Small Packet International Air',
            'INT.SP.SURF' => 'Small Packet International Surface',
            'INT.TP' => 'Tracked Packet – International',
        ];
    }

    /**
     * Get the Canada Post-specific option codes,
     *
     * @return array
     *   The array of option codes.
     *
     * @see https://www.canadapost.ca/cpo/mc/business/productsservices/developers/services/rating/getrates/default.jsf
     */
    public static function getOptionCodes()
    {
        return [
            'SO' => 'Signature (SO)',
            'PA18' => 'Proof of Age Required - 18 (PA18)',
            'PA19' => 'Proof of Age Required - 19 (PA19)',
            'HFP' => 'Card for pickup (HFP)',
            'DNS' => 'Do not safe drop (DNS)',
            'LAD' => 'Leave at door - do not card (LAD)',
        ];
    }

    /**
     * Helper function to extract the option codes.
     *
     * @param array $optionCodes
     *   The optionCodes array.
     *
     * @return array
     *   The list of options with the option-code.
     */
    protected function parseOptionCodes(array $optionCodes)
    {
        $valid_options = [];
        foreach ($optionCodes as $optionCode) {
            if (!array_key_exists(strtoupper($optionCode), self::getOptionCodes())) {
                $message = sprintf(
                    'Unsupported option code: "%s". Supported options are %s',
                    $optionCode,
                    implode(', ', array_keys(self::getOptionCodes()))
                );
                throw new \InvalidArgumentException($message);
            }

            $valid_options[] = [
                'option-code' => $optionCode,
            ];
        }

        return $valid_options;
    }

    /**
     * Helper function to extract the service codes.
     *
     * @param array $serviceCodes
     *   The serviceCodes array.
     *
     * @return array
     *   The list of services to look up.
     */
    protected function parseServiceCodes(array $serviceCodes)
    {
        $services = [];
        foreach ($serviceCodes as $serviceCode) {
            if (!array_key_exists(strtoupper($serviceCode), self::getServiceCodes())) {
                $message = sprintf(
                    'Unsupported service code: "%s". Supported services are %s',
                    $serviceCode,
                    implode(', ', array_keys(self::getServiceCodes()))
                );
                throw new \InvalidArgumentException($message);
            }
            $services[] = $serviceCode;
        }

        return $services;
    }

    /**
     * Set the API configuration array for the Client.
     *
     * @param array $config
     *   The configuration array.
     */
    protected function setCredentials(array $config = [])
    {
        if (empty(Config('canadapost.CANADA_POST_USERNAME')) || empty(Config('canadapost.CANADA_POST_PASSWORD')) || empty(Config('canadapost.CANADA_POST_CUSTOMER_NUMBER'))) {
            $message = 'A username, password and customer number are required for authenticated to the Canada Post API.';
            throw new \InvalidArgumentException($message);
        }

        return [
        	'username' => Config('canadapost.CANADA_POST_USERNAME'),
        	'password' => Config('canadapost.CANADA_POST_PASSWORD'),
        	'customer_number' => Config('canadapost.CANADA_POST_CUSTOMER_NUMBER'),
        	'contract_id' => Config('canadapost.CANADA_POST_CONTRACT_ID') ?? null
        ];
    }

    /**
     * Return the base url for the Canada Post API.
     *
     * @param array $config
     *
     * @return mixed|string
     *   The base url.
     * @throws \InvalidArgumentException
     */
    protected function baseUrl()
    {
    	if (Config('canadapost.CANADA_POST_SANDBOX')) {
    		return self::BASE_URL_DEVELOPMENT;
    	}

    	return self::BASE_URL_PRODUCTION;
    }
}
