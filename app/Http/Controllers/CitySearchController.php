<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Zipcode;

class CitySearchController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchCity(Request $request)
    {
        $response = [];

        if ($request->has('search')) {
			$search = $request->search;
            
            $results = Zipcode::where('zip', 'LIKE', "%$search%")
            ->orWhere('city', 'LIKE', "%$search%")
            // ->where('country', 'ca')
			->get();

            foreach($results as $result) {
                $response[] = [
                    'value' => $result->city,
                    'label' => $result->city . ' | ' . $result->zip,
                    'city' => $result->city,
					'state' => $result->state,
                    'country' => $result->country,
                    'zipcode' => $result->zip,
                ];
            }
        }

        return response()->json($response);
    }
}
