<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use FedEx\RateService\Request as FedExRequest;
use FedEx\RateService\ComplexType;
use FedEx\RateService\SimpleType;
use FedEx\OpenShipService\Request as OpenShipRequest;
use FedEx\OpenShipService\ComplexType as OpenShipComplexType;
use FedEx\OpenShipService\SimpleType as OpenShipSimpleType;

class FedExController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rateRequest = new ComplexType\RateRequest();

        //authentication & client details
		$rateRequest->WebAuthenticationDetail->UserCredential->Key = Config('fedex.FEDEX_API_KEY');
		$rateRequest->WebAuthenticationDetail->UserCredential->Password = Config('fedex.FEDEX_PASSWORD');
		$rateRequest->ClientDetail->AccountNumber = Config('fedex.FEDEX_ACCOUNT_NUMBER');
		$rateRequest->ClientDetail->MeterNumber = Config('fedex.FEDEX_METER_NUMBER');

		$rateRequest->TransactionDetail->CustomerTransactionId = 'testing rate service request';

		//version
		$rateRequest->Version->ServiceId = 'crs';
		$rateRequest->Version->Major = 28;
		$rateRequest->Version->Minor = 0;
		$rateRequest->Version->Intermediate = 0;

		$rateRequest->ReturnTransitAndCommit = true;

		//shipper

		$rateRequest->RequestedShipment->PreferredCurrency = 'USD';
		$rateRequest->RequestedShipment->Shipper->Address->StreetLines = ['10 Fed Ex Pkwy'];
		$rateRequest->RequestedShipment->Shipper->Address->City = 'Memphis';
		$rateRequest->RequestedShipment->Shipper->Address->StateOrProvinceCode = 'TN';
		$rateRequest->RequestedShipment->Shipper->Address->PostalCode = 38115;
		$rateRequest->RequestedShipment->Shipper->Address->CountryCode = 'US';

		//recipient
		$rateRequest->RequestedShipment->Recipient->Address->StreetLines = ['Farmcrest Court 13450'];
		$rateRequest->RequestedShipment->Recipient->Address->City = 'Herndon';
		$rateRequest->RequestedShipment->Recipient->Address->StateOrProvinceCode = 'VA';
		$rateRequest->RequestedShipment->Recipient->Address->PostalCode = 20171;
		$rateRequest->RequestedShipment->Recipient->Address->CountryCode = 'US';

		//shipping charges payment
		$rateRequest->RequestedShipment->ShippingChargesPayment->PaymentType = SimpleType\PaymentType::_SENDER;

		//rate request types
		$rateRequest->RequestedShipment->RateRequestTypes = [SimpleType\RateRequestType::_LIST];

		// For double
		// $rateRequest->RequestedShipment->RateRequestTypes = [SimpleType\RateRequestType::_PREFERRED, SimpleType\RateRequestType::_LIST];


		// Set Package type
		$rateRequest->RequestedShipment->PackagingType = 'YOUR_PACKAGING'; //valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, FEDEX_ENVELOPE

		$rateRequest->RequestedShipment->PackageCount = 1;

		//create package for single line items
		$rateRequest->RequestedShipment->RequestedPackageLineItems = [new ComplexType\RequestedPackageLineItem()];

		// Create package for double line items
		// $rateRequest->RequestedShipment->RequestedPackageLineItems = [new ComplexType\RequestedPackageLineItem(), new ComplexType\RequestedPackageLineItem()];

		//package 1
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Weight->Value = 1100;
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Weight->Units = SimpleType\WeightUnits::_LB; //LB or KG
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Dimensions->Length = 10;
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Dimensions->Width = 10;
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Dimensions->Height = 3;
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Dimensions->Units = SimpleType\LinearUnits::_IN;
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->GroupPackageCount = 1;


		// $rateRequest->RequestedShipment->SpecialServicesRequested = 1;

		// $rateRequest->RequestedShipment->RequestedPackageLineItems[0]->SpecialServicesRequested = 1;

		// Set signature option
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->SpecialServicesRequested->SpecialServiceTypes= 'SIGNATURE_OPTION';
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->SpecialServicesRequested->SignatureOptionDetail->OptionType = SimpleType\SignatureOptionType::_DIRECT;

		$rateServiceRequest = new FedExRequest();

		// For production
		if (!Config('fedex.FEDEX_SANDBOX')) {
			$rateServiceRequest->getSoapClient()->__setLocation(FedExRequest::PRODUCTION_URL); //use production URL
		}

		$rateReply = $rateServiceRequest->getGetRatesReply($rateRequest); // send true as the 2nd argument to return the SoapClient's stdClass response.

		$rates = [];


		// In case of failure and warning
		if ($rateReply->HighestSeverity == 'FAILURE' || $rateReply->HighestSeverity == 'ERROR' || $rateReply->HighestSeverity == 'WARNING') {
			$errorMessage = $this->getCombinedErrorMessage($rateReply->Notifications);
			dd($errorMessage);
		}

		// In case of success response
		if ($rateReply->HighestSeverity == 'SUCCESS') {
		    foreach ($rateReply->RateReplyDetails as $rate) {
		        $rates[$rate->ServiceType] = $rate->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Amount;
		    }
		}

		dd($rates);
    }

    /**
     * Get combined error message of all types
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getCombinedErrorMessage($notifications)
    {
    	$messages = '';
    	foreach($notifications as $noteKey => $note) {
    		$notification = $note->toArray();
    		$messages.= $notification['Message'];

    		if (count($notifications) !== $noteKey+1) {
    			$messages.= '<br>';
    		}
		}

		return $messages;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function ltl(Request $request)
    {
        $rateRequest = new ComplexType\RateRequest();

        //authentication & client details
		$rateRequest->WebAuthenticationDetail->UserCredential->Key = Config('fedex.FEDEX_API_KEY');
		$rateRequest->WebAuthenticationDetail->UserCredential->Password = Config('fedex.FEDEX_PASSWORD');
		$rateRequest->ClientDetail->AccountNumber = Config('fedex.FEDEX_ACCOUNT_NUMBER');
		$rateRequest->ClientDetail->MeterNumber = Config('fedex.FEDEX_METER_NUMBER');

		$rateRequest->TransactionDetail->CustomerTransactionId = 'testing rate service request';

		//version
		$rateRequest->Version->ServiceId = 'crs';
		$rateRequest->Version->Major = 28;
		$rateRequest->Version->Minor = 0;
		$rateRequest->Version->Intermediate = 0;

		$rateRequest->ReturnTransitAndCommit = true;

		//shipper

		$rateRequest->RequestedShipment->PreferredCurrency = 'USD';
		$rateRequest->RequestedShipment->Shipper->Address->StreetLines = ['395 Boul de L\'ile'];
		$rateRequest->RequestedShipment->Shipper->Address->City = 'ETOBICOKE';
		$rateRequest->RequestedShipment->Shipper->Address->StateOrProvinceCode = 'ON';
		$rateRequest->RequestedShipment->Shipper->Address->PostalCode = 'M9W5V8';
		$rateRequest->RequestedShipment->Shipper->Address->CountryCode = 'CA';

		//recipient
		$rateRequest->RequestedShipment->Recipient->Address->StreetLines = ['726 CHESTER AVE'];
		$rateRequest->RequestedShipment->Recipient->Address->City = 'SURREY';
		$rateRequest->RequestedShipment->Recipient->Address->StateOrProvinceCode = 'BC';
		$rateRequest->RequestedShipment->Recipient->Address->PostalCode = 'V3Z0N2';
		$rateRequest->RequestedShipment->Recipient->Address->CountryCode = 'CA';

		//shipping charges payment
		$rateRequest->RequestedShipment->ShippingChargesPayment->PaymentType = SimpleType\PaymentType::_SENDER;

		//rate request types
		$rateRequest->RequestedShipment->RateRequestTypes = [SimpleType\RateRequestType::_LIST];

		// For double
		// $rateRequest->RequestedShipment->RateRequestTypes = [SimpleType\RateRequestType::_PREFERRED, SimpleType\RateRequestType::_LIST];


		// Set Package type
		$rateRequest->RequestedShipment->PackagingType = 'YOUR_PACKAGING'; //valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, FEDEX_ENVELOPE

		$rateRequest->RequestedShipment->PackageCount = 1;

		$freightShipmentDetail = new ComplexType\FreightShipmentDetail();

		// Indicates the role of the party submitting the transaction.
		$freightShipmentDetail->Role = 'CONSIGNEE';

		// Account number used with FEDEX_FREIGHT service.
		$freightShipmentDetail->FedExFreightAccountNumber = 672589576;

		// Used for validating FedEx Freight account number and (optionally) identifying third party payment on the bill of lading.
		$contactAndAddress = new ComplexType\ContactAndAddress();

		// ToDO add contact

		$contactAndAddress->Address->StreetLines = '395 BOUL DE L\'ILE';
		$contactAndAddress->Address->City = 'PINCOURT';
		$contactAndAddress->Address->StateOrProvinceCode = 'PQ';
		$contactAndAddress->Address->PostalCode = 'J7W9Y3';
		$contactAndAddress->Address->CountryCode = 'CA';

		$freightShipmentDetail->FedExFreightBillingContactAndAddress = $contactAndAddress;

		//create package for single line items
		$freightShipmentDetail->LineItems = [new ComplexType\FreightShipmentLineItem()];

		// Create package for double line items
		// $freightShipmentDetail->LineItems = [new ComplexType\FreightShipmentLineItem(), new ComplexType\FreightShipmentLineItem()];

		//package 1
		$freightShipmentDetail->LineItems[0]->Id = 1; // A unique identifier assigned to this line item.

		// FreightClassType for freight class type
		// It should include in all lineitems
		// Only for LTL
		$freightShipmentDetail->LineItems[0]->FreightClass = SimpleType\FreightClassType::_CLASS_050;

		// Specification of handling-unit packaging for this commodity or class line.
		$freightShipmentDetail->LineItems[0]->Packaging = SimpleType\PhysicalPackagingType::_BOX;

		// Number of pieces for this commodity or class line.
		// $freightShipmentDetail->LineItems[0]->Pieces = 1;

		// Customer-provided description for this commodity or class line.
		$freightShipmentDetail->LineItems[0]->Description = 'Test Description';

		// Weight for this commodity or class line.
		$freightShipmentDetail->LineItems[0]->Weight->Value = 10;
		$freightShipmentDetail->LineItems[0]->Weight->Units = SimpleType\WeightUnits::_LB; //LB or KG
		$freightShipmentDetail->LineItems[0]->Dimensions->Length = 10;
		$freightShipmentDetail->LineItems[0]->Dimensions->Width = 10;
		$freightShipmentDetail->LineItems[0]->Dimensions->Height = 3;
		$freightShipmentDetail->LineItems[0]->Dimensions->Units = SimpleType\LinearUnits::_IN;


		// Add all freight shipment details
		$rateRequest->RequestedShipment->FreightShipmentDetail = $freightShipmentDetail;

		// Set signature option
		// $shipmentSpecialServicesRequested = new ComplexType\ShipmentSpecialServicesRequested();
		// $shipmentSpecialServicesRequested->SpecialServiceTypes= 'SIGNATURE_OPTION';

		// $rateRequest->RequestedShipment->SpecialServicesRequested->SpecialServiceTypes= 'SIGNATURE_OPTION';
		// $rateRequest->RequestedShipment->SpecialServicesRequested->SignatureOptionDetail->OptionType = SimpleType\SignatureOptionType::_DIRECT;

		// dd($rateRequest);

		$rateServiceRequest = new FedExRequest();

		// For production
		if (!Config('fedex.FEDEX_SANDBOX')) {
			$rateServiceRequest->getSoapClient()->__setLocation(FedExRequest::PRODUCTION_URL); //use production URL
		}

		$rateReply = $rateServiceRequest->getGetRatesReply($rateRequest); // send true as the 2nd argument to return the SoapClient's stdClass response.

		$rates = [];


		// In case of failure and warning
		if ($rateReply->HighestSeverity == 'FAILURE' || $rateReply->HighestSeverity == 'ERROR' || $rateReply->HighestSeverity == 'WARNING') {
			$errorMessage = $this->getCombinedErrorMessage($rateReply->Notifications);
			dd($errorMessage);
		}

		// In case of success response
		if ($rateReply->HighestSeverity == 'SUCCESS') {
		    foreach ($rateReply->RateReplyDetails as $rate) {
		        $rates[$rate->ServiceType] = $rate->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Amount;
		    }
		}

		dd($rates);
    }

    /**
     * 
     * 
     */
    public function createOpenShipment()
    {
    	$shipDate = new \DateTime();

		$createOpenShipmentRequest = new OpenShipComplexType\CreateOpenShipmentRequest();

        //authentication & client details
		$createOpenShipmentRequest->WebAuthenticationDetail->UserCredential->Key = Config('fedex.FEDEX_API_KEY');
		$createOpenShipmentRequest->WebAuthenticationDetail->UserCredential->Password = Config('fedex.FEDEX_PASSWORD');
		$createOpenShipmentRequest->ClientDetail->AccountNumber = Config('fedex.FEDEX_ACCOUNT_NUMBER');
		$createOpenShipmentRequest->ClientDetail->MeterNumber = Config('fedex.FEDEX_METER_NUMBER');

		// version
		$createOpenShipmentRequest->Version->ServiceId = 'ship';
		$createOpenShipmentRequest->Version->Major = 15;
		$createOpenShipmentRequest->Version->Intermediate = 0;
		$createOpenShipmentRequest->Version->Minor = 0;

		// package 1
		$requestedPackageLineItem1 = new OpenShipComplexType\RequestedPackageLineItem();
		$requestedPackageLineItem1->SequenceNumber = 1;
		$requestedPackageLineItem1->ItemDescription = 'Product description 1';
		$requestedPackageLineItem1->Dimensions->Width = 10;
		$requestedPackageLineItem1->Dimensions->Height = 10;
		$requestedPackageLineItem1->Dimensions->Length = 15;
		$requestedPackageLineItem1->Dimensions->Units = OpenShipSimpleType\LinearUnits::_IN;
		$requestedPackageLineItem1->Weight->Value = 2;
		$requestedPackageLineItem1->Weight->Units = OpenShipSimpleType\WeightUnits::_LB;

		// package 2
		$requestedPackageLineItem2 = new OpenShipComplexType\RequestedPackageLineItem();
		$requestedPackageLineItem2->SequenceNumber = 1;
		$requestedPackageLineItem2->ItemDescription = 'Product description 1';
		$requestedPackageLineItem2->Dimensions->Width = 10;
		$requestedPackageLineItem2->Dimensions->Height = 10;
		$requestedPackageLineItem2->Dimensions->Length = 15;
		$requestedPackageLineItem2->Dimensions->Units = OpenShipSimpleType\LinearUnits::_IN;
		$requestedPackageLineItem2->Weight->Value = 2;
		$requestedPackageLineItem2->Weight->Units = OpenShipSimpleType\WeightUnits::_LB;

		// requested shipment
		$createOpenShipmentRequest->RequestedShipment->DropoffType = OpenShipSimpleType\DropoffType::_REGULAR_PICKUP;
		$createOpenShipmentRequest->RequestedShipment->ShipTimestamp = $shipDate->format('c');
		$createOpenShipmentRequest->RequestedShipment->ServiceType = OpenShipSimpleType\ServiceType::_FEDEX_2_DAY;
		$createOpenShipmentRequest->RequestedShipment->PackagingType = OpenShipSimpleType\PackagingType::_YOUR_PACKAGING;
		$createOpenShipmentRequest->RequestedShipment->LabelSpecification->ImageType = OpenShipSimpleType\ShippingDocumentImageType::_PDF;
		$createOpenShipmentRequest->RequestedShipment->LabelSpecification->LabelFormatType = OpenShipSimpleType\LabelFormatType::_COMMON2D;
		$createOpenShipmentRequest->RequestedShipment->LabelSpecification->LabelStockType = OpenShipSimpleType\LabelStockType::_PAPER_4X6;
		$createOpenShipmentRequest->RequestedShipment->RateRequestTypes = [OpenShipSimpleType\RateRequestType::_PREFERRED];
		$createOpenShipmentRequest->RequestedShipment->PackageCount = 1;
		$createOpenShipmentRequest->RequestedShipment->RequestedPackageLineItems = [$requestedPackageLineItem1];

		// requested shipment shipper
		$createOpenShipmentRequest->RequestedShipment->Shipper->AccountNumber = Config('fedex.FEDEX_ACCOUNT_NUMBER');
		$createOpenShipmentRequest->RequestedShipment->Shipper->Address->StreetLines = ['1990  Rose Street'];
		$createOpenShipmentRequest->RequestedShipment->Shipper->Address->City = 'Regina';
		$createOpenShipmentRequest->RequestedShipment->Shipper->Address->StateOrProvinceCode = 'SK';
		$createOpenShipmentRequest->RequestedShipment->Shipper->Address->PostalCode = 'S4P3Y2';
		$createOpenShipmentRequest->RequestedShipment->Shipper->Address->CountryCode = 'CA';
		$createOpenShipmentRequest->RequestedShipment->Shipper->Contact->CompanyName = 'Veronica';
		$createOpenShipmentRequest->RequestedShipment->Shipper->Contact->PersonName = 'Veronica';
		$createOpenShipmentRequest->RequestedShipment->Shipper->Contact->EMailAddress = 'shipper@example.com';
		$createOpenShipmentRequest->RequestedShipment->Shipper->Contact->PhoneNumber = '306-530-9866';

		// requested shipment recipient
		$createOpenShipmentRequest->RequestedShipment->Recipient->Address->StreetLines = ['1748  Carling Avenue'];
		$createOpenShipmentRequest->RequestedShipment->Recipient->Address->City = 'Ottawa';
		$createOpenShipmentRequest->RequestedShipment->Recipient->Address->StateOrProvinceCode = 'ON';
		$createOpenShipmentRequest->RequestedShipment->Recipient->Address->PostalCode = 'K1Z7B5';
		$createOpenShipmentRequest->RequestedShipment->Recipient->Address->CountryCode = 'CA';
		$createOpenShipmentRequest->RequestedShipment->Recipient->Contact->PersonName = 'John Doe';
		$createOpenShipmentRequest->RequestedShipment->Recipient->Contact->EMailAddress = 'recipient@example.com';
		$createOpenShipmentRequest->RequestedShipment->Recipient->Contact->PhoneNumber = '613-295-1422';

		// shipping charges payment
		$createOpenShipmentRequest->RequestedShipment->ShippingChargesPayment->Payor->ResponsibleParty = $createOpenShipmentRequest->RequestedShipment->Shipper;
		$createOpenShipmentRequest->RequestedShipment->ShippingChargesPayment->PaymentType = OpenShipSimpleType\PaymentType::_SENDER;

		// send the create open shipment request
		$openShipServiceRequest = new OpenShipRequest();
		$createOpenShipmentReply = $openShipServiceRequest->getCreateOpenShipmentReply($createOpenShipmentRequest);

		// shipment is created and we have an index number
		$index = $createOpenShipmentReply->Index;

		//Confirm open shipment
		$confirmOpenShipmentRequest = new OpenShipComplexType\ConfirmOpenShipmentRequest();
		$confirmOpenShipmentRequest->WebAuthenticationDetail = $createOpenShipmentRequest->WebAuthenticationDetail;
		$confirmOpenShipmentRequest->ClientDetail = $createOpenShipmentRequest->ClientDetail;
		$confirmOpenShipmentRequest->Version = $createOpenShipmentRequest->Version;
		$confirmOpenShipmentRequest->Index = $index;

		$confirmOpenShipmentReply = $openShipServiceRequest->getConfirmOpenShipmentReply($confirmOpenShipmentRequest);

		dd($confirmOpenShipmentReply);

    }
}
