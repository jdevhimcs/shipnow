<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Image;
use App\Models\User;
use App\Models\UserProfile;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $user = User::where(['id' => Auth::user()->id])->first();

        return view('admin.profile.create', ['user' => $user]);

    }

    public function store(Request $request)
    {
        // dd($request->all());
        $id = Auth::user()->id;
        $request->validate([
           'user_profile.first_name' => 'bail|required|string',
           'user_profile.last_name'  => 'bail|required|string',
           'email'      => 'bail|required|string|unique:users,email,'.$id,
           'user_profile.phone_no'   => 'bail|required|string',
        ]);
            
        if($request->file('photo')) {
            $this->saveImage($request);
        }

        
        $userData = $request->input();
        
        $settingData = $request->input('user_profile');
        unset($userData['user_profile']);
        
        $user = User::where(['id' => $id])->first();
        $user->update($userData);        
        $user->profile()->updateOrCreate(['user_id' => $id], $settingData);

        return redirect()->route('admin.profile.create')->with('success', "Profile has been updated successfully.");

    }

    protected function saveImage($request) {
        request()->validate([
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($files = $request->file('photo')) {
            $desinationFolder = 'public/avatar/'.$request->user()->id;
            // for save original image
            $path = $request->file('photo')
                ->storeAs($desinationFolder, $files->getClientOriginalName());

            // for save thumnail image
            $ImageUpload = Image::make($files);
            $thumbnailPath = storage_path().'/app/'.$desinationFolder.'/thumb_';

            // Creating thumbnail for image;
            $ImageUpload->resize(250,250);
            $ImageUpload = $ImageUpload->save($thumbnailPath.$files->getClientOriginalName());
            //updating image in profile table profile table
            $photo = UserProfile::firstOrNew([
                'user_id' => $request->user()->id
            ]);

            $photo->image = $files->getClientOriginalName();
            $photo->save();
        }
    }

}
