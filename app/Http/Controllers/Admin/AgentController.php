<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendPasswordToAgentMail;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where([ 
            'role_id' => 3 
        ])->withTrashed()->paginate(15);

        return view('admin.agent.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.agent.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $rules = [
                'first_name' => [
                    'required'
                ],
                'last_name' => [
                    'required'
                ],
                'email' => [
                    'required',
                    'email',
                    'unique:users,email'
                ],
                'password' => [
                    'required'
                ],
                'confirm_password' => [
                    'required',
                    'same:password'
                ]
            ];

            $messages = [
                'first_name.required' => 'Please enter a first name.',
                'last_name.required' => 'Please enter a last name.',
                'email.required' => 'Please enter a email.',
                'email.email' => 'Please enter a correct email.',
                'email.unique' => 'This email has already been used.',
                'password.required' => 'Please enter a password.',
                'confirm_password.required' => 'Please enter a confirm password.',
                'confirm_password.same' => 'The confirm password is not matching with password.',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                return back()->withErrors(['exception_error' => [$validator->errors()->first()]])->withInput();
            }

            DB::transaction(function () use ($request) {
                $user = User::create([
                    'email' => $request->get('email'),
                    'password' => Hash::make($request->get('password')),
                    'role_id' => 3,
                ]);

                $user->profile()->create([
                    'first_name' => $request->get('first_name'),
                    'last_name' => $request->get('last_name'),
                    'image' => ''
                ]);

                //Notify Agent
                Mail::send(new SendPasswordToAgentMail([
                    'email' => $request->get('email'),
                    'password' => $request->get('password')
                ]));
            });

            session()->flash('success', __("The agent has been saved successfully."));
            return redirect()->route('admin.agents.index');
        } catch (\Illuminate\Database\QueryException $exception) {
            return back()->withErrors(['exception_error' => [$exception->getMessage()]])->withInput();
        } catch (\Exception $exception) {
            return back()->withErrors(['exception_error' => [$exception->getMessage()]])->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
