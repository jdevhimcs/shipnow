<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\AgentUser;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AssignUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        $assignUsers = $user->assignCustomers->pluck('user_id')->toArray();

        $customers = User::where('role_id', 2)->with(['profile'])->get();

        return view('admin.assign-user.create', compact('user', 'assignUsers', 'customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        try {
            $rules = [
                'users' => [
                    'required',
                    'exists:users,id'
                ]
            ];

            $messages = [
                'users.required' => 'Please select a users.',
                'users.exists' => 'Please select a correct user.'
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                return back()->withErrors(['exception_error' => [$validator->errors()->first()]])->withInput();
            }

            DB::transaction(function () use ($request, $user) {
                // Delete all the assign customer
                $user->assignCustomers()->delete();

                foreach ($request->get('users') as $key => $value) {
                    $user->assignCustomers()->create(
                        ['user_id' => (int) $value]
                    );
                }
            });

            session()->flash('success', __("The user has been assigned successfully."));
            return redirect()->route('admin.agents.index');
        } catch (\Illuminate\Database\QueryException $exception) {
            return back()->withErrors(['exception_error' => [$exception->getMessage()]])->withInput();
        } catch (\Exception $exception) {
            return back()->withErrors(['exception_error' => [$exception->getMessage()]])->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
