<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $userCount = User::where('role_id',2)->count();
        $adminCount = User::where('role_id',1)->count();
        return view('admin.dashboard.index',compact('userCount', 'adminCount'));
    }

}
