<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users = User::where([ 
            'role_id' =>2 
        ])->withTrashed()->paginate(15);

        return view('admin.user.index',compact('users'));
    }

    public function show($id)
    {
        $user = User::withTrashed()->find($id);
        return view('admin.user.show',compact('user'));

    }

    public function destroy($id)
    {
        $user = User::withTrashed()->find($id);
        $message='deleted';
        if ($user->trashed()) {
            $user->restore();
            $message='restored';
        } else {
            $user->delete();
        }

        return back()
            ->with('success', "User has been $message successfully.");
    }
}
