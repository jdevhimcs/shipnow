<?php

namespace App\Http\Controllers\Agent;

use Auth;
use App\Models\AgentUser; 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customerCount = AgentUser::where('agent_id', Auth::user()->id)->count();

        return view('agent.dashboard.index',compact('customerCount'));
    }
}
