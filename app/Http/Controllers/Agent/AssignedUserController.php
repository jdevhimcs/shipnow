<?php

namespace App\Http\Controllers\Agent;

use Auth;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\AgentUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AssignedUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = AgentUser::where('agent_id', Auth::user()->id)->get();

        return view('agent.assign-user.index',compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $assignUsers = AgentUser::where('agent_id', Auth::user()->id)->pluck('user_id')->toArray();

        $customers = User::where('role_id', 2)->with(['profile'])->get();

        return view('agent.assign-user.create', compact('assignUsers', 'customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $rules = [
                'users' => [
                    'required',
                    'exists:users,id'
                ]
            ];

            $messages = [
                'users.required' => 'Please select a users.',
                'users.exists' => 'Please select a correct user.'
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                return back()->withErrors(['exception_error' => [$validator->errors()->first()]])->withInput();
            }

            DB::transaction(function () use ($request) {
                // Delete all the assign customer
                AgentUser::where('agent_id', Auth::user()->id)->delete();

                foreach ($request->get('users') as $key => $value) {
                    $agentUser = new AgentUser();

                    $agentUser->user_id = (int) $value;
                    $agentUser->agent_id = Auth::user()->id;
                    $agentUser->save();
                }
            });

            session()->flash('success', __("The user has been assigned successfully."));
            return redirect()->route('agent.assigned-user.index');
        } catch (\Illuminate\Database\QueryException $exception) {
            return back()->withErrors(['exception_error' => [$exception->getMessage()]])->withInput();
        } catch (\Exception $exception) {
            return back()->withErrors(['exception_error' => [$exception->getMessage()]])->withInput();
        }
    }

    /**
     * loginAsCustomer method
     * 
     * @param \Illuminate\Http\Request $request
     */
    public function loginAsCustomer(Request $request)
    {
        $userId = $request->get('user_id');

        //if session exists remove it and return login to original user
        if (session()->has('hasClonedUser')) {
            Auth::loginUsingId(session()->get('hasClonedUser'));
            session()->remove('hasClonedUser');
            return redirect()->route('agent.assigned-user.index');
        }

        // Set session for client cloning
        session()->put('hasClonedUser', Auth::user()->id);
        Auth::loginUsingId($userId);
        return redirect()->route('user.dashboard.index');
    }
}
