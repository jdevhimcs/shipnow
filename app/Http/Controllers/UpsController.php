<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use UpsRate;
use UpsRateTimeInTransit;
use UpsShipping;
use UpsTracking;

class UpsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		try {
		    $shipment = new \Ups\Entity\Shipment();


		    $shipperAddress = $shipment->getShipper()->getAddress();
		    $shipperAddress->setPostalCode('21093');

		    $address = new \Ups\Entity\Address();
		    $address->setPostalCode('21093');
		    $shipFrom = new \Ups\Entity\ShipFrom();
		    $shipFrom->setAddress($address);

		    $shipment->setShipFrom($shipFrom);

		    $shipTo = $shipment->getShipTo();
		    $shipTo->setCompanyName('Test Ship To');
		    $shipToAddress = $shipTo->getAddress();
		    $shipToAddress->setPostalCode('92656');

		    $package = new \Ups\Entity\Package();
		    $package->getPackagingType()->setCode(\Ups\Entity\PackagingType::PT_PACKAGE);
		    $package->getPackageWeight()->setWeight(10);
		    
		    // if you need this (depends of the shipper country)
		    $weightUnit = new \Ups\Entity\UnitOfMeasurement;
		    $weightUnit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_LBS);
		    $package->getPackageWeight()->setUnitOfMeasurement($weightUnit);

		    $dimensions = new \Ups\Entity\Dimensions();
		    $dimensions->setHeight(10);
		    $dimensions->setWidth(10);
		    $dimensions->setLength(10);

		    $unit = new \Ups\Entity\UnitOfMeasurement;
		    $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_IN);

		    $dimensions->setUnitOfMeasurement($unit);
		    $package->setDimensions($dimensions);

		    $shipment->addPackage($package);

		    // Insurance is in PackageServiceOptions so we can use them

		    $rates = UpsRate::getRate($shipment);

		    dd($rates);
		} catch(\Ups\Exception\InvalidResponseException $e) {
			dd($e);
		} catch (Exception $e) {
			dd($e);
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function rateTimeInTransit(Request $request)
    {
		try {
		    $shipment = new \Ups\Entity\Shipment();

		    $shipperAddress = $shipment->getShipper()->getAddress();
		    $shipperAddress->setPostalCode('21093');

		    $address = new \Ups\Entity\Address();
		    $address->setPostalCode('21093');
		    $shipFrom = new \Ups\Entity\ShipFrom();
		    $shipFrom->setAddress($address);

		    $shipment->setShipFrom($shipFrom);

		    $shipTo = $shipment->getShipTo();
		    $shipTo->setCompanyName('Test Ship To');
		    $shipToAddress = $shipTo->getAddress();
		    $shipToAddress->setPostalCode('92656');

		    $package = new \Ups\Entity\Package();
		    $package->getPackagingType()->setCode(\Ups\Entity\PackagingType::PT_PACKAGE);
		    $package->getPackageWeight()->setWeight(10);
		    
		    // if you need this (depends of the shipper country)
		    $weightUnit = new \Ups\Entity\UnitOfMeasurement;
		    $weightUnit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_LBS);
		    $package->getPackageWeight()->setUnitOfMeasurement($weightUnit);

		    $dimensions = new \Ups\Entity\Dimensions();
		    $dimensions->setHeight(10);
		    $dimensions->setWidth(10);
		    $dimensions->setLength(10);

		    $unit = new \Ups\Entity\UnitOfMeasurement;
		    $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_IN);

		    $dimensions->setUnitOfMeasurement($unit);
		    $package->setDimensions($dimensions);

		    $shipment->addPackage($package);

			//Shipment Weight & unit international Shipment
			$shipmentTotalWeight = new \Ups\Entity\ShipmentTotalWeight();
			$shipmentTotalWeight->setWeight(10);
			$shipmentTotalWeight->setUnitOfMeasurement($weightUnit);
			$shipment->setShipmentTotalWeight($shipmentTotalWeight);

		    $deliveryTimeInformation = new \Ups\Entity\DeliveryTimeInformation();
		    $deliveryTimeInformation->setPackageBillType(\Ups\Entity\DeliveryTimeInformation::PBT_NON_DOCUMENT);
		    
		    $pickup = new \Ups\Entity\Pickup();
		    $pickup->setDate("20210220");
		    $pickup->setTime("160000");
		    $shipment->setDeliveryTimeInformation($deliveryTimeInformation);

		    // Insurance is in PackageServiceOptions so we can use them

		    $rates = UpsRateTimeInTransit::shopRatesTimeInTransit($shipment);

		    dd($rates);
		} catch(\Ups\Exception\InvalidResponseException $e) {
			dd($e);
		} catch (Exception $e) {
			dd($e);
		}
	}
	
	public function createShipment() {
		$order_id = time();
	
		// Start shipment
		$shipment = new \Ups\Entity\Shipment();

		// Set shipper
		$shipper = $shipment->getShipper();
		$shipper->setShipperNumber('W32771');
		$shipper->setName('shipnow');
		$shipper->setAttentionName('shipnow');
		$shipperAddress = $shipper->getAddress();
		$shipperAddress->setAddressLine1('395 Boulevard de');
		$shipperAddress->setPostalCode('J7V0B4');
		$shipperAddress->setCity('Pincourt');
		$shipperAddress->setStateProvinceCode('QC'); // required in US
		$shipperAddress->setCountryCode('CA');
		$shipper->setAddress($shipperAddress);
		// $shipper->setEmailAddress('ups-group.com'); 
		$shipper->setPhoneNumber('112223333333');
		$shipment->setShipper($shipper);		

		// From address
		$address = new \Ups\Entity\Address();
		$address->setAddressLine1('44 Charles Street West');
		$address->setPostalCode('M4Y1R8');
		$address->setCountryCode('CA');
		$address->setStateProvinceCode('ON');  
		$address->setCity('Toronto');
		$shipFrom = new \Ups\Entity\ShipFrom();
		$shipFrom->setAddress($address);
		$shipFrom->setName('Calgary Gardens');
		$shipFrom->setAttentionName($shipFrom->getName());
		$shipFrom->setCompanyName($shipFrom->getName());
		// $shipFrom->setEmailAddress('ups-group.com');
		$shipFrom->setPhoneNumber('112223333333');
		$shipment->setShipFrom($shipFrom);

		// To address
		$address = new \Ups\Entity\Address();
		$address->setAddressLine1('59 Charles Street West');
		$address->setPostalCode('M5S1K5');
		$address->setCountryCode('CA');
		$address->setStateProvinceCode('ON');  // Required in US
		$address->setCity('Toronto');
		$shipTo = new \Ups\Entity\ShipTo();
		$shipTo->setAddress($address);
		$shipTo->setCompanyName('ACOA Head Office');
		$shipTo->setAttentionName('Mark Sanborn');
		// $shipTo->setEmailAddress('XX'); 
		$shipTo->setPhoneNumber('112223333333');
		$shipment->setShipTo($shipTo);

		// Sold to
		$address = new \Ups\Entity\Address();
		$address->setAddressLine1('59 Charles Street West');
		$address->setPostalCode('M5S1K5');
		$address->setCountryCode('CA');
		$address->setStateProvinceCode('ON');
		$address->setCity('Toronto');
		$soldTo = new \Ups\Entity\SoldTo;
		$soldTo->setAddress($address);
		$soldTo->setCompanyName('ACOA Head Office');
		$soldTo->setAttentionName('Mark Sanborn');
		// $soldTo->setEmailAddress('XX');
		$soldTo->setPhoneNumber('112223333333');
		$shipment->setSoldTo($soldTo);

		// Set service
		$service = new \Ups\Entity\Service;
		$service->setCode(\Ups\Entity\Service::S_AIR_2DAY);
		$service->setDescription($service->getName());
		$shipment->setService($service);

		// Mark as a return (if return)
		// if ($return) {
		// 	$returnService = new \Ups\Entity\ReturnService;
		// 	$returnService->setCode(\Ups\Entity\ReturnService::PRINT_RETURN_LABEL_PRL);
		// 	$shipment->setReturnService($returnService);
		// }

		// Set description
		$shipment->setDescription('This is testing Please ignore');

		// Add Package
		$package = new \Ups\Entity\Package();
		$package->getPackagingType()->setCode(\Ups\Entity\PackagingType::PT_UPSLETTER);
		$package->getPackageWeight()->setWeight(2);
		$unit = new \Ups\Entity\UnitOfMeasurement;
		$unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_LBS);
		$package->getPackageWeight()->setUnitOfMeasurement($unit);

		// Set Package Service Options
		// $packageServiceOptions = new \Ups\Entity\PackageServiceOptions();
		// $deliveryConfirmation = new \Ups\Entity\DeliveryConfirmation;
		// $deliveryConfirmation->setDcisType(1);

		// $packageServiceOptions = new \Ups\Entity\PackageServiceOptions();
		// $packageServiceOptions->setDeliveryConfirmation($deliveryConfirmation);
		// $package->setPackageServiceOptions($packageServiceOptions);
		// $packageServiceOptions->setShipperReleaseIndicator(true);
		// $package->setPackageServiceOptions($packageServiceOptions);

		// Set dimensions
		// $dimensions = new \Ups\Entity\Dimensions();
		// $dimensions->setHeight(50);
		// $dimensions->setWidth(50);
		// $dimensions->setLength(50);
		// $unit = new \Ups\Entity\UnitOfMeasurement;
		// $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_CM);
		// $dimensions->setUnitOfMeasurement($unit);
		// $package->setDimensions($dimensions);

		// Add descriptions because it is a package
		$package->setDescription('Testing item');

		// Add this package
		$shipment->addPackage($package);

		// Set Reference Number
		$referenceNumber = new \Ups\Entity\ReferenceNumber;
		// if ($return) {
		// 	$referenceNumber->setCode(\Ups\Entity\ReferenceNumber::CODE_RETURN_AUTHORIZATION_NUMBER);
		// 	$referenceNumber->setValue($return_id);
		// } else {
			$referenceNumber->setCode(\Ups\Entity\ReferenceNumber::CODE_INVOICE_NUMBER);
			$referenceNumber->setValue($order_id);
		// }
		$shipment->setReferenceNumber($referenceNumber);

		// Set payment information
		$shipment->setPaymentInformation(new \Ups\Entity\PaymentInformation('prepaid', (object)array(
			'AccountNumber' => 'W32771', 
			// 'ShipmentCharge' => \Ups\Entity\ShipmentCharge::SHIPMENT_CHARGE_TYPE_TRANSPORTATION
			))
		);

		// Ask for negotiated rates (optional)
		// $rateInformation = new \Ups\Entity\RateInformation;
		// $rateInformation->setNegotiatedRatesIndicator(1);
		// $shipment->setRateInformation($rateInformation);

		// Get shipment info
		try {
			$confirm = UpsShipping::confirm(\Ups\Shipping::REQ_NONVALIDATE, $shipment);
			// dd($confirm); // Confirm holds the digest you need to accept the result
			
			if ($confirm) {
				$accept = UpsShipping::accept($confirm->ShipmentDigest);
				dd($accept); // Accept holds the label and additional information
			}
		} catch (\Exception $e) {
			dd($e);
		}
	}

	public function trackShipment($trackId) {
		try {
			// dd($trackId);
			$shipment = UpsTracking::track($trackId);
			dd($shipment);
			foreach($shipment->Package->Activity as $activity) {
				dd($activity);
			}

		} catch (Exception $e) {
			dd($e);
		} catch (\Ups\Exception\InvalidResponseException $e) {
			dd($e->getMessage());
		}
	}

	public function cancelShipment($shipmentId) {
		try {
			// $shipmentData['shipmentId'] = $shipmentId;

			$response = UpsShipping::void($shipmentId);
			
			dd($response);

			return $response;
					
		} catch (\Exception $e) {
			dd($e);
		}
	}
}
