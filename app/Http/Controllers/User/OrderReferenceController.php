<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\OrderReference;
use Illuminate\Support\Facades\Auth;

class OrderReferenceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {  
        $orderPreferenceRef = OrderReference::find($id);
        $orderPreferenceRef->delete();
        return response()->json([
            'message' => 'Data deleted successfully!'
        ]);
    }
}
