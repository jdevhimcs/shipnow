<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\OrderBroker;
use Illuminate\Support\Facades\Auth;

class OrderBrokerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {  
        $orderPreferenceBroker = OrderBroker::find($id);
        $orderPreferenceBroker->delete();
        return response()->json([
            'message' => 'Data deleted successfully!'
        ]);
    }
}
