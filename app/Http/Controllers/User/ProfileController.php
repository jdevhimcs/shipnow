<?php

namespace App\Http\Controllers\User;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    public function edit($id)
    {
        $user = User::find($id);

        return view('user.profile.edit',compact('user'));
    }

    public function update(Request $request,$id)
    {
        $request->validate([
            'first_name' => 'bail|required|string',
            'last_name'  => 'bail|required|string',
            'email'      => 'bail|required|string|unique:users,email,'.$id,
            'phone_no'   => 'bail|required|string',
        ]);
        $data = $request->all();
        $user = User::find($id);
        $user->update($data);
        return back()->with('success','Profile updated successfull');

    }
}
