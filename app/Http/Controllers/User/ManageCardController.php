<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UserCard;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use Exception;

class ManageCardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->isMethod('post'))
		{
			$stripeToken = $request->get('stripeToken');
			//Charge Customer
			return $this->createPaymentProfile($stripeToken);
		}
    }

    /**
	* Charge a Stripe customer.
	*
	* @var Stripe\Customer $customer
	* @param string $token
	* @return createStripeCharge()
	*/
	public function createPaymentProfile($token)
	{
		$isDefault = (int) false;

		if (!Auth::user()->stripe_key)
		{
			$isDefault = (int) false;
			$customer = $this->createStripeCustomer($token);
		}

		try {
			// Add a card to an existing customer.
			$customer = Customer::retrieve(Auth::user()->stripe_key);

			$card = Customer::createSource(
				$customer->id,
				['source' => $token]
			);

			//Save Card Last Digits
			$lastCard = UserCard::create([
				'user_id' => Auth::user()->id,
				'name' => $card->name,
				'last_digits' => $card->last4,
				'type' => $card->brand,
				'expire_month' => $card->exp_month,
				'expire_year' => $card->exp_year,
				'source_id' => $card->id,
				'is_default' => $isDefault
			]);

            $lastCardID = $lastCard->id;
            
            return redirect()
                ->back()
                ->with('message', 'Thanks!! Your card has been added successfully');

		}	catch (Exception $e) {
				return redirect()
				->back()
				->with('error', 'Your credit card was been declined. Please try again or contact us. ' . $e->getMessage());
		}
    }
    
    /**
	* Create a new Stripe customer for a given user.
	*
	* @var Stripe\Customer $customer
	* @param string $token
	* @return Stripe\Customer $customer
	*/
	public function createStripeCustomer($token)
	{
		try {
			$customer = Customer::create(array(
				"description" => Auth::user()->email,
				"email" => Auth::user()->email,
			));

			Auth::user()->stripe_key = $customer->id;
			Auth::user()->save();

			return $customer;
		} 	catch (Exception $e) {
				return redirect()
				->back()
				->with('error', 'Your credit card was been declined. Please try again or contact us.');
		}
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
