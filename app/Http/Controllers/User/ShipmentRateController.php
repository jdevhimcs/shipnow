<?php

namespace App\Http\Controllers\User;

use Auth;
use App\Models\Unit;
use App\Models\Shipment;
use App\Http\Controllers\Controller;
use App\Http\Traits\ShipmentRateTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ShipmentRateController extends Controller
{
    use ShipmentRateTrait;

    /**
     * Get Rates All shipement Apis
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getRates(Request $request) {
        //Apis error array
        $errors = [];

        $data = $request->all();

        //Update request based on dimension unit
        if (!empty($data['dimension_unit_id'])) {
            //Get weight unit based on dimension unit
            $weightUnit = Unit::DimensionWeightUnit($data['dimension_unit_id']);

            foreach ($data['item'] as $key => $value) {
                $data['item'][$key]['weight_unit_id'] = $weightUnit;
                $data['item'][$key]['dimension_unit_id'] = $data['dimension_unit_id'];
            }

        }

        //Fetch Fedex Api response
        $fedexApiResponse = $this->getFedexShipmentRate($data);

        if(!empty($fedexApiResponse['error'])) {
            array_push($errors, $fedexApiResponse['error']);
        }

        //Fetch Ups Api response
        $upsApiResponse = $this->getUpsShipmentRateTimeInTransit($data);

        if(!empty($upsApiResponse['error'])) {
            array_push($errors, $upsApiResponse['error']);
        }

        $apiResponse = array_merge($fedexApiResponse['rates'], $upsApiResponse['rates']);

        $refNumber = time();

        $quotes = [
            'ship_from_city'=> $data['ship_from_city'],
            'ship_from_state'=> $data['ship_from_state'],
            'ship_from_postal'=> $data['ship_from_postal'],
            'ship_from_country'=> $data['ship_from_country'],
            'ship_from_address'=> $data['street_from_address'],
            'ship_to_city'=> $data['ship_to_city'],
            'ship_to_state'=> $data['ship_to_state'],
            'ship_to_postal'=> $data['ship_to_postal'],
            'ship_to_country'=> $data['ship_to_country'],
            'ship_to_address'=> $data['street_to_address'],
            'origin_company'=> $data['ship_from_business'],
			'destination_company'=> $data['ship_to_business'],
            'origin_attn_name' => $data['origin_attn_name'],
            'destination_attn_name' => $data['destination_attn_name'],
            'origin_phone' => $data['origin_phone'],
            'destination_phone' => $data['destination_phone'],
            'origin_email' => $data['origin_email'],
            'destination_email' => $data['destination_email'],
            'service_name' => $data['final_service_type'] ?? NULL,
            'is_signature_required'=> $data['is_signature_required'] ?? NULL,
            'dimension_unit_id'=> $data['dimension_unit_id'] ?? NULL,
            'currency'=> $data['currency'],
            'type'=> $data['type'],
            'items' => $data['item'],
            'rates' => $apiResponse,
            'ref_num' => $refNumber, //ref code as like eshiper
            'errors' => $errors
        ];

        $request->session()->put('quotes', $quotes);

        return response()->json([
            'errors' => $errors,
            'response' => $apiResponse,
            'quote_id' => $refNumber
        ]);
    }

    /**
     * @method setShipmentRate
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * 
     * The method will save the rates in session and database and respond with success flag
     */ 
    public function setShipmentRate(Request $request)
    {
        try {
            $input = $request->all();

            // Save data in session
            $request->session()->put('quotes.shipment_final_rate', $input['final_shipment_rate']);
            $request->session()->put('quotes.shipment_final_service',  $input['final_service_type']);
            $request->session()->put('quotes.service_code',  $input['final_service_code']);
            $request->session()->put('quotes.carrier',  $input['carrier']);
            $request->session()->put('quotes.total_base_charge',  $input['total_base_charge']);
            $request->session()->put('quotes.transport_charges',  $input['transport_charges']);
            $request->session()->put('quotes.service_charges',  $input['service_charges']);
            $request->session()->put('quotes.sur_charges',  $input['sur_charges']);
            $request->session()->put('quotes.delivery_timestamp',  $input['delivery_timestamp']);

            DB::transaction(function () use ($request, $input) {
                // Save data in database
                $shipment = Shipment::create([
                    'user_id' => Auth::user()->id,
                    'ref_code' => $request->session()->get('quotes')['ref_num'],
                    'package_type' => strtolower($request->session()->get('quotes')['type']), // Letter, PAK, PACKAGE etc.
                    'carrier' => strtolower($input['carrier']),
                    'origin_company' => $request->session()->get('quotes')['origin_company'],
                    'total_transportation_charges' => $input['transport_charges'],
                    'total_service_options_charges' => $input['service_charges'],
                    'total_charges' => $input['final_shipment_rate'],
                    'currency' => $request->session()->get('quotes')['currency'],
                    'address_from' => $request->session()->get('quotes')['ship_from_address'] ?? '',
                    'city_from' => $request->session()->get('quotes')['ship_from_city'],
                    'postal_from' => $request->session()->get('quotes')['ship_from_postal'],
                    'address_to' => $request->session()->get('quotes')['ship_to_address'] ?? '',
                    'city_to' => $request->session()->get('quotes')['ship_to_city'],
                    'postal_to' => $request->session()->get('quotes')['ship_to_postal'],
                    'is_paid' => (int) false,
                    'status' => array_search('Pending', Config('constants.SHIPMENT_STATUS')),
                    'destination_company' => $request->session()->get('quotes')['destination_company'],
                    'origin_attn_name' => $request->session()->get('quotes')['origin_attn_name'],
                    'destination_attn_name' => $request->session()->get('quotes')['destination_attn_name'],
                    'origin_phone' => $request->session()->get('quotes')['origin_phone'],
                    'destination_phone' => $request->session()->get('quotes')['destination_phone'],
                    'origin_email' => $request->session()->get('quotes')['origin_email'],
                    'destination_email' => $request->session()->get('quotes')['destination_email'],
                    'service_name' => $input['final_service_type']
                ]);

                $shipment->shipmentItems()->createMany($request->session()->get('quotes')['items']);
            });

            // Save Shipping Item Address

            return response()->json([
                'success' => true
            ]);

        } catch (\Illuminate\Database\QueryException $exception) {
            return response()->json([ 'message' => $exception->getMessage()], 400);
        } catch (\Exception $exception) {
            return response()->json([ 'message' => $exception->getMessage()], 400);
        }
    }

    public function checkout(Request $request) {
        $publishkey = config('app.STRIPE_PUBLISHABLE_KEY');

        $quotes = $request->session()->get('quotes');

		if (empty($quotes)) {
			return redirect()->route('user.package-quote.create');
		}

        return view('user.shipment.checkout', compact('quotes', 'publishkey'));
    }
}
