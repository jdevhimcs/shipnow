<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserProfile;
use App\Models\CompanyDetail;
use App\Models\Country;
use App\Models\Language;
use App\Models\Unit;
use App\Models\NotificationSetting;
use App\Models\OrderPreference;
use Illuminate\Support\Facades\Auth;
use App\Models\UserCard;
use App\Models\AccountPayableDetail;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use Exception;

class AccountSettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $publishkey = config('app.STRIPE_PUBLISHABLE_KEY');

        $user = User::where(['id' => Auth::user()->id])->first();

        //Country
        $countries = Country::get(['name', 'id']);

        //Languages
        $languages = Language::pluck('title', 'id');

        //Units
        $units = Unit::pluck('title', 'id');

        //Notifications
        $userId = Auth::user()->id;
        
        //Nitification Settings
        $notificationSettings = NotificationSetting::pluck('title', 'id');

        //Order Preferences        
        $orders = OrderPreference::where('user_id', $userId)->get();

        //My Cards
        $myCards = UserCard::where('user_id', Auth::user()->id)->get();

        return view('user.settings.account.create', compact(
            'user', 
            'countries', 
            'units', 
            'languages', 
            'orders', 
            'notificationSettings',
            'publishkey',
            'myCards'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Auth::user()->id;

        $request->validate([
            'first_name' => 'required|string',
            'last_name'  => 'required|string',
            'email'      => 'required|string|unique:users,email,'.$id,
            'phone_number'=> 'required|string',
            'company_name' => 'required|string',
            'address' => 'required|string',
            'postal_code' => 'required|string',
            'company_email' => 'required|string|email',
            'company_phone' => 'required|string',
            'company_country' => 'required',
            'company_state' => 'required',
            'company_city' => 'required|string|max:255',
            'billing_address' => 'required|string|max:255',
            'billing_country' => 'required|string|max:255',
            'billing_state' => 'required|string|max:255',
            'billing_city' => 'required|string|max:255',
            'billing_postal_code' => 'required|string|max:255',
            'billing_email' => 'required|string|max:255',
            'billing_phone' => 'required|string|max:255',
            'contact_name' => 'required|string',
            'account_payable_email' => 'required|string|email',
            'account_payable_phone' => 'required|string',
            'payment_terms' => 'required|string'
        ]);

        $input = $request->all();

        $user = User::find($id);

        $user->email = $input['email'];

        $user->save();

        //Profile
        $user->profile()->updateOrCreate(['user_id' => $id], [
            'first_name' => $input['first_name'],
            'last_name' => $input['last_name'],
            'phone_no' => $input['full_phone'],
        ]);

        //Account Payable Details
        $user->accountPayableDetail()->updateOrCreate(['user_id' => $id], [
            'contact_name' => $input['contact_name'],
            'email' => $input['account_payable_email'],
            'phone' => $input['account_payable_phone'],
            'payment_terms' => $input['payment_terms'],
            'is_prepaid' => (int) $request->has('is_prepaid'),
            'is_same_main_contact' => (int) $request->has('is_same_main_contact'),
        ]);

        //Billing Address
        $user->billingAddress()->updateOrCreate(['user_id' => $id], [
            'address' => $input['billing_address'],
            'country' => $input['billing_country'],
            'state' => $input['billing_state'],
            'city' => $input['billing_city'],
            'postal_code' => $input['billing_postal_code'],
            'email' => $input['billing_email'],
            'phone_no' => $input['billing_phone'],
        ]);

        //Company Information
        $user->companyDetail()->updateOrCreate(['user_id' => $id], [
            'company_name' => $input['company_name'],
            'address' => $input['address'],
            'postal_code' => $input['postal_code'],
            'email' => $input['company_email'],
            'phone_no' => $input['company_phone'],
            'country_id' => $input['company_country'],
            'state_id' => $input['company_state'],
            'city' => $input['company_city'],
            'is_bill_same' => (int) $request->has('is_bill_same')
        ]);

        //Notification Settings
        $user->notificationSettings()->sync($request->notificationSettings);

        return redirect()->back()->with('message', "Account settings has been updated successfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
