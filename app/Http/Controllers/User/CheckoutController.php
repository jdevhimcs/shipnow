<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Traits\ShipmentRateTrait;
use Illuminate\Support\Facades\Auth;
use App\Models\UserCard;
use App\Models\Transaction;
use App\Models\Shipment;
use App\Models\ShipmentItem;
use App\Models\ShipmentPackageResult;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use Exception;

class CheckoutController extends Controller
{
    use ShipmentRateTrait;

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $quotes = $request->session()->get('quotes');

        //Apis error array
        $errors = [];

		if ($quotes['carrier'] == 'ups') {
			//Ups Api response
			$upsApiResponse = $this->createUpsShipment($quotes);

			if(!empty($upsApiResponse['error']) && $upsApiResponse['status'] == false) {
				// array_push($errors, $upsApiResponse['error']);
				return back()->with('error', $upsApiResponse['error']);
			}

			$shipment  = $upsApiResponse['shipment'];

			$quotes['is_confirm'] = $upsApiResponse['is_confirm'];

			$quotes['order_id'] = $upsApiResponse['order_id'];
		}

		if ($quotes['carrier'] == 'fedex') {
			//Fedex Api response
			$fedexApiResponse = $this->createFedexOpenShipment($quotes);

			if(!empty($fedexApiResponse['error']) && $fedexApiResponse['status'] == false) {
				// array_push($errors, $fedexApiResponse['error']);
				return back()->with('error', $fedexApiResponse['error']);
			}

			$shipment  = $fedexApiResponse['shipment'];

			$quotes['is_confirm'] = $fedexApiResponse['is_confirm'];
			
			$quotes['order_id'] = '';
		}

        //Charge User with Stripe Payment and if success then create shipment
        $stripeToken = $request->get('stripeToken');

        $userId = Auth::user()->id;

        $isPaymentSucceed = $this->handlePayment($stripeToken, $userId, $quotes, $shipment, $request);

        // Cancel Carrier Shipment and update database in case of payment failure
        $shipmentStatus = array_search('Completed', Config('constants.SHIPMENT_STATUS'));
        $paymentStatus = (int) true;

        if ($isPaymentSucceed['error']) {
        	$shipmentStatus = array_search('Cancelled and payment failed', Config('constants.SHIPMENT_STATUS'));
        	$paymentStatus = (int) false;
        	$this->storeShipmentData(null, null, $userId, $quotes, $shipment, $request, $shipmentStatus, $paymentStatus);

        	return back()->with('error', $isPaymentSucceed['message']);
        }

        // Update and Store data on tables
        $response = $this->storeShipmentData($isPaymentSucceed['data']['charge'], $isPaymentSucceed['data']['last_card_id'], $userId, $quotes, $shipment, $request, $shipmentStatus, $paymentStatus);

        if ($response['error']) {
        	return back()->with('error', $response['message']);
        }

        return redirect()
		->route('user.order.summary', base64_encode($response['data']['shipment_id']))
		->with('success', 'Thanks!! Shipment created successfully');
    }

    /**
	* Handle Payment.
	*
	* @param string $token,  $userId, $quotes, $shipment, $request
	* @return createStripeCharge()
	*/
	public function handlePayment($token, $userId, $quotes, $shipment, $request)
	{
		// Check 
        $isDefault = false;

        $isCustomerCreated = [
        	'error' => false
        ];

		if (!Auth::user()->stripe_key) {
			$isDefault = true;
			$isCustomerCreated = $this->createStripeCustomer($token);
		}

		// throw error in case error receive from createStripeCustomer
		if ($isCustomerCreated['error']) {
			return $isCustomerCreated;
		}

        //Charge Customer
		$isCardAdded = $this->addCard($token, $userId, $quotes, $shipment, $request, $isDefault);

		if ($isCardAdded['error']) {
			return $isCardAdded;
		}

		$isCharge = $this->createStripeCharge($isCardAdded['data']['customer'], $isCardAdded['data']['card_id'], $isCardAdded['data']['last_card_id'], $userId, $quotes, $shipment, $request);

		return $isCharge;

	}

    /**
	* Charge a Stripe customer.
	*
	* @var Stripe\Customer $customer
	* @param string $token
	* @return createStripeCharge()
	*/
	public function addCard($token, $userId, $quotes, $shipment, $request, $isDefault)
	{
		try {
			// Add a card to an existing customer.
			$customer = Customer::retrieve(Auth::user()->stripe_key);

			$card = Customer::createSource(
				$customer->id,
				['source' => $token]
			);

			//Save Card Last Digits
			$lastCard = UserCard::create([
				'user_id' => Auth::user()->id,
				'name' => $card->name,
				'last_digits' => $card->last4,
				'type' => $card->brand,
				'expire_month' => $card->exp_month,
				'expire_year' => $card->exp_year,
				'source_id' => $card->id,
				'is_default' => $isDefault
			]);

			$lastCardID = $lastCard->id;

			return [
				'error' => false,
				'data' => [
					'customer' => $customer,
					'card_id' => $card->id,
					'last_card_id' => $lastCardID
				]
			];
		} catch (Exception $e) {
			return [
				'error' => true,
				'message' => 'Your credit card was been declined. Please try again or contact us. ' . $e->getMessage()
			];
		}
	}

    /**
	* Create a Stripe charge.
	*
	* @var Stripe\Charge $charge
	* @var Stripe\Error\Card $e
	* @param string $customer | $sourceCardId | $lastCardID
	* @param Stripe\Customer $customer
	* @return postStoreOrder()
	*/
	public function createStripeCharge($customer, $sourceCardId, $lastCardID, $userId, $quotes, $shipment, $request)
	{
		try {
			if ($quotes['carrier'] == 'ups') {
				$amount = $shipment->ShipmentCharges->TotalCharges->MonetaryValue;
				$trackingNumber = $shipment->ShipmentIdentificationNumber;
			}

			if ($quotes['carrier'] == 'fedex') {
				$amount = $shipment->CompletedShipmentDetail->ShipmentRating->ShipmentRateDetails[0]->TotalNetCharge->Amount;
				$trackingNumber = $shipment->CompletedShipmentDetail->MasterTrackingId->TrackingNumber;
			}

			$charge = Charge::create(array(
				"amount" => $amount * 100,
				"currency" => config('app.STRIPE_CURRENCY'),
				"customer" => $customer->id,
				"source" => $sourceCardId,
				"description" => "New Shipment Created: " . $trackingNumber
			));

			if ($charge->status !== 'succeeded') {
				return [
					'error' => true,
					'message' => 'Payment Failed.'
				];
			}

			return [
				'error' => false,
				'data' => [
					'charge' => $charge,
					'last_card_id' => $lastCardID
				]
			];
		} catch(Exception $e) {
			return [
				'error' => true,
				'message' => 'Payment Failed. ' . $e->getMessage()
			];
		}
	}

	/**
	* cancel a shipment
	*
	* @param string $charge object | $card with paid
	* @return redirect()
	*/
	public function cancelShipment($shipment, $quotes)
	{
		$response = [
			'error' => false
		];

		if ($quotes['carrier'] == 'ups') {
			$trackingNumber = $shipment->ShipmentIdentificationNumber;
			$response = $this->cancelUPSShipment($trackingNumber);
		}

		if ($quotes['carrier'] == 'fedex') {
			$trackingNumber = $shipment->CompletedShipmentDetail->MasterTrackingId->TrackingNumber;
			$response = $this->cancelFedExShipment($trackingNumber);
		}

		return $response;
	}


    /**
	* Store a transaction.
	*
	* @param string $charge object | $card with paid
	* @return redirect()
	*/
	public function storeShipmentData($charge, $lastCardID, $userId, $quotes, $shipment, $request, $shipmentStatus, $paymentStatus)
	{
		if ($quotes['carrier'] == 'ups') {
			$data = [
				'user_id' => $userId,
				'package_type' => strtolower($quotes['type']), // Letter, PAK, PACKAGE etc.
				'identification_number' => $shipment->ShipmentIdentificationNumber,
				'carrier' => strtolower($quotes['carrier']),
				'origin_company' => $quotes['origin_company'],
				'order_ref_id' => $quotes['order_id'],
				'total_transportation_charges' => $shipment->ShipmentCharges->TransportationCharges->MonetaryValue,
				'total_service_options_charges' =>$shipment->ShipmentCharges->ServiceOptionsCharges->MonetaryValue,
				'total_charges' =>$shipment->ShipmentCharges->TotalCharges->MonetaryValue,
				'currency' =>$shipment->ShipmentCharges->TotalCharges->CurrencyCode,
				'address_from' => !empty($quotes['ship_from_address']) ? $quotes['ship_from_address'] : '',
				'city_from' => $quotes['ship_from_city'],
				'postal_from' => $quotes['ship_from_postal'],
				'address_to' => !empty($quotes['ship_to_address']) ? $quotes['ship_to_address'] : '',
				'city_to' => $quotes['ship_to_city'],
				'postal_to' => $quotes['ship_to_postal'],
				'ship_date' => $quotes['delivery_timestamp'],
				'is_residential' => !empty($quotes['is_residential']) ? $quotes['is_residential'] : NULL,
				'is_paid' => $paymentStatus,
				'status' => $shipmentStatus,
				'destination_company' => $quotes['destination_company'],
				'origin_attn_name' => $quotes['origin_attn_name'],
				'destination_attn_name' => $quotes['destination_attn_name'],
				'origin_phone' => $quotes['origin_phone'],
				'destination_phone' => $quotes['destination_phone'],
				'origin_email' => $quotes['origin_email'],
				'destination_email' => $quotes['destination_email'],
				'service_name' => $quotes['shipment_final_service']
			];
			
			/**
			 * TODO: The UPS is the only shipping which gives separate package tracking numbers
			 * Save Shipment Package Result
			 */ 
			// $packageResults = [];
			// if (is_array($shipment->PackageResults)) {
			// 	foreach ($shipment->PackageResults as $key => $package) {				
			// 		$packageResults[$key] = [
			// 			'tracking_number' 		 => $package->TrackingNumber,
			// 			'service_options_charges'=> $package->ServiceOptionsCharges->MonetaryValue,
			// 			'options_currency' 		 => $package->ServiceOptionsCharges->CurrencyCode,
			// 			'label_image' 			 => NULL
			// 		];
			// 	}
			// } else {
			// 	$packageResults[] = [
			// 		'tracking_number' 		 => $shipment->PackageResults->TrackingNumber,
			// 		'service_options_charges'=> $shipment->PackageResults->ServiceOptionsCharges->MonetaryValue,
			// 		'options_currency' 		 => $shipment->PackageResults->ServiceOptionsCharges->CurrencyCode,
			// 		'label_image' 			 => NULL
			// 	];			
			// }

			// $shipmentLast->shipmentPackageResults()->createMany($packageResults);
		}
		
		if ($quotes['carrier'] == 'fedex') {
			$data = [
				'user_id' => $userId,
				'package_type' => strtolower($quotes['type']), // Letter, PAK, PACKAGE etc.
				'identification_number' => $shipment->CompletedShipmentDetail->MasterTrackingId->TrackingNumber,
				'carrier' => strtolower($quotes['carrier']),
				'origin_company' => $quotes['origin_company'],
				'order_ref_id' => $quotes['order_id'],
				'total_transportation_charges' => $shipment->CompletedShipmentDetail->ShipmentRating->ShipmentRateDetails[0]->TotalSurcharges->Amount,
				'total_service_options_charges' => 0,
				'total_charges' => $shipment->CompletedShipmentDetail->ShipmentRating->ShipmentRateDetails[0]->TotalNetCharge->Amount,
				'currency' => $shipment->CompletedShipmentDetail->ShipmentRating->ShipmentRateDetails[0]->TotalNetCharge->Currency,
				'address_from' => !empty($quotes['ship_from_address']) ? $quotes['ship_from_address'] : '',
				'city_from' => $quotes['ship_from_city'],
				'postal_from' => $quotes['ship_from_postal'],
				'address_to' => !empty($quotes['ship_to_address']) ? $quotes['ship_to_address'] : '',
				'city_to' => $quotes['ship_to_city'],
				'postal_to' => $quotes['ship_to_postal'],
				'ship_date' => $quotes['delivery_timestamp'],
				'is_residential' => !empty($quotes['is_residential']) ? $quotes['is_residential'] : NULL,
				'is_paid' => $paymentStatus,
				'status' => $shipmentStatus,
				'destination_company' => $quotes['destination_company'],
				'origin_attn_name' => $quotes['origin_attn_name'],
				'destination_attn_name' => $quotes['destination_attn_name'],
				'origin_phone' => $quotes['origin_phone'],
				'destination_phone' => $quotes['destination_phone'],
				'origin_email' => $quotes['origin_email'],
				'destination_email' => $quotes['destination_email'],
				'service_name' => $shipment->CompletedShipmentDetail->ServiceDescription->ServiceType
			];
		}

		$shipmentModel = Shipment::updateOrCreate(
			['ref_code' => $quotes['ref_num']],
			$data
		);

		//Save Items
		$shipmentModel->shipmentItems()->delete();
		$shipmentModel->shipmentItems()->createMany($quotes['items']);

		if ($charge) {
	        //Save Transaction
	        Transaction::create([
				'user_id' => $userId,
				'shipment_id' => $shipmentModel->id,
				'user_card_id' => $lastCardID,
				'trx_id' => $charge->id,
				'trx_amount' => $charge->amount / 100,
				'description' => $charge->description,
				'status' => $paymentStatus
			]);
		}
        
        //Remove Quotes in session
        $request->session()->forget('quotes');

        return [
        	'error' => false,
        	'data' => [
        		'shipment_id' => $shipmentModel->id
        	]
        ];
	}

    /**
	* Create a new Stripe customer for a given user.
	*
	* @var Stripe\Customer $customer
	* @param string $token
	* @return Stripe\Customer $customer
	*/
	public function createStripeCustomer($token)
	{
		try {
			$customer = Customer::create(array(
				"description" => Auth::user()->email,
				"email" => Auth::user()->email,
			));

			Auth::user()->stripe_key = $customer->id;
			Auth::user()->save();

			return [
				'error' => false,
				'customer' => $customer
			];
		} 	catch (Exception $e) {
			return [
				'error' => true,
				'message' => 'Your credit card was been declined. Please try again or contact us.'
			];
		}
	}
}
