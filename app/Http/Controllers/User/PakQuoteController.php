<?php

namespace App\Http\Controllers\User;

use App\Models\Unit;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use FedEx\RateService\Request as FedExRequest;
use FedEx\RateService\ComplexType;
use FedEx\RateService\SimpleType;

class PakQuoteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Allow only LBS
        $weightUnits = Unit::WeightUnits()->where('title', 'LBS')->pluck('title', 'id');
        return view('user.pak.create', compact('weightUnits'));
    }    
}
