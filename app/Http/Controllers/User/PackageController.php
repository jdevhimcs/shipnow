<?php

namespace App\Http\Controllers\User;

use App\Models\Unit;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use FedEx\RateService\Request as FedExRequest;
use FedEx\RateService\ComplexType;
use FedEx\RateService\SimpleType;

class PackageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dimensionUnits = Unit::DimensionUnits()->pluck('title', 'id');

        return view('user.package.create', compact('dimensionUnits'));
    }    
}
