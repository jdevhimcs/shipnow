<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Shipment;

class MyShipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Shipment::when($request->tracking_number, function ($query, $trackNumber) {
			return $query->whereTrackNumber($trackNumber);
        })
		->when($request->city_from, function ($query, $city) {
			return $query->whereCityOrigin($city);
        })
        ->when($request->city_to, function ($query, $city) {
			return $query->whereCityDestination($city);
		})
        ->when($request->address_from, function ($query, $address) {
			return $query->whereAddressOrigin($address);
        })
        ->when($request->address_to, function ($query, $address) {
			return $query->whereAddressDestination($address);
		})
        ->when($request->ref_number, function ($query, $refNumber) {
			return $query->whereRefNumber($refNumber);
        })
        ->when($request->carrier, function ($query, $carrier) {
			return $query->whereCarrier($carrier);
        })
        ->when($request->status, function ($query, $status) {
			return $query->whereStatus($status);
        })
		->whereShipDateBetween($request->ship_from_date, $request->ship_to_date)
        ->whereOrder('shipments.id', 'DESC');

		$shipments = $query->get();

        return view('user.my-shipments.index', compact('shipments', 'request'));
    }
}
