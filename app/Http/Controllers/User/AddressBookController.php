<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AddressBook;
use App\Models\Country;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class AddressBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userId = Auth::user()->id;

        //Address Books
        $addressBooks = AddressBook::where('user_id', $userId);

        if ($request->has('filter')) {
            $addressBooks->where(function($query) use($request) {                
                $query->where('company_name', 'like', '%'.$request->filter.'%')
                ->orWhere('email', 'like', '%'.$request->filter.'%');
            });
        }

        $addressBooks = $addressBooks->paginate(15);

        //Country
        $countries = Country::get(['name', 'id']);

        return view('user.address-book.index', compact('addressBooks', 'countries', 'request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

		$messages = [
			'company_name.required' => 'Company is required',
			'address.required' => 'The address is required',
			'email.required' => 'The Email is required',
            'postal_code.required' => 'Postal Code is required.',
            'phone_no.required' => 'Phone Number is required.',
			'company_country.required' => 'The country is required',                        
            'company_state.required' => 'The State is required',
			'company_city.required' => 'The city is required',
			'is_picked.required' => 'Please select atleast one option.',
            'is_picked.min' => 'Please select atleast one option.',
            'is_business_dock.required' => 'Please select atleast one option.',
            'is_business_dock.min' => 'Please select atleast one option.',
            'appointment_delivery.required' => 'Please select atleast one option.',
            'appointment_delivery.min' => 'Please select atleast one option.',
            'straight_truck.required' => 'Please select atleast one option.',
            'straight_truck.min' => 'Please select atleast one option.',            
            'ship_hours_from.required' => 'Ship Hours From is required.',
            'ship_hours_to.required' => 'Ship Hours To is required.',
            'note.required' => 'Note is required.',
		];

        $validator = Validator::make($input, [
            'company_name' => 'required|string',
            'address' => 'required|string',
            'email' => 'required|string|email',
            'postal_code' => 'required|string',
            'phone_no' => 'required|string',
            'company_country' => 'required',
            'company_state' => 'required',
            'company_city' => 'required|string|max:255'  ,
            'is_picked' => 'min:1|required',
            'is_business_dock' => 'min:1|required',
            'appointment_delivery' => 'min:1|required',
            'straight_truck' => 'min:1|required',
            'ship_hours_from' => 'required',
            'ship_hours_to' => 'required',
            'note' => 'required',
		], $messages);


		if ($validator->passes()) {
			$userId = Auth::user()->id;			

			//Address Create
			AddressBook::create([
                'user_id' => $userId,
				'company_name' => $input['company_name'],
				'address'  => $input['address'],
				'country_id'  => $input['company_country'],
				'state_id'  => $input['company_state'],
				'city'  => $input['company_city'],
                'postal_code'=> $input['postal_code'],
                'email'=> $input['email'],
                'phone_no'=> $input['phone_no'],
                'ship_hours_from'=> $input['ship_hours_from'],
                'ship_hours_to'=> $input['ship_hours_to'],
                'note'=> $input['note'],
                'is_picked'=> $input['is_picked'],
                'is_business_dock'=> $input['is_business_dock'],
                'is_residence'=> $input['is_residence'],
                'appointment_delivery'=> $input['appointment_delivery'],
                'straight_truck'=> $input['straight_truck'],
			]);
			
			session()->flash('message', 'Address book created successfully');

			return response()->json(['success' => true, 'statusCode'=>200]);
		}

		return response()->json(['errors' => $validator->errors(), 'statuscode' => 422]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $addressBook = AddressBook::findOrFail($id);

        //Country
        $countries = Country::get(['name', 'id']);

        return view('user.address-book.edit', compact('addressBook', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

		$messages = [
			'company_name.required' => 'Company is required',
			'address.required' => 'The address is required',
			'email.required' => 'The Email is required',
            'postal_code.required' => 'Postal Code is required.',
            'phone_no.required' => 'Phone Number is required.',
			'company_country.required' => 'The country is required',                        
            'company_state.required' => 'The State is required',
			'company_city.required' => 'The city is required',
			'is_picked.required' => 'Please select atleast one option.',
            'is_picked.min' => 'Please select atleast one option.',
            'is_business_dock.required' => 'Please select atleast one option.',
            'is_business_dock.min' => 'Please select atleast one option.',
            'appointment_delivery.required' => 'Please select atleast one option.',
            'appointment_delivery.min' => 'Please select atleast one option.',
            'straight_truck.required' => 'Please select atleast one option.',
            'straight_truck.min' => 'Please select atleast one option.',            
            'ship_hours_from.required' => 'Ship Hours From is required.',
            'ship_hours_to.required' => 'Ship Hours To is required.',
            'note.required' => 'Note is required.',
		];

        $request->validate([
            'company_name' => 'required|string',
            'address' => 'required|string',
            'email' => 'required|string|email',
            'postal_code' => 'required|string',
            'phone_no' => 'required|string',
            'company_country' => 'required',
            'company_state' => 'required',
            'company_city' => 'required|string|max:255'  ,
            'is_picked' => 'min:1|required',
            'is_business_dock' => 'min:1|required',
            'appointment_delivery' => 'min:1|required',
            'straight_truck' => 'min:1|required',
            'ship_hours_from' => 'required',
            'ship_hours_to' => 'required',
            'note' => 'required',
		], $messages);

		
        $userId = Auth::user()->id;			

        $addressBook = AddressBook::findOrFail($id);

        //Address Create
        $addressBook->update([
            'company_name' => $input['company_name'],
            'address'  => $input['address'],
            'country_id'  => $input['company_country'],
            'state_id'  => $input['company_state'],
            'city'  => $input['company_city'],
            'postal_code'=> $input['postal_code'],
            'email'=> $input['email'],
            'phone_no'=> $input['phone_no'],
            'ship_hours_from'=> $input['ship_hours_from'],
            'ship_hours_to'=> $input['ship_hours_to'],
            'note'=> $input['note'],
            'is_picked'=> $input['is_picked'],
            'is_business_dock'=> $input['is_business_dock'],
            'is_residence'=> $input['is_residence'],
            'appointment_delivery'=> $input['appointment_delivery'],
            'straight_truck'=> $input['straight_truck'],
        ]);
        
        session()->flash('message', 'Address book updated successfully');
        
        return redirect()->route('user.address-book-list.index')->with('message', "Address Book has been updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = decrypt($id);

		//AddressBook
		$addressBook = AddressBook::find($id);

		$addressBook->delete();

		return redirect()->back()->with('message', "Address Book has been deleted successfully.");
    }
}
