<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\OrderPreference;
use App\Models\Language;
use App\Models\Unit;
use App\Models\OrderBroker;
use App\Models\OrderReference;
use App\Models\Package;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OrderPreferenceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = Auth::user()->id;

        //Order Preferences
        $orders = OrderPreference::where('user_id', $userId)->get();

        return view('user.order-preferences.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Languages
        $languages = Language::pluck('title', 'id');

        //Units
		$units = Unit::pluck('title', 'id');

		//Packages
        $packages = Package::pluck('title', 'id');

        return view('user.order-preferences.create', compact('languages', 'units', 'packages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Auth::user()->id;

		$messages = [
            'references.*.ref_title.required' => 'This field is required',
			'brokers.*.broker_title.required' => 'This field is required',
			'order_email.required' => 'The Email is required',
			'order_phone.required' => 'The Phone Number is required',
			// 'carrier_name.required' => 'The Carrier name is required',
			'label_size.required' => 'The Label size is required',
			'languages.required' => 'Please select atleast one option.',
			'languages.min' => 'Please select atleast one option.',
			'units.required' => 'Please select atleast one option.',
			'units.min' => 'Please select atleast one option.',
		];

        $request->validate([
			'references.*.ref_title' => ['required', 'string', 'max:255'],
			'brokers.*.broker_title' => ['required', 'string', 'max:255'],
            'order_email'  => 'required|string|email|max:255',
            'order_phone'  => 'required|string|max:255',
            // 'carrier_name'  => 'nullable',
			'label_size' => 'required|string|max:255',
			'languages' => 'array|min:1|required',
            'units' => 'array|min:1|required',
            'package' => 'required'
		], $messages);

        $input = $request->all();

        //Order Preferences
        $order = OrderPreference::create([
            'user_id' => $id,
            // 'reference' => $input['reference'],
            // 'broker_name'  => $input['broker_name'],
            'email'  => $input['order_email'],
            'phone'  => $input['order_phone'],
            // 'carrier_name'  => null,
            'label_size'=> $input['label_size'],
        ]);

        //Order Languages
        $order->languages()->attach($request->languages);

        //Order Units
		$order->units()->attach($request->units);

		//Order Default Package
        $order->orderPreferencePackage()->updateOrCreate(['order_preference_id' => $id], [
			'package_id' => (int) $request->package
		]);

        //References
        if($request->has('references'))
        {
            foreach($request->references as $key => $reference)
            {
                //Save Reference
                OrderReference::create([
                    'order_preference_id' => $order->id,
                    'reference_title' => $reference['ref_title'],
                ]);
            }
        }

        //Custome Brokers
        if($request->has('brokers'))
        {
            foreach($request->brokers as $key => $broker)
            {
                //Save Broker
                OrderBroker::create([
                    'order_preference_id' => $order->id,
                    'broker_title' => $broker['broker_title'],
                ]);
            }
        }

        return redirect()->route('user.order-preferences.index')->with('message', "Order Preferences has been added successfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = OrderPreference::with(['languages', 'units'])->findOrFail($id);

        //Languages
        $languages = Language::pluck('title', 'id');

        //Units
		$units = Unit::pluck('title', 'id');
		
		//Packages
        $packages = Package::pluck('title', 'id');

        return view('user.order-preferences.edit', compact('order', 'languages', 'units', 'packages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$input = $request->all();

		$messages = [
            'references.*.ref_title.required' => 'This field is required',
			'brokers.*.broker_title.required' => 'This field is required',
			'order_email.required' => 'The Email is required',
			'order_phone.required' => 'The Phone Number is required',
			// 'carrier_name.required' => 'The Carrier name is required',
			'label_size.required' => 'The Label size is required',
			'languages.required' => 'Please select atleast one option.',
			'languages.min' => 'Please select atleast one option.',
			'units.required' => 'Please select atleast one option.',
			'units.min' => 'Please select atleast one option.',
		];

        $request->validate([
			'references.*.ref_title' => ['required', 'string', 'max:255'],
			'brokers.*.broker_title' => ['required', 'string', 'max:255'],
            'order_email'  => 'required|string|email|max:255',
            'order_phone'  => 'required|string|max:255',
            // 'carrier_name'  => 'nullable',
			'label_size' => 'required|string|max:255',
			'languages' => 'array|min:1|required',
			'units' => 'array|min:1|required',
            'package' => 'required'
		], $messages);
        //Order
        $order = OrderPreference::findOrFail($id);

        //Order Preferences
        $order->update([
            'email'  => $input['order_email'],
            'phone'  => $input['order_phone'],
            // 'carrier_name'  => $input['carrier_name'],
            'label_size'=> $input['label_size'],
        ]);

        //Order Languages
        $order->languages()->sync($request->languages);

        //Order Units
        $order->units()->sync($request->units);
		
		//Order Default Package
        $order->orderPreferencePackage()->updateOrCreate(['order_preference_id' => $id], [
			'package_id' => (int) $request->package
		]);

		 //References
        if($request->has('references'))
        {
            foreach($request->references as $key => $reference)
            {
				$id = !empty($reference['id']) ? $reference['id'] : NULL;
                //Save Reference
                OrderReference::updateOrCreate(['id' => $id], [
                    'order_preference_id' => $order->id,
                    'reference_title' => $reference['ref_title'],
                ]);
            }
        }

        //Custome Brokers
        if($request->has('brokers'))
        {
            foreach($request->brokers as $key => $broker)
            {
				$id = !empty($broker['id']) ? $broker['id'] : NULL;
                //Save Broker
                OrderBroker::updateOrcreate(['id' => $id], [
                    'order_preference_id' => $order->id,
                    'broker_title' => $broker['broker_title'],
                ]);
            }
		}

        session()->flash('message', 'Order Preferences updated successfully');

        return redirect()->route('user.order-preferences.index')->with('message', "Order Preferences has been updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$id = decrypt($id);

		//Order
		$order = OrderPreference::find($id);

		$order->languages()->detach();

		$order->units()->detach();

		$order->delete();

		return redirect()->back()->with('message', "Order Preferences has been deleted successfully.");
    }
}
