<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator,Redirect,Response;
use App\Models\{Country,State,City};

class CountryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct()
    {
        $this->middleware(['auth','verified']);
	}
	
    public function getStates(Request $request)
    {
        $data['states'] = State::where('country_id',$request->country_id)
                    ->get(['name', 'id']);
        return response()->json($data);
    }

    public function getCities(Request $request)
    {
        $data['cities'] = City::where('state_id',$request->state_id)
                    ->get(['name', 'id']);
        return response()->json($data);
    }

}