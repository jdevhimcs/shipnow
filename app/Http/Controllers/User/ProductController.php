<?php

namespace App\Http\Controllers\User;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id = Auth::user()->id;
        $products = Product::where('user_id',$id);
        $filter = $request->get('filter');
        if($filter){
            $products->where(function($query) use ($filter){
                $query->where('name','like',"%".$filter."%");
            });
        }
        $products = $products->paginate(15);
        return view('user.products.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Auth::user()->id;
        // dd($request->all());
        $request->validate([
            'name'          => 'bail|required|string',
            'description'   => 'bail|required|string',
            'length'        => 'bail|required',
            'height'        => 'bail|required',
            'width'         => 'bail|required',
            'weight'        => 'bail|required',
            'class'         => 'bail|required|string',
        ]);

        $product = Product::create([
            'user_id'       => $id,
            'name'          => $request->name,
            'description'   => $request->description,
            'length'        => $request->length,
            'height'        => $request->height,
            'width'         => $request->width,
            'weight'        => $request->weight,
            'class'         => $request->class,
        ]);

        return redirect()->route('user.products.index')->with('success', "Product has been added successfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$product = Product::find($id);
        $html = view('user.products.edit',compact('product'))->render();
        return $html;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$user_id = Auth::user()->id;
        $request->validate([
            'name'          => 'bail|required|string',
            'description'   => 'bail|required|string',
            'length'        => 'bail|required',
            'height'        => 'bail|required',
            'width'         => 'bail|required',
            'weight'        => 'bail|required',
            'class'         => 'bail|required|string',
		]);
		
		$product = Product::find($id);
		$product->user_id       = $user_id;
		$product->name          = $request->name;
		$product->description   = $request->description;
		$product->length        = $request->length;
		$product->height        = $request->height;
		$product->width         = $request->width;
		$product->weight        = $request->weight;
		$product->class         = $request->class;
		$product->save();
		
		return redirect()->route('user.products.index')->with('success', "Product has been updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$product = Product::find($id);

		$product->delete();
		return redirect()->route('user.products.index')->with('success', "Product has been deleted successfully.");
    }
}
