<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CourierApiController extends Controller
{
    
    public function canparApiCourierTracking() {

        require  base_path() . '/vendor/autoload.php';

        $key = '7ccbf30f-7257-47c3-be4c-42c5506cd0f6';

		//List All couriers
		$couriers = new \AfterShip\Couriers('AFTERSHIP_API_KEY');
		$response = $couriers->get();
		dd($response);

		//Trackings
        $trackings = new \AfterShip\Trackings($key);
        $tracking_info = [
            'slug'    => 'dhl',
            'title'   => 'My Title',
        ];
        $response = $trackings->create('RA123456789', $tracking_info);
        dd($response);

        $trackings = new \AfterShip\Trackings('AFTERSHIP_API_KEY');
        $params = array(
            'smses'             => [],
            'emails'            => [],
            'title'             => '',
            'customer_name'     => '',
            'order_id'          => '',
            'order_id_path'     => '',
            'custom_fields'     => []
        );
        $response = $trackings->update('dhl', 'RA123456789US', $params);
        
        $couriers = new \AfterShip\Couriers($key);
        $response = $couriers->get();
		dd($response);
		
        $trackings = new \AfterShip\Trackings($key);
        $last_check_point = new \AfterShip\LastCheckPoint($key);
	}
	
    public function arcbestapiGetRates() {
        $params = [
            'DL' => '2',
            'ID' => 'Q6PJZPF5',
            'ShipCity' => 'DALLAS',
            'ShipState' => 'TX',
            'ShipZip' => '75201',
            'ShipCountry' => 'US',
            'ConsCity' => 'TULSA',
            'ConsState' => 'OK',
            'ConsZip' => '74104',
            'ConsCountry' => 'US',
            'FrtLng1' => '48',
            'FrtWdth1' => '48',
            'FrtHght1' => '48',
            'FrtLWHType' => 'IN',
            'UnitNo1' => '3',
            'UnitType1' => 'PLT',
            'Wgt1' => '400',
            'Class1' => '50.0',
            'ShipAff' => 'Y',
            'ShipMonth' => '01',
            'ShipDay' => '07',
            'ShipYear' => '2021',
            'Test' => 'Y'
        ];
        $response = Http::withHeaders([
            'cache-control' => 'no-cache',
            'content-type' => 'application/json'
        ])->post('https://www.abfs.com/xml/aquotexml.asp', $params);
    
        try {
            dd($response);
        } catch (HttpException $ex) {
            dd($ex);
        }        
	}
	

	public function canadaPostApiGetRates() {
		// $sandBoxUrl 	= 'https://ct.soa-gw.canadapost.ca.';

		$service_url = 'https://ct.soa-gw.canadapost.ca/rs/ship/price';
		
		$usernameTemp = "jatin.ucc";
		
		$passTemp = "Jatin.kumar1";
		
		$customerNumberTemp = '2004381';
		
		$contractIdTemp 	= '42708517';
		
		$apiKeyTemp 		= '6e93d53968881714:0bfa9fcb9853d1f51ee57a';

		// Create GetRates request xml
		$originPostalCode = 'H2B1A0';

		$postalCode = 'K1K4T3';

		$weight = 1;

		$auth = 'Basic '. base64_encode ($usernameTemp . ':' . $passTemp);

		$response = Http::withHeaders([
			'Authorization' => $auth,
			"Content-Type" => "application/vnd.cpc.ship.rate-v4+xml",
			"Accept" => "application/vnd.cpc.ship.rate-v4+xml"
		])->send("POST", $service_url, [
			"body" => '<?xml version="1.0" encoding="UTF-8"?>
					<mailing-scenario xmlns="http://www.canadapost.ca/ws/ship/rate-v4">
					<customer-number>{$customerNumberTemp}</customer-number>
					<parcel-characteristics>
						<weight>{$weight}</weight>
					</parcel-characteristics>
					<origin-postal-code>{$originPostalCode}</origin-postal-code>
					<destination>
						<domestic>
						<postal-code>{$postalCode}</postal-code>
						</domestic>
					</destination>
					</mailing-scenario>'
		]);

		dd($response);
	}
}
