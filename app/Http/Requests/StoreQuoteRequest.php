<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreQuoteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name'	=> ['required', 'string', 'max:100'],
            'name'	        => ['required', 'string', 'max:100'],
            'email'	        => ['required', 'string', 'email'],
            'phone_number'	=> ['required', 'string', 'max:255'],
            'comment'	    => ['required', 'string', 'max:500'],
            'ship'	        => ['required', 'array'],
        ];
    }

    /**
     * Messages
     *
     * @param  NULL
     * @return array
     */
    public function messages() {
        return [
            'required' => 'The :attribute field is required.',
			'email' => 'The :attribute must be a valid :attribute address',
			'ship.required' => 'Please select atleast one option'
        ];
    }
}
