<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Auth;
use Closure;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            if($request->is('admin/*')){
                return route('admin.login');
            }
            return route('login');
        }
    }

    public function handel($request, Closure $next, ...$guards)
    {
        $this->authenticate($request, $guards);
        
        $is_admin_url = $request->is('admin/*');


        return $next($request);
        
        
    }
}
