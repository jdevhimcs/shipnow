<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        $isAllow = false;

        foreach ($roles as $key => $role) {

            $user = $request->user();

            if ($user === null) {
               return redirect('/login');
            }

            if($user->hasRole($role)) {
                $isAllow = true;
                break;
            }
        }

        if (! $isAllow) {
            abort(401, 'This action is unauthorized.');
        }

        return $next($request);
    }
}
