<?php

namespace App\Http\Traits;

use App\Models\Unit;
use Illuminate\Http\Request;
use FedEx\RateService\Request as FedExRequest;
use FedEx\RateService\ComplexType;
use FedEx\RateService\SimpleType;
use FedEx\ShipService\Request as FedExShipServiceRequest;
use FedEx\ShipService\ComplexType as FedExShipServiceComplexType;
use FedEx\ShipService\SimpleType as FedExShipServiceSimpleType;
use FedEx\OpenShipService\Request as OpenShipRequest;
use FedEx\OpenShipService\ComplexType as OpenShipComplexType;
use FedEx\OpenShipService\SimpleType as OpenShipSimpleType;
use UpsRate;
use UpsRateTimeInTransit;
use UpsShipping;

trait ShipmentRateTrait {

	/**
     * Get Shipment Rates: via FeDEX API
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getFedexShipmentRate($input) {
        $rateRequest = new ComplexType\RateRequest();

        //authentication & client details
		$rateRequest->WebAuthenticationDetail->UserCredential->Key = Config('fedex.FEDEX_API_KEY');
		$rateRequest->WebAuthenticationDetail->UserCredential->Password = Config('fedex.FEDEX_PASSWORD');
		$rateRequest->ClientDetail->AccountNumber = Config('fedex.FEDEX_ACCOUNT_NUMBER');
		$rateRequest->ClientDetail->MeterNumber = Config('fedex.FEDEX_METER_NUMBER');

		$rateRequest->TransactionDetail->CustomerTransactionId = 'Rate Service Request';

		//version
		$rateRequest->Version->ServiceId = 'crs';
		$rateRequest->Version->Major = 28;
		$rateRequest->Version->Minor = 0;
		$rateRequest->Version->Intermediate = 0;

		$rateRequest->ReturnTransitAndCommit = true;

		//shipper
		// $rateRequest->RequestedShipment->PreferredCurrency = $input['currency'];
		// $rateRequest->RequestedShipment->Shipper->Address->StreetLines = (array) $input['ship_from_street'];
		$rateRequest->RequestedShipment->Shipper->Address->City = $input['ship_from_city'];
		$rateRequest->RequestedShipment->Shipper->Address->StateOrProvinceCode = $input['ship_from_state'];
		$rateRequest->RequestedShipment->Shipper->Address->PostalCode = $input['ship_from_postal'];
		$rateRequest->RequestedShipment->Shipper->Address->CountryCode = $input['ship_from_country'];

		//recipient
		// $rateRequest->RequestedShipment->Recipient->Address->StreetLines = (array) $input['ship_to_street'];
		$rateRequest->RequestedShipment->Recipient->Address->City = $input['ship_to_city'];
		$rateRequest->RequestedShipment->Recipient->Address->StateOrProvinceCode = $input['ship_to_state'];
		$rateRequest->RequestedShipment->Recipient->Address->PostalCode = $input['ship_to_postal'];
		$rateRequest->RequestedShipment->Recipient->Address->CountryCode = $input['ship_to_country'];

		//shipping charges payment
		$rateRequest->RequestedShipment->ShippingChargesPayment->PaymentType = SimpleType\PaymentType::_SENDER;

		//rate request types
		$rateRequest->RequestedShipment->RateRequestTypes = [SimpleType\RateRequestType::_LIST];

		// For double
		// $rateRequest->RequestedShipment->RateRequestTypes = [SimpleType\RateRequestType::_PREFERRED, SimpleType\RateRequestType::_LIST];

		if ($input['type'] == 'PAK' || $input['type'] == 'LETTER') {
			// Set Package type
			$rateRequest->RequestedShipment->PackagingType =  $input['type'] == 'LETTER' ? 'FEDEX_ENVELOPE' :'FEDEX_PAK';

			$rateRequest->RequestedShipment->PackageCount = !empty($input['item']) ? count($input['item']) : '';

			//Package dynamic
			$lineItemsArr = [];


			if(!empty($input['item'])) {
				foreach ($input['item'] as $key => $value) {
					//Create package for mulitple line items
					array_push($lineItemsArr, new ComplexType\RequestedPackageLineItem());

					//Package Per line item
					$rateRequest->RequestedShipment->RequestedPackageLineItems = $lineItemsArr;
					$rateRequest->RequestedShipment->RequestedPackageLineItems[$key]->Weight->Value = $value['weight'];
					$rateRequest->RequestedShipment->RequestedPackageLineItems[$key]->Weight->Units = $this->getFedExWeightUnit($value['weight_unit_id']);
					$rateRequest->RequestedShipment->RequestedPackageLineItems[$key]->GroupPackageCount = 1;
				}
			}
		}

		if ($input['type'] == 'PACKAGE') {
			// Set Package type
			$rateRequest->RequestedShipment->PackagingType = 'YOUR_PACKAGING';

			$rateRequest->RequestedShipment->PackageCount = !empty($input['item']) ? count($input['item']) : '';

			//Package dynamic
			$lineItemsArr = [];

			if(!empty($input['item'])) {
				foreach ($input['item'] as $key => $value) {
					//Create package for mulitple line items
					array_push($lineItemsArr, new ComplexType\RequestedPackageLineItem());

					//Package Per line item
					$rateRequest->RequestedShipment->RequestedPackageLineItems = $lineItemsArr;
					$rateRequest->RequestedShipment->RequestedPackageLineItems[$key]->Weight->Value = $value['weight'];
					$rateRequest->RequestedShipment->RequestedPackageLineItems[$key]->Weight->Units = $this->getFedExWeightUnit($value['weight_unit_id']); //LB or KG
					$rateRequest->RequestedShipment->RequestedPackageLineItems[$key]->Dimensions->Length = $value['length'];
					$rateRequest->RequestedShipment->RequestedPackageLineItems[$key]->Dimensions->Width = $value['width'];
					$rateRequest->RequestedShipment->RequestedPackageLineItems[$key]->Dimensions->Height = $value['height'];
					$rateRequest->RequestedShipment->RequestedPackageLineItems[$key]->Dimensions->Units = $this->getFedExDimensionUnit($value['dimension_unit_id']); //LB or KG
					$rateRequest->RequestedShipment->RequestedPackageLineItems[$key]->GroupPackageCount = 1;

					// Set signature option
					if(!empty($input['is_signature_required'])) {
						$rateRequest->RequestedShipment->RequestedPackageLineItems[$key]->SpecialServicesRequested->SpecialServiceTypes= 'SIGNATURE_OPTION';
						$rateRequest->RequestedShipment->RequestedPackageLineItems[$key]->SpecialServicesRequested->SignatureOptionDetail->OptionType = SimpleType\SignatureOptionType::_DIRECT;
					}

					/*
					//Insurance
					$rateRequest->RequestedShipment->RequestedPackageLineItems[$key]->InsuredValue = new ComplexType\Money([
						'Currency' => (string) $input['currency'],
						'Amount' => floatval($input['insurance_amount'])
					]);
					*/
				}
			}
		}

		$rateServiceRequest = new FedExRequest();

		// For production
		if (!Config('fedex.FEDEX_SANDBOX')) {
			$rateServiceRequest->getSoapClient()->__setLocation(FedExRequest::PRODUCTION_URL); //use production URL
		}

		$rateReply = $rateServiceRequest->getGetRatesReply($rateRequest); // send true as the 2nd argument to return the SoapClient's stdClass response.

		$rates = [];
		$errorMessage = NULL;

		// In case of failure and warning
		if ($rateReply->HighestSeverity == 'FAILURE' || $rateReply->HighestSeverity == 'ERROR' || $rateReply->HighestSeverity == 'WARNING' || $rateReply->HighestSeverity == 'NOTE') {
			$errorMessage = $this->getCombinedErrorMessage($rateReply->Notifications);
		}

		// In case of success response
		if ($rateReply->HighestSeverity == 'SUCCESS' || $rateReply->HighestSeverity == 'NOTE') {
		    foreach ($rateReply->RateReplyDetails as $rate) {
				$rates[$rate->ServiceType]['service_code'] = $rate->ServiceType;
				$rates[$rate->ServiceType]['total_base_charge'] = $rate->RatedShipmentDetails[0]->ShipmentRateDetail->TotalBaseCharge->Amount;
				$rates[$rate->ServiceType]['total_taxes'] = $rate->RatedShipmentDetails[0]->ShipmentRateDetail->TotalTaxes->Amount;
				$rates[$rate->ServiceType]['amount'] = $rate->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Amount;
				$rates[$rate->ServiceType]['sur_charges'] = $rate->RatedShipmentDetails[0]->ShipmentRateDetail->TotalSurcharges->Amount;
				$rates[$rate->ServiceType]['transport_charges'] = 0;
				$rates[$rate->ServiceType]['service_charges'] = 0;;
				$rates[$rate->ServiceType]['currency'] = $rate->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Currency;
				$rates[$rate->ServiceType]['delivery_timestamp'] = date('Y-m-d H:i:s', strtotime($rate->DeliveryTimestamp));
				$rates[$rate->ServiceType]['type'] = 'fedex';
				$rates[$rate->ServiceType]['logo'] = '/img/fedex_logo_sm.png';
		    }
		}

		$result = [
			'error' => $errorMessage,
            'rates' => $rates
		];

		return $result;
	}

	/**
     * Get combined error message of all types
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function getCombinedErrorMessage($notifications)
    {
    	$messages = '';
    	foreach($notifications as $noteKey => $note) {
			$notification = $note->toArray();
			$messages.= "FedEx Failure: ";
    		$messages.= $notification['Message'];

    		if (count($notifications) !== $noteKey+1) {
    			$messages.= '<br>';
    		}
		}

		return $messages;
    }

	/**
     * Get Shipment Rates Time in Transit: Via UPS API
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getUpsShipmentRateTimeInTransit($input) {
		try {
			$upsRates = [];

			$totalWeight = 0;

			$shipment = new \Ups\Entity\Shipment();

		    $shipperAddress = $shipment->getShipper()->getAddress();
			$shipperAddress->setPostalCode($input['ship_from_postal']);

		    $address = new \Ups\Entity\Address();
			$address->setPostalCode($input['ship_from_postal']);
			$address->setCity($input['ship_from_city']);
			$address->setStateProvinceCode($input['ship_from_state']);
			$address->setCountryCode($input['ship_from_country']);
		    $shipFrom = new \Ups\Entity\ShipFrom();
		    $shipFrom->setAddress($address);

		    $shipment->setShipFrom($shipFrom);

		    $shipTo = $shipment->getShipTo();
			$shipToAddress = $shipTo->getAddress();
			$shipToAddress->setPostalCode($input['ship_to_postal']);

			$addressTo = new \Ups\Entity\Address();
			$addressTo->setPostalCode($input['ship_to_postal']);
			$addressTo->setCity($input['ship_to_city']);
			$addressTo->setStateProvinceCode($input['ship_to_state']);
			$addressTo->setCountryCode($input['ship_to_country']);
		    $shipToo = new \Ups\Entity\ShipTo();
		    $shipToo->setAddress($addressTo);

			$shipment->setShipTo($shipToo);

			if(!empty($input['is_residential'])) {
				$shipToAddress->setResidentialAddressIndicator(true);
			}

			if ($input['type'] == 'PAK' || $input['type'] == 'LETTER') {
				if(!empty($input['item'])) {
            		foreach ($input['item'] as $key => $value) {
						$package = new \Ups\Entity\Package();
						$package->getPackagingType()->setCode($input['type'] == 'LETTER' ? \Ups\Entity\PackagingType::PT_UPSLETTER : \Ups\Entity\PackagingType::PT_PAK);
						$package->getPackageWeight()->setWeight($value['weight']);

						// if you need this (depends of the shipper country)
						$weightUnit = new \Ups\Entity\UnitOfMeasurement;
						$weightUnit->setCode($this->getUpsWeightUnit($value['weight_unit_id']));
						$package->getPackageWeight()->setUnitOfMeasurement($weightUnit);
						$shipment->addPackage($package);

						//Shipment Weight & unit international Shipment
						$totalWeight += $value['weight'];
						$shipmentTotalWeight = new \Ups\Entity\ShipmentTotalWeight();
						$shipmentTotalWeight->setWeight($totalWeight);
						$shipmentTotalWeight->setUnitOfMeasurement($weightUnit);
						$shipment->setShipmentTotalWeight($shipmentTotalWeight);
					}
				}
			}

			if ($input['type'] == 'PACKAGE') {
				if(!empty($input['item'])) {
            		foreach ($input['item'] as $key => $value) {
						$itemWeightUnit = $this->getUpsWeightUnit($value['weight_unit_id']);

						$itemDimensionUnit = $this->getUpsDimensionUnit($value['dimension_unit_id']);

						$package = new \Ups\Entity\Package();
						$package->getPackagingType()->setCode(\Ups\Entity\PackagingType::PT_PACKAGE);
						$package->getPackageWeight()->setWeight($value['weight']);

						// if you need this (depends of the shipper country)
						$weightUnit = new \Ups\Entity\UnitOfMeasurement;
						$weightUnit->setCode($itemWeightUnit);
						$package->getPackageWeight()->setUnitOfMeasurement($weightUnit);

						$dimensions = new \Ups\Entity\Dimensions();
						$dimensions->setHeight($value['length']);
						$dimensions->setWidth($value['width']);
						$dimensions->setLength($value['length']);

						$unit = new \Ups\Entity\UnitOfMeasurement;
						$unit->setCode($itemDimensionUnit);

						$dimensions->setUnitOfMeasurement($unit);
						$package->setDimensions($dimensions);

						if(!empty($input['is_signature_required'])) {
							$deliveryConfirmation = new \Ups\Entity\DeliveryConfirmation;
							$deliveryConfirmation->setDcisType(1);

							$packageServiceOptions = new \Ups\Entity\PackageServiceOptions();
							$packageServiceOptions->setDeliveryConfirmation($deliveryConfirmation);
							$package->setPackageServiceOptions($packageServiceOptions);
						}

						$shipment->addPackage($package);

						//Shipment Weight & unit international Shipment
						$totalWeight += $value['weight'];
						$shipmentTotalWeight = new \Ups\Entity\ShipmentTotalWeight();
						$shipmentTotalWeight->setWeight($totalWeight);
						$shipmentTotalWeight->setUnitOfMeasurement($weightUnit);
						$shipment->setShipmentTotalWeight($shipmentTotalWeight);
					}
				}
			}

			$deliveryTimeInformation = new \Ups\Entity\DeliveryTimeInformation();
		    $deliveryTimeInformation->setPackageBillType(\Ups\Entity\DeliveryTimeInformation::PBT_NON_DOCUMENT);

		    // $pickup = new \Ups\Entity\Pickup();
		    // $pickup->setDate("20210220");
		    // $pickup->setTime("160000");
			$shipment->setDeliveryTimeInformation($deliveryTimeInformation);

		    // Insurance is in PackageServiceOptions so we can use them

			$rates = UpsRateTimeInTransit::shopRatesTimeInTransit($shipment);

			foreach ($rates->RatedShipment as $rate) {
				$upsRates[$rate->Service->getName()]['service_code'] = $rate->Service->getCode();
				$upsRates[$rate->Service->getName()]['total_base_charge'] = 0;
				$upsRates[$rate->Service->getName()]['total_taxes'] = 0;
				$upsRates[$rate->Service->getName()]['delivery_timestamp'] = date('Y-m-d H:i:s', strtotime($rate->TimeInTransit->ServiceSummary->getEstimatedArrival()->getArrival()->getTime()));
				$upsRates[$rate->Service->getName()]['transport_charges'] = $rate->TransportationCharges->MonetaryValue;
				$upsRates[$rate->Service->getName()]['service_charges'] = $rate->ServiceOptionsCharges->MonetaryValue;
				$upsRates[$rate->Service->getName()]['sur_charges'] = 0;
				$upsRates[$rate->Service->getName()]['amount'] = $rate->TotalCharges->MonetaryValue;
				$upsRates[$rate->Service->getName()]['currency'] = $rate->TotalCharges->CurrencyCode;
				$upsRates[$rate->Service->getName()]['type'] = 'ups';
				$upsRates[$rate->Service->getName()]['logo'] = '/img/ups_logo.png';
			}

			$result = [
				'error' => implode("<br />", $this->getUPSCombinedErrorMessage($rates->RatedShipment)),
				'rates' => $upsRates
			];

			return $result;

		} catch(\Ups\Exception\InvalidResponseException $e) {
			$error = $e->getMessage();
			return [
				'error' => 'UPS ' . $error,
				'rates' => []
			];
		} catch (Exception $e) {
			$error = $e->getMessage();
			return [
				'error' => 'UPS ' . $error,
				'rates' => []
			];
		}
	}

	/**
     * Get combined UPS error message of all types
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function getUPSCombinedErrorMessage($ratedShipments, array $messages=[])
    {
    	foreach($ratedShipments as $rateKey => $rate) {
    		// In case it rate is string i.e message but is empty
    		if (is_string($rate) && trim($rate) == '') {
    			continue;
    		}

    		// In case it rate is string i.e message and already exists in message array
    		// To avoid duplicacy
    		if (is_string($rate) && in_array($rate, $messages)) {
    			continue;
    		}

    		// In case it rate is string i.e message and didn't exists in message array
    		if (is_string($rate)) {
    			array_push($messages, $rate);
				continue;
    		}

    		// In case it is array with multiple messages
    		$rateShipmentWarning = $rate->RateShipmentWarning;

    		// Check if rateShipmentWarning is empty
    		if (empty($rateShipmentWarning)) {
    			continue;
    		}

    		if (is_array($rateShipmentWarning)) {
    			$messages = $this->getUPSCombinedErrorMessage($rateShipmentWarning, $messages);
    		}
		}

		return $messages;
    }

	/**
	 *  Create Shipment: Via UPI API
	 *
	 * @param $request | Array
	 * @return $data | Array | Exception
	 *
	 */
	public function createUpsShipment($quotes) {
		//Ref Number
		$order_id 	= time(); //$quotes['ref_num'];

		//Origin
		$fromCity 	= $quotes['ship_from_city'];
		$fromState 	= $quotes['ship_from_state'];
		$fromCountry= $quotes['ship_from_country'];
		$fromPostal = $quotes['ship_from_postal'];
		$fromAddress= $quotes['ship_from_address'];
		$fromCompany= $quotes['origin_company'];
		$fromAttn 	= $quotes['origin_attn_name'];
		$fromPhone 	= $quotes['origin_phone'];
		$fromEmail 	= $quotes['origin_email'];

		//Destination
		$toCity 	= $quotes['ship_to_city'];
		$toState 	= $quotes['ship_to_state'];
		$toCountry 	= $quotes['ship_to_country'];
		$toPostal 	= $quotes['ship_to_postal'];
		$toAddress 	= $quotes['ship_to_address'];
		$toCompany 	= $quotes['destination_company'];
		$toAttn 	= $quotes['destination_attn_name'];
		$toPhone 	= $quotes['destination_phone'];
		$toEmail 	= $quotes['destination_email'];

		$shipment = new \Ups\Entity\Shipment();

		// Set shipper
		$shipper = $shipment->getShipper();
		$shipper->setShipperNumber(config('app.UPS_SHIPPER_ACCOUNT_NUMBER'));
		$shipper->setName('shipnow');
		$shipper->setAttentionName('shipnow');
		$shipperAddress = $shipper->getAddress();
		$shipperAddress->setAddressLine1('395 Boulevard de');
		$shipperAddress->setPostalCode('J7V0B4');
		$shipperAddress->setCity('Pincourt');
		$shipperAddress->setStateProvinceCode('QC'); // required in US
		$shipperAddress->setCountryCode('CA');
		$shipper->setAddress($shipperAddress);
		$shipper->setPhoneNumber('112223333333');
		$shipment->setShipper($shipper);

		// From address
		$address = new \Ups\Entity\Address();
		$address->setAddressLine1($fromAddress);
		$address->setPostalCode($fromPostal);
		$address->setCountryCode($fromCountry);
		$address->setStateProvinceCode($fromState);
		$address->setCity($fromCity);
		$shipFrom = new \Ups\Entity\ShipFrom();
		$shipFrom->setAddress($address);
		$shipFrom->setName($fromCompany);
		$shipFrom->setAttentionName($shipFrom->getName());
		$shipFrom->setCompanyName($shipFrom->getName());
		$shipFrom->setPhoneNumber($fromPhone);
		$shipment->setShipFrom($shipFrom);

		// To address
		$address = new \Ups\Entity\Address();
		$address->setAddressLine1($toAddress);
		$address->setPostalCode($toPostal);
		$address->setCountryCode($toCountry);
		$address->setStateProvinceCode($toState);  // Required in US
		$address->setCity($toCity);
		$shipTo = new \Ups\Entity\ShipTo();
		$shipTo->setAddress($address);
		$shipTo->setCompanyName($toCompany);
		$shipTo->setAttentionName($toAttn);
		$shipTo->setPhoneNumber($toPhone);
		$shipment->setShipTo($shipTo);

		// Sold to
		$address = new \Ups\Entity\Address();
		$address->setAddressLine1($toAddress);
		$address->setPostalCode($toPostal);
		$address->setCountryCode($toCountry);
		$address->setStateProvinceCode($toState);
		$address->setCity($toCity);
		$soldTo = new \Ups\Entity\SoldTo;
		$soldTo->setAddress($address);
		$soldTo->setCompanyName($toCompany);
		$soldTo->setAttentionName($toAttn);
		$shipment->setSoldTo($soldTo);

		// Set service
		$service = new \Ups\Entity\Service;
		$service->setCode($quotes['service_code']); //\Ups\Entity\Service::S_STANDARD
		$service->setDescription($service->getName());
		$shipment->setService($service);

		// Mark as a return (if return)
		// if ($return) {
		// 	$returnService = new \Ups\Entity\ReturnService;
		// 	$returnService->setCode(\Ups\Entity\ReturnService::PRINT_RETURN_LABEL_PRL);
		// 	$shipment->setReturnService($returnService);
		// }

		// Set description
		$shipment->setDescription('This is testing Please ignore');

		// Add Package
		if ($quotes['type'] == 'PAK' || $quotes['type'] == 'LETTER') {
			if(!empty($quotes['items'])) {
				foreach ($quotes['items'] as $key => $value) {
					$package = new \Ups\Entity\Package();
					$package->getPackagingType()->setCode($quotes['type'] == 'LETTER' ? \Ups\Entity\PackagingType::PT_UPSLETTER : \Ups\Entity\PackagingType::PT_PAK);
					$package->getPackageWeight()->setWeight($value['weight']);

					// if you need this (depends of the shipper country)
					$weightUnit = new \Ups\Entity\UnitOfMeasurement;
					$weightUnit->setCode($this->getUpsWeightUnit($value['weight_unit_id']));
					$package->getPackageWeight()->setUnitOfMeasurement($weightUnit);

					$shipment->addPackage($package);
				}
			}
		}

		if ($quotes['type'] == 'PACKAGE') {

			if(!empty($quotes['items'])) {
				foreach ($quotes['items'] as $key => $value) {
					$itemWeightUnit = $this->getUpsWeightUnit($value['weight_unit_id']);

					$itemDimensionUnit = $this->getUpsDimensionUnit($value['dimension_unit_id']);

					$package = new \Ups\Entity\Package();
					$package->getPackagingType()->setCode(\Ups\Entity\PackagingType::PT_PACKAGE);
					$package->getPackageWeight()->setWeight($value['weight']);

					// if you need this (depends of the shipper country)
					$weightUnit = new \Ups\Entity\UnitOfMeasurement;
					$weightUnit->setCode($itemWeightUnit);
					$package->getPackageWeight()->setUnitOfMeasurement($weightUnit);

					$packageServiceOptions = new \Ups\Entity\PackageServiceOptions();
					$deliveryConfirmation = new \Ups\Entity\DeliveryConfirmation;

					if($quotes['is_signature_required']) {
						$deliveryConfirmation->setDcisType(1);
						$packageServiceOptions->setDeliveryConfirmation($deliveryConfirmation);
						$package->setPackageServiceOptions($packageServiceOptions);
					}

					$dimensions = new \Ups\Entity\Dimensions();
					$dimensions->setHeight($value['length']);
					$dimensions->setWidth($value['width']);
					$dimensions->setLength($value['length']);

					$unit = new \Ups\Entity\UnitOfMeasurement;
					$unit->setCode($itemDimensionUnit);
					$dimensions->setUnitOfMeasurement($unit);

					$package->setDimensions($dimensions);

					$package->setDescription('Testing item');

					$shipment->addPackage($package);
				}
			}
		}

		// Set Reference Number
		$referenceNumber = new \Ups\Entity\ReferenceNumber;
		// if ($return) {
		// 	$referenceNumber->setCode(\Ups\Entity\ReferenceNumber::CODE_RETURN_AUTHORIZATION_NUMBER);
		// 	$referenceNumber->setValue($return_id);
		// } else {
			$referenceNumber->setCode(\Ups\Entity\ReferenceNumber::CODE_INVOICE_NUMBER);
			$referenceNumber->setValue($order_id);
		// }
		$shipment->setReferenceNumber($referenceNumber);

		// Set payment information
		$shipment->setPaymentInformation(new \Ups\Entity\PaymentInformation('prepaid', (object)array(
				'AccountNumber' => config('app.UPS_SHIPPER_ACCOUNT_NUMBER'),
			))
		);

		// Ask for negotiated rates (optional)
		// $rateInformation = new \Ups\Entity\RateInformation;
		// $rateInformation->setNegotiatedRatesIndicator(1);
		// $shipment->setRateInformation($rateInformation);

		// Get shipment info
		try {
			$isConfirm = (int) false;

			$confirm = UpsShipping::confirm(\Ups\Shipping::REQ_VALIDATE, $shipment);
			// dd($confirm); // Confirm holds the digest you need to accept the result

			if ($confirm) {
				$isConfirm = (int) true;
				$accept = UpsShipping::accept($confirm->ShipmentDigest);
				// dd($accept); // Accept holds the label and additional information
			}

			return [
				'status' => true,
				'is_confirm' => $isConfirm,
				'order_id' => $order_id,
				'shipment' => $accept,
			];
		} catch (\Exception $e) {
			$error = $e->getMessage();

			return [
				'status' => false,
				'error' => $error,
			];
		}
	}

	/**
	 *  Create Shipment: Via FeDEX API
	 *
	 * @param $request | Array
	 * @return $data | Array | Exception
	 *
	 */
    public function createFedexOpenShipment($quotes)
    {
		//Origin
		$fromCity 	= $quotes['ship_from_city'];
		$fromState 	= $quotes['ship_from_state'];
		$fromCountry= $quotes['ship_from_country'];
		$fromPostal = $quotes['ship_from_postal'];
		$fromAddress= $quotes['ship_from_address'];
		$fromCompany= $quotes['origin_company'];
		$fromAttn 	= $quotes['origin_attn_name'];
		$fromPhone 	= $quotes['origin_phone'];
		$fromEmail 	= $quotes['origin_email'];

		//Destination
		$toCity 	= $quotes['ship_to_city'];
		$toState 	= $quotes['ship_to_state'];
		$toCountry 	= $quotes['ship_to_country'];
		$toPostal 	= $quotes['ship_to_postal'];
		$toAddress 	= $quotes['ship_to_address'];
		$toCompany 	= $quotes['destination_company'];
		$toAttn 	= $quotes['destination_attn_name'];
		$toPhone 	= $quotes['destination_phone'];
		$toEmail 	= $quotes['destination_email'];

    	$shipDate = new \DateTime();

		$createOpenShipmentRequest = new OpenShipComplexType\CreateOpenShipmentRequest();

        //authentication & client details
		$createOpenShipmentRequest->WebAuthenticationDetail->UserCredential->Key = Config('fedex.FEDEX_API_KEY');
		$createOpenShipmentRequest->WebAuthenticationDetail->UserCredential->Password = Config('fedex.FEDEX_PASSWORD');
		$createOpenShipmentRequest->ClientDetail->AccountNumber = Config('fedex.FEDEX_ACCOUNT_NUMBER');
		$createOpenShipmentRequest->ClientDetail->MeterNumber = Config('fedex.FEDEX_METER_NUMBER');

		// version
		$createOpenShipmentRequest->Version->ServiceId = 'ship';
		$createOpenShipmentRequest->Version->Major = 15;
		$createOpenShipmentRequest->Version->Intermediate = 0;
		$createOpenShipmentRequest->Version->Minor = 0;

		$totalWeight = 0;
		// package items dynamic
		if ($quotes['type'] == 'PAK' || $quotes['type'] == 'LETTER') {
			//Package dynamic
			$lineItemsArr = [];
			if(!empty($quotes['items'])) {
				foreach ($quotes['items'] as $key => $value) {
					//Create package for mulitple line items
					array_push($lineItemsArr, new OpenShipComplexType\RequestedPackageLineItem());
					//Package Per line item
					$createOpenShipmentRequest->RequestedShipment->RequestedPackageLineItems = $lineItemsArr;
					$createOpenShipmentRequest->RequestedShipment->RequestedPackageLineItems[$key]->Weight->Value = $value['weight'];
					$createOpenShipmentRequest->RequestedShipment->RequestedPackageLineItems[$key]->Weight->Units = $this->getFedExWeightUnit($value['weight_unit_id']); //$value['weight_unit_id']; //LB or KG
					$createOpenShipmentRequest->RequestedShipment->RequestedPackageLineItems[$key]->GroupPackageCount = 1;
					$totalWeight += $value['weight'];
				}
			}
		}
		if ($quotes['type'] == 'PACKAGE') {
			//Package dynamic
			$lineItemsArr = [];
			if(!empty($quotes['items'])) {
				foreach ($quotes['items'] as $key => $value) {
					//Create package for mulitple line items
					array_push($lineItemsArr, new OpenShipComplexType\RequestedPackageLineItem());
					//Package Per line item
					$createOpenShipmentRequest->RequestedShipment->RequestedPackageLineItems = $lineItemsArr;
					$createOpenShipmentRequest->RequestedShipment->RequestedPackageLineItems[$key]->Weight->Value = $value['weight'];
					$createOpenShipmentRequest->RequestedShipment->RequestedPackageLineItems[$key]->Weight->Units = $this->getFedExWeightUnit($value['weight_unit_id']); //$value['weight_unit_id']; //LB or KG
					$createOpenShipmentRequest->RequestedShipment->RequestedPackageLineItems[$key]->Dimensions->Length = $value['length'];
					$createOpenShipmentRequest->RequestedShipment->RequestedPackageLineItems[$key]->Dimensions->Width = $value['width'];
					$createOpenShipmentRequest->RequestedShipment->RequestedPackageLineItems[$key]->Dimensions->Height = $value['height'];
					$createOpenShipmentRequest->RequestedShipment->RequestedPackageLineItems[$key]->Dimensions->Units = $this->getFedExDimensionUnit($value['dimension_unit_id']);
					$createOpenShipmentRequest->RequestedShipment->RequestedPackageLineItems[$key]->GroupPackageCount = 1;
					// Set signature option
					if(!empty($quotes['is_signature_required'])) {
						$createOpenShipmentRequest->RequestedShipment->RequestedPackageLineItems[$key]->SpecialServicesRequested->SpecialServiceTypes= ['SIGNATURE_OPTION'];
						$createOpenShipmentRequest->RequestedShipment->RequestedPackageLineItems[$key]->SpecialServicesRequested->SignatureOptionDetail->OptionType = SimpleType\SignatureOptionType::_DIRECT;
					}

					$totalWeight += $value['weight'];
				}
			}
		}

		// requested shipment
		$createOpenShipmentRequest->RequestedShipment->DropoffType = OpenShipSimpleType\DropoffType::_REGULAR_PICKUP;
		$createOpenShipmentRequest->RequestedShipment->ShipTimestamp = $shipDate->format('c');
		$createOpenShipmentRequest->RequestedShipment->ServiceType = $quotes['shipment_final_service'];

		$createOpenShipmentRequest->RequestedShipment->TotalWeight->Value = $totalWeight;
		$createOpenShipmentRequest->RequestedShipment->TotalWeight->Units = OpenShipSimpleType\WeightUnits::_LB;

		if ($quotes['type'] == 'LETTER') {
			$createOpenShipmentRequest->RequestedShipment->PackagingType = OpenShipSimpleType\PackagingType::_FEDEX_ENVELOPE;
		}
		
		if ($quotes['type'] == 'PAK') {
			$createOpenShipmentRequest->RequestedShipment->PackagingType = OpenShipSimpleType\PackagingType::_FEDEX_PAK;
		}
		
		if ($quotes['type'] == 'PACKAGE') {
			$createOpenShipmentRequest->RequestedShipment->PackagingType = OpenShipSimpleType\PackagingType::_YOUR_PACKAGING;
		}

		$createOpenShipmentRequest->RequestedShipment->LabelSpecification->ImageType = OpenShipSimpleType\ShippingDocumentImageType::_PDF;
		$createOpenShipmentRequest->RequestedShipment->LabelSpecification->LabelFormatType = OpenShipSimpleType\LabelFormatType::_COMMON2D;
		$createOpenShipmentRequest->RequestedShipment->LabelSpecification->LabelStockType = OpenShipSimpleType\LabelStockType::_PAPER_4X6;
		$createOpenShipmentRequest->RequestedShipment->RateRequestTypes = [OpenShipSimpleType\RateRequestType::_LIST];
		$createOpenShipmentRequest->RequestedShipment->PackageCount = !empty($quotes['items']) ? count($quotes['items']) : '';
		// $createOpenShipmentRequest->RequestedShipment->RequestedPackageLineItems = [$requestedPackageLineItem1];

		// requested shipment shipper
		$createOpenShipmentRequest->RequestedShipment->Shipper->AccountNumber = Config('fedex.FEDEX_ACCOUNT_NUMBER');
		$createOpenShipmentRequest->RequestedShipment->Shipper->Address->StreetLines = $fromAddress;
		$createOpenShipmentRequest->RequestedShipment->Shipper->Address->City = $fromCity;
		$createOpenShipmentRequest->RequestedShipment->Shipper->Address->StateOrProvinceCode = $fromState;
		$createOpenShipmentRequest->RequestedShipment->Shipper->Address->PostalCode = $fromPostal;
		$createOpenShipmentRequest->RequestedShipment->Shipper->Address->CountryCode = $fromCountry;
		$createOpenShipmentRequest->RequestedShipment->Shipper->Contact->CompanyName = $fromCompany;
		$createOpenShipmentRequest->RequestedShipment->Shipper->Contact->PersonName = $fromAttn;
		$createOpenShipmentRequest->RequestedShipment->Shipper->Contact->EMailAddress = $fromEmail;
		$createOpenShipmentRequest->RequestedShipment->Shipper->Contact->PhoneNumber = $fromPhone;

		// requested shipment recipient
		$createOpenShipmentRequest->RequestedShipment->Recipient->Address->StreetLines = $toAddress;
		$createOpenShipmentRequest->RequestedShipment->Recipient->Address->City = $toCity;
		$createOpenShipmentRequest->RequestedShipment->Recipient->Address->StateOrProvinceCode = $toState;
		$createOpenShipmentRequest->RequestedShipment->Recipient->Address->PostalCode = $toPostal;
		$createOpenShipmentRequest->RequestedShipment->Recipient->Address->CountryCode = $toCountry;
		$createOpenShipmentRequest->RequestedShipment->Recipient->Contact->PersonName = $toAttn;
		$createOpenShipmentRequest->RequestedShipment->Recipient->Contact->EMailAddress = $toEmail;
		$createOpenShipmentRequest->RequestedShipment->Recipient->Contact->PhoneNumber = $toPhone;

		// shipping charges payment
		$createOpenShipmentRequest->RequestedShipment->ShippingChargesPayment->Payor->ResponsibleParty = $createOpenShipmentRequest->RequestedShipment->Shipper;
		$createOpenShipmentRequest->RequestedShipment->ShippingChargesPayment->PaymentType = OpenShipSimpleType\PaymentType::_SENDER;

		// send the create open shipment request
		$openShipServiceRequest = new OpenShipRequest();
		$createOpenShipmentReply = $openShipServiceRequest->getCreateOpenShipmentReply($createOpenShipmentRequest);

		// shipment is created and we have an index number
		$index = $createOpenShipmentReply->Index;
		
		//Confirm open shipment
		$confirmOpenShipmentRequest = new OpenShipComplexType\ConfirmOpenShipmentRequest();
		$confirmOpenShipmentRequest->WebAuthenticationDetail = $createOpenShipmentRequest->WebAuthenticationDetail;
		$confirmOpenShipmentRequest->ClientDetail = $createOpenShipmentRequest->ClientDetail;
		$confirmOpenShipmentRequest->Version = $createOpenShipmentRequest->Version;
		$confirmOpenShipmentRequest->Index = $index;

		$confirmOpenShipmentReply = $openShipServiceRequest->getConfirmOpenShipmentReply($confirmOpenShipmentRequest);

		if ($confirmOpenShipmentReply->HighestSeverity == 'FAILURE' 
			|| $confirmOpenShipmentReply->HighestSeverity == 'ERROR' 
			|| $confirmOpenShipmentReply->HighestSeverity == 'WARNING') {
			$errorMessage = $this->getCombinedErrorMessage($confirmOpenShipmentReply->Notifications);

			return [
				'error' => $errorMessage,
				'status' => false,
			];
		}

		// In case of success response
		if ($confirmOpenShipmentReply->HighestSeverity == 'SUCCESS' || $confirmOpenShipmentReply->HighestSeverity == 'NOTE') {			
			return [
				'status' => true,
				'is_confirm' => (int) true,
				'shipment' => $confirmOpenShipmentReply,
			];
		}	
    }

    /**
     * Cancel UPS shipment
     * 
     */
    public function cancelUPSShipment($shipmentId) {
		try {
			$response = UpsShipping::void($shipmentId);

			return [
				'status' => true,
				'response' => $response,
			];	
		} catch (\Exception $e) {
			$error = $e->getMessage();

			return [
				'status' => false,
				'error' => $error,
			];
		}
	}

	/**
     * Cancel FEDEX shipment
     * 
     */
    public function cancelFedExShipment($shipmentId) {
		try {
			$deleteShipmentRequest = new FedExShipServiceComplexType\DeleteShipmentRequest();

        	//authentication & client details
			$deleteShipmentRequest->WebAuthenticationDetail->UserCredential->Key = Config('fedex.FEDEX_API_KEY');
			$deleteShipmentRequest->WebAuthenticationDetail->UserCredential->Password = Config('fedex.FEDEX_PASSWORD');
			$deleteShipmentRequest->ClientDetail->AccountNumber = Config('fedex.FEDEX_ACCOUNT_NUMBER');
			$deleteShipmentRequest->ClientDetail->MeterNumber = Config('fedex.FEDEX_METER_NUMBER');

			$deleteShipmentRequest->Version->ServiceId = 'ship';
			$deleteShipmentRequest->Version->Major = 26;
			$deleteShipmentRequest->Version->Intermediate = 0;
			$deleteShipmentRequest->Version->Minor = 0;

			$deleteShipmentRequest->TrackingId->TrackingNumber = $shipmentId;
			$deleteShipmentRequest->TrackingId->TrackingIdType = 'FEDEX';
			$deleteShipmentRequest->DeletionControl = OpenShipSimpleType\DeletionControlType::_DELETE_ALL_PACKAGES;

			$shipServiceRequest = new FedExShipServiceRequest();

			// For production
			if (!Config('fedex.FEDEX_SANDBOX')) {
				$shipServiceRequest->getSoapClient()->__setLocation(FedExShipServiceRequest::PRODUCTION_URL); //use production URL
			}

			$cancelPendingShipmentReply = $shipServiceRequest->getDeleteShipmentReply($deleteShipmentRequest);

			return [
				'status' => true,
				'response' => $cancelPendingShipmentReply,
			];	
		} catch (\Exception $e) {
			$error = $e->getMessage();

			return [
				'status' => false,
				'error' => $error,
			];
		}
	}

	/**
     * Get fedex weight unit
     * 
     */
    public function getFedExWeightUnit($unitId)
    {
    	$unit = Unit::title($unitId);

    	$title = $unit->title;

    	if ($title == 'LBS') {
    		return SimpleType\WeightUnits::_LB; //LB or KG
    	}

    	return SimpleType\WeightUnits::_KG; //LB or KG
    }

    /**
     * Get fedex dimension unit
     * 
     */
    public function getFedExDimensionUnit($dimensionUnitId)
    {
    	$dimensionUnit = Unit::title($dimensionUnitId);

    	$title = $dimensionUnit->title;

    	if ($title == 'IMPERIAL (In)') {
    		return SimpleType\LinearUnits::_IN; //IN or CM
    	}

    	return SimpleType\LinearUnits::_CM;
    }

    /**
     * Get ups weight unit
     * 
     */
    public function getUpsWeightUnit($unitId)
    {
    	$unit = Unit::title($unitId);

    	$title = $unit->title;

    	if ($title == 'LBS') {
    		return \Ups\Entity\UnitOfMeasurement::UOM_LBS; //LB or KG
    	}

    	return \Ups\Entity\UnitOfMeasurement::UOM_KGS; //LB or KG
    }

    /**
     * Get ups dimension unit
     * 
     */
    public function getUpsDimensionUnit($dimensionUnitId)
    {
    	$dimensionUnit = Unit::title($dimensionUnitId);

    	$title = $dimensionUnit->title;

    	if ($title == 'IMPERIAL (In)') {
    		return \Ups\Entity\UnitOfMeasurement::UOM_IN; //IN or CM
    	}

    	return \Ups\Entity\UnitOfMeasurement::UOM_CM;
    }
}