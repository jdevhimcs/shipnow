<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('user_id')->constrained();
            $table->foreignId('user_card_id')->constrained();
            $table->foreignId('shipment_id')->constrained();
            $table->string('trx_id');
            $table->decimal('trx_amount', 8, 2);
            $table->tinyInteger('status', 0);
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['user_card_id']);
            $table->dropForeign(['shipment_id']);
        });
        Schema::dropIfExists('transactions');
    }
}
