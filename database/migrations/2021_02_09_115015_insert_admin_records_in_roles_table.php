<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertAdminRecordsInRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('roles')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        DB::table('roles')->insert([
            [
                'name' => 'admin',
                'is_deleted' => (int) false,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'user',
                'is_deleted' => (int) false,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'agent',
                'is_deleted' => (int) false,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('roles')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        // Need to add the agent as default admin are added in seed file but seed file will run after migration. Hence will not add the agent record.
        DB::table('roles')->insert([
            [
                'name' => 'admin',
                'is_deleted' => (int) false,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'user',
                'is_deleted' => (int) false,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'agent',
                'is_deleted' => (int) false,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
