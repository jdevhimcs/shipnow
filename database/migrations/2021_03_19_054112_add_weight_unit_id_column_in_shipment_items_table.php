<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddWeightUnitIdColumnInShipmentItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('shipment_items', 'weight_unit_id')) {
            Schema::table('shipment_items', function (Blueprint $table) {
                $table->foreignId('weight_unit_id')->nullable(true)->after('dimension_unit_id')->constrained('units')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('shipment_items', 'weight_unit_id')) {
            Schema::table('shipment_items', function (Blueprint $table) {
                $table->dropForeign(['weight_unit_id']);
                $table->dropColumn('weight_unit_id');
            });
        }
    }
}
