<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address_books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('user_id')->constrained();
            $table->string('company_name');
            $table->text('address');
            $table->foreignId('country_id')->constrained();
            $table->foreignId('state_id')->constrained();
            $table->string('city');
            $table->string('postal_code', 15);
            $table->string('email');
            $table->string('phone_no');
            $table->time('ship_hours_from');
   			$table->time('ship_hours_to');
            $table->text('note');
            $table->tinyInteger('is_picked')->default('0');
            $table->tinyInteger('is_business_dock')->default('0');
            $table->tinyInteger('is_residence')->default('0');
            $table->tinyInteger('appointment_delivery')->default('0');
            $table->tinyInteger('straight_truck')->default('0');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address_books');
    }
}
