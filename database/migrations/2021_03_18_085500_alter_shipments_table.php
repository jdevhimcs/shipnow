<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipments', function (Blueprint $table) {
            // Remove column
            $table->dropColumn('is_confirm');
            $table->dropColumn('is_accept');
            $table->dropColumn('business_name');
            $table->dropColumn('billing_unit');
            $table->dropColumn('billing_weight');

            // Set nullable column
            $table->string('identification_number')->nullable(true)->change();
            $table->string('order_ref_id')->nullable(true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipments', function (Blueprint $table) {
            // Add column
            $table->tinyInteger('is_confirm')->nullable()->after('postal_to');
            $table->tinyInteger('is_accept')->nullable()->after('postal_to');
            $table->string('business_name')->nullable()->after('postal_to');
            $table->string('billing_unit')->after('postal_to');
            $table->string('billing_weight')->after('postal_to');

            // Set non-nullable column
            $table->string('identification_number')->after('postal_to');
            $table->string('order_ref_id')->after('postal_to');
        });
    }
}
