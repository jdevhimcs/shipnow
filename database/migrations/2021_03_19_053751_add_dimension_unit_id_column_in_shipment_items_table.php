<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDimensionUnitIdColumnInShipmentItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('shipment_items', 'dimension_unit_id')) {
            Schema::table('shipment_items', function (Blueprint $table) {
                $table->foreignId('dimension_unit_id')->nullable(true)->after('weight')->constrained('units')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('shipment_items', 'dimension_unit_id')) {
            Schema::table('shipment_items', function (Blueprint $table) {
                $table->dropForeign(['dimension_unit_id']);
                $table->dropColumn('dimension_unit_id');
            });
        }
    }
}
