<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertRecordInUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('units')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        DB::table('units')->insert([
            [
                'title' => 'KG',
                'type' => array_search('Weight unit', config('constants.UNIT_TYPE')),
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'title' => 'LBS',
                'type' => array_search('Weight unit', config('constants.UNIT_TYPE')),
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'title' => 'IMPERIAL (In)',
                'type' => array_search('Dimension unit', config('constants.UNIT_TYPE')),
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'title' => 'METRIC (Cm)',
                'type' => array_search('Dimension unit', config('constants.UNIT_TYPE')),
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('units')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        DB::table('units')->insert([
            [
                'title' => 'KG',
                'type' => array_search('Weight unit', config('constants.UNIT_TYPE')),
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'title' => 'LBS',
                'type' => array_search('Weight unit', config('constants.UNIT_TYPE')),
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'title' => 'IMPERIAL (In)',
                'type' => array_search('Dimension unit', config('constants.UNIT_TYPE')),
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'title' => 'METRIC (Cm)',
                'type' => array_search('Dimension unit', config('constants.UNIT_TYPE')),
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
