<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewColumnsToShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipments', function (Blueprint $table) {
            $table->string('origin_company', 255)->after('carrier')->nullable();
            $table->string('destination_company', 255)->nullable();
            $table->string('origin_attn_name', 255)->nullable();
            $table->string('destination_attn_name', 255)->nullable();
            $table->string('origin_phone', 255)->nullable();
            $table->string('destination_phone', 255)->nullable();
            $table->string('origin_email', 255)->nullable();
            $table->string('destination_email', 255)->nullable();
            $table->string('service_name', 255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipments', function (Blueprint $table) {
            $table->dropColumn('origin_company');
            $table->dropColumn('destination_company');
            $table->dropColumn('origin_attn_name');
            $table->dropColumn('destination_attn_name');
            $table->dropColumn('origin_phone');
            $table->dropColumn('destination_phone');
            $table->dropColumn('origin_email');
            $table->dropColumn('destination_email');
            $table->dropColumn('service_name');
        });
    }
}
