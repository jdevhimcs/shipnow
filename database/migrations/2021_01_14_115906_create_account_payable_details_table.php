<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountPayableDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_payable_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('user_id');          
            $table->string('contact_name');
            $table->string('email');
            $table->string('phone');
            $table->string('payment_terms');
            $table->tinyInteger('is_prepaid')->default('0');
            $table->tinyInteger('is_same_main_contact')->default('0');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_payable_details');
    }
}
