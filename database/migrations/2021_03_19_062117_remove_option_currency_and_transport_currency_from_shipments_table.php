<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveOptionCurrencyAndTransportCurrencyFromShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('shipments', 'transport_currency')) {
            Schema::table('shipments', function (Blueprint $table) {
                $table->dropColumn('transport_currency');
            });
        }

        if (Schema::hasColumn('shipments', 'options_currency')) {
            Schema::table('shipments', function (Blueprint $table) {
                $table->dropColumn('options_currency');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('shipments', 'transport_currency')) {
            Schema::table('shipments', function (Blueprint $table) {
                $table->string('transport_currency')->default('CAD');
            });
        }

        if (!Schema::hasColumn('shipments', 'options_currency')) {
            Schema::table('shipments', function (Blueprint $table) {
                $table->string('options_currency')->default('CAD');
            });
        }
    }
}
