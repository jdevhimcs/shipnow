<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderPreferencePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_preference_packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('order_preference_id');
            $table->foreignId('package_id');
            $table->timestamps();
            $table->foreign('order_preference_id')->references('id')->on('order_preferences');
            $table->foreign('package_id')->references('id')->on('packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_preference_packages');
    }
}
