<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('user_id')->constrained();
            $table->string('identification_number');
            $table->string('carrier');
            $table->string('order_ref_id');
            $table->decimal('total_transportation_charges', 8, 2);
            $table->string('transport_currency')->default('CAD');
            $table->decimal('total_service_options_charges', 8, 2);
            $table->string('options_currency')->default('CAD');
            $table->decimal('total_charges', 8, 2);
            $table->string('currency')->default('CAD');
            $table->string('billing_unit');
            $table->string('billing_weight');
            $table->string('address_from');
            $table->string('city_from');
            $table->string('postal_from');
            $table->string('address_to');
            $table->string('city_to');
            $table->string('postal_to');
            $table->dateTime('ship_date')->nullable();
            $table->string('business_name')->nullable();
            $table->tinyInteger('is_residential')->nullable();
            $table->tinyInteger('is_confirm')->nullable();
            $table->tinyInteger('is_accept')->nullable();
            $table->tinyInteger('is_paid')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipments', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });

        Schema::dropIfExists('shipments');
    }
}
