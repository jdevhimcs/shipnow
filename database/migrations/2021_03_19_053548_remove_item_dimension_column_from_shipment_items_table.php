<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveItemDimensionColumnFromShipmentItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('shipment_items', 'item_dimension')) {
            Schema::table('shipment_items', function (Blueprint $table) {
                $table->dropColumn('item_dimension');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('shipment_items', 'item_dimension')) {
            Schema::table('shipment_items', function (Blueprint $table) {
                $table->string('item_dimension')->nullable();
            });
        }
    }
}
