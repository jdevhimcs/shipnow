<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Unit;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $units = [
			[                
			   'title'=>'Inch',
			],
			[
			   'title'=>'Centimeter',
			]                
		];
  
		foreach ($units as $key => $value) {
			Unit::create($value);
		}
    }
}
