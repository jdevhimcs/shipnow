<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Admin
        $admin = [            
                'email'     => 'admin_shaw@yopmail.com',
                'role_id'   => 1,
                'password'  => Hash::make('passw0rd'),                   
        ];

        $adminUser = User::create($admin);

        $adminUser->profile()->create([
            'first_name'=> 'admin',
            'last_name' => 'shaw',
            'phone_no'  => '+919876543210',
            'image' => ''
        ]);
        
        //Fake User
        $user = [                        
                'email'   => 'user_shaw@yopmail.com',
                'role_id' => 2,
                'password'=> Hash::make('passw0rd'),          
        ];
        $fakeUser = User::create($user);
        $fakeUser->profile()->create([
            'first_name'=> 'user',
            'last_name' => 'shaw',
            'phone_no'  => '+919876543211',
            'image' => ''
        ]);         
    }
}
