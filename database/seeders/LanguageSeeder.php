<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Language;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = [
			[                
			   'title'=>'English',
			],
			[
			   'title'=>'Francais',
			]                
		];
  
		foreach ($languages as $key => $value) {
			Language::create($value);
		}
    }
}
