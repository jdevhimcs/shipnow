<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(LanguageSeeder::class);
        $this->call(UnitSeeder::class);
        $this->call(NotificationSettingSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(StateSeeder::class);
        // $this->call(CitySeeder::class);
    }
}
