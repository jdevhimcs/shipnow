<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\NotificationSetting;

class NotificationSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
			[                
               'title'=>'Picked up',
               'type' => 'LTL'
			],
			[
			   'title'=>'Delivered',
               'type' => 'LTL'
			]                
		];
  
		foreach ($settings as $key => $value) {
			NotificationSetting::create($value);
		}
    }
}
